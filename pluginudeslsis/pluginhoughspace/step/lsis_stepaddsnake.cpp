#include "lsis_stepaddsnake.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_circle.h"

//Qt dependencies
#include <QFile>
#include <QTextStream>

// Alias for indexing models
#define DEFout_RsltSnake "RsltSnake"
#define DEFout_GrpGrpCircle "GrpGrpCircle"
#define DEFout_GrpCircle "GrpCircle"
#define DEFout_ItmCircle "ItmCircle"


// Constructor : initialization of parameters
LSIS_StepAddSnake::LSIS_StepAddSnake(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString LSIS_StepAddSnake::getStepDescription() const
{
    return tr("Ajoute un ensemble de cercles");
}

// Step detailled description
QString LSIS_StepAddSnake::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepAddSnake::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepAddSnake::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepAddSnake(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepAddSnake::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void LSIS_StepAddSnake::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_RsltSnake = createNewOutResultModel(DEFout_RsltSnake, tr("Result"));
    res_RsltSnake->setRootGroup(DEFout_GrpGrpCircle, new CT_StandardItemGroup(), tr("Group"));
    res_RsltSnake->addGroupModel(DEFout_GrpGrpCircle, DEFout_GrpCircle, new CT_StandardItemGroup(), tr("Group"));
    res_RsltSnake->addItemModel(DEFout_GrpCircle, DEFout_ItmCircle, new CT_Circle(), tr("Circle"));
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepAddSnake::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice("Snake file", CT_FileChoiceButton::OneExistingFile, "*.ct_snake", _fileName);
}

void LSIS_StepAddSnake::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultSnakes = outResultList.at(0);

    CT_StandardItemGroup* grp_GrpGrpCircle= new CT_StandardItemGroup(DEFout_GrpGrpCircle, res_ResultSnakes);
    res_ResultSnakes->addGroup(grp_GrpGrpCircle);

    QFile file( _fileName.first() );

    if( !file.open( QIODevice::ReadOnly ) )
    {
        PS_LOG->addErrorMessage( PS_LOG->error, "Can not access file : \""+file.fileName()+"\"" );
        return;
    }

    // Outils de lecture
    float x,y,z,r;
    QString toto;
    QTextStream fileStream(&file);
    CT_Point curCenter;
    CT_Point curOrientation;
    curOrientation.setValues(0,0,1);

    QVector<CT_CircleData*> circles;
    QVector<CT_Point> orientation;

    // Ignore la premiere ligne d'entete
    fileStream.readLine();

    while( !fileStream.atEnd() )
    {
        fileStream >> r >> x >> y >> z;
        curCenter.setValues( x, y, z );
        circles.push_back( new CT_CircleData( curCenter, curOrientation, r ) );
    }

    // Pour chaque cercle
    int smoothSize = 5;
    int nCircles = circles.size();
    for ( int i = 0 ; i < nCircles ; i++ )
    {
        CT_CircleData* curCircleData;
        CT_CircleData* nextCircle;
        CT_CircleData* prevCircle;
        CT_Point       currCircleCenter;
        CT_Point       nextCircleCenter;
        CT_Point       prevCircleCenter;
        CT_Point       dir; dir.setZero();

        // Recupere le centre courant
        curCircleData = circles.at(i);
        currCircleCenter = curCircleData->getCenter();

        // A partir du point courant on va chercher les sizeSmooth points precedents et suivants pour calculer la direction
        for ( int j = 1 ; j <= smoothSize ; j++ )
        {

            // SI le i+j eme point existe (on arrive pas aux extremites)
            // On met a jour la dir
            if ( i+j < nCircles )
            {
                nextCircle = circles.at(i+j);
                nextCircleCenter = nextCircle->getCenter();
                dir(0) = ( dir.x() + ( nextCircleCenter.x() - currCircleCenter.x() ) );
                dir(1) = ( dir.y() + ( nextCircleCenter.y() - currCircleCenter.y() ) );
                dir(2) = ( dir.z() + ( nextCircleCenter.z() - currCircleCenter.z() ) );
            }

            // SI le i-j eme point existe (on arrive pas aux extremites)
            // On met a jour la dir
            if ( i-j >= 0 )
            {
                prevCircle = circles.at(i-j);
                prevCircleCenter = prevCircle->getCenter();
                dir(0) = ( dir.x() + ( currCircleCenter.x() - prevCircleCenter.x() ) );
                dir(1) = ( dir.y() + ( currCircleCenter.y() - prevCircleCenter.y() ) );
                dir(2) = ( dir.z() + ( currCircleCenter.z() - prevCircleCenter.z() ) );
            }
        }

        // On donne la direction au cercle
        curCircleData->setDirection( dir );

        // Et on l'ajoute au resultat
        CT_Circle* curCircle = new CT_Circle( DEFout_ItmCircle,
                                              res_ResultSnakes,
                                              curCircleData );

        CT_StandardItemGroup* grp_GrpCircle= new CT_StandardItemGroup(DEFout_GrpCircle, res_ResultSnakes);
        grp_GrpCircle->addItemDrawable(curCircle);
        grp_GrpGrpCircle->addGroup( grp_GrpCircle );
    }
}
