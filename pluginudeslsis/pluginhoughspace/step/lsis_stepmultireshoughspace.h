/************************************************************************************
* Filename :  lsis_stepmultireshoughspace.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_STEPMULTIRESHOUGHSPACE_H
#define LSIS_STEPMULTIRESHOUGHSPACE_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "../houghspace/lsis_houghspace4d.h"

#include "ct_normalcloud/ct_normalcloudstdvector.h"

#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_line.h"

template< typename DataT >
class LSIS_ActiveContour4DContinuous;

template< typename DataImage >
struct DebugStructure
{
    CT_Grid4D<DataImage>*   debugHoughSpace;        // L'espace de Hough a afficher lors d'un stop pendant le debug
    DocumentInterface*      documentHoughSpace;     // Le document sur lequel cette grille sera affichee
    QVector<CT_Line*>*      debugSkeleton;          // Squelette du contours sous forme de lignes consecutives
    QVector<CT_Circle*>*    debugCircles;           // Liste de cercles d'un contour a afficher
    DocumentInterface*      documentCircle;         // Le document sur lequel cette grille sera affichee
};

/*!
 * \class LSIS_StepMultiResHoughSpace
 * \ingroup Steps_LSIS
 * \brief <b>Lignes de cretes de l'espace.</b>
 *
 * No detailled description for this step
 *
 * \param _nIterMax
 * \param _growCoeff
 * \param _timeStep
 * \param _threshGradMove
 * \param _threshGradLength
 *
 */
class LSIS_StepMultiResHoughSpace: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    LSIS_StepMultiResHoughSpace(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

    // CT_AbstractStep non obligatoire :
    // Que faire avant de s'arreter pendant le mode debug ?
    void preWaitForAckIfInDebugMode();

    // CT_AbstractStep non obligatoire :
    // Que faire apres s'etre arete pendant le mode debug ?
    void postWaitForAckIfInDebugMode();

    // Methode qui en mode debug permet de selectionner le document dans lequel afficher les elements de debug.
    // Voir aussi la variable _debugStructure
    void setDocumentForDebugDisplay(QList<DocumentInterface*> docList);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    LSIS_HoughSpace4D* computeRoughHoughSpace(const CT_PointsAttributesNormal *normals);

    QVector<LSIS_ActiveContour4DContinuous<int> *> *computeRoughSnakes(LSIS_HoughSpace4D* houghSpace);

    void getPointCloudAndNormalsAroundEachSnake(QVector< LSIS_ActiveContour4DContinuous<int>* > &snakes,
                                                QVector<float> &minx,
                                                QVector<float> &miny,
                                                QVector<float> &minz,
                                                QVector<float> &maxx,
                                                QVector<float> &maxy,
                                                QVector<float> &maxz,
                                                CT_PCIR sceneIndexCloud,
                                                CT_AbstractNormalCloud* sceneNormalCloud,
                                                QVector< CT_PointCloudIndexVector* > &outIndexClouds,
                                                QVector< CT_NormalCloudStdVector* > &outNormalClouds );

    LSIS_HoughSpace4D* computeFineHoughSpace(float minx,
                                              float miny,
                                              float minz,
                                              float maxx,
                                              float maxy,
                                              float maxz,
                                              CT_PointCloudIndexVector* pointCloud,
                                              CT_NormalCloudStdVector* normalCloud );

    void compute(const LSIS_HoughSpace4D *houghSpace );

    void filterSnakes(QVector<LSIS_ActiveContour4DContinuous<int> *> *snakes, float minLength );

private:

    // Step parameters
    double      _minr;
    double      _minx;
    double      _miny;
    double      _minz;
    double      _maxr;
    double      _maxx;
    double      _maxy;
    double      _maxz;
    double      _resr;
    double      _resx;
    double      _resy;
    double      _resz;
    double      _fineResr;
    double      _fineResx;
    double      _fineResy;
    double      _fineResz;

    int       _nIterMax;    /*!<  */
    double    _growCoeff;    /*!<  */
    double    _timeStep;    /*!<  */
    double    _threshGradMove;    /*!<  */
    double    _threshGradLength;    /*!<  */

    CT_ResultGroup*  _snakesResult;

// Toute la partir du dessous est copiee/colle simplement d'une etape d'un autre plugin (plugin houghspace initial)
private :
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // Parametres relatifs au debug mode                                                                                                            //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    /**/    DebugStructure<int>                     _debugStructure;  /*!< Structure de debug, contient tout pour l'affichage lors du debug mode */ //
    /**/    bool                                    _verbose; /*!< Indique si l'etape est "verbeuse" ou non (sortie console lors du debug) */       //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

public :
    /*!
     * \brief setDebugHoughSpace
     *
     * Permet de selectionner une grille a visualiser en mode debug
     *
     * \param hs : grille a visualiser en mode debug
     */
    template< typename DataImage >
    inline void setDebugHoughSpace( CT_Grid4D<DataImage>* hs )
    {
        _debugStructure.debugHoughSpace = hs;
    }

    /*!
     * \brief setDebugCirclesList
     *
     * Permet de selectionner une liste de cercles a visualiser en mode debug
     *
     * \param circlesList : le vecteur de cercles a visualiser en mod debug
     */
    inline void setDebugCirclesList( QVector<CT_Circle*>* circlesList )
    {
        _debugStructure.debugCircles = circlesList;
    }

    /*!
     * \brief setDebugSkeleton
     *
     * Permet de selectionner un squelette sous forme de vecteur de lignes a visualiser en mode debug
     *
     * \param lines : squelette du contours sous forme de suite de lignes
     */
    inline void setDebugSkeleton( QVector<CT_Line*>* lines )
    {
        _debugStructure.debugSkeleton = lines;
    }

    /*!
     * \brief callDebugVisualization
     *
     * Place l'etape en mode debug afin de visualiser les cercles d'un contours actif ainsi que l'espace de hough associe
     *
     * \param hs : espace de hough a visualiser
     * \param contour : contour actif dont on veut visualiser les cercles
     */
    template< typename DataImage >
    void callDebugVisualization( CT_Grid4D<DataImage> * hs, LSIS_ActiveContour4DContinuous<DataImage> * contour )
    {
        if ( isDebugModeOn() )
        {
            // Transforme le snake en vecteur de cercles pour pouvoir les afficher
            QVector<CT_Circle*>         vecCircles;
            QVector<CT_Line*>           vecLines;
            int nbCercles = 0;
            int nbLignes = 0;

            if ( contour != NULL )
            {
                vecLines = contour->toLines();
                vecCircles = contour->toCircles(5, LSIS_ActiveContour4DContinuous<DataImage>::NO_COLOR, "", NULL );

                setDebugCirclesList( &vecCircles );
                setDebugSkeleton( &vecLines );
            }

            else
            {
                setDebugCirclesList( NULL );
                setDebugSkeleton( NULL );
            }

            setDebugHoughSpace( hs );

            waitForAckIfInDebugMode();

            if ( contour != NULL )
            {
                // On vide le vecteur de cercles et libere la memoire
                nbCercles = vecCircles.size();
                for ( int i = 0 ; i < nbCercles ; i++ )
                {
                    // Le circleData est en autodelete quand on supprime le circle
                    delete vecCircles[i];
                }

                // On vide le vecteur de lignes et libere la memoire
                nbLignes = vecLines.size();
                for ( int i = 0 ; i < nbLignes ; i++ )
                {
                    // Le lineData est en autodelete quand on supprime la ligne
                    delete vecLines[i];
                }
            }
        }
    }

    template< typename DataImage >
    void callDebugVisualizationHoughSpaceOnSecondWindow( CT_Grid4D<DataImage>* hs )
    {
        if ( isDebugModeOn() )
        {
            setDebugHoughSpace( hs );
            waitForAckIfInDebugMode();
        }
    }

    /*!
     * \brief isVerbose
     *
     * Indique si l'etape est en mode verbose
     *
     * \return true si l'etape est en mode verbose, false sinon
     */
    inline bool isVerbose()
    {
        return _verbose;
    }
};

#endif // LSIS_STEPMULTIRESHOUGHSPACE_H
