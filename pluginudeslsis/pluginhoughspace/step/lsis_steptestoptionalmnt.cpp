#include "lsis_steptestoptionalmnt.h"

#include "ct_itemdrawable/ct_grid2dxy.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"

#include "../houghspace/lsis_houghspace4d.h"
#include "../streamoverload/streamoverload.h"

// Alias for indexing models
#define DEFin_rsltOptionalDtm "rsltOptionalDtm"
#define DEFin_grpOptionalDtm "grpOptionalDtm"
#define DEFin_itmOptionalDtm "itmOptionalDtm"

#define DEFin_rsltHoughSpace "rsltHoughSpace"
#define DEFin_grpHoughSpace "grpHoughSpace"
#define DEFin_itmHoughSpace "itmHoughSpace"

// Constructor : initialization of parameters
LSIS_StepTestOptionalMNT::LSIS_StepTestOptionalMNT(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString LSIS_StepTestOptionalMNT::getStepDescription() const
{
    return tr("Etape avec un mnt optionnel");
}

// Step detailled description
QString LSIS_StepTestOptionalMNT::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepTestOptionalMNT::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepTestOptionalMNT::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepTestOptionalMNT(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepTestOptionalMNT::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltOptionalDtm = createNewInResultModel(DEFin_rsltOptionalDtm, tr("Optional DTM resule"), tr("") );
    resIn_rsltOptionalDtm->setZeroOrMoreRootGroup();
    resIn_rsltOptionalDtm->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);
    resIn_rsltOptionalDtm->addGroupModel("", DEFin_grpOptionalDtm, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resIn_rsltOptionalDtm->addItemModel(DEFin_grpOptionalDtm, DEFin_itmOptionalDtm, CT_Grid2DXY<float>::staticGetType(), tr("DTM"), tr(""), CT_InAbstractModel::C_ChooseOneIfMultiple );

    CT_InResultModelGroup *resIn_rsltHoughSpace = createNewInResultModel(DEFin_rsltHoughSpace, tr("Hough Space"), tr("Hough Space"), true);
    resIn_rsltHoughSpace->setRootGroup(DEFin_grpHoughSpace, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resIn_rsltHoughSpace->addItemModel(DEFin_grpHoughSpace, DEFin_itmHoughSpace, LSIS_HoughSpace4D::staticGetType(), tr("Hough Space"));
}

// Creation and affiliation of OUT models
void LSIS_StepTestOptionalMNT::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy( DEFin_rsltOptionalDtm );
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepTestOptionalMNT::createPostConfigurationDialog()
{
    // No parameter dialog for this step
}

void LSIS_StepTestOptionalMNT::compute()
{
    QList<CT_ResultGroup*> inResultMntList = getInputResultsForModel( DEFin_rsltOptionalDtm );
    QList<CT_ResultGroup*> inResultImageList = getInputResultsForModel( DEFin_rsltHoughSpace );

    // Recupere l(es)'espace(s) de Hough
    QVector<LSIS_HoughSpace4D*> houghSpaces;
    CT_ResultGroup* resIn_rsltImage = inResultImageList.at(0);
    CT_ResultGroupIterator itIn_grpImage(resIn_rsltImage, this, DEFin_grpHoughSpace);
    while (itIn_grpImage.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpImage = (CT_AbstractItemGroup*) itIn_grpImage.next();

        LSIS_HoughSpace4D* itemIn_itmImage = (LSIS_HoughSpace4D*)grpIn_grpImage->firstItemByINModelName(this, DEFin_itmHoughSpace);
        if (itemIn_itmImage != NULL)
        {
            houghSpaces.push_back( itemIn_itmImage );
            qDebug() << (*itemIn_itmImage);
        }
    }

    if( inResultMntList.empty() )
    {
        qDebug() << "Pas de reusltat de mnt";
    }

    else
    {
        qDebug() << "Dispose d'un resultat mnt";
        // IN results browsing
        CT_ResultGroup* resIn_rsltDtm = inResultMntList.at(0);
        CT_ResultGroupIterator itIn_grpDtm(resIn_rsltDtm, this, DEFin_grpOptionalDtm);
        while (itIn_grpDtm.hasNext() && !isStopped())
        {
            const CT_AbstractItemGroup* grpIn_grpDtm = (CT_AbstractItemGroup*) itIn_grpDtm.next();

            const CT_Grid2DXY<float>* itemIn_itmDtm = (CT_Grid2DXY<float>*)grpIn_grpDtm->firstItemByINModelName(this, DEFin_itmOptionalDtm);
            if (itemIn_itmDtm != NULL)
            {
                qDebug() << "J'ai bien un MNT, je peux recuperer une valeur min";
            }

            else
            {
                qDebug() << "Pas de MNT, je met une fausse valeur min";
            }
        }
    }
}
