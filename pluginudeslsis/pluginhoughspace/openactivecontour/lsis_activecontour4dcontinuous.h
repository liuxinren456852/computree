/************************************************************************************
* Filename :  lsis_activecontour4dcontinuous.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_ACTIVECONTOUR4DCONTINUOUS_H
#define LSIS_ACTIVECONTOUR4DCONTINUOUS_H

//To add to the model viewer
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithoutpointcloud.h"

// Se deplace sur une image
#include "ct_itemdrawable/ct_grid4d.h"
#include "ct_itemdrawable/ct_grid4d_sparse.h"

#include "ct_normalcloud/ct_normalcloudstdvector.h"

// Un contours actif est forme d'un ensemble de points 4D ranges dans une matrice a n lignes et 4 colonnes, n etant le nombre de points dans le contours
#include <Eigen/Core>

// On manipule des pixels4D
// Aisni que des points 4D
#include "../pixel/lsis_pixel4d.h"
#include "../point4d/lsis_point4d.h"

// TEMPORAIRE : pour les logs
#include <QString>
#include <QFile>
#include <QTextStream>
// TEMPORAIRE FIN

class LSIS_StepSnakesInHoughSpace;
class CT_Circle;
class CT_CircleData;
class CT_Line;
class CT_LineData;
class CT_Mesh;
class CT_MeshModel;
template< typename DataT > class CT_Image2D;

template< typename DataImage >
class LSIS_ActiveContour4DContinuous : public CT_AbstractItemDrawableWithoutPointCloud
{
    CT_TYPE_IMPL_MACRO(LSIS_ActiveContour4DContinuous, CT_AbstractItemDrawableWithoutPointCloud, Active Contour(Trees))
public :
    enum ColorMode
    {
        NO_COLOR,
        GLOBAL_SCORE,
        INTERNAL_SCORE
    };

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Empty Constructor (used for outResult passing)
     */
    LSIS_ActiveContour4DContinuous( );

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Constructor for the model (for copy)
     */
    LSIS_ActiveContour4DContinuous(const CT_OutAbstractSingularItemModel *model,
                                   const CT_AbstractResult *result);

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Constructor
     *
     * \param image : image sur laquelle se deplace le contour
     */
    LSIS_ActiveContour4DContinuous(const CT_OutAbstractSingularItemModel *model,
                                   const CT_AbstractResult *result,
                                   const CT_Grid4D_Sparse<DataImage>* image,
                                   CT_Grid4D_Sparse<bool>* repulseImage );

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Constructor
     *
     * \param image : image sur laquelle se deplace le contour
     */
    LSIS_ActiveContour4DContinuous(const QString &modelName,
                                   const CT_AbstractResult *result,
                                   const CT_Grid4D_Sparse<DataImage>* image,
                                   CT_Grid4D_Sparse<bool>* repulseImage );

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Constructeur avec initialisation de la tete et la queue
     * Attention : ce constructeur resample le snake pour qu'il comporte au moins 4 points
     * La matrice des points est une matrice npoints*4
     *
     * \param image : image sur laquelle se deplace le contour
     * \param head : tete du contour (un pixel sur l'image)
     * \param back : queue du contour (un pixel sur l'image)
     */
    LSIS_ActiveContour4DContinuous(const CT_OutAbstractSingularItemModel *model,
                                   const CT_AbstractResult *result,
                                   CT_Grid4D_Sparse<DataImage> const * image,
                                   CT_Grid4D_Sparse<bool>* repulseImage,
                                   const LSIS_Pixel4D &head,
                                   const LSIS_Pixel4D &back );

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Constructeur avec initialisation de la tete et la queue
     * Attention : ce constructeur resample le snake pour qu'il comporte au moins 4 points
     * La matrice des points est une matrice npoints*4
     *
     * \param image : image sur laquelle se deplace le contour
     * \param head : tete du contour (un pixel sur l'image)
     * \param back : queue du contour (un pixel sur l'image)
     */
    LSIS_ActiveContour4DContinuous(const QString &modelName,
                                   const CT_AbstractResult *result,
                                   CT_Grid4D_Sparse<DataImage> const * image,
                                   CT_Grid4D_Sparse<bool>* repulseImage,
                                   const LSIS_Pixel4D &head,
                                   const LSIS_Pixel4D &back );

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Constructeur d'initialisation :
     * a partir de la position de la tete, on effectue une acp locale ponderee pour trouver la meilleure direction dans laquelle positionner la queue
     * On positionne la queue a (head + v1) ou v1 est la premiere direction propre de l'acp normalisee
     * La matrice des points est une matrice npoints*4
     *
     * \warning L'acp ne tient pas compte des pixels nuls
     * \warning Cette methode peut etre optimisee, voir la conversion faite ligne 96 a cause de l'operation de multiplication par un scalaire
     * \todo Cette methode peut etre optimisee, voir la conversion faite ligne 96 a cause de l'operation de multiplication par un scalaire
     *
     * \param image : image sur laquelle se deplace le contour
     * \param head : tete du contour (un pixel sur l'image)
     * \param size : taille du voisinage pour lequel effectuer l'acp
     */
    LSIS_ActiveContour4DContinuous(const CT_OutAbstractSingularItemModel *model,
                                   const CT_AbstractResult *result,
                                   const CT_Grid4D_Sparse<DataImage>* image,
                                   CT_Grid4D_Sparse<bool>* repulseImage,
                                   const LSIS_Pixel4D &head,
                                   int size );

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Constructeur d'initialisation :
     * a partir de la position de la tete, on effectue une acp locale ponderee pour trouver la meilleure direction dans laquelle positionner la queue
     * On positionne la queue a (head + v1) ou v1 est la premiere direction propre de l'acp normalisee
     * La matrice des points est une matrice npoints*4
     *
     * \warning L'acp ne tient pas compte des pixels nuls
     * \warning Cette methode peut etre optimisee, voir la conversion faite ligne 96 a cause de l'operation de multiplication par un scalaire
     * \todo Cette methode peut etre optimisee, voir la conversion faite ligne 96 a cause de l'operation de multiplication par un scalaire
     *
     * \param image : image sur laquelle se deplace le contour
     * \param head : tete du contour (un pixel sur l'image)
     * \param size : taille du voisinage pour lequel effectuer l'acp
     */
    LSIS_ActiveContour4DContinuous(const QString &modelName,
                                   const CT_AbstractResult *result,
                                   const CT_Grid4D_Sparse<DataImage>* image,
                                   CT_Grid4D_Sparse<bool>* repulseImage,
                                   const LSIS_Pixel4D &head,
                                   int size );

    /*!
     * \brief LSIS_ActiveContour4DContinuous
     *
     * Constructeur d'initialisation :
     * a partir de la position de la tete, on effectue une acp locale ponderee pour trouver la meilleure direction dans laquelle positionner la queue
     * On positionne la queue a (head + v1) ou v1 est la premiere direction propre de l'acp normalisee
     * La matrice des points est une matrice npoints*4
     *
     * \warning L'acp ne tient pas compte des pixels nuls
     * \warning Cette methode peut etre optimisee, voir la conversion faite ligne 96 a cause de l'operation de multiplication par un scalaire
     * \todo Cette methode peut etre optimisee, voir la conversion faite ligne 96 a cause de l'operation de multiplication par un scalaire
     *
     * \param image : image sur laquelle se deplace le contour
     * \param head : tete du contour (un pixel sur l'image)
     * \param size : taille du voisinage pour lequel effectuer l'acp
     */
    LSIS_ActiveContour4DContinuous(const CT_Grid4D_Sparse<DataImage>* image,
                                   CT_Grid4D_Sparse<bool>* repulseImage,
                                   const LSIS_Pixel4D &head,
                                   int size );

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList);

    /*!
     * \brief points
     *
     * Acces en lecture a la matrice des points
     * La matrice des points est une matrice npoints*4
     *
     * \return la matrice des points du contours
     */
    inline const Eigen::MatrixXf& points() const { return _points; }

    /*!
     * \brief npoints
     *
     * Nombre de points dans le contour
     *
     * \return le nombre de points du contour
     */
    inline int npoints() const { return _points.rows(); }

    /*!
     * \brief pointAt
     *
     * Accede a une copie du ieme point du contour
     *
     * \param i : indice du point auquel on veut acceder
     *
     * \return une copie du ieme point du contour sous la forme d'un point4D
     */
    inline LSIS_Point4DFloat pointAt( int i ) const { return _points.row(i) ;}

    /*!
     * \brief forks
     *
     * Acces en lecture a la matrice des forks
     * La matrice des points est une matrice nforks*4
     *
     * \return la matrice des forks du contours
     */
    inline const QList<LSIS_Point4DFloat>& forks() const { return _forkStartPoints; }

    /*!
     * \brief nforks
     *
     * number of forks in the snake
     *
     * \return le nombre de forks du contour
     */
    inline int nforks() const { return _forkStartPoints.size(); }

    /*!
     * \brief forkAt
     *
     * Accede a une copie du ieme fork du contour
     *
     * \param i : indice du fork auquel on veut acceder
     *
     * \return une copie du ieme fork du contour sous la forme d'un point4D
     */
    inline LSIS_Point4DFloat forkAt( int i ) const { return _forkStartPoints.at(i) ;}

    inline LSIS_Pixel4D pixelAt( int i ) const { LSIS_Pixel4D pix; pix.setFromCartesian( pointAt(i), _image ); return pix; ;}


    /*!
     * \brief head
     *
     * Accede a une copie de la tete du contour
     *
     * \return une copie de la tete du contour sous la forme d'un point4D
     */
    inline LSIS_Point4DFloat head() const { return pointAt(0); }

    /*!
     * \brief back
     *
     * Accede a une copie de la queue du contours
     *
     * \return une copue de la tete du contours sous la forme d'un point 4D
     */
    inline LSIS_Point4DFloat back() const { return pointAt( npoints()-1 ); }

    /*!
     * \brief tangentAtPoint
     *
     * Acces a la tangente a la courbe en un point donne
     *
     * \param i : indice du point pour lequel on cherche une tangente
     *
     * \return la tangente en ce point (obtenue par difference centree si le point est dans le contours et par difference a droite/gauche si le point est en tete/queue)
     */
    LSIS_Point4DFloat tangentAtPoint( int i ) const;

    /*!
     * \brief tangentNormAtPoint
     *
     * Calcule la normae de la tangente a la courbe en un point donne
     *
     * \param i : indice du point pour lequel on cherche une tangente
     *
     * \return la norme de la tangente
     */
    inline float tangentNormAtPoint( int i ) const
    {
        return tangentAtPoint(i).norm();
    }

    /*!
     * \brief tangentAtHead
     *
     * Accede a la tangente a la tete du contour
     *
     * \return la tangente a la tete obtenue par difference finie ( points(1) - point(0) )
     */
    inline LSIS_Point4DFloat tangentAtHead() const { return tangentAtPoint(0); }

    /*!
     * \brief tangentAtBack
     *
     * Accede a la tangente a la queue du contour
     *
     * \return la tangente a la queue obtenue par difference finie ( points( n-1 ) - point( n-2 ) )
     */
    inline LSIS_Point4DFloat tangentAtBack() const { return tangentAtPoint( npoints() - 1 ); }

    /*!
     * \brief directionContoursAtPoint
     *
     * Donne la direction du contours au point indexPoint
     * Trois cas possibles pour avoir cette direction :
     * - on la demande pour le pixel de tete : on renvoie la tangente a la tete
     * - on la demande pour un pixel dans le snake : on renvoie la difference entre le point suivant et le point precedent
     * - on la demande pour le pixel de queue : on renvoie la tangente a la queue
     *
     * \param indexPoint : indice du point dont on veut la direction
     *
     * \return la direction du snake au point indexPoint
     */
    LSIS_Point4DFloat directionContoursAtPoint( int indexPoint ) const;

    /*!
     * \brief secondDifferentialAtPoint
     *
     * Donne la derivee seconde du contour en un point du contour
     * Cette derivee seconde est approximee par difference finie
     * \warning : puisqu'on impose une courbure nulle aux extremites, cette methode renvoie un vecteur null aux extremites
     *
     * \param indexPoint : indice du point dont on veut la differentielle seconde
     *
     * \return la differentielle seconde au point d'interet approximee par difference finie
     */
    LSIS_Point4DFloat secondDifferentialAtPoint( int indexPoint ) const;

    /*!
     * \brief averageSampling
     *
     * Renvoie la distance moyenne entre deux points consecutifs du snake
     *
     * \return
     */
    float averageSampling() const;

    /*!
     * \brief hasToBeResampled
     *
     * Indique si le snake doit etreresample
     * C'est le cas si la distance entre deux points consecutifs est superieure a deux fois la resolution de l'image en 3D
     *
     * \return true si le snake doit etre resample, false sinon
     */
    bool hasToBeResampled() const;

    /*!
     * \brief resample
     *
     * Reechantillone le contour
     *
     * \param sampleRes : resolution d'echantillonage (espace entre deux echantillons sur le contour)
     */
    void resample( float sampleRes );

    /*!
     * \brief length
     *
     * Calcule la longueur du contour comme la somme des distances euclidienne entre les points successifs en 4D
     *
     * \return la longueur du contour
     */
    float length4D () const;

    /*!
     * \brief length
     *
     * Calcule la longueur du contour comme la somme des distances euclidienne entre les points successifs en 3D
     *
     * \return la longueur du contour
     */
    float length3D () const;

    /*!
     * \brief toCircleData
     *
     * Transforme le contour actif en tuboide :
     * Chaque point est transforme en un representant de cercle
     *
     * \param smoothSize : taille de la fenetre de lissage pour l'orientation
     *
     * \return le contour actif transforme en tuboide
     */
    QVector< CT_CircleData* > toCircleData( int smoothSize ) const;

    CT_Mesh* toMesh(QVector< CT_CircleData* >& circles , int nSidesCylinder) const;

    Eigen::Matrix3d getBaseAvecXAligneFromDirection( const CT_Point& dir ) const;

    Eigen::Matrix3d getNewCircleBaseFromPreviousBase(const Eigen::Matrix3d& prevBase, CT_Point& dir ) const;

    CT_MeshModel* toMeshModel(QString modelName, CT_AbstractResult* result, int smoothSize, int nSidesCylinder) const;

    CT_MeshModel* toMeshModelParall(QString modelName, CT_AbstractResult* result, int smoothSize, int nSidesCylinder) const;

    /*!
     * \brief toCircles
     *
     * Transforme le contour actif en tuboide :
     * Chaque point est transforme en un cercle
     *
     * \param smoothSize : taille de la fenetre de lissage pour l'orientation
     * \param color : comment colorer les cercles ?
     *
     * \return le contour actif transforme en tuboide
     */
    QVector< CT_Circle* > toCircles(int smoothSize, ColorMode color, const QString& modelName, CT_AbstractResult* result ) const;

    CT_MeshModel* toMeshModel(int smoothSize, ColorMode color, const QString& modelName, CT_AbstractResult* result ) const;

    /*!
     * \brief toLines
     *
     * Transforme le contour actif en squelette 3D
     *
     * \return le squelette 3D du tuboide
     */
    QVector< CT_LineData* > toLineData() const;

    /*!
     * \brief toLines
     *
     * Transforme le contour actif en squelette 3D
     *
     * \return le squelette 3D du tuboide
     */
    QVector< CT_Line* > toLines() const;

    /*!
     * \brief getGeometricMatrix
     *
     * Calcule la matrice de geometrie (la matrice pentadiagonale construite a partir des coefficients alpha et beta)
     *
     * \warning : faire le travail sur les conditions aux bords de la matricede geometrie
     * \warning : faire le travail de division par h pour avoir une homogeneite (dans la plupart des cas on est a peu pres homogene puisque lesnake est souvent resample mais ce n'est pas le cas exactement)
     * \todo : faire le travail sur les conditions aux bords de la matricede geometrie
     * \todo : faire le travail de division par h pour avoir une homogeneite (dans la plupart des cas on est a peu pres homogene puisque lesnake est souvent resample mais ce n'est pas le cas exactement)
     *
     * \param alpha : coefficient de ponderation de l'elasticite
     * \param beta : coefficient de ponderation de la courbure
     *
     * \return la matrice de geometrie du contour (matrice npoints*npoints )
     */
    Eigen::MatrixXf* getGeometricMatrix ( float alpha, float beta ) const ;

    Eigen::MatrixXf* getGeometricMatrixHomogeneousElasticityCurvature ( float alpha, float beta ) const ;

    /*!
     * \brief getInverseOfGeometricMatrix
     *
     * Calcule le premier terme du systeme matriciel pour l'optimisation des snakes ( (A + rho * I )^(-1) )
     *
     * \param alpha : coefficient de ponderation de l'elasticite
     * \param beta : coefficient de ponderation de la courbure
     * \param timeStep : pas de temps pour l'optimisation
     *
     * \return le premier terme du systeme matriciel pour l'optimisation des snakes
     */
    Eigen::MatrixXf* getAPlusRhoIInverse(float alpha, float beta , float timeStep);

    Eigen::MatrixXf* getRhoIMoinsAInverse(float alpha, float beta , float timeStep);

    /*!
     * \brief getGradientEnergyGrowMatrix
     *
     * Calcule le gradient de l'energie de croissance en chaque point du nuage
     * Ce gradient est nul partout sauf pour la tete et la queue du contour
     * \warning Si le gradient est non nul partout sauf en deux endroits il est surementpossible d'optimiser le code dans le calcul de la relaxation
     * \todo Si le gradient est non nul partout sauf en deux endroits il est surementpossible d'optimiser le code dans le calcul de la relaxation
     *
     * \param angleDegresMax : angle maximum du cone de recherche
     * \param size : taille du voisinage de recherche
     * \param seuilVariance : dans le cone, si la variance est inférieure a ce seuil, on donne une croissance nulle
     *
     * \return une matrice npoints*4 dont les lignes sont les gradients de l'energie de croissance en chaque point du contour
     */
    Eigen::MatrixXf* getGradientEnergyGrowMatrix(float angleDegresMax,
                                                 int size,
                                                 float seuilVariance,
                                                 int tmpIteration,
                                                 float seuilSigma, float seuilSigma2,
                                                 CT_Grid4D<bool> *repulseImage,
                                                 QList<float>& snrHead,
                                                 QList<float>& snrBack,
                                                 QList<bool> &growHead,
                                                 QList<bool> &growBack, QList<float> &sigmasHead, QList<float> &sigmasBack, int &nManquePixelsDansConeHead, int &nManquePixelsDansConeBack, int nSigmaSmooth, bool &hasGrownHead, bool &hasGrownBack, float globalWeight) const;

//    Eigen::MatrixXf* getGradientEnergyGrowMatrixv2(CT_Grid4D<bool> *repulseImage,
//                                                   float coneAngleDegres,
//                                                   int coneSizePixels,
//                                                   float seuilSigmaL1,
//                                                   float &outSigmaL1Head,
//                                                   float &outSigmaL1Back,
//                                                   bool& outHasGrownHead,
//                                                   bool& outHasGrownBack,
//                                                   bool& outHasGrown3DHead,
//                                                   bool& outHasGrown3DBack) const;

    void getGrowingDirections(CT_Grid4D_Sparse<bool> *repulseImage,
                              float coneAngleDegres,
                              int coneSizePixels,
                              float seuilSigmaL1,
                              float &outSigmaL1Head,
                              float &outSigmaL1Back,
                              bool& outHasGrownHead,
                              bool& outHasGrownBack,
                              LSIS_Point4DFloat& outGrowDirHead,
                              LSIS_Point4DFloat& outGrowDirBack ) const;

    /*!
     * \brief getGradientEnergyGrowProportionalResolution
     *
     * Regroupe les deux methodes ci dessus :
     * Calcule le gradient de l'energie de croissance a la tete du contours
     * La direction de ce gradient est calcule comme l'oppose de la direction de croissance obtenue par acp dans un cone autour de la tengante a la tete
     * On prend l'oppose puisque dans le schema d'optimisation on se deplace dans la direction opposee au gradient (donc on se deplace dans l'oppose de la direction oppose a celle qu'on veut, doncla direction que l'on souhaite)
     * \warning La norme de ce vecteur est proportionnelle au produit scalaire entre la tangente et le vecteur de croissance
     * \warning La norme de ce vecteur est aussi proportionnelle a la resolution de l'espace de Hough
     * \todo Cette methode peut etre optimisee (voir l'ajout des pixels du cone dans le vecteur de pixels sur lesquels effectuer l'acp)
     * \warning Cette methode peut etre optimisee (voir l'ajout des pixels du cone dans le vecteur de pixels sur lesquels effectuer l'acp)
     *
     * \param pix : pixel appex du cone de recherche
     * \param direction : direction du cone de recherche : direction de sortie du snake (i.e. doit etre la tangente en queue et oppose detangente en tete)
     * \param angleDegresMax : angle maximum du cone de recherche
     * \param size : taille du voisinage de recherche
     * \param seuilVariance : dans le cone, si la variance est inférieure a ce seuil, on donne une croissance nulle
     *
     * \return un point 4D representant le gradient de l'energie de croissance au pixel pix
     */
    inline LSIS_Point4DFloat getGradientEnergyGrowProportionalResolution(LSIS_Pixel4D const & pix,
                                                                         LSIS_Point4DFloat const & direction,
                                                                         float angleDegresMax,
                                                                         int size,
                                                                         float seuilVariance,
                                                                         int tmpIteration,
                                                                         float seuilSigma, float seuilSigma2,
                                                                         QList<float> &sigmas,
                                                                         int nSigmasSmooth,
                                                                         bool &outGrow, int &nIterationsManquePixelsInCone,
                                                                         QList<bool> &grow ,
                                                                         float globalWeight,
                                                                         CT_Grid4D<bool> *repulseImage) const;

    inline LSIS_Point4DFloat getGradientEnergyGrowProportionalResolutionv2(CT_Grid4D_Sparse<bool> *repulseImage,
                                                                           LSIS_Point4DFloat& snakeExtremityPoint,
                                                                           LSIS_Pixel4D & snakeExtremityPix,
                                                                           LSIS_Point4DFloat const & snakeTangentAtExtremity,
                                                                           float coneAngleDegres,
                                                                           int coneSizePixels,
                                                                           float &outSigmaL1,
                                                                           bool& outHasGrown,
                                                                           bool verbose = false) const;

    inline LSIS_Point4DFloat getGradientEnergyGrowProportionalResolutionv3(CT_Grid4D_Sparse<bool> *repulseImage,
                                                                           LSIS_Point4DFloat& snakeExtremityPoint,
                                                                           LSIS_Pixel4D & snakeExtremityPix,
                                                                           LSIS_Point4DFloat const & snakeTangentAtExtremity,
                                                                           float coneAngleDegres,
                                                                           int coneSizePixels,
                                                                           float &outSigmaL1,
                                                                           bool& outHasGrown ) const;

    inline LSIS_Point4DFloat getGradientEnergyGrowProportionalResolutionv4(CT_Grid4D_Sparse<bool> *repulseImage,
                                                                           LSIS_Point4DFloat& snakeExtremityPoint,
                                                                           LSIS_Pixel4D & snakeExtremityPix,
                                                                           LSIS_Point4DFloat const & snakeTangentAtExtremity,
                                                                           float coneAngleDegres,
                                                                           int coneSizePixels,
                                                                           float &outSigmaL1,
                                                                           bool& outHasGrown ) const;

    /*!
     * \brief getGradientEnergyImageMatrix
     *
     * Calcule les gradients de l'energie image en chaque point du contours
     *
     * \param globalWeight : poids de l'energie globale dans le calcul de l'energie image (inclu dans [0,1])
     *
     * \return une matrice npoints*4 dont les lignes sont les gradients de l'energie image en chaque points du contours
     */
    Eigen::MatrixXf* getGradientEnergyOrthogonalImageMatrix (float globalWeight) const;

    LSIS_Point4DFloat getGradientEnergyOrthogonalImageAtIndex( float globalWeight, int index );

    Eigen::MatrixXf* getGradientEnergyOrthogonalImageMatrixMultipliedByTangentNorm (float globalWeight) const;

    Eigen::MatrixXf* getSecondDifferentialOnTangentDividedByTangentNormMultiplyedByImageEnergy( float globalWeight );

    /*!
     * \brief gradientEnergyImageInterpolatedAtPoint
     *
     * Calcule le gradient de l'energie image pour le ieme point du contour
     * Se calcule grace a une interpolation lineaire du gradient entre le pixel contenant le ieme point et le pixel voisin le plus proche du point
     *
     * \param indexPoint : indice du point du contour
     * \param globalWeight : poids de l'energie globale dans le calcul de l'energie image (inclu dans [0,1])
     * \return le gradient de l'energie image au ieme point du contour
     */
    LSIS_Point4DFloat gradientEnergyImageInterpolatedAtPoint(int indexPoint , float globalWeight ) const;

    /*!
     * \brief gradientEnergyImageAtPixel
     *
     * Calcule le gradient de l'energie image en un pixel donne
     * Dans le cas general le gradient est calcule par difference finie ( (energieImage( i+1 ) - energieImage( i-1 )) / 2.0 )
     * Toutefois sur les bords, le gradient est calcule par difference finie a droite ou a gauche selon le bord atteint ( energieImage(i) - energieImage(i-1) ou energieImage(i+1) - energieImage(i) )
     *
     * \param p : pixel d'interet
     * \param globalWeight : poids de l'energie globale dans le calcul de l'energie image (inclu dans [0,1])
     *
     * \return le gradient de l'energie image au pixel p obtenu par difference finie (centree/droite/gauche selon la position du pixel p dans l'image)
     */
    LSIS_Point4DFloat gradientEnergyImageAtPixel(LSIS_Pixel4D const & p , float globalWeight) const;

    /*!
     * \brief partialDerivativeEnergyImageAtPixel
     *
     * Calcule la derivee partielle de l'energie image dans une coordonnee donne (derivee partielle par rapport a w/x/y/z)
     * Dans le cas general le gradient est calcule par difference finie ( (energieImage( i+1 ) - energieImage( i-1 )) / 2.0 )
     * Toutefois sur les bords, le gradient est calcule par difference finie a droite ou a gauche selon le bord atteint ( energieImage(i) - energieImage(i-1) ou energieImage(i+1) - energieImage(i) )
     *
     * \param p : pixel d'interet
     * \param globalWeight : poids de l'energie globale dans le calcul de l'energie image (inclu dans [0,1])
     * \param coordToDerive : coordonnee sur laquelle on prend la derivee partielle
     *
     * \return la derivee partielle de l'energie image au pixel p selon la coordonnee demandee en utilisant la difference finie (centree/droite/gauche selon la position du pixel p dans l'image)
     */
    float partialDerivativeEnergyImageAtPixel(const LSIS_Pixel4D &p, float globalWeight, int coordToDerive ) const;

    /*!
     * \brief partialDerivativeEnergyImageAtPixelInImage
     *
     * \toto Faire la description de cette methode
     * p est dans l'image, on peut toujours calculer une derivee partielle :
     * 0 si l'image n'a qu'un pixel d'epaisser pour la coordonnee demandee
     * difference finie centree si p n'est pas un bord
     * difference finiea gauche/droite si p est un bord
     *
     * \param p
     * \param globalWeight
     * \param coordToDerive
     * \return
     */
    float partialDerivativeEnergyImageAtPixelInImage(const LSIS_Pixel4D &p, float globalWeight, int coordToDerive ) const;

    /*!
     * \brief partialDerivativeEnergyImageAtPixelInImage
     *
     * \toto Faire la description de cette methode
     *
     * \param p
     * \param globalWeight
     * \param coordToDerive
     * \return
     */
    float partialDerivativeEnergyImageAtPixelOutsideImage(const LSIS_Pixel4D &p, float globalWeight, int coordToDerive ) const;

    /*!
     * \brief energyImageAtPixel
     *
     * Calcule l'energie image pour un pixel donne
     * On calcule cette energie comme une ponderation d'un terme d'energie locale et d'un terme d'energie globale :
     * Energie locale = ( minInNeighbourhood - image(p) ) / ( maxInNeighbourhood - minInNeighbourhood ) (c.f. energyImageLocalAtPixel( LSIS_Pixel4D const & p ) )
     * Energie globale = ( minInHoughSpace - image(p) ) / ( maxInHoughSpace - minInHoughSpace ) (c.f. energyImageGlobalAtPixel( LSIS_Pixel4D const & p ) )
     *
     * \param p : pixel d'interet
     * \param globalWeight : poids de la composante "globale" dans le calcul de l'energie (la composante locale a un poids de 1-globalWeight). Ce parametre doit etre compris dans [0,1]
     *
     * \return l'energie image au pixel d'interet
     */
    virtual float energyImageAtPixel( LSIS_Pixel4D const & p, float globalWeight ) const;

    /*!
     * \brief energyImageLocalAtPixel
     *
     * Calcule le terme d'energie image locale pour un pixel donne
     * Energie locale = ( minInNeighbourhood - image(p) ) / ( maxInNeighbourhood - minInNeighbourhood ) (c.f. energyImageLocalAtPixel( LSIS_Pixel4D const & p ) )
     *
     * \param p : pixel d'interet
     *
     * \return l'energie image locale au pixel d'interet
     */
    float energyImageLocalAtPixel( LSIS_Pixel4D const & p ) const;

    /*!
     * \brief energyImageGlobalAtPixel
     *
     * Calcule le terme d'energie image globale pour un pixel donne
     * Energie globale = ( minInHoughSpace - image(p) ) / ( maxInHoughSpace - minInHoughSpace ) (c.f. energyImageGlobalAtPixel( LSIS_Pixel4D const & p ) )
     *
     * \param p : pixel d'interet
     *
     * \return l'energie image globale au pixel d'interet
     */
    float energyImageGlobalAtPixel( LSIS_Pixel4D const & p ) const;

    /*!
     * \brief updatePoints
     *
     * Met a jour la position des points durant le schema d'optimisation
     * En fonction de la nouvelle position des points (dans/hors de l'image)on fait un traitement different :
     * Un point dans l'image est simplement recopie
     * Un point hors de l'image est repositionne dans l'image
     *
     * \param newPoints : la position des points a l'iteration suivante obtenue par application des gradients et de la matrice geometrique (ces nouvelles positions peuvent etre en dehors de l'image)
     *
     * \return le nombre de points qui ont du etre remis dans l'image
     */
    int updatePoints(const Eigen::MatrixXf* newPoints );

    /*!
     * \brief updatePoints
     *
     * Met a jour la position des points durant le schema d'optimisation
     * En fonction de la nouvelle position des points (dans/hors de l'image)on fait un traitement different :
     * Un point dans l'image est simplement recopie
     * Un point hors de l'image est repositionne dans l'image
     *
     * \param newPoints : la position des points a l'iteration suivante obtenue par application des gradients et de la matrice geometrique (ces nouvelles positions peuvent etre en dehors de l'image)
     *
     * \return le nombre de points qui ont du etre remis dans l'image
     */
    int updatePoints(const Eigen::MatrixXf &newPoints );

    void updatePointsFromNewSize(const Eigen::MatrixXf &newPoints );

    void updatePointsAndGetAverageAndStDevMovement(const Eigen::MatrixXf &newPoints,
                                                   float& outAverageMovemet,
                                                   float& outStdDevMovement );

    void updatePoints();

    /*!
     * \brief grow
     *
     * Temporaire pour visualisation : faire croitre le snake un nombre d'iterations fixe
     *
     * \param nIterMax : nombre d'iteration de croissance
     * \param alpha : coefficient de ponderation de l'elasticite
     * \param beta : coefficient de ponderation de la courbure
     * \param gama : coefficient de ponderation de l'energie image
     * \param globalWeight : ponderation entre l'energie image globale et locale
     * \param k : coefficient de croissance
     * \param angleMaxDegres : angle de recherche pour la direction de croissance
     * \param size : taille du voisinage de recherche pour la direction de croissance
     * \param timeStep : pas de temps pendant l'optimisation
     * \param seuilVariance : difference de longueur minimum pour continuer l'optimisation
     * \param gradMovementMin : norme du mouvement minimum pur continuer l'optimisation
     * \param gradLengthMin : difference de longueur minimum pour continuer l'optimisation
     */
    void grow(CT_Grid4D<bool> *repulseImage, int nIterMax,
              float alpha, float beta,
              float gama, float globalWeight,
              float k, float angleMaxDegres, float size,
              float timeStep,
              float seuilVariance,
              float seuilSigma, float seuilSigma2,
              float gradMovementMin,
              float gradLengthMin);

    void growv2(CT_Grid4D_Sparse<bool> *repulseImage,
                int nIterMax,
                float growCoeff,
                float coneAngleMaxDegres,
                float coneSizeMaxPixels,
                float seuilSigmaL1 );

    void relax(int nIterMax,
               float alpha, float beta,
               float gama, float globalWeight,
               float timeStep,
               float threshAverageMovement4D);

    void goToMaximumScoreInRadius();

    /*!
     * \brief markRepulsion
     *
     * Marque le snake dans l'image de repulsion
     *
     * \param repulseImage
     */
    void markRepulsion(CT_Grid4D_Sparse<bool>* repulseImage , float repulseFactor);

    void markRepulsionv2(CT_Grid4D_Sparse<bool>* repulseImage , float repulseFactor);

    /*!
     * \brief stopGrowing
     *
     * Indique si la croissance doit s'arreter.
     * Pour cela, deux criteres :
     * - un seuil sur le movement du snake entre deux iterations (seuil sur la norme du gradient de la courbe par rapport au temps)
     *   \warning : On prend en compte le mouvement moyen
     * - un seuil sur la longueur du snake entre deux iterations
     * \warning Cette methode peut etre optimisee
     * \todo Cette methode peut etre optimisee
     *
     * \param oldPoints : les points avant une etape d'optimisation
     * \param newPoints : les points apres une etape du schema d'optimisation
     * \param gradMovementMin : seuil sur la norme du gradient de movement par rapport au temps
     * \param gradLengthMin : seuil sur le gradient de la longueur du snake par rapport au temps
     *
     * \return trua si l'optimisation doit s'arreter, false sinon
     */
    bool stopGrowing(const Eigen::MatrixXf *oldPoints,
                     Eigen::MatrixXf const * newPoints,
                      float gradMovementMin,
                      float gradLengthMin );

    /*!
     * \brief getScoreStats
     *
     * Calcule le score min et max de spixels du contour
     * \todo : completer cette methode avec plus de statistiques
     * \warning : completer cette methode avec plus de statistiques
     *
     * \param outMin
     * \param outMax
     */
    void getScoreStats( DataImage& outMin, DataImage& outMax ) const;

    /*!
     * \brief getPointCloudAndNormalsAroundEachSnake
     *
     * Pour chaque contours du tableau d'entree on recupere les points dans la bbox du contour ainsi que leur normales
     *
     * \param snakes : tableau de contours en entree
     * \param sceneIndexCloud : nuage de points de la scene a diviser entre les contours
     * \param sceneNormalCloud : nuage de normales de la scene a diviser entre les contours
     * \param outIndexClouds : sortie : vecteur de nuage d'indices
     * \param outNormalClouds : sortie : vecteur de nuage de normales
     */
    void getPointCloudAndNormalsAroundEachSnake( QVector< LSIS_ActiveContour4DContinuous<DataImage>* > &snakes,
                                                 CT_PCIR sceneIndexCloud,
                                                 CT_AbstractNormalCloud* sceneNormalCloud,
                                                 QVector< CT_PointCloudIndexVector* > &outIndexClouds,
                                                 QVector< CT_NormalCloudStdVector* > &outNormalClouds);

    void save (QString directory, QString fileName );

    void saveWithMntHeight(QString directory, QString fileName, CT_Image2D<float>* mnt );

    void getBoundingBox( float& outMinw, float& outMinx, float& outMiny, float& outMinz,
                         float& outMaxw, float& outMaxx, float& outMaxy, float& outMaxz );

    const LSIS_StepSnakesInHoughSpace *step() const;
    void setStep(LSIS_StepSnakesInHoughSpace *step);

    const CT_Grid4D<DataImage> *image() const;
    void setImage(const CT_Grid4D<DataImage> *image);

    CT_Grid4D<float> *gradw() const;
    void setGradw(CT_Grid4D<float> *gradw);

    CT_Grid4D<float> *gradx() const;
    void setGradx(CT_Grid4D<float> *gradx);

    CT_Grid4D<float> *grady() const;
    void setGrady(CT_Grid4D<float> *grady);

    CT_Grid4D<float> *gradz() const;
    void setGradz(CT_Grid4D<float> *gradz);

    CT_Grid4D<float> *imageEnergy() const;
    void setImageEnergy(CT_Grid4D<float> *imageEnergy);

    CT_Image2D<float>* getOptionalMnt() const;
    void setOptionalMnt(CT_Image2D<float>* optionalMnt);

    DataImage getIntensityMin() const;
    void setIntensityMin(const DataImage& intensityMin);

    bool addFork(LSIS_Point4DFloat forkFirstPoint);
    bool removeFork(int i);

    bool isFork() const;
    void setIsFork(bool isFork);

    void sortPointsbyHeight();
    bool calculateSnakeValidity();

    inline static int getUniqueId()
    {
        _validSnakesCpt++;
        return _validSnakesCpt;
    }   
private :
//    CT_DEFAULT_IA_BEGIN(LSIS_ActiveContour4DContinuous<DataImage>)
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataXDimension(), &LSIS_ActiveContour4DContinuous::gradw, QObject::tr("X dimension"), "xd")
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataYDimension(), &LSIS_ActiveContour4DContinuous::gradw, QObject::tr("Y dimension"), "yd")
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataZDimension(), &LSIS_ActiveContour4DContinuous::gradw, QObject::tr("Z dimension"), "zd")
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataXResolution(), &LSIS_ActiveContour4DContinuous::gradw, QObject::tr("X Resolution"), "xres")
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataYResolution(),&LSIS_ActiveContour4DContinuous::gradw, QObject::tr("Y Resolution"), "yres")
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataZResolution(), &LSIS_ActiveContour4DContinuous::gradw, QObject::tr("Z Resolution"), "zres")
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataNa(), &LSIS_ActiveContour4DContinuous::gradw, QObject::tr("NA"), "na")
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataValue(), &LSIS_ActiveContour4DContinuous::gradw, QObject::tr("Min Value"), "min")
//    CT_DEFAULT_IA_V3(LSIS_ActiveContour4DContinuous<DataImage>, CT_AbstractCategory::staticInitDataValue(), &LSIS_ActiveContour4DContinuous::gradw, QObject::tr("Max Value"), "max")
//    CT_DEFAULT_IA_END(LSIS_ActiveContour4DContinuous<DataImage>)

    Eigen::MatrixXf                 _points;      /*!< Points du contours places dans une matrice n lignes, 4 colonnes, la precision flottante est suffisante */
    QList<LSIS_Point4DFloat>        _forkStartPoints;      /*!< Fork starting points for export in further options*/
    bool                            _isFork = false;              /*!< Since a fork is still a snake(activecontour4dcontinuous), I want to ensure that fork maximum finding isn't eternally recursive*/
    CT_Grid4D_Sparse<DataImage> const *    _image;       /*!< Un contours actif se deplace sur une image */
    CT_Grid4D_Sparse<bool> const *    _repulseImage;
    LSIS_StepSnakesInHoughSpace*    _step;        /*!< Un pointeur vers une etape pour pouvoir la mettre en pause en mode debug etc */
    CT_Grid4D<float>*               _gradw;       /*!< Un pointeur vers l'image des gradients en w */
    CT_Grid4D<float>*               _gradx;       /*!< Un pointeur vers l'image des gradients en x */
    CT_Grid4D<float>*               _grady;       /*!< Un pointeur vers l'image des gradients en y */
    CT_Grid4D<float>*               _gradz;       /*!< Un pointeur vers l'image des gradients en z */
    CT_Grid4D<float>*               _imageEnergy; /*!< Un pointeur vers l'image d'energie */
    CT_Image2D<float>*              _optionalMnt;
    DataImage                       _intensityMin;

    static int                      _validSnakesCpt;

    // Pour ne pas m'embeter a tout rebouger le code je met ca en public...
public:
    int                                                         _id;
    int                                                         _ordre;
    LSIS_ActiveContour4DContinuous<DataImage>*                  _parent;
    QVector< LSIS_ActiveContour4DContinuous<DataImage>* >       _children;
};

template <typename DataImage>
int LSIS_ActiveContour4DContinuous<DataImage>::_validSnakesCpt = 0;

// Implementation templates
#include "lsis_activecontour4dcontinuous.hpp"

#endif // LSIS_ACTIVECONTOUR4DCONTINUOUS_H
