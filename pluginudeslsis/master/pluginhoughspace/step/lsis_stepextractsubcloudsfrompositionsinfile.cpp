#include "lsis_stepextractsubcloudsfrompositionsinfile.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_scene.h"

//Tools dependencies
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_point.h"
#include "ct_iterator/ct_pointiterator.h"

//Qt dependencies
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltPositions "rsltPositions"
#define DEFin_groGrpCircles "groGrpCircles"
#define DEFin_grpCircles "grpCircles"
#define DEFin_itmCircle "itmCircle"

#define DEFin_rsltInitialPointCloud "rsltInitialPointCloud"
#define DEFin_grpPointCloud "grpPointCloud"
#define DEFin_itmPointCloud "itmPointCloud"

#define DEFout_rsltSubClouds "rsltSubClouds"
#define DEFout_grpGrpPointCloud "grpGrpPointCloud"
#define DEFout_grpPointCloud "grpPointCloud"
#define DEFout_itmPointCloud "itmPointCloud"


// Constructor : initialization of parameters
LSIS_StepExtractSubCloudsFromPositionsInFile::LSIS_StepExtractSubCloudsFromPositionsInFile(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _size = 2;
    _saveMode = false;
    _outputDirPath = ".";
    _inputCloudFileName = ".";
}

// Step description (tooltip of contextual menu)
QString LSIS_StepExtractSubCloudsFromPositionsInFile::getStepDescription() const
{
    return tr("Extrait des sous nuages de points a partir des estimations de position de troncs");
}

// Step detailled description
QString LSIS_StepExtractSubCloudsFromPositionsInFile::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString LSIS_StepExtractSubCloudsFromPositionsInFile::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepExtractSubCloudsFromPositionsInFile::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepExtractSubCloudsFromPositionsInFile(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepExtractSubCloudsFromPositionsInFile::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltPositions = createNewInResultModel(DEFin_rsltPositions, tr("Result Positions"));
    resIn_rsltPositions->setRootGroup(DEFin_groGrpCircles, tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resIn_rsltPositions->addGroupModel(DEFin_groGrpCircles, DEFin_grpCircles, CT_AbstractItemGroup::staticGetType(), tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resIn_rsltPositions->addItemModel(DEFin_grpCircles, DEFin_itmCircle, CT_Circle::staticGetType(), tr("Position"));
}

// Creation and affiliation of OUT models
void LSIS_StepExtractSubCloudsFromPositionsInFile::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltSubClouds = createNewOutResultModel(DEFout_rsltSubClouds, tr("Result Extraction"));
    res_rsltSubClouds->setRootGroup(DEFout_grpGrpPointCloud, new CT_StandardItemGroup(), tr("Group"));
    res_rsltSubClouds->addGroupModel(DEFout_grpGrpPointCloud, DEFout_grpPointCloud, new CT_StandardItemGroup(), tr("Group"));
    res_rsltSubClouds->addItemModel(DEFout_grpPointCloud, DEFout_itmPointCloud, new CT_Scene(), tr("Sub Cloud around position"));
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepExtractSubCloudsFromPositionsInFile::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Taille de decoupe", "de chaque cote", 0.0001, 9999, 4, _size, 1);
    configDialog->addBool("Save","","",_saveMode);
    configDialog->addString("","",_outputDirPath);
    configDialog->addString("Original file Name","",_inputCloudFileName);
}

void LSIS_StepExtractSubCloudsFromPositionsInFile::compute()
{
    // Recupere le resultat de sortie que l'on va remplir avec le(s) sous nuage(s)
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _rsltSubClouds = outResultList.at(0);
    _grpGrpPointClouds = new CT_StandardItemGroup( DEFout_grpGrpPointCloud, _rsltSubClouds );
    _rsltSubClouds->addGroup(_grpGrpPointClouds);

    // Recupere le(s) nuage(s) d'entree
    QVector< CT_Circle* > circles;
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltCircles = inResultList.at(0);

    CT_ResultGroupIterator itIn_grpCircle(resIn_rsltCircles, this, DEFin_grpCircles);
    while (itIn_grpCircle.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpCircle = (CT_AbstractItemGroup*) itIn_grpCircle.next();

        CT_Circle* itemIn_itmCircle = (CT_Circle*)grpIn_grpCircle->firstItemByINModelName(this, DEFin_itmCircle );
        if (itemIn_itmCircle != NULL)
        {
            circles.push_back( itemIn_itmCircle );
        }
    }

    if( _saveMode )
    {
        computeAndSave( _inputCloudFileName, circles, _outputDirPath );
    }

    else
    {
        compute( _inputCloudFileName, circles );
    }
}

void LSIS_StepExtractSubCloudsFromPositionsInFile::computeAndSave(QString &fileCloudFullName,
                                                                  QVector<CT_Circle *> &circles,
                                                                  QString outDirPath )
{
    int nCircles = circles.size();
    CT_Circle* curCircle;

    // On regarde pour chaque cercle si le point est dans une zone autour du centre
    for( int j = 0 ; j < nCircles ; j++ )
    {
        curCircle = circles.at(j);

        QFile fileInput( fileCloudFullName );
        if( !fileInput.open( QIODevice::ReadOnly ) )
        {
            qDebug() << "Impossible d'ecrire le fichier " << fileCloudFullName;
            exit(0);
        }
        QTextStream streamInput( &fileInput );

        bool first = true;
        QString outputFileName = "tree" + QString::number(j) + ".xyz";
        QFile fileOutput( outDirPath + "/" + outputFileName );
        if( !fileOutput.open( QIODevice::WriteOnly ) )
        {
            qDebug() << "Impossible d'ecrire le fichier " << outDirPath + "/" + outputFileName;
            exit(0);
        }
        QTextStream streamOutput( &fileOutput );

        // Parcours  tous les points
        float x, y, z;
        while( !streamInput.atEnd() )
        {
            streamInput >> x >> y >> z;
            // Avance l'iterateur et recupere le point sous l'iterateur
            CT_Point currentPoint = createCtPoint( x, y, z );

            if( isPointIn2dBox( currentPoint,
                                curCircle->getCenterX() - _size,
                                curCircle->getCenterY() - _size,
                                curCircle->getCenterX() + _size,
                                curCircle->getCenterY() + _size) )
            {
                if( first == false )
                {
                    streamOutput << "\n";
                }

                else
                {
                    first = false;
                }

                // Alors on l'ajoute au nuage de points de sortie correspondant au cercle
                streamOutput << currentPoint.x() << " "
                             << currentPoint.y() << " "
                             << currentPoint.z();
            }
        }

        fileInput.close();
        fileOutput.close();
    }
}

void LSIS_StepExtractSubCloudsFromPositionsInFile::compute(QString& fileCloudFullName,
                                                           QVector< CT_Circle* >& circles)
{
    int nCircles = circles.size();
    CT_Circle* curCircle;

    // On regarde pour chaque cercle si le point est dans une zone autour du centre
    for( int j = 0 ; j < nCircles ; j++ )
    {
        curCircle = circles.at(j);

        QFile fileInput( fileCloudFullName );
        if( !fileInput.open( QIODevice::ReadOnly ) )
        {
            qDebug() << "Impossible d'ecrire le fichier " << fileCloudFullName;
            exit(0);
        }
        QTextStream streamInput( &fileInput );

        CT_AbstractUndefinedSizePointCloud* curOutScene = PS_REPOSITORY->createNewUndefinedSizePointCloud();;
        CT_Point bboxBot = createCtPoint( std::numeric_limits<float>::max(),
                                          std::numeric_limits<float>::max(),
                                          std::numeric_limits<float>::max());
        CT_Point bboxTop = createCtPoint( -std::numeric_limits<float>::max(),
                                          -std::numeric_limits<float>::max(),
                                          -std::numeric_limits<float>::max());

        // Parcours  tous les points
        float x, y, z;
        while( !streamInput.atEnd() )
        {
            streamInput >> x >> y >> z;
            // Avance l'iterateur et recupere le point sous l'iterateur
            CT_Point currentPoint = createCtPoint( x, y, z );

            if( isPointIn2dBox( currentPoint,
                                curCircle->getCenterX() - _size,
                                curCircle->getCenterY() - _size,
                                curCircle->getCenterX() + _size,
                                curCircle->getCenterY() + _size) )
            {
                // Alors on l'ajoute au nuage de points de sortie correspondant au cercle
                curOutScene->addPoint( currentPoint );
                updateBBox( bboxBot, bboxTop, currentPoint );
            }
        }

        CT_NMPCIR registeredCurSubCloud = PS_REPOSITORY->registerUndefinedSizePointCloud( curOutScene );
        CT_Scene* curScene = new CT_Scene( DEFout_itmPointCloud, _rsltSubClouds, registeredCurSubCloud );
        curScene->setBoundingBox( bboxBot.x(), bboxBot.y(), bboxBot.z(),
                                  bboxTop.x(), bboxTop.y(), bboxTop.z() );

        CT_StandardItemGroup* grpCurExtractedCloud = new CT_StandardItemGroup( DEFout_grpPointCloud, _rsltSubClouds );
        grpCurExtractedCloud->addItemDrawable( curScene );
        _grpGrpPointClouds->addGroup( grpCurExtractedCloud );
        _rsltSubClouds->addGroup(grpCurExtractedCloud);
    }
}

bool LSIS_StepExtractSubCloudsFromPositionsInFile::isPointIn2dBox(const CT_Point &point, float xmin, float ymin, float xmax, float ymax)
{
    return ( point.x() >= xmin &&
             point.x() <= xmax &&
             point.y() >= ymin &&
             point.y() <= ymax );
}

void LSIS_StepExtractSubCloudsFromPositionsInFile::updateBBox(CT_Point &bot, CT_Point &top, const CT_Point &point)
{
    for( int i = 0 ; i < 3 ; i++ )
    {
        if( point(i) < bot(i) )
        {
            bot(i) = point(i);
        }

        if( point(i) > top(i) )
        {
            top(i) = point(i);
        }
    }
}
