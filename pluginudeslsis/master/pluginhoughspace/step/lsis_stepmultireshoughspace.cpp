/************************************************************************************
* Filename :  lsis_stepmultireshoughspace.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_stepmultireshoughspace.h"

#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "../pixel/lsis_pixel4ddecrescentvaluesorter.h"
#include "../openactivecontour/lsis_activecontour4dcontinuous.h"

// Alias for indexing models
#define DEFin_rsltNormals "rsltNormals"
#define DEFin_grpNormals "grpNormals"
#define DEFin_itmNormals "itmNormals"

#define DEFout_rsltSnakes "rsltSnakes"
#define DEFout_grpSnakes "grpSnakes"
#define DEFout_grpCircles "grpCircles"
#define DEFout_itmCircle "itmCircle"


// Constructor : initialization of parameters
LSIS_StepMultiResHoughSpace::LSIS_StepMultiResHoughSpace(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _nIterMax = 0;
    _growCoeff = 1e-05;
    _timeStep = 1e-05;
    _threshGradMove = 1e-06;
    _threshGradLength = 0;

    setDebuggable( true );
}

// Step description (tooltip of contextual menu)
QString LSIS_StepMultiResHoughSpace::getStepDescription() const
{
    return tr("Lignes de cretes de l'espace");
}

// Step detailled description
QString LSIS_StepMultiResHoughSpace::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepMultiResHoughSpace::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepMultiResHoughSpace::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepMultiResHoughSpace(dataInit);
}

void LSIS_StepMultiResHoughSpace::preWaitForAckIfInDebugMode()
{
    // Si il est possible d'acceder a la GUI (ce qui est le cas uniquement en mode debug)
    if ( getGuiContext() != NULL )
    {
        // On choisi un document dans lequel afficher
        setDocumentForDebugDisplay( getGuiContext()->documentManager()->documents() );

        // Si un document est disponible pour l'affichage de la grille
        if ( _debugStructure.documentHoughSpace != NULL )
        {
            // Si on a un espace de Hough a afficher en debug mode on l'ajoute au document
            if ( _debugStructure.debugHoughSpace != NULL )
            {
                _debugStructure.documentHoughSpace->addItemDrawable( *(_debugStructure.debugHoughSpace) );
            }

            // On ajoute le squelette au meme document que celui qui contient l'espace de Hough
            if ( _debugStructure.debugSkeleton != NULL )
            {
                int nbLines = _debugStructure.debugSkeleton->size();
                for ( int i = 0 ; i < nbLines ; i++ )
                {
                    _debugStructure.documentHoughSpace->addItemDrawable( *(_debugStructure.debugSkeleton->at(i)) );
                }
            }

            // On actualise l'affichage du document
            _debugStructure.documentHoughSpace->redrawGraphics();
        }

        // Si un document est disponible pour l'affichage des cercles
        if ( _debugStructure.documentCircle != NULL )
        {
            // Si on a un espace de Hough a afficher en debug mode on l'ajoute au document
            if ( _debugStructure.debugCircles != NULL )
            {
                int nbCercles = _debugStructure.debugCircles->size();
                for ( int i = 0 ; i < nbCercles ; i++ )
                {
                    _debugStructure.documentCircle->addItemDrawable( *((*(_debugStructure.debugCircles))[i]) );
                }
            }

            // On ajoute le squelette au meme document que celui qui contient l'espace de Hough
            if ( _debugStructure.debugSkeleton != NULL )
            {
                int nbLines = _debugStructure.debugSkeleton->size();
                for ( int i = 0 ; i < nbLines ; i++ )
                {
                    _debugStructure.documentCircle->addItemDrawable( *(_debugStructure.debugSkeleton->at(i)) );
                }
            }

            // On actualise l'affichage du document
            _debugStructure.documentCircle->redrawGraphics();
        }
    }
}

void LSIS_StepMultiResHoughSpace::postWaitForAckIfInDebugMode()
{
    // Si un document etait disponible pour affichage
    if(_debugStructure.documentHoughSpace != NULL)
    {
        if ( _debugStructure.debugHoughSpace != NULL )
        {
            // On enleve la grille des elements du document
            _debugStructure.documentHoughSpace->removeItemDrawable(*_debugStructure.debugHoughSpace);
        }

        if ( _debugStructure.debugSkeleton != NULL )
        {
            int nbLines = _debugStructure.debugSkeleton->size();
            for ( int i = 0 ; i < nbLines ; i++ )
            {
                _debugStructure.documentHoughSpace->removeItemDrawable( *((*(_debugStructure.debugSkeleton))[i]) );
            }
        }

        // Et on lui demande de redessinner le tout (sans la grille)
        _debugStructure.documentHoughSpace->redrawGraphics();

        _debugStructure.debugHoughSpace = NULL;
        _debugStructure.debugSkeleton = NULL;
    }

    // Si un document etait disponible pour affichage
    if(_debugStructure.documentCircle != NULL)
    {
        if ( _debugStructure.debugCircles != NULL )
        {
            // On enleve les cercles des elements du document
            int nbCercles = _debugStructure.debugCircles->size();
            for ( int i = 0 ; i < nbCercles ; i++ )
            {
                _debugStructure.documentCircle->removeItemDrawable( *((*(_debugStructure.debugCircles))[i]) );
            }
        }

        if ( _debugStructure.debugSkeleton != NULL )
        {
            // On enleve les cercles des elements du document
            int nbLines = _debugStructure.debugSkeleton->size();
            for ( int i = 0 ; i < nbLines ; i++ )
            {
                _debugStructure.documentCircle->removeItemDrawable( *((*(_debugStructure.debugSkeleton))[i]) );
            }
        }

        // Et on lui demande de redessinner le tout (sans la grille)
        _debugStructure.documentCircle->redrawGraphics();

        _debugStructure.documentCircle = NULL;
    }
}

void LSIS_StepMultiResHoughSpace::setDocumentForDebugDisplay( QList<DocumentInterface *> docList )
{
    // On choisit le premier document disponible pour l'affichage des infos de debug
    // Si aucun n'est disponible on recupere null
    if(docList.isEmpty())
    {
        _debugStructure.documentHoughSpace = NULL;
        _debugStructure.documentCircle = NULL;
    }

    else
    {
        _debugStructure.documentCircle = docList.first();

        if ( docList.size() > 1 )
        {
            _debugStructure.documentHoughSpace = docList.at(1);
        }

        else
        {
            _debugStructure.documentHoughSpace = NULL;
        }
    }
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepMultiResHoughSpace::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltNormals = createNewInResultModel(DEFin_rsltNormals, tr("Normals"));
    resIn_rsltNormals->setRootGroup( DEFin_grpNormals, tr("Group"), tr(""), CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resIn_rsltNormals->addItemModel(DEFin_grpNormals, DEFin_itmNormals, CT_PointsAttributesNormal::staticGetType(), tr("Normals"));
}

// Creation and affiliation of OUT models
void LSIS_StepMultiResHoughSpace::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltSnakes = createNewOutResultModel(DEFout_rsltSnakes, tr("Stems"));
    res_rsltSnakes->setRootGroup(DEFout_grpSnakes, new CT_StandardItemGroup(), tr("Snakes"));
    res_rsltSnakes->addGroupModel(DEFout_grpSnakes, DEFout_grpCircles, new CT_StandardItemGroup(), tr("Group"));
    res_rsltSnakes->addItemModel(DEFout_grpCircles, DEFout_itmCircle, new CT_Circle(), tr("Circle"));

}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepMultiResHoughSpace::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("min r", "(m)", 0.0001, 100, 4, _minr, 1);
    configDialog->addDouble("max r", "(m)", 0.0002, 100, 4, _maxr, 1);
    configDialog->addDouble("Resolution r", "(m)", 0.0001, 2, 4, _resr, 1);
    configDialog->addDouble("Resolution x/y/z", "(m)", 0.0001, 2, 4, _resx, 1);

    configDialog->addText("Multi resolution","","");

    configDialog->addDouble("Resolution r", "(m)", 0.0001, 2, 4, _fineResr, 1);
    configDialog->addDouble("Resolution x/y/z", "(m)", 0.0001, 2, 4, _fineResx, 1);

    configDialog->addEmpty();

    configDialog->addInt("NIterMax", "", 0, 999999, _nIterMax);
    configDialog->addDouble("GrowCoeff", "", 1e-06, 100, 6, _growCoeff, 1);
    configDialog->addDouble("TimeStep", "", 1e-06, 100, 6, _timeStep, 1);
    configDialog->addDouble("ThreshGradMove", "", 1e-06, 100, 6, _threshGradMove, 1);
    configDialog->addDouble("ThreshGradLenght", "", 0, 100, 2, _threshGradLength, 1);
}

void LSIS_StepMultiResHoughSpace::compute()
{
    _resy = _resx;
    _resz = _resx;
    _fineResy = _fineResx;
    _fineResz = _fineResx;

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltNormals = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _snakesResult = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpNormals(resIn_rsltNormals, this, DEFin_grpNormals);
    while (itIn_grpNormals.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpNormals = (CT_AbstractItemGroup*) itIn_grpNormals.next();

        const CT_PointsAttributesNormal* itemIn_itmNormals = (CT_PointsAttributesNormal*)grpIn_grpNormals->firstItemByINModelName(this, DEFin_itmNormals);
        if (itemIn_itmNormals != NULL)
        {
            // Recupere l'espace de Hough grossier
            LSIS_HoughSpace4D* roughHoughSpace = computeRoughHoughSpace( itemIn_itmNormals );

            // Recupere l'ensemble des snakes a partir de la resolution grossiere
            QVector<LSIS_ActiveContour4DContinuous<int>*>* roughSnakes = computeRoughSnakes( roughHoughSpace );
            filterSnakes( roughSnakes, 2 );

            // Libere la memoire de l'espace de Hough grossier
            delete roughHoughSpace;

            // Recupere le nuage de points/normales de chacun des contours extraits precedemment
            QVector<CT_PointCloudIndexVector*> pointClouds;
            QVector<CT_NormalCloudStdVector*> normalClouds;
            QVector<float> minx, miny, minz, maxx, maxy, maxz;
            getPointCloudAndNormalsAroundEachSnake( *roughSnakes,
                                                    minx, miny, minz, maxx, maxy, maxz,
                                                    itemIn_itmNormals->getPointCloudIndexRegistered(),
                                                    itemIn_itmNormals->getNormalCloud(),
                                                    pointClouds,
                                                    normalClouds );

            // On peuty liberer la memoire des contours grossiers ainsi que les tableaux qui les contenaient
            foreach( LSIS_ActiveContour4DContinuous<int>* currSnake, *roughSnakes )
            {
                delete currSnake;
            }
            delete roughSnakes;

            // Pour chaque contours precedemment extrait on recree un espace de Hough de resolution fine
            int nRoughSnakes = pointClouds.size();
            QVector<LSIS_ActiveContour4DContinuous<int>*>* allFineSnakes = new QVector<LSIS_ActiveContour4DContinuous<int>*>();
            for( int i = 0 ; i < nRoughSnakes ; i++ )
            {
                // Recupere l'espace de hough a la resolution fine
                LSIS_HoughSpace4D* fineHoughSpace = computeFineHoughSpace( minx.at(i), miny.at(i), minz.at(i),
                                                                           maxx.at(i), maxy.at(i), maxz.at(i),
                                                                           pointClouds.at(i),
                                                                           normalClouds.at(i) );

                // On recupere les contours actifs sur les nouveaux espaces de Hough
                // TEMPORAIRE
//                _nIterMax *= 2;
                // TEMPORAIRE FIN
                QVector<LSIS_ActiveContour4DContinuous<int>*>* fineSnakes = computeRoughSnakes( fineHoughSpace );

                // On les fait se relacher sans croissance pour voir si ca arrange un peu la situation
                float alpha = 1;
                float beta = 1;
                float gama = 1;
                float timeStep = _timeStep/2.0;
                float energyImageGlobalWeight = 0.35;
                float gradMoveMin = 0;
                float gradLengthMin = 0;
                foreach( LSIS_ActiveContour4DContinuous<int>* currSnake, *fineSnakes )
                {
                    currSnake->relax( _nIterMax/4,
                                      alpha, beta, gama,
                                      energyImageGlobalWeight, timeStep,
                                      gradMoveMin, gradLengthMin );
                }

                // Que l'on ajoute a la liste totale
                foreach( LSIS_ActiveContour4DContinuous<int>* currSnake, *fineSnakes )
                {
                    allFineSnakes->push_back( currSnake );
                }

                // On peut liberer le tableau de contours
                delete fineSnakes;
            }

            foreach( LSIS_ActiveContour4DContinuous<int>* currSnake, *allFineSnakes )
            {
                // On ajoute les contours de haute resolution au resultat de l'etape
                // On transforme chaque snake en tuboide qu'on attache au resultat
                CT_StandardItemGroup* grp_grpSnakes= new CT_StandardItemGroup(DEFout_grpSnakes, _snakesResult);
                _snakesResult->addGroup(grp_grpSnakes);

                qDebug() << "Contours final " << *currSnake;
                QVector<CT_Circle*> tuboid = currSnake->toCircles( 5,
                                                                   LSIS_ActiveContour4DContinuous<int>::GLOBAL_SCORE,
                                                                   DEFout_itmCircle,
                                                                   _snakesResult );
                foreach( CT_Circle* currCircle, tuboid )
                {
                    CT_StandardItemGroup* grp_grpCircles= new CT_StandardItemGroup(DEFout_grpCircles, _snakesResult);
                    grp_grpSnakes->addGroup(grp_grpCircles);
                    grp_grpCircles->addItemDrawable(currCircle);
                }

                // Libere la memoire du snake de haute resolution
                delete currSnake;
            }

            // Libere la memoire du tableau de snakes a haute resolution
//            delete allFineSnakes;
        }
    }
}

LSIS_HoughSpace4D *LSIS_StepMultiResHoughSpace::computeRoughHoughSpace(const CT_PointsAttributesNormal *normals)
{
    _minx = normals->minX();
    _miny = normals->minY();
    _minz = normals->minZ();
    _maxx = normals->maxX();
    _maxy = normals->maxY();
    _maxz = normals->maxZ();

    qDebug() << _minx << _miny << _minz << _maxx << _maxy << _maxz;
    qDebug() << _resr << _resx << _resy << _resz;

    // On cree un espace de Hough a partir de la bounding box du nuage de points
    LSIS_HoughSpace4D* hs = new LSIS_HoughSpace4D( NULL, NULL,
                                                   _minr,
                                                   _minx,
                                                   _miny,
                                                   _minz,
                                                   _maxr,
                                                   _maxx,
                                                   _maxy,
                                                   _maxz,
                                                   _resr,
                                                   _resx,
                                                   _resy,
                                                   _resz,
                                                   true,
                                                   normals,
                                                   this );

    qDebug() << "Rough Hough Space " << hs;

    return hs;
}

QVector<LSIS_ActiveContour4DContinuous<int>*>* LSIS_StepMultiResHoughSpace::computeRoughSnakes(LSIS_HoughSpace4D *houghSpace)
{
    qDebug() << "Espace de Hough : ";
    qDebug() << *houghSpace;

    // On recupere les maximas de l'espace de Hough
    QVector< LSIS_Pixel4D >* maximas = houghSpace->getLocalMaximasWithinHeightRange( 0.5, 1 );

    // On les trie par ordre decroissant
    maximas = sortPixelsByValueDecrescentOrder( maximas, houghSpace );

    // Pour chaque maxima on initialise un contour que l'on stocke dans un tableau
    QVector< LSIS_ActiveContour4DContinuous<int>* >* contours = new QVector< LSIS_ActiveContour4DContinuous<int>* >();

    // Cree une image de repulsion
    CT_Grid4D<bool> repulseImage( NULL, NULL,
                                  houghSpace->minW(), houghSpace->minX(), houghSpace->minY(), houghSpace->minZ(),
                                  houghSpace->maxW(), houghSpace->maxX(), houghSpace->maxY(), houghSpace->maxZ(),
                                  houghSpace->wres(), houghSpace->xres(), houghSpace->yres(), houghSpace->zres(),
                                  false, false,
                                  true);

    // TEMPORAIRE : on ne prend que le premier maxima
//    LSIS_Pixel4D currMax = maximas->first();
    int cpt = 0;
    foreach( LSIS_Pixel4D currMax, *maximas )
    // TEMPORAIRE FIN
    {
        qDebug() << "Debut " << cpt++;
        if( cpt == 23 )
        {
            qDebug()<<"Skip";
        } else

        if( currMax.valueIn( repulseImage ) == false )
        {
            qDebug() << "Non repousse";
            LSIS_ActiveContour4DContinuous<int>* curSnake = new LSIS_ActiveContour4DContinuous<int>( houghSpace,
                                                                                                     currMax,
                                                                                                     5 );
            contours->push_back( curSnake );

            // On lui donne l'etape courante pour que le snake puisse la mettre en pause
//            contours->first()->setStep( this );

            float alpha = 1;
            float beta = 1;
            float gama = 1;
            float seuilVariance = 150;
            float energyImageGlobalWeight = 0.1;
            float croissanceK = 1;
            float angleDegresMax = 50;
            int tailleRecherche = 3;
            float seuilSigma = 0.12;
            float seuilSigma2 = 0.75;
            curSnake->grow( &repulseImage,
                            _nIterMax,
                            alpha, beta,
                            gama, energyImageGlobalWeight,
                            croissanceK, angleDegresMax, tailleRecherche,
                            _timeStep,
                            seuilVariance,
                            seuilSigma,
                            seuilSigma2,
                            0.0005,
                            _threshGradLength );

            // On marque la repulsion du contour
            curSnake->markRepulsion(&repulseImage);

            qDebug() << "Contours final " << (*(curSnake));
        }
    }

    return contours;
}

void LSIS_StepMultiResHoughSpace::compute( const LSIS_HoughSpace4D *houghSpace )
{
//    qDebug() << "Espace de Hough : ";
//    qDebug() << *houghSpace;

//    // On recupere les maximas de l'espace de Hough
//    QVector< LSIS_Pixel4D >* maximas = houghSpace->getLocalMaximasWithinHeightRange( 1, 1.5 );

//    // On les trie par ordre decroissant
//    maximas = sortPixelsByValueDecrescentOrder( maximas, houghSpace );

//    // Pour chaque maxima on initialise un contour que l'on stocke dans un tableau
//    QVector< LSIS_ActiveContour4DContinuous<int>* >* contours = new QVector< LSIS_ActiveContour4DContinuous<int>* >();

//    // Cree une image de repulsion
//    CT_Grid4D<bool> repulseImage( NULL, NULL,
//                                  houghSpace->minW(), houghSpace->minX(), houghSpace->minY(), houghSpace->minZ(),
//                                  houghSpace->maxW(), houghSpace->maxX(), houghSpace->maxY(), houghSpace->maxZ(),
//                                  houghSpace->wres(), houghSpace->xres(), houghSpace->yres(), houghSpace->zres(),
//                                  false, false,
//                                  true);

//    // TEMPORAIRE : on ne prend que le premier maxima
////    LSIS_Pixel4D currMax = maximas->first();
//    int cpt = 0;
//    foreach( LSIS_Pixel4D currMax, *maximas )
//    // TEMPORAIRE FIN
//    {
//        qDebug() << "Debut " << cpt++;
//        if( currMax.valueIn( repulseImage ) == false )
//        {
//            qDebug() << "Non repousse";
//            contours->push_back( new LSIS_ActiveContour4DContinuous<int>( houghSpace,
//                                                                          currMax,
//                                                                          5 ) );

//            // On lui donne l'etape courante pour que le snake puisse la mettre en pause
////            contours->first()->setStep( this );

//            float alpha = 1;
//            float beta = 1;
//            float gama = 1;
//            float seuilVariance = 150;
//            float energyImageGlobalWeight = 0.1;
//            float croissanceK = 1;
//            float angleDegresMax = 50;
//            int tailleRecherche = 3;
//            contours->first()->grow( &repulseImage,
//                                     _nIterMax,
//                                     alpha, beta,
//                                     gama, energyImageGlobalWeight,
//                                     croissanceK, angleDegresMax, tailleRecherche,
//                                     _timeStep,
//                                     seuilVariance,
//                                     0.0005,
//                                     0.0005 );

//            // On marque la repulsion du contour
//            contours->first()->markRepulsion(&repulseImage);

//            // On transforme chaque snake en tuboide qu'on attache au resultat
//            CT_StandardItemGroup* grp_grpSnakes= new CT_StandardItemGroup(DEFout_grpSnakes, _snakesResult);
//            _snakesResult->addGroup(grp_grpSnakes);

//            qDebug() << "Contours final " << (*(contours->first()));
//            QVector<CT_Circle*> tuboid = contours->last()->toCircles( 5,
//                                                                      LSIS_ActiveContour4DContinuous<int>::GLOBAL_SCORE,
//                                                                      DEFout_itmCircle,
//                                                                      _snakesResult );
//            foreach( CT_Circle* currCircle, tuboid )
//            {
//                CT_StandardItemGroup* grp_grpCircles= new CT_StandardItemGroup(DEFout_grpCircles, _snakesResult);
//                grp_grpSnakes->addGroup(grp_grpCircles);
//                grp_grpCircles->addItemDrawable(currCircle);
//            }
//        }

//        else
//        {
//            qDebug() << "Repousse";
//        }
    //    }
}

void LSIS_StepMultiResHoughSpace::filterSnakes( QVector< LSIS_ActiveContour4DContinuous<int>* >* snakes, float minLength)
{
    QVector< LSIS_ActiveContour4DContinuous<int>* >::iterator itSnakes = snakes->begin();
    while( itSnakes != snakes->end() )
    {
        if( (*itSnakes)->length3D() < minLength )
        {
            itSnakes = snakes->erase( itSnakes );
        }

        itSnakes++;
    }
}

void LSIS_StepMultiResHoughSpace::getPointCloudAndNormalsAroundEachSnake(QVector<LSIS_ActiveContour4DContinuous<int> *> &snakes,
                                                                         QVector<float>& minx,
                                                                         QVector<float>& miny,
                                                                         QVector<float>& minz,
                                                                         QVector<float>& maxx,
                                                                         QVector<float>& maxy,
                                                                         QVector<float>& maxz,
                                                                         CT_PCIR sceneIndexCloud,
                                                                         CT_AbstractNormalCloud* sceneNormalCloud,
                                                                         QVector< CT_PointCloudIndexVector* > &outIndexClouds,
                                                                         QVector< CT_NormalCloudStdVector* > &outNormalClouds )
{
    // On cree autant de point cloud index que de contours actifs
    outIndexClouds.clear();
    size_t nSnakes= snakes.size();
    for ( size_t i = 0 ; i < nSnakes ; i++ )
    {
        outIndexClouds.push_back( new CT_PointCloudIndexVector() );
        outNormalClouds.push_back( new CT_NormalCloudStdVector() );
    }

    // On calcule la bounding box de chaque contour actif
    float currMinw,currMinx, currMiny, currMinz, currMaxw, currMaxx, currMaxy, currMaxz;
    foreach ( LSIS_ActiveContour4DContinuous<int>* currSnake, snakes )
    {
        currSnake->getBoundingBox(currMinw, currMinx, currMiny, currMinz,
                                  currMaxw, currMaxx, currMaxy, currMaxz );

        // On transforme la bbox 4D en bbox 3D que l'on aggradit un peu
        minx.push_back( currMinx - (currMaxw + _resr ) );
        miny.push_back( currMiny - (currMaxw + _resr ) );
        minz.push_back( currMinz - (currMaxw + _resr ) );
        maxx.push_back( currMaxx + (currMaxw + _resr ) );
        maxy.push_back( currMaxy + (currMaxw + _resr ) );
        maxz.push_back( currMaxz + (currMaxw + _resr ) );
    }

    CT_PointAccessor pAccess;

    // On parcours tous les points pour les mettre dans les bbox appropriees
    size_t nbPoints = sceneIndexCloud->size();
    for ( size_t i = 0 ; i < nbPoints ; i++ )
    {
        // On regarde si ce point appartient a une (ou plusieurs) bounding box de snake
        for( size_t j = 0 ; j < nSnakes ; j++ )
        {
            const CT_Point &currPoint = pAccess.constPointAt(sceneIndexCloud->indexAt( i ));
            if( currPoint.x() >= minx.at(j) &&
                currPoint.y() >= miny.at(j) &&
                currPoint.z() >= minz.at(j) &&
                currPoint.x() <= maxx.at(j) &&
                currPoint.y() <= maxy.at(j) &&
                currPoint.z() <= maxz.at(j) )
            {
                // Si le point appartient a la bbox on l'ajoute au nuage d'indice qui correspond au snake
                outIndexClouds[j]->addIndex( sceneIndexCloud->indexAt(i) );

                // Ainsi que la normale qui va avec
                outNormalClouds[j]->addNormal( sceneNormalCloud->normalAt(i) );
            }
        }
    }
}

LSIS_HoughSpace4D *LSIS_StepMultiResHoughSpace::computeFineHoughSpace(float minx, float miny, float minz,
                                                                       float maxx, float maxy, float maxz,
                                                                       CT_PointCloudIndexVector *pointCloud, CT_NormalCloudStdVector *normalCloud)
{
    // On cree un espace de Hough a partir de la bounding box du nuage de points
    LSIS_HoughSpace4D* hs = new LSIS_HoughSpace4D( NULL, NULL,
                                                   _minr,
                                                   minx,
                                                   miny,
                                                   minz,
                                                   _maxr,
                                                   maxx,
                                                   maxy,
                                                   maxz,
                                                   _fineResr,
                                                   _fineResx,
                                                   _fineResy,
                                                   _fineResz,
                                                   true,
                                                   pointCloud,
                                                   normalCloud,
                                                   this );

    qDebug() << "Fine Hough Space " << *hs;

    return hs;
}
