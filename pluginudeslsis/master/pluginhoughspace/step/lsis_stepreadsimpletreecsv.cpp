#include "lsis_stepreadsimpletreecsv.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_scene.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/ct_cylinder.h"

//Qt dependencies
#include <QString>

// Alias for indexing models
#define DEFout_rsltTree "rsltTree"
#define DEFout_grpBranche "grpBranche"
#define DEFout_grpCylinder "grpCylinder"
#define DEFout_itmCylinder "itmSlice"

// Constructor : initialization of parameters
LSIS_StepReadSimpleTreeCsv::LSIS_StepReadSimpleTreeCsv(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _alphaMax = 20;
    _rMin = 0.03;
    _onlySphereFollowing = true;
    _onlyRANSAC = false;
}

// Step description (tooltip of contextual menu)
QString LSIS_StepReadSimpleTreeCsv::getStepDescription() const
{
    return tr("Charge un arbre simpletree");
}

// Step detailled description
QString LSIS_StepReadSimpleTreeCsv::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString LSIS_StepReadSimpleTreeCsv::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepReadSimpleTreeCsv::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepReadSimpleTreeCsv(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepReadSimpleTreeCsv::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void LSIS_StepReadSimpleTreeCsv::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_RsltTree = createNewOutResultModel(DEFout_rsltTree, tr("ResultTree"));
    res_RsltTree->setRootGroup(DEFout_grpBranche, new CT_StandardItemGroup(), tr("GroupBranche"));
    res_RsltTree->addGroupModel(DEFout_grpBranche,DEFout_grpCylinder, new CT_StandardItemGroup(), tr("GroupCylinder"));
    res_RsltTree->addItemModel(DEFout_grpCylinder, DEFout_itmCylinder, new CT_Cylinder(), tr("Cylindre"));
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepReadSimpleTreeCsv::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice("Snake files", CT_FileChoiceButton::OneOrMoreExistingFiles, "*", _fileNames);
    configDialog->addDouble("Alpha max","[deg]",0,90,4,_alphaMax);
    configDialog->addDouble("Rayon minimum","[m]",0,90,4,_rMin);
    configDialog->addBool("Only Sphere following","","",_onlySphereFollowing );
    configDialog->addBool("Only RANSAC","","",_onlyRANSAC );
}

void LSIS_StepReadSimpleTreeCsv::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltTree = outResultList.at(0);

    CT_StandardItemGroup* grp_GrpBranche= new CT_StandardItemGroup(DEFout_grpBranche, res_RsltTree);
    res_RsltTree->addGroup(grp_GrpBranche);

    foreach ( QString curFileName, _fileNames )
    {
        QFile file( curFileName );
        if( !file.open( QIODevice::ReadOnly ) )
        {
            PS_LOG->addErrorMessage( PS_LOG->error, "Can not access file : \""+file.fileName()+"\"" );
            return;
        }

        // Outils de lecture
        QTextStream fileStream(&file);
        QStringList wordsOfLine;
        QString curLine;

        // Skip header
        fileStream.readLine();

        float newbranchid, sx, sy, sz, ex, ey, ez, r, h;
        float oldbranchid = -1;

        CT_Point curCenter;
        CT_Point curDirection;
//        int nCircles = 0;



        while( !fileStream.atEnd() )
        {
            curLine = fileStream.readLine();
            wordsOfLine = curLine.split(';', QString::SkipEmptyParts );

            newbranchid = wordsOfLine.at(0).toDouble();
            sx = wordsOfLine.at(8).toDouble();
            sy = wordsOfLine.at(9).toDouble();
            sz = wordsOfLine.at(10).toDouble();
            ex = wordsOfLine.at(11).toDouble();
            ey = wordsOfLine.at(12).toDouble();
            ez = wordsOfLine.at(13).toDouble();
            r = wordsOfLine.at(14).toDouble();
            h = wordsOfLine.at(15).toDouble();

            if( r >= _rMin )
            {
                if( (!_onlySphereFollowing)
                    ||
                    (_onlySphereFollowing && wordsOfLine.at(6) == "spherefollowing") )
                {
                    if( (!_onlyRANSAC)
                        ||
                        (_onlyRANSAC && wordsOfLine.at(7) == "RANSAC") )
                    {
                        curCenter.setValues( (ex + sx)/2.0, (ey + sy)/2.0, (ez + sz)/2.0 );
                        curDirection.setValues( (ex - sx), (ey - sy), (ez - sz) );


                        float alpha = acos( (ez - sz) / sqrt( ((ex - sx)*(ex - sx)) + ((ey - sy)*(ey - sy)) + ((ez - sz)*(ez - sz)) ) );// angle avec la verticale
                        alpha *= 180.0/M_PI;

                        if( alpha <= _alphaMax )
                        {
                            CT_Cylinder* curCylinder = new CT_Cylinder( DEFout_itmCylinder,
                                                                        res_RsltTree,
                                                                        new CT_CylinderData( curCenter, curDirection, r, h, newbranchid, 0 ) );

                            if( newbranchid != oldbranchid )
                            {
                                // Nouvelle branche
                                grp_GrpBranche= new CT_StandardItemGroup(DEFout_grpBranche, res_RsltTree);
                            }
                            oldbranchid = newbranchid;

                            CT_StandardItemGroup* grp_GrpCylinder= new CT_StandardItemGroup(DEFout_grpCylinder, res_RsltTree);
                            grp_GrpCylinder->addItemDrawable(curCylinder);
                            grp_GrpBranche->addGroup(grp_GrpCylinder);
                            res_RsltTree->addGroup(grp_GrpBranche);
                        }
                    }
                }
            }
        }

        file.close();
    }
}
