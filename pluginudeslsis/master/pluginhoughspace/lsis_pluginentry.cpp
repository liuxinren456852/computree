#include "lsis_pluginentry.h"
#include "lsis_pluginmanager.h"

LSIS_PluginEntry::LSIS_PluginEntry()
{
    _pluginManager = new LSIS_PluginManager();
}

LSIS_PluginEntry::~LSIS_PluginEntry()
{
    delete _pluginManager;
}

QString LSIS_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* LSIS_PluginEntry::getPlugin() const
{
    return _pluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_houghspace, LSIS_PluginEntry)
#endif
