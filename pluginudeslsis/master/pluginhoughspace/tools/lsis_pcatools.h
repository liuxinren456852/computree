/************************************************************************************
* Filename :  lsis_pcatools.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_PCATOOLS_H
#define LSIS_PCATOOLS_H

#include <QVector>
#include "../pixel/lsis_pixel4d.h"

namespace LSIS_PcaTools
{
    /*!
     * \brief getCentroid
     *
     * Calcule le barycentre du nuage de pixels passe en parametre
     *
     * \param pixels : pixels sur lesquels effectuer l'acp
     * \return le barycentre des pixels
     */
    LSIS_Point4DFloat getCentroid(QVector< LSIS_Pixel4D >& pixels);

    LSIS_Point4DFloat getCentroid(QVector< LSIS_Point4DFloat >& pixels);

    LSIS_Point4DFloat getCentroid3D(QVector< LSIS_Pixel4D >& pixels);

    LSIS_Point4DFloat getCentroid3D(QVector< LSIS_Point4DFloat >& pixels);

    /*!
     * \brief acp4D
     *
     * Fait una analyse en composantes principales du nuage de pixels 4D passe en parametre
     * Les valeurs propres et les vecteurs propres correspondant sont tries par ordre decroissant : lambda1 > lambda2 > lambda3 > lambda4
     *
     * \param pixels : pixels sur lesquels effectuer l'acp
     * \param outV1 : premier vecteur propre de l'acp
     * \param outV2 : second vecteur propre de l'acp
     * \param outV3 : troisieme vecteur propre de l'acp
     * \param outV4 : quatrieme vecteur propre de l'acp
     * \param outLambda1 : premiere valeur propre de l'acp
     * \param outLambda2 : seconde valeur propre de l'acp
     * \param outLambda3 : troisieme valeur propre de l'acp
     * \param outLambda4 : quatrieme valeur propre de l'acp
     */
    void acp4D( QVector< LSIS_Pixel4D >& pixels,
                LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 );

    void acp4DNonCentree( QVector< LSIS_Pixel4D >& pixels,
                          LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                          float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 );

    void acp4D( QVector< LSIS_Point4DFloat >& pixels,
                LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 );

    void acp4D( QVector< LSIS_Point4DFloat >& pixels,
                LSIS_Point4DFloat& centroid,
                LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 );

    void acp4DNonCentree(QVector< LSIS_Point4DFloat >& pixels,
                          LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                          float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 );

    void acp4DPonderee(QVector< LSIS_Point4DFloat >& points,
                        QVector< float >& poids, float sommeDesPoids,
                        LSIS_Point4DFloat& centroidPondere,
                        LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3, LSIS_Point4DFloat& outV4,
                        float& outLambda1, float& outLambda2, float& outLambda3, float& outLambda4 );

    void acp3D( QVector< LSIS_Pixel4D >& pixels,
                LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3,
                float& outLambda1, float& outLambda2, float& outLambda3 );

    void acp3D( QVector< LSIS_Point4DFloat >& pixels,
                LSIS_Point4DFloat& outV1, LSIS_Point4DFloat& outV2, LSIS_Point4DFloat& outV3,
                float& outLambda1, float& outLambda2, float& outLambda3 );

    Eigen::Matrix4f computeWeightedCovarianceMatrix( QVector< LSIS_Point4DFloat >& points,
                                           QVector<float>& poids,
                                           float poidsTotal );
}

// Include template implementations
#include "lsis_pcatools.hpp"

#endif // LSIS_PCATOOLS_H
