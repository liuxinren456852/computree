#include "icro_stepcomputelabeledgridfromstempositions.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_itemdrawable/ct_scene.h"

#include <QQueue>

// Alias for indexing models
#define DEF_SearchInResult "r"
#define DEF_SearchInScene   "sc"
#define DEF_SearchInGroup   "gr"

#define DEFout_rsltResultGrid "rsltResultGrid"
#define DEFout_grpGrid3d "grpGrid3d"
#define DEFout_itmGrid3d "itmGrid3d"


// Constructor : initialization of parameters
ICRO_StepComputeLabeledGridFromStemPositions::ICRO_StepComputeLabeledGridFromStemPositions(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _resolution = 0.2;
}

// Step description (tooltip of contextual menu)
QString ICRO_StepComputeLabeledGridFromStemPositions::getStepDescription() const
{
    return tr("Etiquette les voxels (int) faisant partie de chaque cercle en entree");
}

// Step detailled description
QString ICRO_StepComputeLabeledGridFromStemPositions::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepComputeLabeledGridFromStemPositions::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepComputeLabeledGridFromStemPositions::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepComputeLabeledGridFromStemPositions(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepComputeLabeledGridFromStemPositions::createInResultModelListProtected()
{
    CT_InResultModelGroup *resultModel = createNewInResultModel(DEF_SearchInResult, tr("Scène(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_Scene::staticGetType(), tr("Scène"));
}

// Creation and affiliation of OUT models
void ICRO_StepComputeLabeledGridFromStemPositions::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltConnectedComponents = createNewOutResultModel(DEFout_rsltResultGrid, tr("Connected components (grid3d)"));
    res_rsltConnectedComponents->setRootGroup(DEFout_grpGrid3d, new CT_StandardItemGroup(), tr("Group of grid3d"));
    res_rsltConnectedComponents->addItemModel(DEFout_grpGrid3d, DEFout_itmGrid3d, new CT_Grid3D<int>(), tr("Grid3d with labeled components"));
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepComputeLabeledGridFromStemPositions::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Resolution", "", 0.0001, 9999, 4, _resolution );
}

void ICRO_StepComputeLabeledGridFromStemPositions::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltRaster3D = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWithScene(resIn_rsltWith3Rasterd, this, DEF_SearchInGroup);
    while (itIn_grpWithScene.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWithScene = (CT_AbstractItemGroup*) itIn_grpWithScene.next();
        const CT_Scene* scene = (const CT_Scene*)grpIn_grpWithScene->firstItemByINModelName(this, DEF_SearchInScene);

        if (scene != NULL)
        {
            double minX = scene->minX();
            double minY = scene->minY();
            double minZ = scene->minZ();
            double maxX = scene->maxX();
            double maxY = scene->maxY();
            double maxZ = scene->minZ();

            CT_Grid3D<int>* itemOut_itmRaster3D = new CT_Grid3D<int>( DEFout_itmGrid3d, resOut_rsltRaster3D,
                                                                      minX, minY, minZ,
                                                                      maxX, maxY, maxZ,
                                                                      _resolution,
                                                                      std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            getCirclesFromFile( "/home/joris/donnees/2-campagne-squatec/erable-sapin/B_er4", "manual-tree-map.csv" );
            labelGrid( itemOut_itmRaster3D );

            // Ajout de la grille au resultat
            CT_StandardItemGroup* grpGrid3D = new CT_StandardItemGroup(DEFout_grpGrid3d, resOut_rsltRaster3D );
            grpGrid3D->addItemDrawable( itemOut_itmRaster3D );
            resOut_rsltRaster3D->addGroup( grpGrid3D );
        }
    }
}

void ICRO_StepComputeLabeledGridFromStemPositions::getCirclesFromFile(QString dir, QString fileName)
{
    QFile file( dir + "/" + fileName );

    if( file.open( QIODevice::ReadOnly ) )
    {
        QTextStream stream( &file );

        float x, y, z, r;
        Eigen::Vector3d center;
        Eigen::Vector3d direction;
        direction << 0, 0, 1;

        while( !stream.atEnd() )
        {
            stream >> r >> x >> y >> z;

            center.x() = x;
            center.y() = y;
            center.z() = z;

            _circles.push_back( new CT_CircleData( center, direction, r ) );
        }

        file.close();
    }

    else
    {
        qDebug() << "Error while opening file " << dir + "/" + fileName;
    }
}

void ICRO_StepComputeLabeledGridFromStemPositions::labelGridFromCircle(CT_Grid3D<int> *grid, CT_CircleData *circle, int label)
{
    // BBox du cercle dans l'espace
    Eigen::Vector3d bot;
    Eigen::Vector3d top;

    for( int i = 0 ; i < 3 ; i++ )
    {
        bot(i) = circle->getCenter()(i) - circle->getRadius();
        top(i) = circle->getCenter()(i) + circle->getRadius();
    }

    // BBox du cercle dans la grille
    size_t botx, boty, botz;
    size_t topx, topy, topz;
    grid->colX( bot.x(), botx );
    grid->linY( bot.y(), boty );
    grid->levelZ( bot.z(), botz );
    grid->colX( top.x(), topx );
    grid->linY( top.y(), topy );
    grid->levelZ( top.z(), topz );

    for( int i = botx ; i <= topx ; i++ )
    {
        for( int j = boty ; j <= topy ; j++ )
        {
            for( int k = botz ; k <= topz ; k++ )
            {
                // Test d'intersection entre le cercle et le voxel

                // Marquage du voxel
                grid->setValue(i,j,k,label);
            }
        }
    }
}

void ICRO_StepComputeLabeledGridFromStemPositions::labelGrid(CT_Grid3D<int> *grid)
{
    int curLabel = 1;

    foreach( CT_CircleData* circleData, _circles )
    {
        labelGridFromCircle( grid, circleData, curLabel );
        curLabel++;
    }

    grid->computeMinMax();
}
