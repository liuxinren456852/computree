/************************************************************************************
* Filename :  norm_newoctree.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_newoctree.h"
#include "norm_newoctreenode.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "norm_newoctreedrawmanager.h"
#include "octree/norm_octreettools.h"
#include "ct_colorcloud/ct_colorcloudstdvector.h"
#include "ct_itemdrawable/ct_scene.h"
#include <QDebug>

const NORM_NewOctreeDrawManager NORM_NewOctree::NEW_OCTREE_DRAW_MANAGER = NORM_NewOctreeDrawManager();

NORM_NewOctree::NORM_NewOctree() : CT_AbstractItemDrawableWithoutPointCloud( NULL, NULL )
{
    _root = NULL;
    this->setBaseDrawManager(&NEW_OCTREE_DRAW_MANAGER);
}

NORM_NewOctree::~NORM_NewOctree()
{
    if( _root != NULL )
    {
        delete _root;
    }
}

NORM_NewOctree::NORM_NewOctree(const CT_Point &bot,
                               const CT_Point &top,
                               float dimensionMin,
                               float dimensionMinAcp,
                               int nbPointsMin,
                               float sigmaThreshPercent,
                               float rmseThresh,
                               CT_PCIR inputIndexCloud,
                               const QString modelName,
                               const CT_AbstractResult *result,
                               QVector<CT_LineData *> &curPolyline )
    : CT_AbstractItemDrawableWithoutPointCloud( modelName, result ),
      _dimensionMin( dimensionMin ),
      _dimensionMinAcp( dimensionMinAcp ),
      _nbPointsMin(nbPointsMin ),
      _sigmaThreshPerCent( sigmaThreshPercent ),
      _rmseThresh( rmseThresh ),
      _indexCloud( inputIndexCloud )
{
    this->setBaseDrawManager(&NEW_OCTREE_DRAW_MANAGER);

    qDebug() << "Octree sur " << inputIndexCloud->size() << " points";
    // On va rechercher le cube englobant au lieu de la boite englobante pour calculer la profondeur maximum
    NORM_OctreeTools::getBoundingCubeFromBoundingBox( bot,
                                                      top,
                                                      _bot,
                                                      _top );
    for( int i = 0 ; i < 3 ; i++ )
    {
        _bot(i) -= 0.1;
        _top(i) += 0.1;
    }

    // Construit tout l'octree jusqu'au feuilles par le seul critere de taille de cellule
    CT_Point center = ( _bot + _top );
    center *= 0.5;
    _root = new NORM_NewOctreeNode( this,
                                    NULL,
                                    center,
                                    ( _top.x() - _bot.x() ) /2.0 );

    // Insere tous les points dans l'octree
    size_t nbPts = _indexCloud->size();
    CT_PointAccessor pAccessor;
    CT_Point currentPoint;
    int currentIndex;
    for( size_t i = 0 ; i < nbPts ; i++ )
    {
        currentIndex = _indexCloud->indexAt(i);
        currentPoint = pAccessor.pointAt( currentIndex );

        _root->insert( currentPoint, i );
    }

    // Mise a jour des barycentres
    _root->updateCentroid();
    qDebug() << "Centroides updates";

    // On supprime les fils a partir des noeuds qui ne verifient plus le critere de nombre de points
    _root->filterFromNbPointsV2( 4 );
    qDebug() << "Filtre nb points ok";

    // On filtre sur critere d'ACP
    _root->filterFromRmseAndUpdateQuadra( _dimensionMinAcp );
    qDebug() << "Filtre pca quadra ok";

    // Fait le calcul des voisins
    _root->computeNeighboursRecursive( true );
    qDebug() << "Calcul des voisins ok";

    // On oriente les patches quadratiques
    orientPcaBasis( curPolyline );
    qDebug() << "Orientation des quadras ok";
}

void NORM_NewOctree::draw(GraphicsViewInterface &view,
                          PainterInterface &painter,
                          bool drawNodeBoxes,
                          QVector<bool> &drawBoxes,
                          bool drawAcpBasis ) const
{
    _root->drawRecursive( view, painter,
                          0,
                          drawNodeBoxes,
                          drawBoxes,
                          drawAcpBasis );
}

float NORM_NewOctree::dimensionMin() const
{
    return _dimensionMin;
}

void NORM_NewOctree::setDimensionMin(float dimensionMin)
{
    _dimensionMin = dimensionMin;
}

CT_AbstractItemDrawable *NORM_NewOctree::copy(const CT_OutAbstractItemModel *model,
                                              const CT_AbstractResult *result,
                                              CT_ResultCopyModeList copyModeList)
{
    Q_UNUSED(model);
    Q_UNUSED(result);
    Q_UNUSED(copyModeList);

    qDebug() << "----------";
    qDebug() << "ATTENTION, la methode :";
    qDebug() << "\"CT_AbstractItemDrawable *NORM_NewOctree::copy(const CT_OutAbstractItemModel *model, \nconst CT_AbstractResult *result, \nCT_ResultCopyModeList copyModeList)\"";
    qDebug() << "n'est pas implementee";
    qDebug() << "----------";

    return NULL;
}

AttributEntier* NORM_NewOctree::getIdAttribute(QString modelName,
                                               CT_AbstractResult *result )
{
    // On parcours l'arbre jusqu'a trouver les feuilles
    // Tout en gardant leur id de fils (entre 0 et 7)
    // Et pour chaque point de chaque feuille on affecte cet identifiant
    AttributEntier* rslt = new AttributEntier( modelName, result, _indexCloud );

    _root->setIdAttributeRecursive( rslt, 0 );

    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

AttributEntier* NORM_NewOctree::getNbPointsAttribute(QString modelName,
                                                     CT_AbstractResult *result )
{
    // On parcours l'arbre jusqu'a trouver les feuilles
    // Tout en gardant leur id de fils (entre 0 et 7)
    // Et pour chaque point de chaque feuille on affecte cet identifiant
    AttributEntier* rslt = new AttributEntier( modelName, result, _indexCloud );

    _root->setNbPointsAttributeRecursive( rslt );

    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

AttributFloat *NORM_NewOctree::getSigmaAttribute(QString modelName, CT_AbstractResult *result)
{
    AttributFloat* rslt = new AttributFloat( modelName, result, _indexCloud );

    _root->setSigmaAttributeRecursive( rslt );

    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

AttributFloat *NORM_NewOctree::getSigmaPercentAttribute(QString modelName, CT_AbstractResult *result)
{
    AttributFloat* rslt = new AttributFloat( modelName, result, _indexCloud );

    _root->setSigmaPercentAttributeRecursive( rslt );

    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

AttributFloat *NORM_NewOctree::getLineaityAttribute(QString modelName, CT_AbstractResult *result)
{
    AttributFloat* rslt = new AttributFloat( modelName, result, _indexCloud );

    _root->setLinearityAttributeRecursive( rslt );

    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

AttributFloat *NORM_NewOctree::getFittingErrorAttribute(QString modelName, CT_AbstractResult *result)
{
    AttributFloat* rslt = new AttributFloat( modelName, result, _indexCloud );

    _root->setFittingErrorAttributeRecursive( rslt );

    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

AttributFloat *NORM_NewOctree::getFittingRmseAttribute(QString modelName, CT_AbstractResult *result)
{
    AttributFloat* rslt = new AttributFloat( modelName, result, _indexCloud );

    _root->setFittingRmseAttributeRecursive( rslt );

    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

AttributeNormals *NORM_NewOctree::getNormalsRecursive(QString modelName, CT_AbstractResult* result,
                                                      const CT_Point& scanPos,
                                                      bool towards )
{
    AttributeNormals* rslt = new AttributeNormals( modelName, result, _indexCloud );

    _root->computeNormalsRecursive( rslt, scanPos, towards );

    return rslt;
}

AttributeNormals *NORM_NewOctree::getNormalsRecursiveLinearInterpolation(QString modelName, CT_AbstractResult *result, const CT_Point &scanPos, bool towards, float radiusMax)
{
    AttributeNormals* rslt = new AttributeNormals( modelName, result, _indexCloud );

    _root->computeNormalsRecursiveLinearInterpolation( rslt, scanPos, towards, radiusMax );

    return rslt;
}

AttributeNormals *NORM_NewOctree::getNormalsRecursiveCRBFInterpolation(QString modelName, CT_AbstractResult *result, const CT_Point &scanPos, bool towards, float radiusMax)
{
    AttributeNormals* rslt = new AttributeNormals( modelName, result, _indexCloud );

    _root->computeNormalsRecursiveCRBFInterpolation( rslt, scanPos, towards, radiusMax );

    return rslt;
}

AttributCouleur* NORM_NewOctree::getColorAttribute(QString modelName, CT_AbstractResult *result )
{
    // On parcours l'arbre jusqu'a trouver les feuilles
    // Tout en gardant leur id de fils (entre 0 et 7)
    // Et pour chaque point de chaque feuille on affecte cet identifiant
    CT_ColorCloudStdVector* rsltColorCloud = new CT_ColorCloudStdVector( _indexCloud->size() );
    AttributCouleur* rslt = new AttributCouleur( modelName, result, _indexCloud, rsltColorCloud );

    _root->setColorAttributeRecursive( rsltColorCloud, 0 );

    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

NORM_NewOctreeNode *NORM_NewOctree::getNodeContainingPoint(const CT_Point &p) const
{
    return _root->getLeafContainingPointRecursive( p );
}

void NORM_NewOctree::getQuadraticPointCloudAndSign(QVector<CT_Scene *> &outScenes,
                                                   QString modelNameScenes,
                                                   CT_AbstractResult *resultScenes,
                                                   AttributEntier* outCoulors,
                                                   QString modelNameCoulors,
                                                   CT_AbstractResult *resultCoulors)
{
    _root->getQuadraticPointCloudAndSignRecursive( outScenes, modelNameScenes, resultScenes,
                                                   outCoulors, modelNameCoulors, resultCoulors );
}

void NORM_NewOctree::orientPcaBasis( QVector<CT_LineData*>& curPolyline )
{
    _root->setVisitedRecursive( false );

    bool tmp = false;
    _root->orientPcaRecursive( tmp, curPolyline );

    _root->setVisitedRecursive( false );
}

int NORM_NewOctree::nbPointsMin() const
{
    return _nbPointsMin;
}

void NORM_NewOctree::setNbPointsMin(int nbPointsMin)
{
    _nbPointsMin = nbPointsMin;
}
NORM_NewOctreeNode *NORM_NewOctree::root() const
{
    return _root;
}

void NORM_NewOctree::setRoot(NORM_NewOctreeNode *root)
{
    _root = root;
}
float NORM_NewOctree::sigmaThreshPerCent() const
{
    return _sigmaThreshPerCent;
}

void NORM_NewOctree::setSigmaThreshPerCent(float sigmaThreshPerCent)
{
    _sigmaThreshPerCent = sigmaThreshPerCent;
}

float NORM_NewOctree::rmseThresh() const
{
    return _rmseThresh;
}

void NORM_NewOctree::setRmseThresh( float rmseThresh )
{
    _rmseThresh = rmseThresh;
}

float NORM_NewOctree::dimensionMinAcp() const
{
    return _dimensionMinAcp;
}

void NORM_NewOctree::setDimensionMinAcp(float dimensionMinAcp)
{
    _dimensionMinAcp = dimensionMinAcp;
}
CT_PCIR NORM_NewOctree::indexCloud() const
{
    return _indexCloud;
}

void NORM_NewOctree::setIndexCloud(const CT_PCIR &indexCloud)
{
    _indexCloud = indexCloud;
}

QVector<CT_Scene *>* NORM_NewOctree::segmentFromPcaDirections(double maxAngle,
                                                              QString modelName,
                                                              CT_AbstractResult *result)
{
    QVector<CT_Scene *>* rslt = new QVector<CT_Scene *>();

    _root->segmentFromPcaDirectionsRecursive( maxAngle,
                                              modelName,
                                              result,
                                              rslt );

    _root->setVisitedRecursive( false );

    return rslt;
}

QVector<CT_Scene *> *NORM_NewOctree::segmentFromNormalDirections(double maxAngle, QString modelName, CT_AbstractResult *result)
{
    QVector<CT_Scene *>* rslt = new QVector<CT_Scene *>();

    _root->segmentFromNormalDirectionsRecursive(maxAngle,
                                                modelName,
                                                result,
                                                rslt );

    _root->setVisitedRecursive( false );

    return rslt;
}
