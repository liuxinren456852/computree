#ifndef NORM_STANDARDBOXDRAWMANAGER_H
#define NORM_STANDARDBOXDRAWMANAGER_H

#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractshapedrawmanager.h"

class NORM_StandardBoxDrawManager : public CT_StandardAbstractShapeDrawManager
{
public:
//********************************************//
//         Constructors/Destructors           //
//********************************************//
    /*!
     * \brief NORM_StandardBoxDrawManager
     *
     * Constructor of the NORM_StandardBoxDrawManager class
     *
     * \param drawConfigurationName : name of the configuration
     */
    NORM_StandardBoxDrawManager(QString drawConfigurationName = "");

//********************************************//
//                Drawing tools               //
//********************************************//
    /*!
     * \brief draw
     *
     * Draw asphere : draw a box
     *
     * \param view : graphic view
     * \param painter : painter to paint objects in the view
     * \param itemDrawable : octree to be drawn
     */
    virtual void draw(GraphicsViewInterface &view,
                      PainterInterface &painter,
                      const CT_AbstractItemDrawable &itemDrawable) const;
};

#endif // NORM_STANDARDBOXDRAWMANAGER_H
