#include "norm_stepfilterpca.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "tools/norm_boundingbox.h"

// Alias for indexing models
#define DEFin_ResultSigmaValues "ResultSigmaValues"
#define DEFin_GroupSigmaValues "GroupSigmaValues"
#define DEFin_ItemSigmaValues "ItemSigmaValues"

#define DEFout_ResultFilteredScene "ResultFilteredScene"
#define DEFout_GroupFilteredScene "GroupFilteredScene"
#define DEFout_ItemFilteredScene "ItemFilteredScene"


// Constructor : initialization of parameters
NORM_StepFilterPCA::NORM_StepFilterPCA(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _sigmaThresh = 5;
}

// Step description (tooltip of contextual menu)
QString NORM_StepFilterPCA::getStepDescription() const
{
    return tr("Filter PCA");
}

// Step detailled description
QString NORM_StepFilterPCA::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepFilterPCA::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepFilterPCA::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepFilterPCA(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepFilterPCA::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_Result = createNewInResultModel(DEFin_ResultSigmaValues, tr("Result from sigma estimation"));
    resIn_Result->setRootGroup(DEFin_GroupSigmaValues, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resIn_Result->addItemModel(DEFin_GroupSigmaValues, DEFin_ItemSigmaValues, CT_PointsAttributesScalarTemplated<float>::staticGetType(), tr("Sigma values"));
}

// Creation and affiliation of OUT models
void NORM_StepFilterPCA::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultFilteredScene = createNewOutResultModel(DEFout_ResultFilteredScene, tr("Filtered scene"));
    res_ResultFilteredScene->setRootGroup(DEFout_GroupFilteredScene, new CT_StandardItemGroup(), tr("Group"));
    res_ResultFilteredScene->addItemModel(DEFout_GroupFilteredScene, DEFout_ItemFilteredScene, new CT_Scene(), tr("Filtered Scene"));

}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepFilterPCA::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Sigma thresh", "(%)", 0, 33.33, 2, _sigmaThresh, 1);
}

void NORM_StepFilterPCA::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_ResultSigmaValues = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultFilteredScene = outResultList.at(0);

    // IN results browsing
    // IN results browsing
    CT_ResultGroupIterator itIn_Group(resIn_ResultSigmaValues, this, DEFin_GroupSigmaValues);
    while (itIn_Group.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_GroupSigmaValues = (CT_AbstractItemGroup*) itIn_Group.next();
        const CT_PointsAttributesScalarTemplated<float>* itemIn_SigmaValues = (CT_PointsAttributesScalarTemplated<float>*)grpIn_GroupSigmaValues->firstItemByINModelName(this, DEFin_ItemSigmaValues);

        // Pour la bbox de sortie
        CT_PointAccessor pAccessor;
        CT_Point curPoint;
        float xmin = std::numeric_limits<float>::max();
        float xmax = -std::numeric_limits<float>::max();
        float ymin = std::numeric_limits<float>::max();
        float ymax = -std::numeric_limits<float>::max();
        float zmin = std::numeric_limits<float>::max();
        float zmax = -std::numeric_limits<float>::max();

        // Create the output index cloud
        CT_PointCloudIndexVector* item_ItemFilteredScene_PCIV = new CT_PointCloudIndexVector();

        // Iterate over the points in order to keep the ones with a sigma value lower then the threshold
        CT_PCIR inputIndexCloud = itemIn_SigmaValues->getPointCloudIndexRegistered();
        size_t nbPts = inputIndexCloud->size();
        for( size_t i = 0 ; i < nbPts ; i++ )
        {
            if( itemIn_SigmaValues->valueAt(i) * 100 / (1.0/3.0) < _sigmaThresh )
            {
                // Add the index to the output index cloud
                item_ItemFilteredScene_PCIV->addIndex( inputIndexCloud->indexAt(i) );

                // Update bounding box
                pAccessor.pointAt( inputIndexCloud->indexAt(i), curPoint );
                NORM_Tools::NORM_BBox::updateBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax, curPoint );
            }
        }

        // Create the output scene
        CT_Scene* item_ItemFilteredScene = new CT_Scene(DEFout_ItemFilteredScene, res_ResultFilteredScene);
        item_ItemFilteredScene->setPointCloudIndexRegistered(PS_REPOSITORY->registerPointCloudIndex(item_ItemFilteredScene_PCIV));
        item_ItemFilteredScene->setBoundingBox( xmin, ymin, zmin, xmax, ymax, zmax );

        // Add it to the result
        CT_StandardItemGroup* grp_GroupFilteredScene= new CT_StandardItemGroup(DEFout_GroupFilteredScene, res_ResultFilteredScene);
        res_ResultFilteredScene->addGroup(grp_GroupFilteredScene);
        grp_GroupFilteredScene->addItemDrawable(item_ItemFilteredScene);
    }
}
