/************************************************************************************
* Filename :  norm_newoctreenode.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef NORM_NEWOCTREENODE_H
#define NORM_NEWOCTREENODE_H

#include "ct_point.h"
#include <QVector>

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"

#include "ct_itemdrawable/ct_line.h"

class NORM_NewOctree;
class CT_Color;
class CT_Scene;

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;
typedef CT_PointsAttributesScalarTemplated<float> AttributFloat;
typedef CT_PointsAttributesColor AttributColor;
typedef CT_PointsAttributesNormal AttributeNormals;

enum NodeBoxStatus
{
    INSIDE_BOX,
    INTERSECT_BOX,
    OUTSIDE_BOX
};

enum CubeNeibourhoogType
{
    VERTEX_NEIGHBOURHOOD,
    EDGE_NEIGHBOURHOOD,
    LEFT_FACE,
    RIGHT_FACE,
    TOP_FACE,
    BOT_FACE,
    FRONT_FACE,
    BACK_FACE
};

/**
 * Octree implementation inspired by :
 * https://github.com/brandonpelfrey/SimpleOctree/blob/master/Octree.h#L75
 */

/**
 * Children follow a predictable pattern to make accesses simple.
 * Here, - means less than 'origin' in that dimension, + means greater than.
 * child:  0 1 2 3 4 5 6 7
 * x:      - - - - + + + +
 * y:      - - + + - - + +
 * z:      - + - + - + - +
*/
class NORM_NewOctreeNode
{
public:
    /*!
     * \brief NORM_NewOctreeNode
     *
     * COnstructeur
     *
     * \param octree : octree auquel appartient le noeud
     * \param father : noeud pere
     * \param center : centre du noeud de l'octree
     * \param dimension : demi-dimension du noeud
     */
    NORM_NewOctreeNode(NORM_NewOctree* octree,
                       NORM_NewOctreeNode* father,
                       CT_Point& center,
                       double dimension);

    /*!
      * Destructeur
      */
    ~NORM_NewOctreeNode();

    /*!
     * \brief isLeaf
     *
     * Indique si le noeud est une feuille
     *
     * \return True si le noeud est une feuille (pas de fils), false sinon.
     */
    inline bool isLeaf() const
    {
        for( int i = 0 ; i < 8 ; i++ )
        {
            if( _children[i] != NULL )
            {
                return false;
            }
        }

        return true;
    }

    /*!
     * \brief isLeafDimension
     *
     * Indique si le noeud est une feuille a cause du critere de dimension de la cellule
     *
     * \return true si le noeud ne peut plus etre subdivise sans briser le critere de taille minimale de cellule, false sinon
     */
    bool isLeafDimension() const;

    /*!
     * \brief isLeafNbPoint
     *
     * Indique si le noeud est une feuille a cause du critere de nombre de points
     *
     * \return true si le noeud ne peux plus subdiviser sans briser le critere de nombre de points, false sinon
     */
    bool isLeafNbPoint() const;

    /*!
     * \brief isLeafNbPointV2
     *
     * Si on a nFilsErreurMax ou moins de fils qui cassent le critere de nombre de points on accepte quand meme la subdivision
     *
     * \return
     */
    bool isLeafNbPointV2( int nFilsErreurMax ) const;

    /*!
     * \brief getOctantContainingPoint
     *
     * Renvoie le numero du fils contenant le point passe en parametre
     *
     * \param point : point dont on veut trouver la place dans la structure d'octree
     *
     * \return L'indice (entre 0 et 7) du fils contenant le point passe en parametre
     */
    int getOctantContainingPoint(const CT_Point& point) const;

    /*!
     * \brief insert
     *
     * Insere un point dans l'octree
     *
     * \param point : point sous la forme d'un ct_point (pour acces aux coordonnees direct)
     * \param index : indice du point dans le tableau d'indice global
     */
    void insert(const CT_Point &point, int index );

    /*!
     * \brief updateCentroid
     *
     * Met a jour les coordonnees du barycentre du noeud
     * Les coordonnees des points ayant ete additionnees tout au long de la creation de l'octree, il ne reste plus qu'a diviser par le nombre de points de la cellule
     */
    void updateCentroid();

    /*!
     * \brief filterFromNbPoints
     *
     * Supprime les noeuds qui ont moins d'un certain nombre de points (defini dans l'octree) et remonte les feuilles en consequence
     *
     */
    void filterFromNbPoints();

    /*!
     * \brief filterFromNbPointsV2
     * Filtre sur le nombre de points mais authorise d'avoir des fils avec moins de points que la limite pour les rattacher ensuite a d'autres cellules
     */
    void filterFromNbPointsV2(int nbFilsErreurMax);

    void filterFromAcp(float minSize);

    void filterFromAcpAndUpdateQuadra(float minSize);

    void filterFromRmseAndUpdateQuadra(float minSize);

    void updateIndicesFromChildren(QVector<int>* indices);

    void drawRecursive(GraphicsViewInterface &view,
                       PainterInterface &painter,
                       size_t idChild,
                       bool drawNodeBoxes,
                       QVector<bool> &drawBoxes,
                       bool drawAcpBasis) const;

    void setIdAttributeRecursive( AttributEntier* attributs, size_t currentID ) const;

    void setNbPointsAttributeRecursive( AttributEntier* attributs ) const;

    void setSigmaAttributeRecursive( AttributFloat* attributs ) const;

    void setSigmaPercentAttributeRecursive( AttributFloat* attributs ) const;

    void setLinearityAttributeRecursive( AttributFloat* attributs ) const;

    void setFittingErrorAttributeRecursive( AttributFloat* attributs ) const;

    void setFittingRmseAttributeRecursive( AttributFloat* attributs ) const;

    void setColorAttributeRecursive( CT_AbstractColorCloud *attributs, size_t currentID ) const;

    void computeNeighboursRecursive(bool onlyLeaves);

    void computeNodeNeighbours();

    void computeAcpLeavesRecursive();

    void computeNormalsRecursive(AttributeNormals* outNormals,
                                 const CT_Point& scanPos,
                                 bool towards);

    void computeNormalsRecursiveLinearInterpolation(AttributeNormals* outNormals,
                                                    const CT_Point& scanPos,
                                                    bool towards,
                                                    float radiusMax );

    void computeNormalsRecursiveCRBFInterpolation(AttributeNormals* outNormals,
                                                  const CT_Point& scanPos,
                                                  bool towards,
                                                  float radiusMax );

    void computeNormalOfPointInNeighbourNode( NORM_NewOctreeNode* nei );

    void computeNormalsRecursiveCRBFInterpolationGlobal(AttributeNormals* outNormals,
                                                        const CT_Point& scanPos,
                                                        bool towards,
                                                        float radiusMax );

    NORM_NewOctreeNode* getLeafContainingPointRecursive( const CT_Point& p );

    void leavesInBBox (const CT_Point& bot, const CT_Point& top, QVector<NORM_NewOctreeNode*>& outputNodes , NORM_NewOctreeNode *callingNode);

    bool intersect( const CT_Point& bot, const CT_Point& top );

    bool intersectNoContactPoint( const CT_Point& bot, const CT_Point& top );

    NodeBoxStatus isInBox ( const CT_Point& bot, const CT_Point& top );

    const QVector<NORM_NewOctreeNode *> &neighbours() const;
    void setNeighbours(const QVector<NORM_NewOctreeNode *> &neighbours);

    void getBBox( CT_Point& outBot, CT_Point& outTop ) const;

    /*
     *     6 ------- 7
     *    /|        /|
     *   5 ------- 8 |                 z
     *   | |       | |                 |   y FRONT
     *   | 2 ------|-3                 |  /
     *   |/        |/                  | /
     *   1 -------- 4                   ------> x
     *                                  BACK
     * */
    void getCorner( int cornerID, CT_Point& outCorner ) const;

    void getCorners( CT_Point& out1,
                     CT_Point& out2,
                     CT_Point& out3,
                     CT_Point& out4,
                     CT_Point& out5,
                     CT_Point& out6,
                     CT_Point& out7,
                     CT_Point& out8 ) const;


    CT_PointCloudIndexVector* getIndexCloud() const;

    inline float getSigma() const { return ( _l3 / (_l1+_l2+_l3) ); }

    inline float getSigmaPerCent() const { return ( ( _l3 / (_l1+_l2+_l3) ) * 100.0 ) / (1.0/3.0); }

    inline float getLinearity() const { return ( _l2 / _l1 ); }

    void getAllPointsFromLeaves ();

    inline void deletePtIndicesArray () { delete _ptIndices; _ptIndices = NULL; }

    void updateAcp ();

    void updateAcpAndQuadraticSurface();

    float updateAcpAndQuadraticSurfaceAndGetRmse();

    CT_Scene* getSegment( double angleMax,
                          QString modelName,
                          CT_AbstractResult* result);

    CT_Scene* getSegmentFromNormals( double angleMax,
                                     QString modelName,
                                     CT_AbstractResult* result);

    void segmentFromPcaDirectionsRecursive( double angleMax,
                                            QString modelName,
                                            CT_AbstractResult* result,
                                            QVector< CT_Scene* >* segments );

    void segmentFromNormalDirectionsRecursive( double angleMax,
                                               QString modelName,
                                               CT_AbstractResult* result,
                                               QVector< CT_Scene* >* segments );

    QVector<CT_Point>* getPointCloudInAcpBasis ();

    CT_Point getPointInAcpBasis( CT_Point& p );

    CT_Point getPointNormalFromCartesianSpace( CT_Point& p );

    CT_NMPCIR getPointCloudInAcpBasisNMPCIR();

    CT_NMPCIR getQuadraticPointCloud(float resx, float resy);

    double quadraInLocalZ( CT_Point& p );

    double quadraInLocalZ( double x, double y );

    void getQuadraticPointCloudAndSignRecursive(QVector<CT_Scene *> &outScenes,
                                                QString modelNameScenes,
                                                CT_AbstractResult *resultScenes,
                                                AttributEntier* outCoulors,
                                                QString modelNameCoulors,
                                                CT_AbstractResult *resultCoulors);

    void orientConnexComponentAcpBasis();

    void orientConnexComponentAcpBasisRecursive(int &nNodesConnex, QVector<CT_LineData *> &outPolyline);
    void orientConnexComponentAcpBasisRecursiveBreathFirst( int& nNodesConnex,
                                                            QVector<CT_LineData*>& outPolyline);

    void orientPcaRecursive(bool& tmpFirstFound , QVector<CT_LineData *> &curPolyline);

    void orientNeighbourAcpBasisFromTopFace(NORM_NewOctreeNode *nei);
    void orientNeighbourAcpBasisFromBotFace(NORM_NewOctreeNode *nei);
    void orientNeighbourAcpBasisFromLeftFace(NORM_NewOctreeNode *nei);
    void orientNeighbourAcpBasisFromRightFace(NORM_NewOctreeNode *nei);
    void orientNeighbourAcpBasisFromFrontFace(NORM_NewOctreeNode *nei);
    void orientNeighbourAcpBasisFromBackFace(NORM_NewOctreeNode *nei);

    void reverseOrientation();

    int signeQuadra(const CT_Point& p , QString text = "");

    //     if(point.x() >= _center.x()) oct |= 4;
    //     if(point.y() >= _center.y()) oct |= 2;
    //     if(point.z() >= _center.z()) oct |= 1;
    CubeNeibourhoogType getCubeNeibourhoogType( NORM_NewOctreeNode *nei );

    CubeNeibourhoogType getOppositeFaceNeibourhoogType( CubeNeibourhoogType type );

    bool visited() const;
    void setVisited(bool visited);

    void setVisitedRecursive(bool visited);

    CT_Point center() const;
    CT_Point getFaceCenter( CubeNeibourhoogType neiType ) const;
    CT_Point& centerRef();
    void setCenter(const CT_Point &center);

private:
    CT_Point    _center;                            /*!< Centre du noeud */
    CT_Point    _centroid;                          /*!< Barycentre du noeud */
    double      _dimension;                         /*!< Demi-dimension du noeud */
    int         _nbPoints;                          /*!< Nombre de points dans le noeud */

    NORM_NewOctree*                 _octree;        /*!< Octree auquel appartient le noeud */
    NORM_NewOctreeNode*             _father;        /*!< Noeud pere */
    QVector<NORM_NewOctreeNode*>    _children;      /*!< Tableau des huit fils */
    QVector<int>*                   _ptIndices;     /*!< Tableau des indices des points dans le noeud (NULL en dehors des feuilles) */

    QVector<NORM_NewOctreeNode*>    _neighbours;    /*!< Tableau des feuilles voisines du noeud */

    float       _a;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _b;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _c;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _d;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _e;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _f;                                 /*!< Coefficient de la quadrique fitee aux points de la cellule : f(x,y) = ax^2 + bxy + cy^2 + dx + ey + f */
    float       _rmse;                              /*!< Rmse du fitting de quadrique */

    float       _l1;                                /*!< Premiere valeur propre de l'ACP sur les points du noeud : l1 > l2 > l3 */
    float       _l2;                                /*!< Premiere valeur propre de l'ACP sur les points du noeud : l1 > l2 > l3 */
    float       _l3;                                /*!< Premiere valeur propre de l'ACP sur les points du noeud : l1 > l2 > l3 */

    CT_Point    _v1;                                /*!< Vecteur propre associe a la premiere valeur propre de l'ACP sur les points du noeud */
    CT_Point    _v2;                                /*!< Vecteur propre associe a la seconde valeur propre de l'ACP sur les points du noeud */
    CT_Point    _v3;                                /*!< Vecteur propre associe a la troisieme valeur propre de l'ACP sur les points du noeud */

    bool        _visited;                           /*!< Flag pour indiquer si le noeud a ete visite ou non pendant un parcours de l'octree */

    const static QVector<QColor>_qColorChild;
    const static QVector<CT_Color>_ctColorChild;
};

#endif // NORM_NEWOCTREENODE_H
