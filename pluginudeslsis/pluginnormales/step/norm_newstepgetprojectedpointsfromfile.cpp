#include "norm_newstepgetprojectedpointsfromfile.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

// Using the point cloud deposit
#include "ct_global/ct_context.h"

#include "ct_itemdrawable/ct_scene.h"
#include "new/norm_newoctreev2.h"

#include <QElapsedTimer>
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltOctree "RsltOctree"
#define DEFin_grpOctree "grpOctree"
#define DEFin_itmOctree "itmOctree"

// Constructor : initialization of parameters
NORM_NewStepGetProjectedPointsFromFile::NORM_NewStepGetProjectedPointsFromFile(CT_StepInitializeData &dataInit) :
    CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString NORM_NewStepGetProjectedPointsFromFile::getStepDescription() const
{
    return tr("B - Transforme un octre compresse en nuage de points");
}

// Step detailled description
QString NORM_NewStepGetProjectedPointsFromFile::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_NewStepGetProjectedPointsFromFile::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_NewStepGetProjectedPointsFromFile::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_NewStepGetProjectedPointsFromFile(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_NewStepGetProjectedPointsFromFile::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltOctree = createNewInResultModelForCopy(DEFin_rsltOctree, "Octree Result");
    resIn_rsltOctree->setZeroOrMoreRootGroup();

    resIn_rsltOctree->addGroupModel("", DEFin_grpOctree, CT_AbstractItemGroup::staticGetType(), tr("Input octree Group"));
    resIn_rsltOctree->addItemModel( DEFin_grpOctree, DEFin_itmOctree, NORM_NewOctreeV2::staticGetType(), tr("Input Octree"));
}

// Creation and affiliation of OUT models
void NORM_NewStepGetProjectedPointsFromFile::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_RsltOctree = createNewOutResultModelToCopy( DEFin_rsltOctree );

    if(res_RsltOctree != NULL)
    {
        res_RsltOctree->addItemModel( DEFin_grpOctree, _autoRenameModelSceneProjectedPoints, new CT_Scene(), "Projected Points", "Points projettes sur les surfaces fitees");
    }
}

// Semi-automatic creation of step parameters DialogBox
void NORM_NewStepGetProjectedPointsFromFile::createPostConfigurationDialog()
{
//    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void NORM_NewStepGetProjectedPointsFromFile::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpOctree( res_RsltOctree, this, DEFin_grpOctree );
    while ( itIn_grpOctree.hasNext()
            &&
            !isStopped())
    {
        CT_StandardItemGroup* grpIn_grpOctree = (CT_StandardItemGroup*) itIn_grpOctree.next();

        NORM_NewOctreeV2* itemIn_itmOctree = (NORM_NewOctreeV2*)grpIn_grpOctree->firstItemByINModelName( this, DEFin_itmOctree );
        if ( itemIn_itmOctree != NULL )
        {
            CT_AbstractUndefinedSizePointCloud* projectedPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

            QElapsedTimer timer;
            timer.start();

            itemIn_itmOctree->getProjectedPointsFromFile( projectedPointCloud );

            qDebug() << "Temps de projection des points sur les surfaces" << timer.elapsed();

            CT_NMPCIR depositProjectedPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(projectedPointCloud);

            CT_Scene* projectedPointScene = new CT_Scene( _autoRenameModelSceneProjectedPoints.completeName(),
                                                          res_RsltOctree,
                                                          depositProjectedPointCloud );

            grpIn_grpOctree->addItemDrawable(projectedPointScene);
        }
    }
}
