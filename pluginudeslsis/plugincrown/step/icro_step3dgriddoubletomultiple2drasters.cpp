#include "icro_step3dgriddoubletomultiple2drasters.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"

#define DEFout_rsltResultStackOf2dRaster "rsltResultStackOf2dRaster"
#define DEFout_grpGroupOfGroupOf2dRaster "grpGroupOfGroupOf2dRaster"
#define DEFout_grpSignel2dRaster "grpSignel2dRaster"
#define DEFout_itmRaster2d "itmRaster2d"


// Constructor : initialization of parameters
ICRO_Step3dGridDoubleToMultiple2dRasters::ICRO_Step3dGridDoubleToMultiple2dRasters(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ICRO_Step3dGridDoubleToMultiple2dRasters::getStepDescription() const
{
    return tr("Cree un raster 2D pour chaque tranche d'une image 3D (Double)");
}

// Step detailled description
QString ICRO_Step3dGridDoubleToMultiple2dRasters::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_Step3dGridDoubleToMultiple2dRasters::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_Step3dGridDoubleToMultiple2dRasters::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_Step3dGridDoubleToMultiple2dRasters(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_Step3dGridDoubleToMultiple2dRasters::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3Rasterd = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3Rasterd->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3Rasterd->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<double>::staticGetType(), tr("3d Raster (int)"));

}

// Creation and affiliation of OUT models
void ICRO_Step3dGridDoubleToMultiple2dRasters::createOutResultModelListProtected()
{
//    CT_OutResultModelGroup *res_rsltResultStackOf2dRaster = createNewOutResultModel(DEFout_rsltResultStackOf2dRaster, tr("Stack of 2D rasters"));
//    res_rsltResultStackOf2dRaster->setRootGroup(DEFout_grpGroupOfGroupOf2dRaster, new CT_StandardItemGroup(), tr("Stack of 2D Rasters"));
//    res_rsltResultStackOf2dRaster->addGroupModel(DEFout_grpGroupOfGroupOf2dRaster, DEFout_grpSignel2dRaster, new CT_StandardItemGroup(), tr("Single 2D Raster"));
//    res_rsltResultStackOf2dRaster->addItemModel(DEFout_grpSignel2dRaster, DEFout_itmRaster2d, new CT_Grid2DXY<double>(), tr("2D raster from 3D raster"));
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_Step3dGridDoubleToMultiple2dRasters::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addFileChoice("Save directory", CT_FileChoiceButton::OneExistingFolder, "", _saveDirList );
}

void ICRO_Step3dGridDoubleToMultiple2dRasters::compute()
{
    if( _saveDirList.empty() )
    {
        return;
    }

    _saveDir= _saveDirList.first();
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        const CT_Grid3D<double>* itemIn_itmRaster3D = (CT_Grid3D<double>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        if (itemIn_itmRaster3D != NULL)
        {
            saveRastersFrom3dGrid( itemIn_itmRaster3D, _saveDir, "raster" );
        }
    }
}

void ICRO_Step3dGridDoubleToMultiple2dRasters::saveRastersFrom3dGrid(const CT_Grid3D<double> *grid,
                                                               QString dirPath,
                                                               QString fileNamePrefix)
{
    int dimz = grid->zArraySize();

    for( int z = 0 ; z < dimz ; z++ )
    {
        saveRaster2dZLevel( grid, z, dirPath, fileNamePrefix + "_" + QString("%1").arg(z, 4, 10, QChar('0')) + ".jpg" );
    }
}

void ICRO_Step3dGridDoubleToMultiple2dRasters::saveRaster2dZLevel( const CT_Grid3D<double>* grid,
                                                             int zLevel,
                                                             QString dirPath,
                                                             QString fileName )
{
    int dimx = grid->xArraySize();
    int dimy = grid->xArraySize();

    // Cree une image open cv
    cv::Mat1i raster( dimx, dimy, CV_8UC1 );

    // Rempli l'image
    for( int x = 0 ; x < dimx ; x++ )
    {
        for( int y = 0 ; y < dimy ; y++ )
        {
            raster.at<int>( cv::Point( y, x ) ) = grid->value( x, y, zLevel );
        }
    }

    // Enregistre l'image
    QString totalFileName = dirPath + fileName;
    cv::imwrite( totalFileName.toStdString(), raster );
}
