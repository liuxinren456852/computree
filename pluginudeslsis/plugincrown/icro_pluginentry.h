#ifndef ICRO_PLUGINENTRY_H
#define ICRO_PLUGINENTRY_H

#include "interfaces.h"

class ICRO_PluginManager;

class ICRO_PluginEntry : public PluginEntryInterface
{
    Q_OBJECT

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID PluginEntryInterface_iid)
#endif

    Q_INTERFACES(PluginEntryInterface)

public:
    ICRO_PluginEntry();
    ~ICRO_PluginEntry();

    QString getVersion() const;
    CT_AbstractStepPlugin* getPlugin() const;

private:
    ICRO_PluginManager *_pluginManager;
};

#endif // ICRO_PLUGINENTRY_H