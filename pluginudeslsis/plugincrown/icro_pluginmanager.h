#ifndef ICRO_PLUGINMANAGER_H
#define ICRO_PLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class ICRO_PluginManager : public CT_AbstractStepPlugin
{
public:
    ICRO_PluginManager();
    ~ICRO_PluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/PLUGINS-PROJECT-NAME-HERE/wiki");}

protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();

};

#endif // ICRO_PLUGINMANAGER_H
