#ifndef ICRO_STEPMULTIRESSPLITCOMPONENTSFROMSTEMPOSITIONS_H
#define ICRO_STEPMULTIRESSPLITCOMPONENTSFROMSTEMPOSITIONS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_grid3d.h"

/*!
 * \class ICRO_StepMultiResSplitComponentsFromStemPositions
 * \ingroup Steps_ICRO
 * \brief <b>Segmente une grille 3D en composantes connexes a partir de troncs multi resolution.</b>
 *
 * No detailled description for this step
 *
 * \param _minValue
 *
 */

enum TypeComposante
{
    TROP_PETITE = -1,
    VIDE = 1,
    SIMPLE,
    COMPLEXE
};

struct ConnectedComponentNonMulti
{
    ConnectedComponentNonMulti()
    {
        _label = -1;
        _nIntersectedCircles = 0;
        _type = VIDE;
        pixels = new QList<Eigen::Vector3i>();
    }

    int _label;
    int _nIntersectedCircles;
    TypeComposante _type;
    QList<Eigen::Vector3i>* pixels;
};

class ICRO_StepMultiResSplitComponentsFromStemPositions: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ICRO_StepMultiResSplitComponentsFromStemPositions(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void getCirclesFromFile( QString dir, QString fileName );

    void labelConnectedComponentNonMultis(const CT_Grid3D<int>* inputGrid,
                                  CT_Grid3D<int>* outLabeledGrid,
                                  double minValue,
                                  int minLabelSize,
                                  QVector< ConnectedComponentNonMulti* >& outComponents );

    void labelConnectedComponentNonMulti(const CT_Grid3D<int>* inputGrid,
                                 CT_Grid3D<int>* outLabeledGrid,
                                 double minValue,
                                 int startx,
                                 int starty,
                                 int startz,
                                 int label,
                                 ConnectedComponentNonMulti *outPixelsOfComponent);

    void getComponentsType(CT_Grid3D<int>* labeledGrid,
                           CT_Grid3D<int>* outTypeGrid,
                           QVector<ConnectedComponentNonMulti *> &components);

    void getComponentsType2(CT_Grid3D<int>* typeGrid);

    void labelComponentInGrid( ConnectedComponentNonMulti* component,
                               CT_Grid3D<int>* grid,
                               int label );

    ConnectedComponentNonMulti* getComponentFromLabel(QVector<ConnectedComponentNonMulti *> &components,
                                              int label );

private:

    // Step parameters
    double      _minValue;    /*!<  */
    int         _minPixelsInComponent;

    QVector< CT_CircleData* >   _circles;
};

#endif // ICRO_STEPMULTIRESSPLITCOMPONENTSFROMSTEMPOSITIONS_H
