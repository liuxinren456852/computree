#include "icro_stepgetdensitygridfromhitgrid.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"

// Resultat contient un groupe d'image
// le groupe d'images contient une grille3d d'int
#define DEFout_rsltDensityImage "rsltResultDensityImage"
#define DEFout_grpDensityImage "grpDensityImage"
#define DEFout_itmDensityImage "itmDensityImage"

// Constructor : initialization of parameters
ICRO_StepGetDensityGridFromHitGrid::ICRO_StepGetDensityGridFromHitGrid(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ICRO_StepGetDensityGridFromHitGrid::getStepDescription() const
{
    return tr("Calcule la densite de points par metre cube a partir d'une grille de hits");
}

// Step detailled description
QString ICRO_StepGetDensityGridFromHitGrid::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepGetDensityGridFromHitGrid::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepGetDensityGridFromHitGrid::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepGetDensityGridFromHitGrid(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepGetDensityGridFromHitGrid::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3RasterAndScene = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3RasterAndScene->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3RasterAndScene->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("3d Raster (int)"));
}

// Creation and affiliation of OUT models
void ICRO_StepGetDensityGridFromHitGrid::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltDensityImage = createNewOutResultModel(DEFout_rsltDensityImage, tr("Density image"));

    // Resultat contient un groupe d'image
    res_rsltDensityImage->setRootGroup(DEFout_grpDensityImage, new CT_StandardItemGroup(), tr("Image Group"));

    // Groupe d'image contient
    // - une grille 3D qui contient l'image normalisee
    res_rsltDensityImage->addItemModel(DEFout_grpDensityImage, DEFout_itmDensityImage, new CT_Grid3D<float>(), tr("Density image"));
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepGetDensityGridFromHitGrid::createPostConfigurationDialog()
{
}

void ICRO_StepGetDensityGridFromHitGrid::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltDensityImage = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        CT_Grid3D<int>* itemIn_itmIntensityGrid = (CT_Grid3D<int>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        if (itemIn_itmIntensityGrid != NULL )
        {
            // Resultat contient un groupe d'image
            CT_StandardItemGroup* grpDensityImage = new CT_StandardItemGroup(DEFout_grpDensityImage, resOut_rsltDensityImage );
            resOut_rsltDensityImage->addGroup( grpDensityImage );

            // Creation de la grille normalisee
            CT_Grid3D<float>* itemOut_itmDensityImage = new CT_Grid3D<float>( DEFout_itmDensityImage, resOut_rsltDensityImage,
                                                                              itemIn_itmIntensityGrid->minX(), itemIn_itmIntensityGrid->minY(), itemIn_itmIntensityGrid->minZ(),
                                                                              itemIn_itmIntensityGrid->xArraySize(), itemIn_itmIntensityGrid->yArraySize(), itemIn_itmIntensityGrid->zArraySize(),
                                                                              itemIn_itmIntensityGrid->resolution(),
                                                                              std::numeric_limits<int>::max(), std::numeric_limits<int>::max() );

            // Normalise l'image
            computeDensityImage( itemIn_itmIntensityGrid,
                                 itemOut_itmDensityImage );

            // Mise a jour des min max de la grille pour affichage
            itemOut_itmDensityImage->computeMinMax();

            qDebug() << itemOut_itmDensityImage->dataMin() << itemOut_itmDensityImage->dataMax();

            // Ajout de la grille au resultat
            grpDensityImage->addItemDrawable( itemOut_itmDensityImage );
        }
    }
}

void ICRO_StepGetDensityGridFromHitGrid::computeDensityImage(CT_Grid3D<int> *intensityImage,
                                                             CT_Grid3D<float> *outputDensityImage)
{
    int xdim = intensityImage->xArraySize();
    int ydim = intensityImage->yArraySize();
    int zdim = intensityImage->zArraySize();
    float volumeVoxel = intensityImage->resolution()*intensityImage->resolution()*intensityImage->resolution();

    // Pour chaque pixel
    for( int x = 0 ; x < xdim ; x++ )
    {
        for( int y = 0 ; y < ydim ; y++ )
        {
            for( int z = 0 ; z < zdim ; z++ )
            {
                // On normalise la valeur
                float density = intensityImage->value(x,y,z) / volumeVoxel;
                outputDensityImage->setValue( x, y, z, density );
            }
        }
    }
}
