#include "icro_stepgetouttersurfaceof3dgriddouble.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include <QQueue>
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltWith3Rasterd "rsltWith3Rasterd"
#define DEFin_grpWith3dRaster "grpWith3dRaster"
#define DEFin_itmRaster3D "itmRaster3D"

#define DEFout_rsltResultOutterSurfaceElements "rsltResultOutterSurfaceElements"
#define DEFout_grpBinarizedVolume "grpBinarizedVolume"
#define DEFout_itmBinarizedVolume "itmBinarizedVolume"


// Constructor : initialization of parameters
ICRO_StepGetOutterSurfaceFrom3dGridDouble::ICRO_StepGetOutterSurfaceFrom3dGridDouble(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ICRO_StepGetOutterSurfaceFrom3dGridDouble::getStepDescription() const
{
    return tr("Extrait la surface externe a partir d'une grille de densite (double)");
}

// Step detailled description
QString ICRO_StepGetOutterSurfaceFrom3dGridDouble::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ICRO_StepGetOutterSurfaceFrom3dGridDouble::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ICRO_StepGetOutterSurfaceFrom3dGridDouble::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ICRO_StepGetOutterSurfaceFrom3dGridDouble(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ICRO_StepGetOutterSurfaceFrom3dGridDouble::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_rsltWith3Rasterd = createNewInResultModel(DEFin_rsltWith3Rasterd, tr("Result with 3d raster"));
    resIn_rsltWith3Rasterd->setRootGroup(DEFin_grpWith3dRaster, CT_AbstractItemGroup::staticGetType(), tr("Group with 3D raster"));
    resIn_rsltWith3Rasterd->addItemModel(DEFin_grpWith3dRaster, DEFin_itmRaster3D, CT_Grid3D<int>::staticGetType(), tr("3d Raster (int)"));

}

// Creation and affiliation of OUT models
void ICRO_StepGetOutterSurfaceFrom3dGridDouble::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_rsltOutterSurfaceElements = createNewOutResultModel(DEFout_rsltResultOutterSurfaceElements, tr("Elements of outter surface"));
    res_rsltOutterSurfaceElements->setRootGroup(DEFout_grpBinarizedVolume, new CT_StandardItemGroup(), tr("Group of elements"));
    res_rsltOutterSurfaceElements->addItemModel(DEFout_grpBinarizedVolume, DEFout_itmBinarizedVolume, new CT_Grid3D<bool>(), tr("Binarized 3D volume"));
}

// Semi-automatic creation of step parameters DialogBox
void ICRO_StepGetOutterSurfaceFrom3dGridDouble::createPostConfigurationDialog()
{
}

void ICRO_StepGetOutterSurfaceFrom3dGridDouble::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_rsltWith3Rasterd = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut_rsltOutterSurfaceElementsGrid = outResultList.at(0);

    CT_ResultGroupIterator itIn_grpWith3dRaster(resIn_rsltWith3Rasterd, this, DEFin_grpWith3dRaster);
    while (itIn_grpWith3dRaster.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_grpWith3dRaster = (CT_AbstractItemGroup*) itIn_grpWith3dRaster.next();

        const CT_Grid3D<double>* itemIn_itmRaster3D = (CT_Grid3D<double>*)grpIn_grpWith3dRaster->firstItemByINModelName(this, DEFin_itmRaster3D);
        if (itemIn_itmRaster3D != NULL)
        {
            CT_Grid3D<bool>* outterSurfaceElementsGrid = new CT_Grid3D<bool>( DEFout_itmBinarizedVolume, resOut_rsltOutterSurfaceElementsGrid,
                                                                              itemIn_itmRaster3D->minX(), itemIn_itmRaster3D->minY(), itemIn_itmRaster3D->minZ(),
                                                                              itemIn_itmRaster3D->xArraySize(), itemIn_itmRaster3D->yArraySize(), itemIn_itmRaster3D->zArraySize(),
                                                                              itemIn_itmRaster3D->resolution(),
                                                                              false, false );

            CT_Grid3D<bool>* innerSurfaceElementsGrid = new CT_Grid3D<bool>(  DEFout_itmBinarizedVolume, resOut_rsltOutterSurfaceElementsGrid,
                                                                              itemIn_itmRaster3D->minX(), itemIn_itmRaster3D->minY(), itemIn_itmRaster3D->minZ(),
                                                                              itemIn_itmRaster3D->xArraySize(), itemIn_itmRaster3D->yArraySize(), itemIn_itmRaster3D->zArraySize(),
                                                                              itemIn_itmRaster3D->resolution(),
                                                                              false, false );

            getOutterSurfaceElements( itemIn_itmRaster3D, outterSurfaceElementsGrid );
            inverseBooleanGrid( outterSurfaceElementsGrid, innerSurfaceElementsGrid );

            delete outterSurfaceElementsGrid;

            // Ajout de la grille binaire au resultat
            CT_StandardItemGroup* grpGrpBinaryGrid = new CT_StandardItemGroup(DEFout_grpBinarizedVolume, resOut_rsltOutterSurfaceElementsGrid );
            grpGrpBinaryGrid->addItemDrawable( innerSurfaceElementsGrid );
            resOut_rsltOutterSurfaceElementsGrid->addGroup( grpGrpBinaryGrid );
        }
    }
}

void ICRO_StepGetOutterSurfaceFrom3dGridDouble::getOutterSurfaceElements(const CT_Grid3D<double>* grid, CT_Grid3D<bool>* outOutterSurfaceElements )
{
    // On considere que le pixel en haut a droite au fond (le top de la bbox) est un pixel d''exterieur
    Eigen::Vector3i firstPixel;
    firstPixel << grid->xArraySize()-1, grid->yArraySize()-1, grid->zArraySize()-1;

    // Parcours en largeur (le parcours en profondeur risque de depasser la taille de la pile de recursivite), on declare une file
    QQueue< Eigen::Vector3i > f;

    // On marque le premier pixel et on l'ajoute a la file
    outOutterSurfaceElements->setValue( firstPixel.x(), firstPixel.y(), firstPixel.z(), true );
    f.enqueue( firstPixel );

    // Tant que la file n'est pas vide
    Eigen::Vector3i curPixel;
    Eigen::Vector3i neiPixel;
    while( !f.empty() )
    {
        // On prend le premier element de la file
        curPixel = f.dequeue();

        // On regarde ses voisins
        for( int i = -1 ; i <= 1 ; i++ )
        {
            for( int j = -1 ; j <= 1 ; j++ )
            {
                for( int k = -1 ; k <= 1 ; k++ )
                {
                    // Pas la peine de tester si le pixel voisin est egal au pixel courant, le test de marquage suffit
                    // Il faut egalement que le pixel dans la grille en entree soit nul
                    if( outOutterSurfaceElements->value( curPixel.x()+i, curPixel.y()+j, curPixel.z()+k ) == false &&
                        grid->value( curPixel.x()+i, curPixel.y()+j, curPixel.z()+k ) == 0 )
                    {
                        neiPixel(0) = curPixel.x()+i;
                        neiPixel(1) = curPixel.y()+j;
                        neiPixel(2) = curPixel.z()+k;

                        // On le marque et on l'ajoute a la file
                        outOutterSurfaceElements->setValue( neiPixel.x(), neiPixel.y(), neiPixel.z(), true );
                        f.enqueue( neiPixel );
                    }
                }
            }
        }
    }
}

void ICRO_StepGetOutterSurfaceFrom3dGridDouble::inverseBooleanGrid(const CT_Grid3D<bool> *inGrid, CT_Grid3D<bool> *outGrid)
{
    size_t nIndices = inGrid->nCells();
    for( size_t i = 0 ; i < nIndices ; i++ )
    {
        outGrid->setValueAtIndex( i, !inGrid->valueAtIndex(i) );
    }
}
