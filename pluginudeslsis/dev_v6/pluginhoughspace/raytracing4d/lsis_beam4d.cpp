/************************************************************************************
* Filename :  lsis_beam4d.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "lsis_beam4d.h"

// Utilise les max des flottants
#include <limits>

 LSIS_Beam4D:: LSIS_Beam4D()
{
    _origin(0) = (0);
    _origin(1) = (0);
    _origin(2) = (0);
    _origin(3) = (0);

    _direction(0) = (0);
    _direction(1) = (0);
    _direction(2) = (0);
    _direction(3) = (0);
}

 LSIS_Beam4D:: LSIS_Beam4D(const LSIS_Point4DDouble &origin, const LSIS_Point4DDouble &direction)
{
    assert( direction.norm() != 0 );

    _origin = origin;

    // Normalizing direction
    _direction = direction.normalized();
}

 LSIS_Beam4D::~ LSIS_Beam4D()
{
}

bool LSIS_Beam4D::intersect(const LSIS_Point4DDouble& bot, const LSIS_Point4DDouble& top, LSIS_Point4DDouble &nearP, LSIS_Point4DDouble &farP) const
{
    double t0 = 0;
    double t1 = std::numeric_limits<double>::max();

    if (!updateIntervals(bot(0), top(0), _origin(0), _direction(0), t0, t1)) {return false;}
    if (!updateIntervals(bot(1), top(1), _origin(1), _direction(1), t0, t1)) {return false;}
    if (!updateIntervals(bot(2), top(2), _origin(2), _direction(2), t0, t1)) {return false;}
    if (!updateIntervals(bot(3), top(3), _origin(3), _direction(3), t0, t1)) {return false;}

    nearP(0) = (_origin(0) + _direction(0)*t0);
    nearP(1) = (_origin(1) + _direction(1)*t0);
    nearP(2) = (_origin(2) + _direction(2)*t0);
    nearP(3) = (_origin(3) + _direction(3)*t0);

    farP(0) = (_origin(0) + _direction(0)*t1);
    farP(1) = (_origin(1) + _direction(1)*t1);
    farP(2) = (_origin(2) + _direction(2)*t1);
    farP(3) = (_origin(3) + _direction(3)*t1);

    return true;
}

bool  LSIS_Beam4D::intersect(const LSIS_Point4DDouble& bot, const LSIS_Point4DDouble& top) const
{
    double t0 = 0;
    double t1 = std::numeric_limits<double>::max();

    if (!updateIntervals(bot(0), top(0), _origin(0), _direction(0), t0, t1)) {return false;}
    if (!updateIntervals(bot(1), top(1), _origin(1), _direction(1), t0, t1)) {return false;}
    if (!updateIntervals(bot(2), top(2), _origin(2), _direction(2), t0, t1)) {return false;}
    if (!updateIntervals(bot(3), top(3), _origin(3), _direction(3), t0, t1)) {return false;}

    return true;
}

bool  LSIS_Beam4D::updateIntervals(const double &bot, const double &top, const double &origin, const double &direction, double &t0, double &t1) const
{
    // Update interval for bounding box slab
    double invRayDir = 1.f / direction;
    double tNear = (bot - origin) * invRayDir;
    double tFar  = (top - origin) * invRayDir;

    // Update parametric interval from slab intersection $t$s
    if (tNear > tFar) std::swap(tNear, tFar);

    t0 = tNear > t0 ? tNear : t0;
    t1 = tFar  < t1 ? tFar  : t1;

    if (t0 > t1 && t0 - t1 > EPSILON_INTERSECTION_RAY ) // t0 being always > t1, (t0-t1) is always positive
    {
        return false;
    }
    return true;
}
