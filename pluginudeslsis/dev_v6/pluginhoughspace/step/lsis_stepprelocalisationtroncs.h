#ifndef LSIS_STEPPRELOCALISATIONTRONCS_H
#define LSIS_STEPPRELOCALISATIONTRONCS_H

#include "ct_step/abstract/ct_abstractstep.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_grid4d_dense.h"
#include "ct_itemdrawable/ct_grid4d_sparse.h"
#include "../houghspace/lsis_houghspace4d.h"
#include "tools/lsis_debugtool.h"

//Qt dependencies
#include <QFile>
#include <QDir>

/*!
 * \class LSIS_StepPreLocalisationTroncs
 * \ingroup Steps_LSIS
 * \brief <b>Fait une transformee de Hough avec une seule case en Z (s'applique sur une tranche uniquement) On prend les maximas locaux uniquement (pas de contours actifs).</b>
 *
 * No detailled description for this step
 *
 * \param _resw 
 * \param _resx 
 * \param _resy 
 *
 */

class LSIS_StepPreLocalisationTroncs: public CT_AbstractStep, public LSIS_DebugTool
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    LSIS_StepPreLocalisationTroncs(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     * 
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

    void setProgress(const int &p) { CT_AbstractStep::setProgress(p); }

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

    void compute( const CT_PointsAttributesNormal* normalCloud );

    void computeAndSave( const CT_PointsAttributesNormal* normalCloud, QString dirPath, QString fileName );

    void markRepulsePixel(const LSIS_Pixel4D &pixel , CT_Grid4D_Sparse<bool> &repulseImage, float coeffRadiusRepulse);

    // CT_AbstractStep non obligatoire :
    // Que faire avant de s'arreter pendant le mode debug ?
    void preWaitForAckIfInDebugMode();

    // CT_AbstractStep non obligatoire :
    // Que faire apres s'etre arete pendant le mode debug ?
    void postWaitForAckIfInDebugMode();

    // Methode qui en mode debug permet de selectionner le document dans lequel afficher les elements de debug.
    // Voir aussi la variable _debugStructure
    void setDocumentForDebugDisplay(QList<DocumentInterface*> docList);

private:

    // Step parameters
    double              _rmin;
    double              _rmax;
    double              _resw;    /*!<  */
    double              _resx;    /*!<  */
    double              _resy;    /*!<  */

    bool                _seuilMode;
    double              _otsuCoeff;
    double              _ratioCoeff;
    int                 _nBinsInHist;

    int                 _neighbourhoodSizeForLocalMaxima;

    bool                _saveMode;
    QString             _dirPath;
    QString             _fileName;

    CT_ResultGroup*         _rsltCircles;
    CT_StandardItemGroup*   _grpGrpCircles;

    LSIS_HoughSpace4D*      _debugHoughSpace;
    DocumentInterface*      _debugDocumentHoughSpace;
};

#endif // LSIS_STEPPRELOCALISATIONTRONCS_H
