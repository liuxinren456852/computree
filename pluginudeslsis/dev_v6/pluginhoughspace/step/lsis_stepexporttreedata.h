#ifndef LSIS_STEPEXPORTTREEDATA_H
#define LSIS_STEPEXPORTTREEDATA_H

#include "ct_step/abstract/ct_abstractstep.h"

//itemdrawable
#include "openactivecontour/lsis_activecontour4dcontinuous.h"

class LSIS_StepExportTreeData:public CT_AbstractStep
{
    Q_OBJECT
public:
    LSIS_StepExportTreeData(CT_StepInitializeData &dataInit);

    /**
     * @brief Return a short description of what do this class
     */
    QString getStepDescription() const;

    /*! \brief Step detailed description
     *
     * Return a detailed description of the step function
     */
    QString getStepDetailledDescription() const;

    /**
     * @brief Return a new empty instance of this class
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:
    /**
     * @brief This method defines what kind of input the step can accept
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPreConfigurationDialog();

    /**
     * @brief This method creates a window for the user to set the different parameters of the step.
     */
    void createPostConfigurationDialog();

    /**
     * @brief This method defines what kind of output the step produces
     */
    void createOutResultModelListProtected();

    /**
     * @brief This method do the job
     */
    void compute();

    void saveTreesFromRootTuboids( QString outDirectory,
                                   QString outFileName,
                                   QVector< LSIS_ActiveContour4DContinuous<int>* > rootTuboids,
                                   CT_ResultGroup* outResult);

    void saveSingleTreeFromRootTuboidRecursive( QTextStream& stream,
                                                LSIS_ActiveContour4DContinuous<int>* tuboid,
                                                CT_ResultGroup* outResult);
private:
    QStringList             _saveDirList;
    QString                 _saveDir;
    QString                 _filePrefix;

};

#endif // LSIS_STEPEXPORTTREEDATA_H
