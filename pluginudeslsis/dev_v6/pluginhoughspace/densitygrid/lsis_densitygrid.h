/************************************************************************************
* Filename :  lsis_densitygrid.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_DENSITYGRID_H
#define LSIS_DENSITYGRID_H

// Herite d'une grille 3D
#include "ct_itemdrawable/ct_grid3d.h"

// Se calcule a partir des points d'un nuage enregistre
#include "ct_cloudindex/registered/abstract/ct_abstractcloudindexregisteredt.h"

class LSIS_DensityGrid : public CT_Grid3D<int>
{
    Q_OBJECT

public:
/* **************************************************************** */
/* Constructeurs et Destructeurs                                    */
/* **************************************************************** */
    /*!
     * \brief LSIS_DensityGrid
     *
     * Constructeur par dimension
     *
     * \param model : modele d'item drawable pour la grille de densite
     * \param result : resultat dans lequel sera place la grille
     * \param xmin : point inferieur gauche de la bounding box
     * \param ymin : point inferieur gauche de la bounding box
     * \param zmin : point inferieur gauche de la bounding box
     * \param dimx : nombre de voxels selon l'axe Ox
     * \param dimy : nombre de voxels selon l'axe Oy
     * \param dimz : nombre de voxels selon l'axe Oz
     * \param resolution : resolution de la grille
     * \param indexCloud : nuage d'indices a partir desquels calculer la densite
     */
    LSIS_DensityGrid(const CT_OutAbstractSingularItemModel *model,
                     CT_AbstractResult *result,
                     double xmin,
                     double ymin,
                     double zmin,
                     size_t dimx,
                     size_t dimy,
                     size_t dimz,
                     double resolution,
                     CT_PCIR indexCloud );

    /*!
     * \brief LSIS_DensityGrid
     *
     * Constructeur par dimension
     *
     * \param modelName : nom du modele d'item pour la grille
     * \param result : resultat dans lequel sera place la grille
     * \param xmin : point inferieur gauche de la bounding box
     * \param ymin : point inferieur gauche de la bounding box
     * \param zmin : point inferieur gauche de la bounding box
     * \param dimx : nombre de voxels selon l'axe Ox
     * \param dimy : nombre de voxels selon l'axe Oy
     * \param dimz : nombre de voxels selon l'axe Oz
     * \param resolution : resolution de la grille
     * \param indexCloud : nuage d'indices a partir desquels calculer la densite
     */
    LSIS_DensityGrid(const QString& modelName,
                     CT_AbstractResult *result,
                     double xmin,
                     double ymin,
                     double zmin,
                     size_t dimx,
                     size_t dimy,
                     size_t dimz,
                     double resolution,
                     CT_PCIR indexCloud );

    /*!
     * \brief LSIS_DensityGrid
     *
     * Constructeur par bounding box
     *
     * \param model : modele d'item drawable pour la grille de densite
     * \param result : resultat dans lequel sera place la grille
     * \param xmin : point inferieur gauche de la bounding box
     * \param ymin : point inferieur gauche de la bounding box
     * \param zmin : point inferieur gauche de la bounding box
     * \param xmax : point superieur droit de la bounding box
     * \param ymax : point superieur droit de la bounding box
     * \param zmax : point superieur droit de la bounding box
     * \param resolution : resolution de la grille
     * \param coordConstructor : variable pour avoir un constructeur de signature differente du constructeur par dimensions
     * \param indexCloud : nuage d'indices a partir desquels calculer la densite
     */
    LSIS_DensityGrid(const CT_OutAbstractSingularItemModel *model,
                     CT_AbstractResult *result,
                     double xmin,
                     double ymin,
                     double zmin,
                     double xmax,
                     double ymax,
                     double zmax,
                     double resolution,
                     bool coordConstructor,
                     CT_PCIR indexCloud);

    /*!
     * \brief LSIS_DensityGrid
     *
     * Constructeur par bounding box
     *
     * \param modelName : nom du modele d'item drawable pour la grille de densite
     * \param result : resultat dans lequel sera place la grille
     * \param xmin : point inferieur gauche de la bounding box
     * \param ymin : point inferieur gauche de la bounding box
     * \param zmin : point inferieur gauche de la bounding box
     * \param xmax : point superieur droit de la bounding box
     * \param ymax : point superieur droit de la bounding box
     * \param zmax : point superieur droit de la bounding box
     * \param resolution : resolution de la grille
     * \param coordConstructor : variable pour avoir un constructeur de signature differente du constructeur par dimensions
     * \param indexCloud : nuage d'indices a partir desquels calculer la densite
     */
    LSIS_DensityGrid(const QString& modelName,
                     CT_AbstractResult *result,
                     double xmin,
                     double ymin,
                     double zmin,
                     double xmax,
                     double ymax,
                     double zmax,
                     double resolution,
                     bool coordConstructor,
                     CT_PCIR indexCloud);

/* **************************************************************** */
/* Outils constructeurs et destructeurs                             */
/* **************************************************************** */
    /*!
     * \brief computeDensityFromIndexCloud
     *
     * Rempli la grille de densite en fonction du nuage d'indices (donc de points) passe en parametre
     *
     * \param indexCloud : nuage d'indices pour calculer la densite
     */
    void computeDensityFromIndexCloud(CT_PCIR indexCloud );
};

#endif // LSIS_DENSITYGRID_H
