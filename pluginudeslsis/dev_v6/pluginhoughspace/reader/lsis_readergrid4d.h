/************************************************************************************
* Filename :  lsis_readergrid4d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_READERGRID4D_H
#define LSIS_READERGRID4D_H

#include "ct_step/abstract/ct_abstractsteploadfileinscene.h"

class LSIS_ReaderGrid4D : public CT_AbstractStepLoadFileInScene
{
    Q_OBJECT

public:
    LSIS_ReaderGrid4D(CT_StepInitializeData &data);

    virtual void init();

    QString getStepDescription() const;

    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);
    QList<FileFormat> getFileExtensionAccepted() const;

protected:
    void createOutResultModelListProtected();
    int readHeaderFile(QFile &f);
    void readDataFile(QFile &f, int offset, bool little_endian = false);

private :
    size_t     _nlevw;
    size_t     _nlevx;
    size_t     _nlevy;
    size_t     _nlevz;
    double   _minw;
    double   _minx;
    double   _miny;
    double   _minz;
    double   _resw;
    double   _resx;
    double   _resy;
    double   _resz;
    double   _noDataVal;
};

#endif // LSIS_READERGRID4D_H
