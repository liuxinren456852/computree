#include "norm_newoctreev2.h"

#include "norm_newoctreenodev2.h"
#include "norm_newoctreedrawmanagerv2.h"
#include "ct_itemdrawable/ct_scene.h"

#include <QFile>
#include <QTextStream>

#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"

const NORM_NewOctreeDrawManagerV2 NORM_NewOctreeV2::NEW_OCTREE_DRAW_MANAGER_V2;
const CT_PointAccessor NORM_NewOctreeV2::_pAccessor;

NORM_NewOctreeV2::NORM_NewOctreeV2() :
    CT_AbstractItemDrawableWithoutPointCloud(),
    _dimensionMin(-1),
    _nbPointsMinForPca(-1),
    _root(NULL),
    _scene( NULL )
{
    setBaseDrawManager( &NEW_OCTREE_DRAW_MANAGER_V2 );

    _bot.setValues( std::numeric_limits<float>::max(),
                    std::numeric_limits<float>::max(),
                    std::numeric_limits<float>::max() );
    _top.setValues( -std::numeric_limits<float>::max(),
                    -std::numeric_limits<float>::max(),
                    -std::numeric_limits<float>::max() );
}

NORM_NewOctreeV2::NORM_NewOctreeV2(const CT_OutAbstractSingularItemModel* model,
                                   const CT_AbstractResult* result) :
    CT_AbstractItemDrawableWithoutPointCloud( model, result ),
    _dimensionMin(-1),
    _nbPointsMinForPca(-1),
    _root(NULL),
    _scene( NULL )
{
    setBaseDrawManager( &NEW_OCTREE_DRAW_MANAGER_V2 );

    _bot.setValues( std::numeric_limits<float>::max(),
                    std::numeric_limits<float>::max(),
                    std::numeric_limits<float>::max() );
    _top.setValues( -std::numeric_limits<float>::max(),
                    -std::numeric_limits<float>::max(),
                    -std::numeric_limits<float>::max() );
}

NORM_NewOctreeV2::NORM_NewOctreeV2(const QString& modelName,
                                   const CT_AbstractResult* result) :
    CT_AbstractItemDrawableWithoutPointCloud( modelName, result ),
    _dimensionMin(-1),
    _nbPointsMinForPca(-1),
    _root(NULL),
    _scene( NULL )
{
    setBaseDrawManager( &NEW_OCTREE_DRAW_MANAGER_V2 );

    _bot.setValues( std::numeric_limits<float>::max(),
                    std::numeric_limits<float>::max(),
                    std::numeric_limits<float>::max() );
    _top.setValues( -std::numeric_limits<float>::max(),
                    -std::numeric_limits<float>::max(),
                    -std::numeric_limits<float>::max() );
}

NORM_NewOctreeV2::NORM_NewOctreeV2(const QString& modelName,
                                   const CT_AbstractResult* result,
                                   CT_Scene* scene,
                                   int nbPointsMinForPca,
                                   float dimensionMaxForPca,
                                   LeafCriteria leafCriteria,
                                   float sigma2Min,
                                   float sigma3Max,
                                   float rmseMax,
                                   float dimensionMin) :
    CT_AbstractItemDrawableWithoutPointCloud( modelName, result ),
    _nbPointsMinForPca( nbPointsMinForPca ),
    _dimensionMaxForPca( dimensionMaxForPca ),
    _sigma2Min( sigma2Min ),
    _sigma3Max( sigma3Max ),
    _leafCriteria( leafCriteria ),
    _maxRMSE ( rmseMax ),
    _dimensionMin( dimensionMin ),
    _root(NULL),
    _scene( scene )
{
    setBaseDrawManager( &NEW_OCTREE_DRAW_MANAGER_V2 );

    // On fixe les dimensions de l'octree
    // i.e. on prend le bounding cube de la scene
    float cubeDimension;
    CT_Point centre;
    initialiseOctreeBoundingCubeFromScene( scene,
                                           cubeDimension,
                                           centre );

    // Pour l'affichage (le recentrage de la camera)
    setCenterCoordinate( centre );

    // On cree le tableau d'indices de la racine (tous les indices de la scene)
    const CT_AbstractPointCloudIndex* indexCloud = scene->getPointCloudIndex();
    int nIndices = indexCloud->size();
    QVector<int>* rootIndices = new QVector<int>( nIndices );
    for( int i = 0 ; i < nIndices ; i++ )
    {
        (*rootIndices)[i] = indexCloud->indexAt(i);
    }

    // On cree la racine de l'octree
    _root = new NORM_NewOctreeNodeV2( centre,
                                      cubeDimension,
                                      0,
                                      NULL,
                                      this,
                                      rootIndices );

    // On divise recursivement la racine
    if( leafCriteria == Lambdas )
    {
        _root->recursiveSplit();
    }
    else if( leafCriteria == RMSE )
    {
        _root->recursiveSplitWithRmse();
    }
}

CT_AbstractItemDrawable* NORM_NewOctreeV2::copy(const CT_OutAbstractItemModel* model,
                                                const CT_AbstractResult* result,
                                                CT_ResultCopyModeList copyModeList)
{
    Q_UNUSED( model );
    Q_UNUSED( result);
    Q_UNUSED( copyModeList );

    NORM_NewOctreeV2* rslt = new NORM_NewOctreeV2();

    return rslt;
}

NORM_NewOctreeV2::~NORM_NewOctreeV2()
{
    delete _root;
}

void NORM_NewOctreeV2::initialiseOctreeBoundingCubeFromScene(CT_Scene* scene,
                                                             float& outDimension,
                                                             CT_Point& outCentre )
{
    // Recupere la bounding box de la scene
    CT_Point sceneBot;
    CT_Point sceneTop;
    scene->getBoundingBox( sceneBot, sceneTop );

    outDimension = std::max( sceneTop(0) - sceneBot(0),
                             std::max( sceneTop(1) - sceneBot(1),
                                       sceneTop(2) - sceneBot(2) ) );

    // Le point bot de la scene est conserve comme point bot de l'octree
    _bot = sceneBot;

    // Le point top par contre est le point superieur droit du cube englobant
    // Le centre est a bot plus la moitie de la dimension
    for( int i = 0 ; i < 3 ; i++ )
    {
        _top(i) = _bot(i) + outDimension;
        outCentre(i) = _bot(i) + ( outDimension / 2 );
    }
}

CT_PointsAttributesNormal* NORM_NewOctreeV2::getNormalsAttribute(QString modelName,
                                                                 CT_AbstractResult* result,
                                                                 CT_PCIR pointCloudIndexRegistered)
{
    CT_PointsAttributesNormal* rslt = new CT_PointsAttributesNormal( modelName,
                                                                     result,
                                                                     pointCloudIndexRegistered );

    // On remplit les valeurs d'id
    _root->setNormalsAttributeRecursive( rslt );

    // BBox pour affichage (rencentrer camera)
    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

CT_PointsAttributesNormal*NORM_NewOctreeV2::getNormalsAttributeWithLinearInterpolation(QString modelName,
                                                                                       CT_AbstractResult* result,
                                                                                       CT_PCIR pointCloudIndexRegistered)
{
    CT_PointsAttributesNormal* rslt = new CT_PointsAttributesNormal( modelName,
                                                                     result,
                                                                     pointCloudIndexRegistered );

    // On remplit les valeurs d'id
    _root->setNormalsAttributeWithLinearInterpolationRecursive( rslt );

    // BBox pour affichage (rencentrer camera)
    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

CT_PointsAttributesNormal*NORM_NewOctreeV2::getNormalsAttributeWithWendlandInterpolation(QString modelName,
                                                                                         CT_AbstractResult* result,
                                                                                         CT_PCIR pointCloudIndexRegistered)
{
    CT_PointsAttributesNormal* rslt = new CT_PointsAttributesNormal( modelName,
                                                                     result,
                                                                     pointCloudIndexRegistered );

    // On remplit les valeurs d'id
    _root->setNormalsAttributeWithWendlandInterpolationRecursive( rslt );

    // BBox pour affichage (rencentrer camera)
    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

void NORM_NewOctreeV2::fitSurfaces()
{
    _root->runQuadraticFittingRecursive();
}

void NORM_NewOctreeV2::getProjectedPoints(CT_AbstractUndefinedSizePointCloud* outProjectedPoints)
{
    _root->getProjectedPointsRecursive( outProjectedPoints );
}

void NORM_NewOctreeV2::getProjectedPointsWithLinearInterpolation(CT_AbstractUndefinedSizePointCloud* outProjectedPoints,
                                                                 float interpolationRadius )
{
    _root->getProjectedPointsWithLinearInterpolationRecursive( outProjectedPoints,
                                                               interpolationRadius );
}

CT_PointsAttributesScalarTemplated<float>* NORM_NewOctreeV2::getErrorSurface(QString modelName,
                                                                             CT_AbstractResult* result,
                                                                             CT_PCIR pointCloudIndexRegistered)
{
    CT_PointsAttributesScalarTemplated<float>* rslt = new CT_PointsAttributesScalarTemplated<float>( modelName,
                                                                                                     result,
                                                                                                     pointCloudIndexRegistered );
    _root->setDistanceFromPointsToSurfaceRecursive( rslt );

    // BBox pour affichage (rencentrer camera)
    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

CT_PointsAttributesScalarTemplated<float>*NORM_NewOctreeV2::getGaussianCurvature(QString modelName,
                                                                                 CT_AbstractResult* result,
                                                                                 CT_PCIR pointCloudIndexRegistered)
{
    CT_PointsAttributesScalarTemplated<float>* rslt = new CT_PointsAttributesScalarTemplated<float>( modelName,
                                                                                                     result,
                                                                                                     pointCloudIndexRegistered );

    int nPoints = pointCloudIndexRegistered.data()->size();
    for( int i = 0 ; i < nPoints ; i++ )
    {
        rslt->setValueAt( i, std::numeric_limits<float>::max() );
    }

    float minCurv = std::numeric_limits<float>::max();
    float maxCurv = -std::numeric_limits<float>::max();
    _root->setGaussianCurvatureRecursive( rslt, minCurv, maxCurv );
    qDebug() << "Courbures min et max" << minCurv << maxCurv;

    // BBox pour affichage (rencentrer camera)
    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

void NORM_NewOctreeV2::getCurvatures(QString modelNameGaussianCurvature,
                                     QString modelNameDir1,
                                     QString modelNameDir2,
                                     CT_AbstractResult* result,
                                     CT_PCIR pointCloudIndexRegistered,
                                     CT_PointsAttributesScalarTemplated<float>*& outGaussianCurvature,
                                     CT_PointsAttributesNormal*& outDir1,
                                     CT_PointsAttributesNormal*& outDir2)
{
    outGaussianCurvature = new CT_PointsAttributesScalarTemplated<float>( modelNameGaussianCurvature,
                                                                          result,
                                                                          pointCloudIndexRegistered );

    outDir1 = new CT_PointsAttributesNormal( modelNameDir1,
                                             result,
                                             pointCloudIndexRegistered );

    outDir2 = new CT_PointsAttributesNormal( modelNameDir2,
                                             result,
                                             pointCloudIndexRegistered );

    int nPoints = pointCloudIndexRegistered.data()->size();
    CT_Normal normaleNulle;
    CT_AbstractNormalCloud* outDir1Cloud = outDir1->getNormalCloud();
    CT_AbstractNormalCloud* outDir2Cloud = outDir2->getNormalCloud();
    normaleNulle.set(0,0,0,0);
    for( int i = 0 ; i < nPoints ; i++ )
    {
        outGaussianCurvature->setValueAt( i, std::numeric_limits<float>::max() );
        outDir1Cloud->replaceNormal( i, normaleNulle );
        outDir2Cloud->replaceNormal( i, normaleNulle );
    }

    _root->setCurvaturesRecursive( outGaussianCurvature,
                                   outDir1,
                                   outDir2 );

    // BBox pour affichage (rencentrer camera)
    outGaussianCurvature->setBoundingBox( _bot(0), _bot(1), _bot(2),
                                          _top(0), _top(1), _top(2) );

    // BBox pour affichage (rencentrer camera)
    outDir1->setBoundingBox( _bot(0), _bot(1), _bot(2),
                             _top(0), _top(1), _top(2) );

    // BBox pour affichage (rencentrer camera)
    outDir2->setBoundingBox( _bot(0), _bot(1), _bot(2),
                             _top(0), _top(1), _top(2) );
}

void NORM_NewOctreeV2::debugFlipAcpBasis()
{
    _root->debugFlipAcpAndSurfaceRecursive();
}

void NORM_NewOctreeV2::orientSurfaces(int nSamplePoints,
                                      CT_PointsAttributesScalarTemplated<int>*& debugAttribut,
                                      int debugNumAttribut)
{
    _root->setFlagRecursive( false );
    _root->orientAllNodesRecursive( nSamplePoints, debugAttribut, debugNumAttribut );
}

void NORM_NewOctreeV2::getNodeContainingPoint(const CT_Point& querryPoint,
                                              NORM_NewOctreeNodeV2*& outputNode,
                                              int& outLevelOfNode,
                                              int level)
{
    if( !boundingCubeContainsPoint(querryPoint) )
    {
        outputNode = NULL;
        outLevelOfNode = -1;
        return;
    }

    int maxLevel = std::numeric_limits<int>::max();
    if( level >=0 )
    {
        maxLevel = level;
    }

    _root->getNodeContainingPointRecursive( querryPoint,
                                            outputNode,
                                            outLevelOfNode,
                                            0,
                                            maxLevel );
}

void NORM_NewOctreeV2::computeNeighboursOfLeaves()
{
    _root->getLeavesNeighboursRecursive();
}

void NORM_NewOctreeV2::draw(GraphicsViewInterface& view,
                            PainterInterface& painter,
                            bool drawBoxes,
                            bool drawCentroid,
                            bool drawPcaV3,
                            double sizeV3 ) const
{
    _root->drawRecursive( view,
                          painter,
                          0,
                          drawBoxes,
                          drawCentroid,
                          drawPcaV3,
                          sizeV3 );
}

CT_PointsAttributesScalarTemplated<int>* NORM_NewOctreeV2::getPointsIdAttribute(QString modelName,
                                                                                CT_AbstractResult* result,
                                                                                CT_PCIR pointCloudIndexRegistered )
{
    CT_PointsAttributesScalarTemplated<int>* rslt = new CT_PointsAttributesScalarTemplated<int>( modelName,
                                                                                                 result,
                                                                                                 pointCloudIndexRegistered );

    // On remplit les valeurs d'id
    _root->setIdAttributeRecursive( rslt, 0 );

    // BBox pour affichage (rencentrer camera)
    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

CT_PointsAttributesScalarTemplated<float>* NORM_NewOctreeV2::getPointsSigmaAttribute(QString modelName,
                                                                                     CT_AbstractResult* result,
                                                                                     CT_PCIR pointCloudIndexRegistered)
{
    CT_PointsAttributesScalarTemplated<float>* rslt = new CT_PointsAttributesScalarTemplated<float>( modelName,
                                                                                                     result,
                                                                                                     pointCloudIndexRegistered );

    // On remplit les valeurs d'id
    _root->setSigmaAttributeRecursive( rslt );

    // BBox pour affichage (rencentrer camera)
    rslt->setBoundingBox( _bot(0), _bot(1), _bot(2),
                          _top(0), _top(1), _top(2) );

    return rslt;
}

void NORM_NewOctreeV2::getPcaAttributes(CT_PointsAttributesScalarTemplated<float>* outCreatedL1Attribute,
                                        CT_PointsAttributesScalarTemplated<float>* outCreatedL2Attribute,
                                        CT_PointsAttributesScalarTemplated<float>* outCreatedL3Attribute,
                                        CT_PointsAttributesScalarTemplated<float>* outCreatedSigmaAttribute,
                                        CT_PointsAttributesScalarTemplated<float>* outCreatedL3L2Attribute)
{
    // On remplit les valeurs de sigma
    _root->setPcaAttributesRecursive( outCreatedL1Attribute,
                                      outCreatedL2Attribute,
                                      outCreatedL3Attribute,
                                      outCreatedSigmaAttribute,
                                      outCreatedL3L2Attribute);

    // BBox pour affichage (rencentrer camera)
    outCreatedL1Attribute->setBoundingBox( _bot(0), _bot(1), _bot(2),
                                           _top(0), _top(1), _top(2) );
    outCreatedL2Attribute->setBoundingBox( _bot(0), _bot(1), _bot(2),
                                           _top(0), _top(1), _top(2) );
    outCreatedL3Attribute->setBoundingBox( _bot(0), _bot(1), _bot(2),
                                           _top(0), _top(1), _top(2) );
    outCreatedSigmaAttribute->setBoundingBox( _bot(0), _bot(1), _bot(2),
                                              _top(0), _top(1), _top(2) );
    outCreatedL3L2Attribute->setBoundingBox( _bot(0), _bot(1), _bot(2),
                                             _top(0), _top(1), _top(2) );
}

bool NORM_NewOctreeV2::boundingCubeContainsPoint(const CT_Point& p)
{
    if( p.x() < _bot.x() || p.y() < _bot.y() || p.z() < _bot.z() ||
        p.x() > _top.x() || p.y() > _top.y() || p.z() > _top.z() )
    {
        return false;
    }
    return true;
}

void NORM_NewOctreeV2::saveToFile(QString fileName)
{
    QFile file( fileName );
    if( !file.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Erreur lors de l'ouverture du fichier" << fileName << "pour enregistrement de l'octree";
        return;
    }

    QTextStream stream( &file );

    stream << _bot.x() << " " << _bot.y() << " " << _bot.z() << " "
           << _top.x() << " " << _top.y() << " " << _top.z()
           << "\n";

    _root->saveToFileRecursive( stream );

    file.close();
}

NORM_NewOctreeV2* NORM_NewOctreeV2::loadFromFile(QString fileName,
                                                 const QString& modelName,
                                                 const CT_AbstractResult* result )
{
    QFile file( fileName );
    if( !file.open( QIODevice::ReadOnly ) )
    {
        qDebug() << "Erreur lors de l'ouverture du fichier" << fileName << "pour chargement de l'octree";
        return NULL;
    }

    NORM_NewOctreeV2* rslt = new NORM_NewOctreeV2( modelName,
                                                   result );
    QTextStream stream( &file );

    // On lit la ligne d'entete qui contient le bounding cube de l'octree
    QString line;
    QStringList listWords;
    line = stream.readLine();
    listWords = line.split(" ", QString::SkipEmptyParts);

    if( listWords.size() != 6 )
    {
        qDebug() << "Erreur il manque des informations a la ligne d'entete";
        qDebug() << "Ligne lue" << line;
        qDebug() << "Soit " << listWords.size() << " champs (6 attendus)";
        return NULL;
    }

    rslt->_bot.x() = listWords.at(0).toFloat();
    rslt->_bot.y() = listWords.at(1).toFloat();
    rslt->_bot.z() = listWords.at(2).toFloat();
    rslt->_top.x() = listWords.at(3).toFloat();
    rslt->_top.y() = listWords.at(4).toFloat();
    rslt->_top.z() = listWords.at(5).toFloat();

    float dimension = rslt->_top.x() - rslt->_bot.x();
    CT_Point centre;
    for( int i = 0 ; i < 3 ; i++ )
    {
        centre[i] = rslt->_bot[i] + (dimension / 2.0);
    }

    // On commence la creation des noeuds de l'octree
    int toto = 2;
    rslt->_root = NORM_NewOctreeNodeV2::loadRecursive( stream,
                                                       centre,
                                                       dimension,
                                                       0,
                                                       NULL,
                                                       rslt,
                                                       toto );

    file.close();

    return rslt;
}

void NORM_NewOctreeV2::getProjectedPointsFromFile(CT_AbstractUndefinedSizePointCloud* outProjectedPoints)
{
    _root->getProjectedPointsFromFileRecursive( outProjectedPoints );
}

void NORM_NewOctreeV2::printAllcentre()
{
    _root->printCentreRecursive();
}

int NORM_NewOctreeV2::nbPointsMinForPca() const
{
    return _nbPointsMinForPca;
}

void NORM_NewOctreeV2::setNbPointsMinForPca(int nbPointsMin)
{
    _nbPointsMinForPca = nbPointsMin;
}

float NORM_NewOctreeV2::sigma3Max() const
{
    return _sigma3Max;
}

void NORM_NewOctreeV2::setSigma3Max(float sigma3Max)
{
    _sigma3Max = sigma3Max;
}

float NORM_NewOctreeV2::getDimensionMaxForPca() const
{
    return _dimensionMaxForPca;
}

void NORM_NewOctreeV2::setDimensionMaxForPca(float dimensionMaxForPca)
{
    _dimensionMaxForPca = dimensionMaxForPca;
}

NORM_NewOctreeNodeV2* NORM_NewOctreeV2::getRoot() const
{
    return _root;
}

void NORM_NewOctreeV2::setRoot(NORM_NewOctreeNodeV2* root)
{
    _root = root;
}

CT_Scene* NORM_NewOctreeV2::getScene() const
{
    return _scene;
}

void NORM_NewOctreeV2::setScene(CT_Scene* scene)
{
    _scene = scene;
}

float NORM_NewOctreeV2::getSigma2Min() const
{
    return _sigma2Min;
}

void NORM_NewOctreeV2::setSigma2Min(float sigma2Min)
{
    _sigma2Min = sigma2Min;
}

float NORM_NewOctreeV2::getMaxRMSE() const
{
    return _maxRMSE;
}

void NORM_NewOctreeV2::setMaxRMSE(float maxRMSE)
{
    _maxRMSE = maxRMSE;
}

float NORM_NewOctreeV2::dimensionMin() const
{
    return _dimensionMin;
}

void NORM_NewOctreeV2::setDimensionMin(float dimensionMin)
{
    _dimensionMin = dimensionMin;
}
