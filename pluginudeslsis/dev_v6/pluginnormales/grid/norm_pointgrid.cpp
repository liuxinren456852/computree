#include "norm_pointgrid.h"

#include "norm_pointgriddrawmanager.h"

const NORM_PointGridDrawManager NORM_PointGrid::POINT_GRID_DRAW_MANAGER;

NORM_PointGrid::NORM_PointGrid()
    : CT_AbstractItemDrawableWithoutPointCloud(),
      _firstImage(NULL),
      _lastImage(NULL)
{
}

NORM_PointGrid::NORM_PointGrid(const CT_OutAbstractSingularItemModel *model,
                               const CT_AbstractResult *result,
                               double xmin,
                               double ymin,
                               double zmin,
                               size_t dimx,
                               size_t dimy,
                               size_t dimz,
                               double resolution)
    : CT_AbstractItemDrawableWithoutPointCloud(model, result),
      _firstImage( new CT_Grid3D<size_t>(model, result,
                                         xmin, ymin, zmin,
                                         dimx, dimy, dimz,
                                         resolution,
                                         std::numeric_limits<size_t>::max(), std::numeric_limits<size_t>::max() ) ),
      _lastImage( new CT_Grid3D<size_t>(model, result,
                                        xmin, ymin, zmin,
                                        dimx, dimy, dimz,
                                        resolution,
                                        std::numeric_limits<size_t>::max(), std::numeric_limits<size_t>::max() ) )
{
}

NORM_PointGrid::NORM_PointGrid(const QString& model,
                               const CT_AbstractResult *result,
                               double xmin,
                               double ymin,
                               double zmin,
                               size_t dimx,
                               size_t dimy,
                               size_t dimz,
                               double resolution)
    : CT_AbstractItemDrawableWithoutPointCloud(model, result),
      _firstImage( new CT_Grid3D<size_t>(model, result,
                                         xmin, ymin, zmin,
                                         dimx, dimy, dimz,
                                         resolution,
                                         std::numeric_limits<size_t>::max(), std::numeric_limits<size_t>::max() ) ),
      _lastImage( new CT_Grid3D<size_t>(model, result,
                                        xmin, ymin, zmin,
                                        dimx, dimy, dimz,
                                        resolution,
                                        std::numeric_limits<size_t>::max(), std::numeric_limits<size_t>::max() ) )
{
}
