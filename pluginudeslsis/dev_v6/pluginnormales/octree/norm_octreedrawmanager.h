#ifndef NORM_OCTREEDRAWMANAGER_H
#define NORM_OCTREEDRAWMANAGER_H

// Inherits from this class
#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractitemdrawablewithoutpointclouddrawmanager.h"

class NORM_OctreeDrawManager : public CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager
{
public:
//********************************************//
//         Constructors/Destructors           //
//********************************************//
    /*!
     * \brief NORM_OctreeDrawManager
     *
     * Constructor of the NORM_OctreeDrawManager class
     *
     * \param drawConfigurationName : name of the configuration
     */
    NORM_OctreeDrawManager(QString drawConfigurationName = "");

//********************************************//
//                Drawing tools               //
//********************************************//
    /*!
     * \brief draw
     *
     * Draw an octree : draw each of the octree nodes box
     *
     * \param view : graphic view
     * \param painter : painter to paint objects in the view
     * \param itemDrawable : octree to be drawn
     */
    virtual void draw(GraphicsViewInterface& view,
                      PainterInterface& painter,
                      const CT_AbstractItemDrawable& itemDrawable) const;

protected :
// ******************************************** //
// Configuration tools                          //
// Member attributes                            //
// ******************************************** //
    /*!
     * \brief createDrawConfiguration
     *
     * Creates the draw configuration menu (displayed in the draw configuration window in computree)
     *
     * \param drawConfigurationName : draw configuration name
     *
     * \return the draw configuration
     */
    virtual CT_ItemDrawableConfiguration createDrawConfiguration(QString drawConfigurationName) const;
};

#endif // NORM_OCTREEDRAWMANAGER_H
