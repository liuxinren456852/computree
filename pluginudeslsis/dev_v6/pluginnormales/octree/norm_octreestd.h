/************************************************************************************
* Filename :  norm_octreestd.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef NORM_OCTREESTD_H
#define NORM_OCTREESTD_H

#include "norm_octree.h"
#include "norm_octreenode.h"

// Juste pour que les types de noeud et le type d'octree soient en conconrdance
class NORM_OctreeStd : public NORM_Octree
{
public:
    NORM_OctreeStd();

    NORM_OctreeStd(float minNodeSize,
                    size_t minNbPoints,
                    CT_Point bot,
                    CT_Point top,
                    CT_PCIR inputIndexCloud);

    NORM_OctreeStd(float minNodeSize,
                    size_t minNbPoints,
                    CT_Point bot,
                    CT_Point top,
                    CT_PCIR inputIndexCloud,
                    const CT_OutAbstractSingularItemModel *model,
                    const CT_AbstractResult *result );

    NORM_OctreeStd( float minNodeSize,
                    size_t minNbPoints,
                    CT_Point bot,
                    CT_Point top,
                    CT_PCIR inputIndexCloud,
                    const QString &modelName,
                    const CT_AbstractResult *result);

    virtual void initRoot();

    NORM_OctreeNode* nodeContainingPoint( CT_Point& p ) const;

    NORM_OctreeNode* nodeAtCode( size_t codex,
                                 size_t codey,
                                 size_t codez ) const;

    NORM_OctreeNode* nodeAtCodeAndMaxDepth( size_t codex,
                                            size_t codey,
                                            size_t codez,
                                            size_t maxDepth ) const;

    NORM_OctreeNode* deepestNodeContainingBBBox(const CT_Point &bot,
                                                const CT_Point &top) const;

    NORM_OctreeNode* root() const;

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model,
                                          const CT_AbstractResult *result,
                                          CT_ResultCopyModeList copyModeList )
    {
        Q_UNUSED( copyModeList );
        CT_PCIR foopcir;
        CT_Point foobot;
        CT_Point footop;
        return new NORM_OctreeStd( 0, 0, foobot, footop, foopcir );
    }
};

#endif // NORM_OCTREESTD_H
