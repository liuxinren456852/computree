#include "norm_standardspheredrawmanager.h"

#include "norm_sphere.h"

NORM_StandardSphereDrawManager::NORM_StandardSphereDrawManager(QString drawConfigurationName)
    : CT_StandardAbstractShapeDrawManager(drawConfigurationName.isEmpty() ? "NORM_Sphere" : drawConfigurationName)
{
}

void NORM_StandardSphereDrawManager::draw(GraphicsViewInterface &view,
                                          PainterInterface &painter,
                                          const CT_AbstractItemDrawable &itemDrawable) const
{
    Q_UNUSED(view);
    const NORM_Sphere &item = dynamic_cast<const NORM_Sphere&>(itemDrawable);

    painter.drawPartOfSphere( item.getCenter()(0),
                              item.getCenter()(1),
                              item.getCenter()(2),
                              item.getRadius(),
                              0, 360,
                              0, 180,
                              false );
}
