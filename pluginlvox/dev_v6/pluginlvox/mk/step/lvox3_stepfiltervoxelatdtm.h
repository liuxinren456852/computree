#ifndef LVOX3_STEPFILTERVOXELATDTM_H
#define LVOX3_STEPFILTERVOXELATDTM_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

#include "../tools/3dgrid/abstract/lvox3_abstractgrid3d.h"

class LVOX3_StepFilterVoxelAtDTM: public CT_AbstractStep
{
    Q_OBJECT
public:
    LVOX3_StepFilterVoxelAtDTM(CT_StepInitializeData &dataInit);

    /**
     * @brief Return a short description of what do this class
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;
    /**
     * @brief Return a new empty instance of this class
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);
protected:
    /**
     * @brief This method defines what kind of input the step can accept
     */
    void createInResultModelListProtected();

    /**
     * @brief This method defines what kind of output the step produces
     */
    void createOutResultModelListProtected();

    /**
     * @brief This method do the job
     */
    void compute();
private:

    CT_AutoRenameModels _grid_ModelName; /* outGrid model name*/

    //Test to see if any part of the voxel is above the DTM level
    bool evaluateVoxel(Eigen::Vector3d centerCoords, double gridResolutionZ, float mntZ);
};

#endif // LVOX3_STEPFILTERVOXELATDTM_H
