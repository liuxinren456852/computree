#ifndef LVOX3_STEPCREATEMESH_H
#define LVOX3_STEPCREATEMESH_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

#include "../tools/3dgrid/abstract/lvox3_abstractgrid3d.h"

class LVOX3_StepCreateMesh: public CT_AbstractStep
{
    Q_OBJECT
public:

    //Pre configure of the step
    enum isMeshType {
        WithMesh = 0,
        NoMesh
    };
    //Post configure of the step
    enum meshType {
        ConvexMesh = 0,
        ConcaveMesh
    };

    LVOX3_StepCreateMesh(CT_StepInitializeData &dataInit);

    /**
     * @brief Return a short description of what do this class
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;
    /**
     * @brief Return a new empty instance of this class
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);
protected:
    /**
     * @brief This method defines what kind of input the step can accept
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPreConfigurationDialog();

    /**
     * @brief Show the post configuration dialog.
     *
     * If you want to show your own configuration dialog your must overload this method and show your dialog when this method is called. Don't forget
     * to call the method "setSettingsModified(true)" if your settings is modified (if user accept your dialog).
     *
     * @return true if the settings was modified.
     */
    void createPostConfigurationDialog();

    /**
     * @brief This method defines what kind of output the step produces
     */
    void createOutResultModelListProtected();

    /**
     * @brief This method do the job
     */
    void compute();
private:
    QHash<QString, isMeshType> m_isMeshTypeCollection;
    QString m_isMeshType;

    QHash<QString, meshType> m_meshTypeCollection;
    QString m_meshType;

    CT_AutoRenameModels _grid_ModelName;
    CT_AutoRenameModels _gridAboveValue_ModelName;
    CT_AutoRenameModels _gridSurface_ModelName;
};

#endif // LVOX3_STEPCREATEMESH_H
