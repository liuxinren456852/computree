/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_METRICRASTEREXTEND_H
#define ONF_METRICRASTEREXTEND_H

#include "ctlibmetrics/ct_metric/abstract/ct_abstractmetric_raster.h"
#include "ctlibmetrics/tools/ct_valueandbool.h"

class ONF_MetricRasterExtend : public CT_AbstractMetric_Raster
{
    Q_OBJECT
public:

    struct Config {
        VaB<double>      xmin;
        VaB<double>      xmax;
        VaB<double>      xsize;
        VaB<double>      ymin;
        VaB<double>      ymax;
        VaB<double>      ysize;
        VaB<double>      min;
        VaB<double>      max;
        VaB<double>      na;
    };

    ONF_MetricRasterExtend();
    ONF_MetricRasterExtend(const ONF_MetricRasterExtend &other);

    QString getShortDescription() const;
    QString getDetailledDescription() const;

    /**
     * @brief Returns the metric configuration
     */
    ONF_MetricRasterExtend::Config metricConfiguration() const;

    /**
     * @brief Change the configuration of this metric
     */
    void setMetricConfiguration(const ONF_MetricRasterExtend::Config &conf);

    CT_AbstractConfigurableElement* copy() const;

protected:
    void computeMetric();
    void declareAttributes();

private:
    Config  m_configAndResults;
};


#endif // ONF_METRICRASTEREXTEND_H
