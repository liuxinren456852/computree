/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "onf_stepcomputehitgrid.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of actions methods
#include "ct_tools/model/ct_outmodelcopyactionaddmodelitemingroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"
#include "ct_iterator/ct_pointiterator.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include <QFileInfo>
#include <QDebug>
#include <limits>

#define DEF_SearchInResult "r"
#define DEF_SearchInScene   "sc"
#define DEF_SearchInGroup   "gr"

#define DEF_itemOut_grxy "grxy"
#define DEF_itemOut_grxz "grxz"
#define DEF_itemOut_gryz "gryz"

ONF_StepComputeHitGrid::ONF_StepComputeHitGrid(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _res = 0.5;
    _gridMode = 1;
    _xBase = 0;
    _yBase = 0;
    _zBase = 0;

}

QString ONF_StepComputeHitGrid::getStepDescription() const
{
    // Gives the descrption to print in the GUI
    return tr("Créer grille 3D de densité de points");
}

// Step description (tooltip of contextual menu)
QString ONF_StepComputeHitGrid::getStepDetailledDescription() const
{
    return tr("Cette étape génère une grille 3D à la <b>résolution</b> spécifiée.<br>"
              "Chaque case reçoit le nombre de points de la scène d'entrée qu'elle contient.");
}

CT_VirtualAbstractStep* ONF_StepComputeHitGrid::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new ONF_StepComputeHitGrid(dataInit);
}

void ONF_StepComputeHitGrid::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Scène(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_Scene::staticGetType(), tr("Scène"));
}

void ONF_StepComputeHitGrid::createOutResultModelListProtected()
{    
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL)
        res->addItemModel(DEF_SearchInGroup, _hits_ModelName, new CT_Grid3D_Sparse<int>(), tr("Hits"));
}

void ONF_StepComputeHitGrid::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Résolution de la grille"),tr("meters"),0.0001,10000,3, _res );

    configDialog->addText(tr("Callage du coin (minX, minY, minZ) :"),"", "");

    CT_ButtonGroup &bg_gridMode = configDialog->addButtonGroup(_gridMode);
    configDialog->addExcludeValue("", "", tr("Sur la boite englobante de la scène"), bg_gridMode, 0);
    configDialog->addExcludeValue("", "", tr("Par rapport aux coordonnées suivantes :"), bg_gridMode, 1);

    configDialog->addDouble(tr("Coordonnée X :"), "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _xBase);
    configDialog->addDouble(tr("Coordonnée Y :"), "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _yBase);
    configDialog->addDouble(tr("Coordonnée Z :"), "", -std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), 4, _zBase);
}

void ONF_StepComputeHitGrid::compute()
{
    // Gets the out result
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
    // iterate over all groups
    while(!isStopped() && it.hasNext())
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*)it.next();
        const CT_Scene* scene = (CT_Scene*)group->firstItemByINModelName(this, DEF_SearchInScene);

        if (scene!=NULL)
        {
            double minX = _xBase;
            double minY = _yBase;
            double minZ = _zBase;

            if (_gridMode == 0)
            {
                minX = scene->minX();
                minY = scene->minY();
                minZ = scene->minZ();
            } else {

                while (minX < scene->minX()) {minX += _res;};
                while (minY < scene->minY()) {minY += _res;};
                while (minZ < scene->minZ()) {minZ += _res;};

                while (minX > scene->minX()) {minX -= _res;};
                while (minY > scene->minY()) {minY -= _res;};
                while (minZ > scene->minZ()) {minZ -= _res;};
            }

            // Declaring the output grids
            CT_Grid3D_Sparse<int>* hitGrid = CT_Grid3D_Sparse<int>::createGrid3DFromXYZCoords(_hits_ModelName.completeName(), outResult,
                                                                                minX, minY, minZ,
                                                                                scene->maxX(), scene->maxY(), scene->maxZ(),
                                                                                _res, -1, 0);

            CT_PointIterator itP(scene->getPointCloudIndex()) ;
            while(itP.hasNext())
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t indice;

                if (hitGrid->indexAtXYZ(point(0), point(1), point(2), indice))
                {
                    // Hits Computing
                    hitGrid->addValueAtIndex(indice, 1);
                } else {
                    qDebug() << "Le point n'est pas dans la grille";
                }
            }

            hitGrid->computeMinMax();
            group->addItemDrawable(hitGrid);
        }
    }


    setProgress(99);
}


