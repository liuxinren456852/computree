/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_ACTIONSELECTSCENEFOREACHPOSITIONOPTIONS_H
#define ONF_ACTIONSELECTSCENEFOREACHPOSITIONOPTIONS_H

#include "ct_view/actions/abstract/ct_gabstractactionforgraphicsviewoptions.h"

class ONF_ActionSelectSceneForEachPosition;

namespace Ui {
class ONF_ActionSelectSceneForEachPositionOptions;
}

class ONF_ActionSelectSceneForEachPositionOptions : public CT_GAbstractActionForGraphicsViewOptions
{
    Q_OBJECT

public:

    explicit ONF_ActionSelectSceneForEachPositionOptions(const ONF_ActionSelectSceneForEachPosition *action);
    ~ONF_ActionSelectSceneForEachPositionOptions();

    void setSelectionWidget(QWidget* wid);
    void clearSelectionWidget(QWidget *wid);

    double defaultHeight() const;
    bool circles() const;
    int highlightedNumberOfTrees() const;
    bool colorUnmatched() const;
    void setSegmentMode(bool segmentMode);
    bool hideNotSelected() const;
    double visibleDist() const;

private:
    Ui::ONF_ActionSelectSceneForEachPositionOptions *ui;


signals:
    void parametersChanged();
    void addCluster();
    void removeCluster();
    void switchCluster();
    void updateColorization();
    void initColors();
    void beginSegmentCluster();
    void endSegmentCluster();
    void hideNotSelectedToggled();

private slots:
    void on_cb_circles_toggled(bool checked);
    void on_dsb_height_valueChanged(double arg1);
    void on_sb_highlightedNumber_valueChanged(int arg1);
    void on_tb_add_clicked();
    void on_tb_remove_clicked();
    void on_tb_switch_clicked();
    void on_cb_colorUnmatched_toggled(bool checked);
    void on_tb_initColors_clicked();
    void on_tb_segment_clicked();
    void on_tb_finishSegmentMode_clicked();
    void on_cb_hideNotSelected_toggled(bool checked);
    void on_dsb_visibleRadius_valueChanged(double arg1);
};

#endif // ONF_ACTIONSELECTSCENEFOREACHPOSITIONOPTIONS_H
