#include "onf_countvisitor.h"
#include "Eigen/Core"

ONF_CountVisitor::ONF_CountVisitor(CT_Grid3D_Sparse<int> *grid)
{
  _grid = grid;
}

void ONF_CountVisitor::visit(const size_t &index, const CT_Beam *beam)
{
    Q_UNUSED(beam);

    _grid->addValueAtIndex(index, 1);
}
