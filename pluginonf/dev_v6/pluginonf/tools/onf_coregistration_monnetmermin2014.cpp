#include "onf_coregistration_monnetmermin2014.h"

ONF_CoRegistration_MonnetMermin2014::ONF_CoRegistration_MonnetMermin2014()
{

}

void ONF_CoRegistration_MonnetMermin2014::ONF_CoRegistration_MonnetMermin2014::computeTranslation(CT_Image2D<float> *dhm, QList<ONF_CoRegistration_MonnetMermin2014> treePositions, double &centerX, double &centerY, double radius)
{
    double baseCenterX = centerX;
    double baseCenterY = centerY;

    double res = dhm->resolution();

    double minX = dhm->minX();
    double minY = dhm->minY();
    double maxX = dhm->maxX();
    double maxY = dhm->maxX();

    CT_Image2D<bool>* mask = CT_Image2D<bool>::createImage2DFromXYCoords(NULL, NULL, minX, minY, maxX, maxY, res, 0, false, false);
    CT_Image2D<float>* trees = CT_Image2D<float>::createImage2DFromXYCoords(NULL, NULL, minX, minY, maxX, maxY, res, 0, -1, 0);





    delete mask;
    delete trees;
}
