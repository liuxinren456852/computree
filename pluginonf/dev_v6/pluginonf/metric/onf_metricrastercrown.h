/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_METRICRASTERCROWN_H
#define ONF_METRICRASTERCROWN_H

#include "ctlibmetrics/ct_metric/abstract/ct_abstractmetric_raster.h"
#include "ctlibmetrics/tools/ct_valueandbool.h"

class ONF_MetricRasterCrown : public CT_AbstractMetric_Raster
{
    Q_OBJECT
public:

    struct Config {
        VaB<double>      volume;
        VaB<double>      volume_topQ25;
        VaB<double>      volume_topQ50;
        VaB<double>      volume_topQ75;
        VaB<double>      slope_max;
        VaB<double>      slope_min;
        VaB<double>      slope_moy;
        VaB<double>      slope_sd;
        VaB<double>      slope_Q25;
        VaB<double>      slope_Q50;
        VaB<double>      slope_Q75;
        VaB<double>      rumple;
        VaB<double>      area3d;
        VaB<double>      area2d;
        VaB<double>      crownThickness;
        VaB<double>      crownThicknessQ25;
        VaB<double>      crownThicknessQ50;
        VaB<double>      crownThicknessQ75;
        VaB<double>      crownThicknessQ95;
        VaB<double>      crownEquivRadius;

    };

    ONF_MetricRasterCrown();
    ONF_MetricRasterCrown(const ONF_MetricRasterCrown &other);

    QString getShortDescription() const;
    QString getDetailledDescription() const;

    /**
     * @brief Returns the metric configuration
     */
    ONF_MetricRasterCrown::Config metricConfiguration() const;

    /**
     * @brief Change the configuration of this metric
     */
    void setMetricConfiguration(const ONF_MetricRasterCrown::Config &conf);

    CT_AbstractConfigurableElement* copy() const;

protected:
    void computeMetric();
    void declareAttributes();

private:
    Config  m_configAndResults;


    double computePercentile(const QList<double> &array, const double &p);
};


#endif // ONF_METRICRASTERCROWN_H
