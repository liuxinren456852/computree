/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_METRICPOINTCROWNSHAPE_H
#define ONF_METRICPOINTCROWNSHAPE_H

#include "ctlibmetrics/ct_metric/abstract/ct_abstractmetric_xyz.h"
#include "ctlibmetrics/tools/ct_valueandbool.h"

#include "Eigen/Core"

class ONF_MetricPointCrownShape : public CT_AbstractMetric_XYZ
{
    Q_OBJECT
public:

    struct Config {
        VaB<double>      _X_Apex;
        VaB<double>      _Y_Apex;
        VaB<double>      _Z_Apex;
        VaB<double>      _NbPts_inf0_5m;
        VaB<double>      _NbPts_inf1m;
        VaB<double>      _NbPts_inf2m;
        VaB<double>      _NbPts_inf3m;
        VaB<double>      _NbPts_inf4m;
        VaB<double>      _NbPts_inf5m;
        VaB<double>      _MeanDistZ_inf0_5m;
        VaB<double>      _MeanDistZ_inf1m;
        VaB<double>      _MeanDistZ_inf2m;
        VaB<double>      _MeanDistZ_inf3m;
        VaB<double>      _MeanDistZ_inf4m;
        VaB<double>      _MeanDistZ_inf5m;
        VaB<double>      _MeanAngle_inf0_5m;
        VaB<double>      _MeanAngle_inf1m;
        VaB<double>      _MeanAngle_inf2m;
        VaB<double>      _MeanAngle_inf3m;
        VaB<double>      _MeanAngle_inf4m;
        VaB<double>      _MeanAngle_inf5m;
        VaB<double>      _MeanDistXY_inf0_5m;
        VaB<double>      _MeanDistXY_inf1m;
        VaB<double>      _MeanDistXY_inf2m;
        VaB<double>      _MeanDistXY_inf3m;
        VaB<double>      _MeanDistXY_inf4m;
        VaB<double>      _MeanDistXY_inf5m;

        VaB<double>      _DistXY_0_5m;
        VaB<double>      _DistXY_1m;
        VaB<double>      _DistXY_2m;
        VaB<double>      _DistXY_3m;
        VaB<double>      _DistXY_4m;
        VaB<double>      _DistXY_5m;
        VaB<double>      _Slope_0_5m;
        VaB<double>      _Slope_1m;
        VaB<double>      _Slope_2m;
        VaB<double>      _Slope_3m;
        VaB<double>      _Slope_4m;
        VaB<double>      _Slope_5m;
        VaB<double>      _Convexity_0_5m;
        VaB<double>      _Convexity_1m;
        VaB<double>      _Convexity_2m;
        VaB<double>      _Convexity_3m;
        VaB<double>      _Convexity_4m;
        VaB<double>      _AreaUnderCurve_1m;
        VaB<double>      _AreaUnderCurve_2m;
        VaB<double>      _AreaUnderCurve_3m;
        VaB<double>      _AreaUnderCurve_4m;
        VaB<double>      _AreaUnderCurve_5m;
    };

    ONF_MetricPointCrownShape();
    ONF_MetricPointCrownShape(const ONF_MetricPointCrownShape &other);

    QString getShortDescription() const;
    QString getDetailledDescription() const;

    /**
     * @brief Returns the metric configuration
     */
    ONF_MetricPointCrownShape::Config metricConfiguration() const;

    /**
     * @brief Change the configuration of this metric
     */
    void setMetricConfiguration(const ONF_MetricPointCrownShape::Config &conf);

    CT_AbstractConfigurableElement* copy() const;

protected:
    void computeMetric();
    void declareAttributes();

private:
    Config  m_configAndResults;
};


#endif // ONF_METRICPOINTCROWNSHAPE_H
