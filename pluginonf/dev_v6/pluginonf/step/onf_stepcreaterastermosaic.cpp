#include "onf_stepcreaterastermosaic.h"

ONF_StepCreateRasterMosaic::ONF_StepCreateRasterMosaic() : SuperClass()
{
}

ONF_StepCreateRasterMosaic::~ONF_StepCreateRasterMosaic()
{
}

QString ONF_StepCreateRasterMosaic::description() const
{
    return tr("Redallage raster");
}

QString ONF_StepCreateRasterMosaic::detailledDescription() const
{
    return tr("Créer des rasters sur les emprises fournies, à partir des rasters d'entrée. Conserve la résolution des rasters d'entrée. ");
}

CT_VirtualAbstractStep* ONF_StepCreateRasterMosaic::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepCreateRasterMosaic();
}

//////////////////// PROTECTED //////////////////

void ONF_StepCreateRasterMosaic::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Emprises cibles"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inItemArea, tr("Emprise cible"));

    manager.addResult(_inResultRaster, tr("Rasters d'entrée"));
    manager.setZeroOrMoreRootGroup(_inResultRaster, _inZeroOrMoreRootGroupRaster);
    manager.addGroup(_inZeroOrMoreRootGroupRaster, _inGroupRaster);
    manager.addItem(_inGroupRaster, _inRaster, tr("Rasters d'entrée"));
}

// Redefine in children steps to complete ConfigurationDialog
void ONF_StepCreateRasterMosaic::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
}

// Redefine in children steps to complete out Models
void ONF_StepCreateRasterMosaic::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outRaster, tr("Raster"));
}

// Redefine in children steps to complete compute method
void ONF_StepCreateRasterMosaic::compute()
{
    bool rasternullptr = true;
    _min(0) = std::numeric_limits<double>::max();
    _min(1) = std::numeric_limits<double>::max();

    for (const CT_AbstractImage2D* rasterIn : _inRaster.iterateInputs(_inResultRaster))
    {
        rasternullptr = false;

        Eigen::Vector2d minTmp;
        rasterIn->getMinCoordinates(minTmp);

        if (minTmp(0) < _min(0)) {_min(0) = minTmp(0);}
        if (minTmp(1) < _min(1)) {_min(1) = minTmp(1);}
        _resolution = rasterIn->resolution();

    }

    if (rasternullptr) {return;}

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractSingularItemDrawable* areaIn : group->singularItems(_inItemArea))
        {
            if (isStopped()) {return;}

            Eigen::Vector3d minArea, maxArea;
            double minRasterX, minRasterY, maxRasterX, maxRasterY;

            int dimx = 0;
            int dimy = 0;

            areaIn->boundingBox(minArea, maxArea);

            minRasterX = _min(0);
            minRasterY = _min(1);

            while (minRasterX < minArea(0)) {minRasterX += _resolution;}
            while (minRasterY < minArea(1)) {minRasterY += _resolution;}

            maxRasterX = minRasterX;
            maxRasterY = minRasterY;

            while (maxRasterX < maxArea(0)) {maxRasterX += _resolution;++dimx;}
            while (maxRasterY < maxArea(1)) {maxRasterY += _resolution;++dimy;}

            double refLevel = 0.0;
            float naVal = -std::numeric_limits<float>::max();
            CT_Image2D<float>* rasterOut = new CT_Image2D<float>(minRasterX, minRasterY, dimx, dimy, _resolution, refLevel, naVal, naVal);

            for (const CT_AbstractImage2D* rasterIn : _inRaster.iterateInputs(_inResultRaster))
            {
                for (int xx = 0 ; xx < dimx ; xx++)
                {
                    for (int yy = 0 ; yy < dimy ; yy++)
                    {
                        Eigen::Vector3d center;
                        size_t index;
                        rasterOut->getCellCenterCoordinates(xx, yy, center);

                        if (rasterIn->indexAtCoords(center(0), center(1), index))
                        {
                            double val = rasterIn->valueAtIndexAsDouble(index);
                            if (val != rasterIn->NAAsDouble())
                            {
                                rasterOut->setValue(xx, yy, (float)val);
                            }
                        }
                    }
                }

            }

            rasterOut->computeMinMax();
            group->addSingularItem(_outRaster, rasterOut);
        }
    }

    setProgress( 100 );
}

