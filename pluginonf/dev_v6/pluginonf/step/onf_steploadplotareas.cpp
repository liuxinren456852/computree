/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_steploadplotareas.h"

#include "ct_view/ct_asciifilechoicebutton.h"
#include "ct_view/ct_combobox.h"
#include "ct_log/ct_logmanager.h"

#include <QFile>
#include <QTextStream>

ONF_StepLoadPlotAreas::ONF_StepLoadPlotAreas() : CT_AbstractStepCanBeAddedFirst()
{
    _neededFields.append(CT_TextFileConfigurationFields("ID_Plot", QRegExp("([iI][dD]|[nN][uU][mM]|[pP][lL][oO][tT]|[pP][lL][aA][cC][eE][tT][tT][eE])"), false));
    _neededFields.append(CT_TextFileConfigurationFields("X", QRegExp("[xX]"), false));
    _neededFields.append(CT_TextFileConfigurationFields("Y", QRegExp("[yY]"), false));
    _neededFields.append(CT_TextFileConfigurationFields("R (m)", QRegExp("([rR][aA][dD]|[iI][uU][sS]|[rR][aA][yY]|[oO][nN]|[R])"), true));

    _refFileName = "";
    _refHeader = true;
    _refSeparator = "\t";
    _refDecimal = ".";
    _refLocale = QLocale(QLocale::English, QLocale::UnitedKingdom).name();
    _refSkip = 0;

    _plotID = "";

    _defaultRadius = 15.0;
}

QString ONF_StepLoadPlotAreas::description() const
{
    return tr("Fichier ASCII contenant le centre de placettes circulaires");
}

QString ONF_StepLoadPlotAreas::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepLoadPlotAreas::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepLoadPlotAreas::createNewInstance() const
{
    return new ONF_StepLoadPlotAreas();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepLoadPlotAreas::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Entête de fichier"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inHeader, tr("Entête de fichier"));
}

void ONF_StepLoadPlotAreas::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _areaModelName, tr("Emprise"));
}

void ONF_StepLoadPlotAreas::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addAsciiFileChoice("Fichier des placettes", "Fichier ASCII (*.txt ; *.asc)", true, _neededFields, _refFileName, _refHeader, _refSeparator, _refDecimal, _refLocale, _refSkip, _refColumns);
    postInputConfigDialog->addDouble(tr("Rayon de placette (si pas de colonne rayon)"), "m", 0, 9999, 2, _defaultRadius);
}

void ONF_StepLoadPlotAreas::compute()
{

    QMap<QString, CT_StandardItemGroup*> groups;

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_FileHeader* fileHeader = group->singularItem(_inHeader);

        if (fileHeader != nullptr)
        {
            QString id = fileHeader->fileInfo().baseName();
            groups.insert(id.toLower(), group);
        }
    }

    // Création des emprises présentes dans le fichier
    QFile fRef(_refFileName);
    if (fRef.exists() && fRef.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&fRef);
        stream.setLocale(_refLocale);

        int colID  = _refColumns.value("ID_Plot", -1);
        int colX   = _refColumns.value("X", -1);
        int colY   = _refColumns.value("Y", -1);
        int colVal = _refColumns.value("R (m)", -1);

        if (colID < 0) {PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Champ ID_Plot non défini")));}
        if (colX < 0) {PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Champ X non défini")));}
        if (colY < 0) {PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Champ Y non défini")));}
        if (colVal < 0) {PS_LOG->addMessage(LogInterface::warning, LogInterface::step, QString(tr("Champ R non défini")));}

        if (colID >=0 && colX >= 0 && colY >= 0)
        {
            int colMax = colID;
            if (colX   > colMax) {colMax = colX;}
            if (colY   > colMax) {colMax = colY;}
            if (colVal > colMax) {colMax = colVal;}

            for (int i = 0 ; i < _refSkip ; i++) {stream.readLine();}
            if (_refHeader) {stream.readLine();}

            size_t cpt = 1;
            while (!stream.atEnd())
            {
                QString line = stream.readLine();
                cpt++;
                if (!line.isEmpty())
                {
                    QStringList values = line.split(_refSeparator);
                    if (values.size() >= colMax)
                    {

                        bool okX, okY, okVal;
                        double x = _refLocale.toDouble(values.at(colX), &okX);
                        double y = _refLocale.toDouble(values.at(colY), &okY);
                        double val = _defaultRadius;

                        okVal = false;
                        if (colVal >= 0)
                        {
                            val = _refLocale.toDouble(values.at(colVal), &okVal);
                        }

                        if (!okVal)
                        {
                            val = _defaultRadius;
                        }

                        QString id = values.at(colID);

                        if (okX && okY)
                        {
                            CT_StandardItemGroup* group = groups.value(id.toLower(), nullptr);
                            groups.remove(id.toLower());

                            if (group != nullptr)
                            {
                                CT_Circle2D* circle = new CT_Circle2D(new CT_Circle2DData(Eigen::Vector2d(x,y), val));
                                circle->setDisplayableName(id);
                                group->addSingularItem(_areaModelName, circle);
                            }

                        } else {
                            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Ligne %1 du fichier REF non valide")).arg(cpt));
                        }
                    }
                }
            }
        }
        fRef.close();
    }

}
