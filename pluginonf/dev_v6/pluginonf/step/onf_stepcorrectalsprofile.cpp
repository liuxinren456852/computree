/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcorrectalsprofile.h"

ONF_StepCorrectALSProfile::ONF_StepCorrectALSProfile() : SuperClass()
{
    _threshold = 0.0;
}

QString ONF_StepCorrectALSProfile::description() const
{
    return tr("Corriger le profil de densité de points ALS");
}

QString ONF_StepCorrectALSProfile::detailledDescription() const
{
    return tr("Chaque valeur du profil est divisée par la somme des valeurs du profil ayant une hauteur inférieure ou égale. "
              "De plus, le profil est séparé en deux profils (haut et bas) à l'aide d'un seuillage d'OTSU. ");
}

QString ONF_StepCorrectALSProfile::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepCorrectALSProfile::createNewInstance() const
{
    return new ONF_StepCorrectALSProfile();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepCorrectALSProfile::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Profil"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inProfile, tr("Profil"));
}

void ONF_StepCorrectALSProfile::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outCorrProfile, tr("Profil corrigé"));
    manager.addItemAttribute(_outCorrProfile, _outAttOTSU, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("Seuil OTSU"));
    manager.addItem(_inGroup, _outCorrProfileLow, tr("Profil corrigé OTSU bas"));
    manager.addItem(_inGroup, _outCorrProfileHigh, tr("Profil corrigé OTSU haut"));
}

void ONF_StepCorrectALSProfile::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Supprimer les données en dessous de"), "m", 0, 10000, 2, _threshold);
}

void ONF_StepCorrectALSProfile::compute()
{

    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Profile<int>* inProfile : grp->singularItems(_inProfile))
        {
            if (isStopped()) {return;}

            const Eigen::Vector3d &dir = inProfile->getDirection();

            CT_Profile<double>* outProfile = new CT_Profile<double>(inProfile->minX(), inProfile->minY(), inProfile->minZ(), dir(0), dir(1), dir(2), inProfile->getDim(), inProfile->resolution(), inProfile->NA(), 0);
            CT_Profile<double>* outProfileLow = new CT_Profile<double>(inProfile->minX(), inProfile->minY(), inProfile->minZ(), dir(0), dir(1), dir(2), inProfile->getDim(), inProfile->resolution(), inProfile->NA(), 0);
            CT_Profile<double>* outProfileHigh = new CT_Profile<double>(inProfile->minX(), inProfile->minY(), inProfile->minZ(), dir(0), dir(1), dir(2), inProfile->getDim(), inProfile->resolution(), inProfile->NA(), 0);

            for (size_t index = 0 ; index < inProfile->getDim() ; index++)
            {
                int inVal = inProfile->valueAtIndex(index);
                if (inVal == inProfile->NA() || (inProfile->lengthForIndex(index) < _threshold))
                {
                    outProfile->setValueAtIndex(index, 0);
                } else {
                    double sum = 0;
                    for (size_t i = 0 ; i <= index ; i++)
                    {
                        int val = inProfile->valueAtIndex(i);
                        if (val != inProfile->NA() && inProfile->lengthForIndex(i) >= _threshold)
                        {
                            sum += (double)val;
                        }
                    }
                    outProfile->setValueAtIndex(index, (double)inVal / sum) ;
                }
            }
            outProfile->computeMinMax();

            grp->addSingularItem(_outCorrProfile, outProfile);

            outProfile->addItemAttribute(_outAttOTSU, new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), outProfile->getOtsuThreshold(outProfileLow, outProfileHigh)));

            grp->addSingularItem(_outCorrProfileLow, outProfileLow);
            grp->addSingularItem(_outCorrProfileHigh, outProfileHigh);

        }
    }

}

