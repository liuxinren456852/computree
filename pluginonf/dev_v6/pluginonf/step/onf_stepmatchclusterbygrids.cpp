/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepmatchclusterbygrids.h"

#include <QFileInfo>

#include <QFile>
#include <QTextStream>

ONF_StepMatchClusterByGrids::ONF_StepMatchClusterByGrids() : SuperClass()
{
    _exportInLoop = false;
}

QString ONF_StepMatchClusterByGrids::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Attribuer les points de cluster à des grilles booléenes");
}

QString ONF_StepMatchClusterByGrids::detailledDescription() const
{
    return tr("Pour chaque point de chaque cluster d'entrée, cette étape identifie la grille voxel contenant le point."
              "L'identifiant correspondant est attribué au point. Les données sont exportées dans un fichier ascii. ");
}

CT_VirtualAbstractStep* ONF_StepMatchClusterByGrids::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepMatchClusterByGrids();
}

void ONF_StepMatchClusterByGrids::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Clusters"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Cluster à attribuer"));

    CT_InResultModelGroup *resultModelGrids = createNewInResultModel(DEF_SearchInResultGrid, tr("Grilles"));

    resultModelGrids->setZeroOrMoreRootGroup();
    resultModelGrids->addGroupModel("", DEF_SearchInGroupGrd);
    resultModelGrids->addItemModel(DEF_SearchInGroupGrd, DEF_SearchInGrid, CT_Grid3D_Sparse<bool>::staticGetType(), tr("Gille (bool)"));
    resultModelGrids->addItemAttributeModel(DEF_SearchInGrid, DEF_SearchInGridAtt, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, tr("Nom"));

    if (_exportInLoop)
    {
        CT_InResultModelGroup* res_counter = createNewInResultModel(_inResultCounter, tr("Résultat compteur"), "", true);
        res_counter->setRootGroup(_inGroupCounter);
        res_counter->addItemModel(_inGroupCounter, _inCounter, CT_LoopCounter::staticGetType(), tr("Compteur"));
        res_counter->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);
    }

}

void ONF_StepMatchClusterByGrids::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);
}

void ONF_StepMatchClusterByGrids::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    postInputConfigDialog->addBool(tr("Export dans une boucle"), "", tr("Activer"), _exportInLoop);
}

void ONF_StepMatchClusterByGrids::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addFileChoice(tr("Fichier d'export des points affiliés"),CT_FileChoiceButton::OneNewFile, tr("Fichier ASCII (*.*)"), _outputFileName);
}

void ONF_StepMatchClusterByGrids::compute()
{
    if (_outputFileName.isEmpty()) {return;}
    QFile outputFile(_outputFileName.first());

    // Gets the out result
    CT_ResultGroup* outResult = getOutResultList().at(0);

    QList<CT_ResultGroup*> resultList = getInputResults();
    CT_ResultGroup* resultGrid = resultList.at(1);

    CT_ResultGroupIterator it0(outResult, this, DEF_SearchInGroup);

    int nclusters = 0;
    while(!isStopped() && it0.hasNext())
    {
        it0.next();
        nclusters++;
    }

    QMap<QString, CT_Grid3D_Sparse<bool>* > grids;
    CT_ResultGroupIterator itGrd0(resultGrid, this, DEF_SearchInGroupGrd);
    while(!isStopped() && itGrd0.hasNext())
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*)itGrd0.next();
        CT_Grid3D_Sparse<bool>* grid = (CT_Grid3D_Sparse<bool>*)group->firstItemByINModelName(this, DEF_SearchInGrid);
        CT_AbstractItemAttribute* att = nullptr;
        if (grid != nullptr)
        {
            att = grid->firstItemAttributeByINModelName(resultGrid, this, DEF_SearchInGridAtt);

            QString name = att->toString(grid,nullptr);

            if (!name.isEmpty())
            {
                grids.insert(name, grid);
            }
        }
    }

    QString exportBaseName = "";
    bool header = true;
    if (_exportInLoop && resultList.size() > 2)
    {
        header = false;
        CT_ResultGroup* resCounter = resultList.at(2);
        CT_ResultItemIterator itCounter(resCounter, this, _inCounter);
        if (itCounter.hasNext())
        {
            const CT_LoopCounter* counter = (const CT_LoopCounter*) itCounter.next();

            if (counter != nullptr)
            {
                QFileInfo fileinfo(counter->getTurnName());
                if (fileinfo.exists())
                {
                    exportBaseName = fileinfo.baseName();
                } else {
                    exportBaseName = counter->getTurnName();
                }
            }
            if (counter->getCurrentTurn() <= 1)
            {
                header = true;
            }
        }
    }

    if (header && outputFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&outputFile);
        stream << "PlotID\tPointID\tX\tY\tZ\tClusterID\tRefID\n";
        outputFile.close();
    }

    if (outputFile.open(QIODevice::Append | QIODevice::Text))
    {
        QTextStream stream(&outputFile);   CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
        int cpt = 0;
        // iterate over all groups
        while(!isStopped() && it.hasNext())
        {
            CT_AbstractItemGroup *group = (CT_AbstractItemGroup*)it.next();
            const CT_AbstractItemDrawableWithPointCloud* cluster = (CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(this, DEF_SearchInScene);

            if (cluster != nullptr)
            {
                CT_PointIterator itP(cluster->pointCloudIndex()) ;
                while(itP.hasNext())
                {
                    const CT_Point &point = itP.next().currentPoint();
                    size_t indice = itP.currentGlobalIndex();

                    QString correspName;

                    QMapIterator<QString, CT_Grid3D_Sparse<bool>* > itGrds(grids);
                    while (itGrds.hasNext() && correspName.isEmpty())
                    {
                        itGrds.next();
                        QString name = itGrds.key();
                        CT_Grid3D_Sparse<bool>* grid = itGrds.value();

                        bool val = grid->valueAtXYZ(point(0), point(1), point(2));
                        if (val)
                        {
                            correspName = name;
                        }
                    }

                    stream << exportBaseName << "\t" << indice << "\t" << CT_NumericToStringConversionT<double>::toString(point(0)) << "\t" << CT_NumericToStringConversionT<double>::toString(point(1)) << "\t" << CT_NumericToStringConversionT<double>::toString(point(2)) << "\t" << cluster->id()<< "\t" << correspName << "\n";
                }
            }

            setProgress(100 * cpt++ / nclusters);
        }

        outputFile.close();
    }

    setProgress(99);
}

