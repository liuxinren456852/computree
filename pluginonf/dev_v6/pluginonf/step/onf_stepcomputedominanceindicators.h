#ifndef ONF_STEPCOMPUTEDOMINANCEINDICATORS_H
#define ONF_STEPCOMPUTEDOMINANCEINDICATORS_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_itemattributelist.h"

#include "ct_itemdrawable/ct_scene.h"

class ONF_StepComputeDominanceIndicators: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    struct Apex {
        Apex(double x, double y, double z, double h, double diameter, CT_StandardItemGroup* group)
        {
            _x = x;
            _y = y;
            _z = z;
            _h = h;
            _diameter = diameter;
            _group = group;
        }

        double  _x;
        double  _y;
        double  _z;
        double  _h;
        double  _diameter;
        CT_StandardItemGroup* _group;
    };

    ONF_StepComputeDominanceIndicators();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable>                            _inApexItem;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER>     _inAttXApex;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER>     _inAttYApex;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER>     _inAttZApex;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER>     _inAttDiameter;

    CT_HandleOutSingularItem<CT_ItemAttributeList>                                      _outitemAtt;
    CT_HandleOutStdItemAttribute<double>                                                _outIndiceSchutzTotal;
    CT_HandleOutStdItemAttribute<double>                                                _outIndiceSchutzHorTotal;
    CT_HandleOutStdItemAttribute<double>                                                _outIndiceSchutzVerTotal;
    CT_HandleOutStdItemAttribute<double>                                                _outIndiceSchutzMax;
    CT_HandleOutStdItemAttribute<double>                                                _outIndiceSchutzHorMax;
    CT_HandleOutStdItemAttribute<double>                                                _outIndiceSchutzVerMax;
    CT_HandleOutStdItemAttribute<double>                                                _outAngleNeighbMax;


    CT_HandleInResultGroup<0,1>                                        _inResultDTM;
    CT_HandleInStdZeroOrMoreGroup                                      _inZeroOrMoreRootGroupDTM;
    CT_HandleInStdGroup<>                                           _inGroupDTM;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inDTM;



};

#endif // ONF_STEPCOMPUTEDOMINANCEINDICATORS_H
