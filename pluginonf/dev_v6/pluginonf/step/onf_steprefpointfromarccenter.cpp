/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_steprefpointfromarccenter.h"

#include "ct_itemdrawable/tools/pointclustertools/ct_polylinesalgorithms.h"

#include "qvector2d.h"

ONF_StepRefPointFromArcCenter::ONF_StepRefPointFromArcCenter() : SuperClass()
{
}

QString ONF_StepRefPointFromArcCenter::description() const
{
    return tr("Créer des points de référence à partir d'Arcs");
}

QString ONF_StepRefPointFromArcCenter::detailledDescription() const
{
    return tr("No detailled description for this step");
}

CT_VirtualAbstractStep* ONF_StepRefPointFromArcCenter::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepRefPointFromArcCenter();
}

//////////////////// PROTECTED //////////////////

void ONF_StepRefPointFromArcCenter::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Polyline(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInPolyline, CT_PointCluster::staticGetType(), tr("Polyligne"));
}

void ONF_StepRefPointFromArcCenter::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != nullptr)
        res->addItemModel(DEF_SearchInGroup, _outRefPointModelName, new CT_ReferencePoint(), tr("Barycentre"));
}

void ONF_StepRefPointFromArcCenter::compute()
{
    // on récupère le résultat copié
    CT_ResultGroup *outRes = getOutResultList().first();

    CT_ResultGroupIterator it(outRes, this, DEF_SearchInGroup);
    while (it.hasNext() && !isStopped())
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*) it.next();

        const CT_PointCluster *item = (const CT_PointCluster*)group->firstItemByINModelName(this, DEF_SearchInPolyline);

        if(item != nullptr)
        {
            double sagitta = 0;
            double chord = 0;
            double radius = 0;
            Eigen::Vector2d center = CT_PolylinesAlgorithms::compute2DArcData(item, sagitta, chord, radius);

            if (radius > chord)
            {
                radius = chord;
            }

            // et on ajoute un referencePoint
            group->addSingularItem(new CT_ReferencePoint(_outRefPointModelName.completeName(), outRes, center.x(), center.y(), item->centerZ(), radius));
        }
    }

    setProgress(100);
}

