/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfilteritemsbyposition.h"

ONF_StepFilterItemsByPosition::ONF_StepFilterItemsByPosition() : SuperClass()
{
    _xcoord = 0;
    _ycoord = 0;
    _radius = 20;
}

QString ONF_StepFilterItemsByPosition::description() const
{
    return tr("Garder les Items proches d'une coordonnée");
}

QString ONF_StepFilterItemsByPosition::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepFilterItemsByPosition::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepFilterItemsByPosition::createNewInstance() const
{
    return new ONF_StepFilterItemsByPosition();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepFilterItemsByPosition::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Items à filtrer"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inItem, tr("Item"));
}

void ONF_StepFilterItemsByPosition::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
}

void ONF_StepFilterItemsByPosition::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble("Coordonnée X :", "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _xcoord, 1);
    postInputConfigDialog->addDouble("Coordonnée Y :", "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _ycoord, 1);
    postInputConfigDialog->addDouble("Rayon", "m", 0, std::numeric_limits<double>::max(), 4, _radius, 1);
}

void ONF_StepFilterItemsByPosition::compute()
{
    QList<CT_StandardItemGroup*> groupsToBeRemoved;

    for (CT_StandardItemGroup* cpy_grp : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractSingularItemDrawable* itemCpy_item = cpy_grp->singularItem(_inItem);

        if (itemCpy_item != nullptr)
        {
            double dist = sqrt(pow(_xcoord - itemCpy_item->centerX(), 2) + pow(_ycoord - itemCpy_item->centerY(), 2));

            if (dist > _radius)
            {
                groupsToBeRemoved.append(cpy_grp);
            }
        }
    }

    QListIterator<CT_StandardItemGroup*> itE(groupsToBeRemoved);
    while(itE.hasNext())
    {
        CT_StandardItemGroup *group = itE.next();
        group->removeFromParent(false);
    }
}

