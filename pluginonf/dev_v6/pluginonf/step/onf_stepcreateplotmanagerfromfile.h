/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPCREATEPLOTMANAGERFROMFILE_H
#define ONF_STEPCREATEPLOTMANAGERFROMFILE_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_plotgridmanager.h"
#include "ct_itemdrawable/ct_circle2d.h"

#include "ct_view/tools/ct_textfileconfigurationdialog.h"

class ONF_StepCreatePlotManagerFromFile: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepCreatePlotManagerFromFile();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters


    double                  _plotRadius;

    QList<CT_TextFileConfigurationFields> _neededFields;

    QString _refFileName;
    bool _refHeader;
    QString _refSeparator;
    QString _refDecimal;
    QLocale _refLocale;
    int _refSkip;
    QMap<QString, int> _refColumns;

    QStringList _plotsIds;


    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractAreaShape2D>                                     _inShape2D;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Circle2D>                                               _outPlot;
    CT_HandleOutStdItemAttribute<QString>                                               _outPlotAtt;


};

#endif // ONF_STEPCREATEPLOTMANAGERFROMFILE_H
