/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepconvertdemtopoints.h"

#include "ct_math/ct_mathpoint.h"

#include <QMessageBox>

ONF_StepConvertDEMToPoints::ONF_StepConvertDEMToPoints() : SuperClass()
{
}

QString ONF_StepConvertDEMToPoints::description() const
{
    return tr("Convert DEM to point cloud");
}

QString ONF_StepConvertDEMToPoints::detailledDescription() const
{
    return tr("Each cell center of the raster is converted into a 3D point, with cell value as Z coordinate.");
}

QString ONF_StepConvertDEMToPoints::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepConvertDEMToPoints::createNewInstance() const
{
    return new ONF_StepConvertDEMToPoints();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepConvertDEMToPoints::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("DEM"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inDEM, tr("DEM"));
}

void ONF_StepConvertDEMToPoints::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scene"));
}

void ONF_StepConvertDEMToPoints::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractImage2D* inDEM : group->singularItems(_inDEM))
        {
            if (isStopped()) {return;}

            CT_AbstractUndefinedSizePointCloud* mpcir = PS_REPOSITORY->createNewUndefinedSizePointCloud();
            CT_Point point;
            Eigen::Vector3d cellCenter;

            for (size_t i = 0 ; i < inDEM->nCells() ; i++)
            {
                double value = inDEM->valueAtIndexAsDouble(i);
                if (value != inDEM->NAAsDouble())
                {
                    if (inDEM->getCellCenterCoordinates(i, cellCenter))
                    {
                        point.setValues(cellCenter(0), cellCenter(1), value);
                        mpcir->addPoint(point);
                    }
                }
            }

            CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerUndefinedSizePointCloud(mpcir));
            outScene->updateBoundingBox();
            group->addSingularItem(_outScene, outScene);
        }
    }
}

