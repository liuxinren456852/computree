/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcreateplotmanagergrid.h"

ONF_StepCreatePlotManagerGrid::ONF_StepCreatePlotManagerGrid() : SuperClass()
{
    _plotSize = 15.0;
    _plotSpacing = 30.0;
    _xref = 15.0;
    _yref = 15.0;
}

QString ONF_StepCreatePlotManagerGrid::description() const
{
    return tr("1- Ajoute les placettes d'une grille sur l'emprise");
}

QString ONF_StepCreatePlotManagerGrid::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepCreatePlotManagerGrid::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepCreatePlotManagerGrid::createNewInstance() const
{
    return new ONF_StepCreatePlotManagerGrid();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepCreatePlotManagerGrid::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Tuiles"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inRootGroup, tr("Groupe Racine"));
    manager.addGroup(_inRootGroup, _inGrpShape2D);
    manager.addItem(_inGrpShape2D, _inShape2D, tr("Emprise (sans buffer)"));
}

void ONF_StepCreatePlotManagerGrid::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGrpShape2D, _outPlotList, tr("Grille de Placettes"));
}

void ONF_StepCreatePlotManagerGrid::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Taille de placette (rayon/circulaire ; DEMI-coté/carrée)"), "m", 0, std::numeric_limits<double>::max(), 2, _plotSize);
    postInputConfigDialog->addDouble(tr("Espacement des placettes"),   "m", 0, std::numeric_limits<double>::max(), 2, _plotSpacing);

    postInputConfigDialog->addEmpty();
    postInputConfigDialog->addTitle(tr("Coordonnées de références pour les centres de placettes :"));
    postInputConfigDialog->addDouble(tr("Coordonnée X de référence "), "m", 0, std::numeric_limits<double>::max(), 2, _xref);
    postInputConfigDialog->addDouble(tr("Coordonnée y de référence "), "m", 0, std::numeric_limits<double>::max(), 2, _yref);
}

void ONF_StepCreatePlotManagerGrid::compute()
{
    Eigen::Vector2d refCoords(_xref, _yref);

    Eigen::Vector2d  minAll, maxAll;
    minAll(0) = std::numeric_limits<double>::max();
    minAll(1) = std::numeric_limits<double>::max();
    maxAll(0) = -std::numeric_limits<double>::max();
    maxAll(1) = -std::numeric_limits<double>::max();

    for (CT_StandardItemGroup* group : _inRootGroup.iterateOutputs(_inResult))
    {
        QList<CT_PlotListInGrid*> plotLists;

        // Add a plot list for each input shape
        for (const CT_StandardItemGroup* grpShape : group->groups(_inGrpShape2D))
        {
            if (isStopped()) {return;}

            const CT_AbstractAreaShape2D* shape = grpShape->singularItem(_inShape2D);

            if (shape != nullptr)
            {
                CT_AreaShape2DData* data = (CT_AreaShape2DData*) shape->getPointerData();

                if (data != nullptr)
                {
                    CT_PlotListInGrid* plotList = new CT_PlotListInGrid(data, refCoords, _plotSpacing, _plotSize);
                    plotLists.append(plotList);

                    Eigen::Vector3d  min, max;
                    plotList->boundingBox(min, max);

                    if (min(0) < minAll(0)) {minAll(0) = min(0);}
                    if (min(1) < minAll(1)) {minAll(1) = min(1);}
                    if (max(0) > maxAll(0)) {maxAll(0) = max(0);}
                    if (max(1) > maxAll(1)) {maxAll(1) = max(1);}

                    const_cast<CT_StandardItemGroup*>(grpShape)->addSingularItem(_outPlotList, plotList);
                }
            }
        }

        size_t indexJump = std::floor((maxAll(0) - minAll(0)) / _plotSpacing) + 1;

        // Set the index bounds for each plot list
        for (int i = 0 ; i < plotLists.size() ; i++)
        {
            CT_PlotListInGrid* plotList = plotLists.at(i);
            Eigen::Vector3d  min, max;
            plotList->boundingBox(min, max);

            size_t firstIndex = std::floor((min(0) - minAll(0)) / _plotSpacing) + 1 + std::floor((maxAll(1) - max(1)) / _plotSpacing) * indexJump;

            plotList->setIndices(firstIndex, indexJump);
        }
    }
}
