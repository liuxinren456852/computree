#include "onf_stepcomputescandirection.h"

#include "ct_log/ct_logmanager.h"

ONF_StepComputeScanDirection::ONF_StepComputeScanDirection() : SuperClass()
{
}

QString ONF_StepComputeScanDirection::description() const
{
    return tr("Calcule les directions de scan (ALS)");
}

QString ONF_StepComputeScanDirection::detailledDescription() const
{
    return tr("Utilise la trajectographie du vol Lidar pour recalculer une direction de tir pour chaque point du nuage. ");
}


CT_VirtualAbstractStep* ONF_StepComputeScanDirection::createNewInstance() const
{
    return new ONF_StepComputeScanDirection();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepComputeScanDirection::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène"));
    manager.addItem(_inGroup, _inLAS, tr("Attributs LAS"));

    manager.addResult(_inResultTraj, tr("Trajectory"));//, "", true);
    manager.setZeroOrMoreRootGroup(_inResultTraj, _inZeroOrMoreRootGroupTraj);
    manager.addGroup(_inZeroOrMoreRootGroupTraj, _inGroupTraj);
    manager.addItem(_inGroupTraj, _inTraj, tr("Trajectory"));
}

void ONF_StepComputeScanDirection::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outDir, tr("Scan direction"));
}

void ONF_StepComputeScanDirection::compute()
{
    QList<CT_ScanPath*> trajectories;
    for (const CT_ScanPath* traj : _inTraj.iterateInputs(_inResultTraj))
    {
            trajectories.append(const_cast<CT_ScanPath*>(traj));
    }

    if (trajectories.size() > 0)
    {
        for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
        {
            for (const CT_AbstractItemDrawableWithPointCloud* scene : grp->singularItems(_inScene))
            {
                if (isStopped()) {return;}

                const CT_StdLASPointsAttributesContainer* attributeLAS = grp->singularItem(_inLAS);

                if (attributeLAS != nullptr)
                {
                    const CT_AbstractPointCloudIndex *pointCloudIndex = scene->pointCloudIndex();
                    size_t n_points = pointCloudIndex->size();

                    // Retrieve attributes
                    QHashIterator<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> itLAS(attributeLAS->lasPointsAttributes());
                    if (!itLAS.hasNext()) {return;}

                    CT_AbstractPointAttributesScalar *firstAttribute = itLAS.next().value();
                    if (firstAttribute == nullptr) {return;}

                    const CT_AbstractPointCloudIndex* pointCloudIndexLAS = firstAttribute->pointCloudIndex();
                    if (pointCloudIndexLAS == nullptr) {return;}

                    CT_AbstractPointAttributesScalar* attributeGPS          = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::GPS_Time));
                    CT_AbstractPointAttributesScalar* attributeIntensity    = static_cast<CT_AbstractPointAttributesScalar*>(attributeLAS->pointsAttributesAt(CT_LasDefine::Intensity));

                    if (attributeIntensity == nullptr || attributeGPS == nullptr) {return;}

                    CT_PointIterator itP(pointCloudIndex);

                    CT_NormalCloudStdVector *normalCloud = new CT_NormalCloudStdVector( n_points );

                    size_t i = 0;
                    size_t errorCount = 0;
                    while (itP.hasNext() && !isStopped())
                    {
                        size_t globalIndex = itP.next().currentGlobalIndex();
                        size_t localIndex = pointCloudIndexLAS->indexOf(globalIndex);
                        CT_Point point = itP.currentPoint();

                        double gpsTime = 0;   // Récupération du temps GPS pour le point
                        double intensity = 0; // Récupération de l'intensité pour le point
                        if (localIndex < pointCloudIndexLAS->size())
                        {
                            gpsTime = attributeGPS->dValueAt(localIndex);
                            intensity = attributeIntensity->dValueAt(localIndex);
                        }

                        bool found = false;
                        Eigen::Vector3d trajCoord;

                        for (int tra = 0 ; tra < trajectories.size() && !found ; tra++)
                        {
                            CT_ScanPath* sp = trajectories.at(tra);
                            if (sp->isInScanPath(gpsTime))
                            {
                                trajCoord = sp->getPathPointForGPSTime(gpsTime);
                                found = true;
                            }
                        }

                        double length = -1;
                        Eigen::Vector3d dir(0,0,0);

                        if (found)
                        {
                            length = sqrt(pow(point(0) - trajCoord(0), 2) + pow(point(1) - trajCoord(1), 2) + pow(point(2) - trajCoord(2), 2));
                            dir = trajCoord - point;
                            dir.normalize();

                        } else {

                            errorCount++;
                            if (errorCount < 5)
                            {
                                PS_LOG->addMessage(LogInterface::warning, LogInterface::step, QString(tr("Pas d'information de trajectoire pour le point (%1 ; %2 ; %3)")).arg(point(0)).arg(point(1)).arg(point(2)));
                            }
                        }

                        CT_Normal &ctNormal = normalCloud->normalAt(i);
                        ctNormal.x() = float(dir(0));
                        ctNormal.y() = float(dir(1));
                        ctNormal.z() = float(dir(2));
                        ctNormal.w() = float(length);

                        setProgress(float(95.0*i++ / n_points));
                    }

                    if (errorCount > 0) {PS_LOG->addMessage(LogInterface::warning, LogInterface::step, QString(tr("Pas d'information de trajectoire pour au total %1 points sur %2")).arg(errorCount).arg(i));}

                    if (i > 0)
                    {
                        CT_PointsAttributesNormal* normalAttribute = new CT_PointsAttributesNormal(scene->pointCloudIndexRegistered(), normalCloud);
                        grp->addSingularItem(_outDir, normalAttribute);
                    }
                }
            }
        }
    }
}

