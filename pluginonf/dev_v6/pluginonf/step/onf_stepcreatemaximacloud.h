#ifndef ONF_STEPCREATEMAXIMACLOUD_H
#define ONF_STEPCREATEMAXIMACLOUD_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/abstract/ct_abstractareashape2d.h"
#include "ct_itemdrawable/ct_image2d.h"

class ONF_StepCreateMaximaCloud: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepCreateMaximaCloud();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    int _createRefPoints;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float> >                                         _inHeights;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                                        _inMaxima;
    CT_HandleInSingularItem<CT_Image2D<float>, 0, 1>                                    _inDTM;
    CT_HandleInSingularItem<CT_AbstractAreaShape2D, 0, 1>                               _inArea;

    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_ReferencePoint>                                         _outRefPt;


};

#endif // ONF_STEPCREATEMAXIMACLOUD_H
