/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepmodifypositions2d.h"

//Inclusion of actions
#include "actions/onf_actionmodifypositions2d.h"

#include <QMessageBox>

ONF_StepModifyPositions2D::ONF_StepModifyPositions2D() : SuperClass()
{
    m_doc = nullptr;
    setManual(true);
}

QString ONF_StepModifyPositions2D::description() const
{
    return tr("Modifier des positions 2D");
}

CT_VirtualAbstractStep* ONF_StepModifyPositions2D::createNewInstance() const
{
    return new ONF_StepModifyPositions2D();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepModifyPositions2D::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroup *res_inRes = createNewInResultModel(_inRes, tr("Positions 2D"));
    res_inRes->setZeroOrMoreRootGroup();
    res_inRes->addGroupModel("", _inGrp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inRes->addItemModel(_inGrp, _inPos, CT_Point2D::staticGetType(), tr("Position 2D"));
}

void ONF_StepModifyPositions2D::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *res = createNewOutResultModel(_outRes, tr("Positions 2D"));
    res->setRootGroup(_outGrp, new CT_StandardItemGroup(), tr("Groupe"));
    res->addItemModel(_outGrp, _outPos, new CT_Point2D(), tr("Position 2D"));
}

void ONF_StepModifyPositions2D::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    // No parameter dialog for this step
}

void ONF_StepModifyPositions2D::compute()
{
    m_doc = nullptr;
    m_status = 0;

    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* inRes = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _outRes = outResultList.at(0);

    _modelCreation = (CT_OutAbstractSingularItemModel*)PS_MODELS->searchModelForCreation(_outPos, _outRes);

    // IN results browsing
    CT_ResultGroupIterator it_inGrp(inRes, this, _inGrp);
    while (it_inGrp.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grp_inGrp = (CT_AbstractItemGroup*) it_inGrp.next();

        CT_Point2D* item_inPos = (CT_Point2D*)grp_inGrp->firstItemByINModelName(this, _inPos);
        if (item_inPos != nullptr)
        {
            _positions.append((CT_Point2D*) item_inPos->copy(_modelCreation, _outRes, CT_ResultCopyModeList() << CT_ResultCopyModeList::CopyItemDrawableCompletely));
        }
    }

    // request the manual mode
    requestManualMode();

    for (int i = 0 ; i < _positions.size() ; i++)
    {
        CT_StandardItemGroup* grp_grp = new CT_StandardItemGroup(_outGrp, _outRes);
        _outRes->addGroup(grp_grp);

        grp_grp->addSingularItem(_positions.at(i));
    }

    m_status = 1;
    requestManualMode();
}

void ONF_StepModifyPositions2D::initManualMode()
{
    // create a new 3D document
    if(m_doc == nullptr)
        m_doc = getGuiContext()->documentManager()->new3DDocument();

    // change camera type to orthographic
    if(m_doc != nullptr && !m_doc->views().isEmpty())
        dynamic_cast<GraphicsViewInterface*>(m_doc->views().at(0))->camera()->setType(CameraInterface::ORTHOGRAPHIC);

    m_doc->removeAllItemDrawable();

    m_doc->setCurrentAction(new ONF_ActionModifyPositions2D(_positions, _modelCreation, _outRes));

    QMessageBox::information(nullptr, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette étape"), QMessageBox::Ok);
}

void ONF_StepModifyPositions2D::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = nullptr;
            quitManualMode();
        }
    }
}
