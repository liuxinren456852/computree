#include "onf_stepkeepintersectingitems.h"

#include <Eigen/Core>

ONF_StepKeepIntersectingItems::ONF_StepKeepIntersectingItems() : SuperClass()
{
}

ONF_StepKeepIntersectingItems::~ONF_StepKeepIntersectingItems()
{
}

QString ONF_StepKeepIntersectingItems::description() const
{
    return tr("Conserve les items intersectant les surfaces de référence");
}

QString ONF_StepKeepIntersectingItems::detailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepKeepIntersectingItems::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepKeepIntersectingItems();
}

//////////////////// PROTECTED //////////////////

void ONF_StepKeepIntersectingItems::declareInputModels(CT_StepInModelStructureManager& manager)
{   
    manager.addResult(_inResult, tr("Items to filter"), "", true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inItem, tr("Item to filter"));

    manager.addResult(_inResultFiltering, tr("Filtering items"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultFiltering, _inZeroOrMoreRootGroupFiltering);
    manager.addGroup(_inZeroOrMoreRootGroupFiltering, _inGroupFiltering);
    manager.addItem(_inGroupFiltering, _inItemFiltering, tr("Filtering items"));
}

// Redefine in children steps to complete out Models
void ONF_StepKeepIntersectingItems::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
}

// Redefine in children steps to complete compute method
void ONF_StepKeepIntersectingItems::compute()
{

    QList<const CT_AbstractSingularItemDrawable*> refList;

    for (const CT_AbstractSingularItemDrawable* item : _inItemFiltering.iterateInputs(_inResultFiltering))
    {
        if (isStopped()) {return;}

        refList.append(item);
    }

    QList<CT_StandardItemGroup*> groupsToBeRemoved;

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractSingularItemDrawable* item = group->singularItem(_inItem);

        if (item != nullptr)
        {
            Eigen::Vector3d min, max;
            item->boundingBox(min, max);

            bool intersected = false;

            for (int i = 0 ; i < refList.size() && !intersected; i++)
            {
                const CT_AbstractSingularItemDrawable* refItem = refList.at(i);
                Eigen::Vector3d minRef, maxRef;
                refItem->boundingBox(minRef, maxRef);

                if (!(maxRef(0) < min(0) || minRef(0) > max(0) ||
                      maxRef(1) < min(1) || minRef(1) > max(1)))
                {
                    intersected = true;
                }
            }

            if (!intersected) {groupsToBeRemoved.append(group);}
        }
    }

    QListIterator<CT_StandardItemGroup*> itE(groupsToBeRemoved);

    while(itE.hasNext())
    {
        CT_StandardItemGroup *group = itE.next();
        group->removeFromParent(false);
    }

    setProgress( 100 );
}
