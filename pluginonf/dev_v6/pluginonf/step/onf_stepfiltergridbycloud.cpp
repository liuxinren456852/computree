/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfiltergridbycloud.h"

#include "tools/onf_computehitsthread.h"

#include <QFileInfo>

ONF_StepFilterGridByCloud::ONF_StepFilterGridByCloud() : SuperClass()
{
    _naVal = -9999;
}

QString ONF_StepFilterGridByCloud::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Filter une grille 3D en fonction de scènes");
}

QString ONF_StepFilterGridByCloud::detailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepFilterGridByCloud::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepFilterGridByCloud();
}

void ONF_StepFilterGridByCloud::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResultScene, tr("Scene(s)"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultScene, _inZeroOrMoreRootGroupScene);
    manager.addGroup(_inZeroOrMoreRootGroupScene, _inGroupScene);
    manager.addItem(_inGroupScene, _inScene, tr("Scene(s)"));

    manager.addResult(_inResultGrid, tr("Grille"));
    manager.setZeroOrMoreRootGroup(_inResultGrid, _inZeroOrMoreRootGroupGrid);
    manager.addGroup(_inZeroOrMoreRootGroupGrid, _inGroupGrid);
    manager.addItem(_inGroupGrid, _inGrid, tr("Grille"));
}

void ONF_StepFilterGridByCloud::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResultGrid);
    manager.addItem(_inGroupGrid, _outGrid, tr("Grille filtrée"));
}

void ONF_StepFilterGridByCloud::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Valeur pour les cases hors scène"),"", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _naVal);
}

void ONF_StepFilterGridByCloud::compute()
{

    double minxScene = std::numeric_limits<double>::max();
    double minyScene = std::numeric_limits<double>::max();
    double minzScene = std::numeric_limits<double>::max();
    double maxxScene = -std::numeric_limits<double>::max();
    double maxyScene = -std::numeric_limits<double>::max();
    double maxzScene = -std::numeric_limits<double>::max();

    for (const CT_AbstractItemDrawableWithPointCloud* scene : _inScene.iterateInputs(_inResultScene))
    {
        if (isStopped()) {return;}

        Eigen::Vector3d min, max;
        scene->boundingBox(min, max);

        if (min(0) < minxScene) {minxScene = min(0);}
        if (min(1) < minyScene) {minyScene = min(1);}
        if (min(2) < minzScene) {minzScene = min(2);}

        if (max(0) > maxxScene) {maxxScene = max(0);}
        if (max(1) > maxyScene) {maxyScene = max(1);}
        if (max(2) > maxzScene) {maxzScene = max(2);}
    }


    for (CT_StandardItemGroup* group : _inGroupGrid.iterateOutputs(_inResultGrid))
    {
        if (isStopped()) {return;}

       const CT_AbstractGrid3D* gridIn = group->singularItem(_inGrid);

        if (gridIn != nullptr)
        {
            Eigen::Vector3d minGr, maxGr;
            double res = gridIn->resolution();
            gridIn->getMinCoordinates(minGr);
            gridIn->getMaxCoordinates(maxGr);

            while (minGr(0) < minxScene) {minGr(0) += res;}
            while (minGr(1) < minyScene) {minGr(1) += res;}
            while (minGr(2) < minzScene) {minGr(2) += res;}

            while (maxGr(0) > maxxScene && maxGr(0) > minGr(0)) {maxGr(0) -= res;}
            while (maxGr(1) > maxyScene && maxGr(1) > minGr(1)) {maxGr(1) -= res;}
            while (maxGr(2) > maxzScene && maxGr(2) > minGr(2)) {maxGr(2) -= res;}

            minGr(0) -= res;
            minGr(1) -= res;
            minGr(2) -= res;

            maxGr(0) += res;
            maxGr(1) += res;
            maxGr(2) += res;

            // Declaring the output grids
            CT_Grid3D<double>* outGrid = CT_Grid3D<double>::createGrid3DFromXYZCoords(minGr(0), minGr(1), minGr(2), maxGr(0), maxGr(1), maxGr(2), res, _naVal, _naVal);

            for (const CT_AbstractItemDrawableWithPointCloud* scene : _inScene.iterateInputs(_inResultScene))
            {
                const CT_AbstractPointCloudIndex *pointCloudIndex = scene->pointCloudIndex();
                CT_PointIterator itP(pointCloudIndex);
                while(itP.hasNext() && (!isStopped()))
                {
                    const CT_Point &point = itP.next().currentPoint();

                    size_t index;
                    gridIn->indexAtXYZ(point(0), point(1), point(2), index);
                    outGrid->setValueAtXYZ(point(0), point(1), point(2), gridIn->valueAtIndexAsDouble(index));
                }
            }

            outGrid->computeMinMax();
            group->addSingularItem(_outGrid, outGrid);
        }
    }

    setProgress(99);
}

