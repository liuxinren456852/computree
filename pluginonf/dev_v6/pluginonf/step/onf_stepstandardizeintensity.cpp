#include "onf_stepstandardizeintensity.h"

ONF_StepStandardizeIntensity::ONF_StepStandardizeIntensity() : SuperClass()
{
    _meanHeight = 500.0;
}

QString ONF_StepStandardizeIntensity::description() const
{
    return tr("Standardiser l'intensité (ALS)");
}

CT_VirtualAbstractStep* ONF_StepStandardizeIntensity::createNewInstance() const
{
    return new ONF_StepStandardizeIntensity();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepStandardizeIntensity::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy * resultSc = createNewInResultModelForCopy(_inResSc, tr("Scene(s)"), "", true);
    resultSc->setZeroOrMoreRootGroup();
    resultSc->addGroupModel("", _inScGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultSc->addItemModel(_inScGrp, _inAttLAS, CT_StdLASPointsAttributesContainer::staticGetType(), tr("Attributs LAS"), tr("Attribut LAS"));

    CT_InResultModelGroup *resultDTM = createNewInResultModel(_inResTraj, tr("Trajectory"), "", true);
    resultDTM->setZeroOrMoreRootGroup();
    resultDTM->addGroupModel("", _inTrajGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultDTM->addItemModel(_inTrajGrp, _inTraj, CT_ScanPath::staticGetType(), tr("Trajectory"));
}

void ONF_StepStandardizeIntensity::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(_inResSc);

    if(res != nullptr)
    {
        res->addItemModel(_inScGrp, _lasAttributesOut_AutoRenameModel, new CT_StdLASPointsAttributesContainer(), tr("All Attributs (Icorr)"));
        res->addItemModel(_inScGrp, _outIntensityAttributeModelName, new CT_PointsAttributesScalarTemplated<double>(), tr("Corrected intensity"));
    }

}

void ONF_StepStandardizeIntensity::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Altitude moyenne du vol"), "m", 1, std::numeric_limits<double>::max(), 2, _meanHeight);
}

void ONF_StepStandardizeIntensity::compute()
{
    CT_ResultGroup* res_inTraj = getInputResults().at(1);

    QList<CT_ScanPath*> trajectories;
    CT_ResultItemIterator it(res_inTraj, this, _inTraj);
    while (it.hasNext())
    {
        trajectories.append((CT_ScanPath*) it.next());
    }

    if (trajectories.size() > 0)
    {
        CT_ResultGroup* resOut = getOutResultList().first();

        CT_ResultGroupIterator itSc(resOut, this, _inScGrp);
        while (itSc.hasNext())
        {
            CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itSc.next();

            if (grp != nullptr)
            {
                const CT_StdLASPointsAttributesContainer* attributeLAS = (CT_StdLASPointsAttributesContainer*)grp->firstItemByINModelName(this, _inAttLAS);

                if (attributeLAS != nullptr && !isStopped())
                {
                    // Retrieve attributes
                    QHashIterator<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> itLAS(attributeLAS->lasPointsAttributes());
                    if (!itLAS.hasNext()) {return;}

                    CT_AbstractPointAttributesScalar *firstAttribute = itLAS.next().value();
                    if (firstAttribute == nullptr) {return;}

                    const CT_AbstractPointCloudIndex* pointCloudIndexLAS = firstAttribute->pointCloudIndex();
                    size_t n_points = pointCloudIndexLAS->size();

                    if (pointCloudIndexLAS == nullptr) {return;}

                    CT_AbstractPointAttributesScalar* att_Intensity = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Intensity);
                    CT_AbstractPointAttributesScalar* att_Return_Number = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Return_Number);
                    CT_AbstractPointAttributesScalar* att_Number_of_Returns = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Number_of_Returns);
                    CT_AbstractPointAttributesScalar* att_Classification_Flags = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Classification_Flags);
                    CT_AbstractPointAttributesScalar* att_Scanner_Channel = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Scanner_Channel);
                    CT_AbstractPointAttributesScalar* att_Scan_Direction_Flag = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Scan_Direction_Flag);
                    CT_AbstractPointAttributesScalar* att_Edge_of_Flight_Line = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Edge_of_Flight_Line);
                    CT_AbstractPointAttributesScalar* att_Classification = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Classification);
                    CT_AbstractPointAttributesScalar* att_Scan_Angle_Rank = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Scan_Angle_Rank);
                    CT_AbstractPointAttributesScalar* att_User_Data = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::User_Data);
                    CT_AbstractPointAttributesScalar* att_Point_Source_ID = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Point_Source_ID);
                    CT_AbstractPointAttributesScalar* att_GPS_Time  = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::GPS_Time);
                    CT_AbstractPointAttributesScalar* att_Red = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Red);
                    CT_AbstractPointAttributesScalar* att_Green = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Green);
                    CT_AbstractPointAttributesScalar* att_Blue = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Blue);
                    CT_AbstractPointAttributesScalar* att_NIR = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::NIR);
                    CT_AbstractPointAttributesScalar* att_Wave_Packet_Descriptor_Index = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Wave_Packet_Descriptor_Index);
                    CT_AbstractPointAttributesScalar* att_Byte_offset_to_waveform_data = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Byte_offset_to_waveform_data);
                    CT_AbstractPointAttributesScalar* att_Waveform_packet_size__inBytes = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Waveform_packet_size__inBytes);
                    CT_AbstractPointAttributesScalar* att_Return_Point_Waveform_Location = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Return_Point_Waveform_Location);

                    if (att_Intensity == nullptr || att_GPS_Time == nullptr) {return;}

                    CT_StdLASPointsAttributesContainer *container = new CT_StdLASPointsAttributesContainer(_lasAttributesOut_AutoRenameModel.completeName(), resOut);

                    // Put old not modified data in new container
                    //CT_LasDefine::Intensity : Modified after
                    container->insertPointsAttributesAt(CT_LasDefine::Return_Number, att_Return_Number);
                    container->insertPointsAttributesAt(CT_LasDefine::Number_of_Returns, att_Number_of_Returns);
                    container->insertPointsAttributesAt(CT_LasDefine::Classification_Flags, att_Classification_Flags);
                    container->insertPointsAttributesAt(CT_LasDefine::Scanner_Channel, att_Scanner_Channel);
                    container->insertPointsAttributesAt(CT_LasDefine::Scan_Direction_Flag, att_Scan_Direction_Flag);
                    container->insertPointsAttributesAt(CT_LasDefine::Edge_of_Flight_Line, att_Edge_of_Flight_Line);
                    container->insertPointsAttributesAt(CT_LasDefine::Classification, att_Classification);
                    container->insertPointsAttributesAt(CT_LasDefine::Scan_Angle_Rank, att_Scan_Angle_Rank);
                    container->insertPointsAttributesAt(CT_LasDefine::User_Data, att_User_Data);
                    container->insertPointsAttributesAt(CT_LasDefine::Point_Source_ID, att_Point_Source_ID);
                    container->insertPointsAttributesAt(CT_LasDefine::GPS_Time, att_GPS_Time);
                    container->insertPointsAttributesAt(CT_LasDefine::Red, att_Red);
                    container->insertPointsAttributesAt(CT_LasDefine::Green, att_Green);
                    container->insertPointsAttributesAt(CT_LasDefine::Blue, att_Blue);
                    container->insertPointsAttributesAt(CT_LasDefine::NIR, att_NIR);
                    container->insertPointsAttributesAt(CT_LasDefine::Wave_Packet_Descriptor_Index, att_Wave_Packet_Descriptor_Index);
                    container->insertPointsAttributesAt(CT_LasDefine::Byte_offset_to_waveform_data, att_Byte_offset_to_waveform_data);
                    container->insertPointsAttributesAt(CT_LasDefine::Waveform_packet_size__inBytes, att_Waveform_packet_size__inBytes);
                    container->insertPointsAttributesAt(CT_LasDefine::Return_Point_Waveform_Location, att_Return_Point_Waveform_Location);

                    CT_PointIterator itP(pointCloudIndexLAS);

                    CT_StandardCloudStdVectorT<double> *intensityAttribute = new CT_StandardCloudStdVectorT<double>();

                    double minAttribute = std::numeric_limits<double>::max();
                    double maxAttribute = -std::numeric_limits<double>::max();

                    size_t localIndex = 0;
                    size_t errorCount = 0;
                    while (itP.hasNext() && !isStopped())
                    {
                        CT_Point point = itP.next().currentPoint();

                        double gpsTime = 0;   // Récupération du temps GPS pour le point
                        double intensity = 0; // Récupération de l'intensité pour le point
                        if (localIndex < pointCloudIndexLAS->size())
                        {
                            gpsTime = att_GPS_Time->dValueAt(localIndex);
                            intensity = att_Intensity->dValueAt(localIndex);
                        }

                        bool found = false;
                        Eigen::Vector3d trajCoord;

                        for (int tra = 0 ; tra < trajectories.size() && !found ; tra++)
                        {
                            CT_ScanPath* sp = trajectories.at(tra);
                            if (sp->isInScanPath(gpsTime))
                            {
                                trajCoord = sp->getPathPointForGPSTime(gpsTime);
                                found = true;
                            }
                        }

                        double length = -1;
                        double corItensity = intensity;
                        if (found)
                        {
                            length = sqrt(pow(point(0) - trajCoord(0), 2) + pow(point(1) - trajCoord(1), 2) + pow(point(2) - trajCoord(2), 2));
                            double ratio = length/_meanHeight;
                            ratio = ratio*ratio;
                            corItensity = corItensity * ratio;

                            if (corItensity < minAttribute) {minAttribute = corItensity;}
                            if (corItensity > maxAttribute) {maxAttribute = corItensity;}
                        } else {

                            errorCount++;
                            if (errorCount < 5)
                            {
                                PS_LOG->addMessage(LogInterface::warning, LogInterface::step, QString(tr("Pas d'information de trajectoire pour le point (%1 ; %2 ; %3)")).arg(point(0)).arg(point(1)).arg(point(2)));
                            }
                        }
                        intensityAttribute->addT(corItensity);

                        setProgress( 95.0*localIndex++ / n_points);
                    }

                    if (errorCount > 0) {PS_LOG->addMessage(LogInterface::warning, LogInterface::step, QString(tr("Pas d'information de trajectoire pour au total %1 points sur %2")).arg(errorCount).arg(localIndex));}

                    CT_PointsAttributesScalarTemplated<double>*  attOut_Intensity  = new CT_PointsAttributesScalarTemplated<double>(_outIntensityAttributeModelName.completeName(),
                                                                                                                                    resOut,
                                                                                                                                    firstAttribute->pointCloudIndexRegistered(),
                                                                                                                                    intensityAttribute,
                                                                                                                                    minAttribute,
                                                                                                                                    maxAttribute);
                    container->insertPointsAttributesAt(CT_LasDefine::Intensity, attOut_Intensity);
                    grp->addSingularItem(attOut_Intensity);
                    grp->addSingularItem(container);
                }
            }
        }
    }
}

