#ifndef ONF_SETFLOATVALUEVISITOR_H
#define ONF_SETFLOATVALUEVISITOR_H

#include "ct_itemdrawable/tools/gridtools/ct_abstractgrid3dbeamvisitor.h"
#include "ct_itemdrawable/ct_grid3d.h"

class ONF_SetFloatValueVisitor : public CT_AbstractGrid3DBeamVisitor
{
public:

    ONF_SetFloatValueVisitor(CT_Grid3D<float> *grid, float value);

    virtual void visit(const size_t &index, const CT_Beam *beam);

private:
    CT_Grid3D<float>*     _grid;
    float                 _value;
};

#endif // ONF_SETFLOATVALUEVISITOR_H
