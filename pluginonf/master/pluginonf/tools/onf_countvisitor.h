#ifndef ONF_COUNTVISITOR_H
#define ONF_COUNTVISITOR_H

#include "ct_itemdrawable/tools/gridtools/ct_abstractgrid3dbeamvisitor.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"

class ONF_CountVisitor : public CT_AbstractGrid3DBeamVisitor
{
public:

    ONF_CountVisitor(CT_Grid3D_Sparse<int> *grid);

    virtual void visit(const size_t &index, const CT_Beam *beam);

private:
    CT_Grid3D_Sparse<int>*     _grid;

};

#endif // ONF_COUNTVISITOR_H
