#ifndef ONF_ACTIONSELECTCLUSTERSINLOGSOPTIONS_H
#define ONF_ACTIONSELECTCLUSTERSINLOGSOPTIONS_H

#include "ct_view/actions/abstract/ct_gabstractactionoptions.h"
#include "actions/onf_actionselectclustersinlogs.h"

namespace Ui {
class ONF_ActionSelectClustersInLogsOptions;
}

class ONF_ActionSelectClustersInLogsOptions : public CT_GAbstractActionOptions
{
    Q_OBJECT

public:
    explicit ONF_ActionSelectClustersInLogsOptions(const ONF_ActionSelectClustersInLogs *action);
    ~ONF_ActionSelectClustersInLogsOptions();

    GraphicsViewInterface::SelectionMode selectionMode() const;
    ONF_ActionSelectClustersInLogs::SelectionTool selectionTool() const;

    bool clusterSelection() const;
    void toggleClusterSelection() const;

//    ONF_ActionSelectClustersInLogs::SelectionDrawMode drawMode() const;

private:
    Ui::ONF_ActionSelectClustersInLogsOptions *ui;

signals:

    void validate();
    void unvalidate();

private slots:
    void on_buttonGroupSelection_buttonReleased(int id);
    void on_buttonGroupType_buttonReleased(int id);
    void on_buttonGroupMode_buttonReleased(int id);
    void on_toolButtonToggleSelection_clicked();

    void on_pb_validate_clicked();
    void on_pb_unvalidate_clicked();

public slots:
    void setSelectionMode(GraphicsViewInterface::SelectionMode mode);
    void setSelectionTool(ONF_ActionSelectClustersInLogs::SelectionTool tool);
};

#endif // ONF_ACTIONSELECTCLUSTERSINLOGSOPTIONS_H
