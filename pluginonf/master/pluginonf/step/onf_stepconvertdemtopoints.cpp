/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepconvertdemtopoints.h"

#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_model/inModel/tools/ct_instdmodelpossibility.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_image2d.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_math/ct_mathpoint.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_global/ct_context.h"

#include <QMessageBox>
#include <limits>

// Alias for indexing models
#define DEF_SearchInResult "r"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInDEM  "dem"


// Constructor : initialization of parameters
ONF_StepConvertDEMToPoints::ONF_StepConvertDEMToPoints(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ONF_StepConvertDEMToPoints::getStepDescription() const
{
    return tr("Convert DEM to point cloud");
}

// Step detailled description
QString ONF_StepConvertDEMToPoints::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString ONF_StepConvertDEMToPoints::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepConvertDEMToPoints::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepConvertDEMToPoints(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepConvertDEMToPoints::createInResultModelListProtected()
{  
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("DEM"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInDEM, CT_AbstractImage2D::staticGetType(), tr("DEM"));
}


// Creation and affiliation of OUT models
void ONF_StepConvertDEMToPoints::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL) {
        res->addItemModel(DEF_SearchInGroup, _outSceneModelName, new CT_Scene(), tr("Point cloud"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepConvertDEMToPoints::createPostConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void ONF_StepConvertDEMToPoints::compute()
{
    CT_ResultGroup* res_out = getOutResultList().first();
    CT_StandardItemGroup* grp = NULL;

    CT_ResultGroupIterator grpPosIt(res_out, this, DEF_SearchInGroup);
    while (grpPosIt.hasNext())
    {
        grp = (CT_StandardItemGroup*) grpPosIt.next();

        CT_AbstractImage2D* inDEM = (CT_AbstractImage2D*)grp->firstItemByINModelName(this, DEF_SearchInDEM);

        if (grp != NULL && inDEM != NULL)
        {
            CT_AbstractUndefinedSizePointCloud* mpcir = PS_REPOSITORY->createNewUndefinedSizePointCloud();
            CT_Point point;
            Eigen::Vector3d cellCenter;

            for (size_t i = 0 ; i < inDEM->nCells() ; i++)
            {
                double value = inDEM->valueAtIndexAsDouble(i);
                if (value != inDEM->NAAsDouble())
                {
                    if (inDEM->getCellCenterCoordinates(i, cellCenter))
                    {
                        point.setValues(cellCenter(0), cellCenter(1), value);
                        mpcir->addPoint(point);
                    }
                }
            }

            CT_Scene* outScene = new CT_Scene(_outSceneModelName.completeName(), res_out, PS_REPOSITORY->registerUndefinedSizePointCloud(mpcir));
            outScene->updateBoundingBox();
            grp->addItemDrawable(outScene);
        }
    }

}


