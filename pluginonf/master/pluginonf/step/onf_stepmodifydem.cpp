/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepmodifydem.h"

#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_model/inModel/tools/ct_instdmodelpossibility.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_math/ct_mathpoint.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_global/ct_context.h"



#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
#include <QtConcurrentMap>
#else
#include <QtConcurrent/QtConcurrentMap>
#endif

#include <QMessageBox>
#include <limits>

// Alias for indexing models
#define DEF_SearchInResult "r"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInDEM  "dem"

#define DEFin_resIMG "resIMG"
#define DEFin_grpIMG "grpIMG"
#define DEFin_red "red"
#define DEFin_green "green"
#define DEFin_blue "blue"



// Constructor : initialization of parameters
ONF_StepModifyDEM::ONF_StepModifyDEM(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    m_doc = NULL;

    setManual(true);
}

// Step description (tooltip of contextual menu)
QString ONF_StepModifyDEM::getStepDescription() const
{
    return tr("Modify DEM");
}

// Step detailled description
QString ONF_StepModifyDEM::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString ONF_StepModifyDEM::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepModifyDEM::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepModifyDEM(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepModifyDEM::createInResultModelListProtected()
{  
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("DEM"), "", true);

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInDEM, CT_Image2D<float>::staticGetType(), tr("DEM à modifier"));

    CT_InResultModelGroup *resIMG = createNewInResultModel(DEFin_resIMG, tr("2D Images (optionnel)"), "", true);
    resIMG->setZeroOrMoreRootGroup();
    resIMG->addGroupModel("", DEFin_grpIMG, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resIMG->addItemModel(DEFin_grpIMG, DEFin_red, CT_AbstractImage2D::staticGetType(), tr("Red band"));
    resIMG->addItemModel(DEFin_grpIMG, DEFin_green, CT_AbstractImage2D::staticGetType(), tr("Green band"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resIMG->addItemModel(DEFin_grpIMG, DEFin_blue, CT_AbstractImage2D::staticGetType(), tr("Blue band"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resIMG->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);
}


// Creation and affiliation of OUT models
void ONF_StepModifyDEM::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL) {
        res->addItemModel(DEF_SearchInGroup, _outDEMModelName, new CT_Image2D<float>(), tr("DEM modifié"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepModifyDEM::createPostConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void ONF_StepModifyDEM::compute()
{
    CT_ResultGroup* res_out = getOutResultList().first();
    CT_StandardItemGroup* grp = NULL;

    CT_ResultGroup* res_IMG = NULL;
    _inred = NULL;
    _ingreen = NULL;
    _inblue = NULL;
    if (getInputResults().size() > 1)
    {
        res_IMG = getInputResults().at(1);
        CT_ResultGroupIterator imgIt(res_IMG, this, DEFin_grpIMG);
        if (imgIt.hasNext())
        {
            CT_StandardItemGroup* grpIMG = (CT_StandardItemGroup*) imgIt.next();

            _inred = (CT_AbstractImage2D*)grpIMG->firstItemByINModelName(this, DEFin_red);
            _ingreen = (CT_AbstractImage2D*)grpIMG->firstItemByINModelName(this, DEFin_green);
            _inblue = (CT_AbstractImage2D*)grpIMG->firstItemByINModelName(this, DEFin_blue);

            if (_inred != NULL)
            {
                if (_ingreen == NULL) {_ingreen = _inred;}
                if (_inblue == NULL) {_inblue = _inred;}
            }
        }
    }

    CT_ResultGroupIterator grpPosIt(res_out, this, DEF_SearchInGroup);
    if (grpPosIt.hasNext())
    {
        grp = (CT_StandardItemGroup*) grpPosIt.next();

        _inDEM = (CT_Image2D<float>*)grp->firstItemByINModelName(this, DEF_SearchInDEM);

        if (grp != NULL && _inDEM != NULL)
        {
            CT_AbstractUndefinedSizePointCloud* mpcir = PS_REPOSITORY->createNewUndefinedSizePointCloud();
            CT_Point point;
            size_t col, lin;
            Eigen::Vector3d cellCenter;

            for (size_t i = 0 ; i < _inDEM->nCells() ; i++)
            {
                float value = _inDEM->valueAtIndex(i);
                if (value != _inDEM->NA())
                {
                    _inDEM->setValueAtIndex(i, value);

                    if (_inDEM->indexToGrid(i, col, lin))
                    {
                        if (_inDEM->getCellCenterCoordinates(i, cellCenter))
                        {
                            point.setValues(cellCenter(0), cellCenter(1), value);
                            mpcir->addPoint(point);
                        }
                    }
                }
            }

            _scene = new CT_Scene("ONF_StepModifyDEM_01", res_out, PS_REPOSITORY->registerUndefinedSizePointCloud(mpcir));
            _scene->updateBoundingBox();


            _modifiedDEM = new CT_Image2D<float>(_outDEMModelName.completeName(), res_out, _inDEM->minX(), _inDEM->minY(), _inDEM->colDim(), _inDEM->linDim(), _inDEM->resolution(), _inDEM->minZ(), -9999, -9999);
            CT_PointIterator itP0(_scene->getPointCloudIndex());
            while(itP0.hasNext() && (!isStopped()))
            {
                const CT_Point &point = itP0.next().currentPoint();
                CT_PointData &pt = itP0.currentInternalPoint();

                _modifiedDEM->setValueAtCoords(point(0), point(1), pt(2));
            }

            // Début de la partie interactive
            m_doc = NULL;

            m_status = 0;
            requestManualMode();

            m_status = 1;
            requestManualMode();
            // Fin de la partie interactive

            _modifiedDEM->computeMinMax();
            grp->addItemDrawable(_modifiedDEM);

            delete _scene;
        }
    }

}


void ONF_StepModifyDEM::initManualMode()
{
    QColor color(0, 0, 75);
    // create a new 3D document
    if(m_doc == NULL)
        m_doc = getGuiContext()->documentManager()->new3DDocument(4.0, true, &color);

    m_doc->removeAllItemDrawable();

    // set the action (a copy of the action is added at all graphics view, and the action passed in parameter is deleted)
    m_doc->setCurrentAction(new ONF_ActionModifyDEM(_scene, _inDEM, _modifiedDEM, _inred, _ingreen, _inblue));

    QMessageBox::information(NULL, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette "
                                                         "étape de modification de MNE."), QMessageBox::Ok);
}

void ONF_StepModifyDEM::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = NULL;

            quitManualMode();
        }
    }
}
