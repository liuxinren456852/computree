/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepapplydtmtocircle2d.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_circle2d.h"
#include "ct_shapedata/ct_linedata.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_result/ct_resultgroup.h"
#include "qdebug.h"

#define DEF_SearchInCircle2D  "rp"
#define DEF_SearchInGroup       "g"
#define DEF_SearchInResult      "r"
#define DEF_SearchInCircle2DAttID      "at1"
#define DEF_SearchInCircle2DAttVal      "at2"

#define DEF_SearchInResultMNT   "iresMNT"
#define DEF_SearchInGroupMNT   "igrpMNT"
#define DEF_SearchInMNT   "imnt"


ONF_StepApplyDTMToCircle2D::ONF_StepApplyDTMToCircle2D(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _offset = 1.3;
}

QString ONF_StepApplyDTMToCircle2D::getStepDescription() const
{
    return tr("Translater des cercles 2D sur un MNT");
}

QString ONF_StepApplyDTMToCircle2D::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

CT_VirtualAbstractStep* ONF_StepApplyDTMToCircle2D::createNewInstance(CT_StepInitializeData &dataInit)
{
    // cree une copie de cette etape
    return new ONF_StepApplyDTMToCircle2D(dataInit);
}

//////////////////// PROTECTED //////////////////

void ONF_StepApplyDTMToCircle2D::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy* resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Cercles"));
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInCircle2D, CT_Circle2D::staticGetType(), tr("Cercles"));
    resultModel->addItemAttributeModel(DEF_SearchInCircle2D, DEF_SearchInCircle2DAttID, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::ANY, tr("ID"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resultModel->addItemAttributeModel(DEF_SearchInCircle2D, DEF_SearchInCircle2DAttVal, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::NUMBER, tr("Value"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);

    CT_InResultModelGroupToCopy *resultModelMNT = createNewInResultModelForCopy(DEF_SearchInResultMNT, tr("MNT"),"", true);

    resultModelMNT->setZeroOrMoreRootGroup();
    resultModelMNT->addGroupModel("", DEF_SearchInGroupMNT);
    resultModelMNT->addItemModel(DEF_SearchInGroupMNT, DEF_SearchInMNT, CT_Image2D<float>::staticGetType(), tr("MNT"));

}

void ONF_StepApplyDTMToCircle2D::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addDouble(tr("Offset"), "m", -1e+10, 1e+10, 2, _offset);
}

void ONF_StepApplyDTMToCircle2D::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL)
    {
        res->addItemModel(DEF_SearchInGroup, _outcircle3DModelName, new CT_Circle(), tr("Cercle 3D"));
        res->addItemAttributeModel(_outcircle3DModelName, _outcircle3DIDModelName, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_VALUE), tr("ID"));
        res->addItemAttributeModel(_outcircle3DModelName, _outcircle3DValModelName, new CT_StdItemAttributeT<double>(CT_AbstractCategory::DATA_NUMBER), tr("Value"));
    }
}

void ONF_StepApplyDTMToCircle2D::compute()
{

    CT_Image2D<float>* mnt = NULL;
    if (getInputResults().size() > 1)
    {
        CT_ResultGroup* resin_DTM = getInputResults().at(1);
        CT_ResultItemIterator it(resin_DTM, this, DEF_SearchInMNT);
        if (it.hasNext())
        {
            mnt = (CT_Image2D<float>*) it.next();
        } else {return;}
    } else {return;}

    // on récupère le résultat copié
    CT_ResultGroup *outRes = getOutResultList().at(0);

    CT_ResultGroupIterator itSection(outRes, this, DEF_SearchInGroup);
    while (itSection.hasNext() && (!isStopped()))
    {
        CT_StandardItemGroup *group = (CT_StandardItemGroup*) itSection.next();

        CT_Circle2D* circle2d = (CT_Circle2D*) group->firstItemByINModelName(this, DEF_SearchInCircle2D);

        if (circle2d != NULL)
        {
            QString circleID;
            CT_AbstractItemAttribute* attID = circle2d->firstItemAttributeByINModelName(outRes, this, DEF_SearchInCircle2DAttID);
            if (attID != NULL) {circleID = attID->toString(circle2d, NULL);}

            double circleVal;
            CT_AbstractItemAttribute* attVal = circle2d->firstItemAttributeByINModelName(outRes, this, DEF_SearchInCircle2DAttVal);
            if (attVal != NULL) {circleVal = attVal->toDouble(circle2d, NULL);}

            double zval = mnt->valueAtCoords(circle2d->getCenterX(), circle2d->getCenterY()) + _offset;

            CT_Circle* circle = new CT_Circle(_outcircle3DModelName.completeName(), outRes, new CT_CircleData(Eigen::Vector3d(circle2d->getCenterX(), circle2d->getCenterY(), zval), Eigen::Vector3d(0, 0, 1), circle2d->getRadius(), 0));
            circle->addItemAttribute(new CT_StdItemAttributeT<QString>(_outcircle3DIDModelName.completeName(), CT_AbstractCategory::DATA_VALUE, outRes, circleID));
            circle->addItemAttribute(new CT_StdItemAttributeT<double>(_outcircle3DValModelName.completeName(), CT_AbstractCategory::DATA_NUMBER, outRes, circleVal));

            group->addItemDrawable(circle);
        }
    }
}

