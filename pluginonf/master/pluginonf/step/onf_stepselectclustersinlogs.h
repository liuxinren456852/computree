﻿#ifndef ONF_STEPSELECTCLUSTERSINLOGS_H
#define ONF_STEPSELECTCLUSTERSINLOGS_H

#include "ct_step/abstract/ct_abstractstep.h"

class CT_AbstractSingularItemDrawable;
class CT_AbstractItemGroup;


class ONF_StepSelectClustersInLogs: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    ONF_StepSelectClustersInLogs(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

    void initManualMode();

    void useManualMode(bool quit = false);

private:

    // Step parameters
    QHash<CT_AbstractItemDrawable*, CT_AbstractItemGroup*>      m_itemDrawableToAdd;
    QList<CT_AbstractItemDrawable*>                             m_validatedItems;
    DocumentInterface                                           *m_doc;
    int                                                         m_status;

    QHash<CT_AbstractItemDrawable*, quint64>                        m_clusterToLog;
    QMultiHash<quint64, CT_AbstractItemDrawable*>                   m_logToCluster;



    void recursiveRemoveGroup(CT_AbstractItemGroup *parent, CT_AbstractItemGroup *group) const;
};

#endif // ONF_STEPSELECTCLUSTERSINLOGS_H
