#include "model/step/models/abstractoutmodel.h"

AbstractOutModel::AbstractOutModel() : QStandardItem()
{
}

AbstractOutModel::~AbstractOutModel()
{
    delete _widget;
}
AbstractOutWidget* AbstractOutModel::getWidget()
{
    return _widget;
}

QString AbstractOutModel::getDef()
{
    return _widget->getDEF();
}

QString AbstractOutModel::getPrefixedAlias()
{
    return _widget->getPrefixedAliad();
}

QString AbstractOutModel::getAlias()
{
    return _widget->getAlias();
}

QString AbstractOutModel::getDisplayableName()
{
    return _widget->getDisplayableName();
}

QString AbstractOutModel::getDescription()
{
    return _widget->getDescription();
}

bool AbstractOutModel::isValid()
{
    for (int i = 0 ; i < rowCount() ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) child(i);
        if (!item->isValid()) {return false;}
    }
    return getWidget()->isvalid();
}

QString AbstractOutModel::getOutModelsDefines()
{
    QString result = "#define ";
    result.append(getDef());
    result.append(QString(" \"%1\"\n").arg(getAlias()));

    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) child(i);
        result.append(item->getOutModelsDefines());
    }
    return result;
}

void AbstractOutModel::getChildrenIncludes(QSet<QString> &list)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) child(i);
        item->getIncludes(list);
    }
}

void AbstractOutModel::getChildrenCreateOutResultModelListProtectedContent(QString &result, QString parentDEF, QString resultModelName)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) child(i);

        QString childContent = item->getCreateOutResultModelListProtectedContent(parentDEF, resultModelName);
        if (childContent.size() > 0)
        {
            result.append(childContent);
        }
    }
}

void AbstractOutModel::getChildrenComputeContent(QString &result, QString resultName)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractOutModel* item = (AbstractOutModel*) child(i);

        QString childContent = item->getComputeContent(resultName, getName());
        if (childContent.size() > 0)
        {
            result.append(childContent);
        }
    }
}

void AbstractOutModel::onAliasChange()
{
    setText(getPrefixedAlias());
}
