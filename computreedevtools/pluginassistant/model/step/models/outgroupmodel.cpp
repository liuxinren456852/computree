#include "outgroupmodel.h"
#include "view/step/models/outgroupwidget.h"
#include "model/step/models/outitemmodel.h"
#include "model/step/tools.h"

OUTGroupModel::OUTGroupModel() : AbstractOutModel()
{
    _widget = new OUTGroupWidget(this);
    setText(getPrefixedAlias());
}

QString OUTGroupModel::getName()
{
    return "grp_" + getAlias();
}

void OUTGroupModel::getIncludes(QSet<QString> &list)
{
    getChildrenIncludes(list);
}

QString OUTGroupModel::getCreateOutResultModelListProtectedContent(QString parentDEF, QString resultModelName, bool rootGroup)
{
    QString result = "";

    result += Tools::getIndentation(1) + resultModelName;

    if (rootGroup)
    {
        result += "->setRootGroup(";
    } else if (parentDEF == "") {
        result += "->addGroupModel(\"\", ";
    } else {
        result += "->addGroupModel(" + parentDEF +", ";
    }

    result += getDef() + ", new CT_StandardItemGroup()";

    if (getDisplayableName() != "" || getDescription() != "")
    {
        result += ", " + Tools::trs(getDisplayableName());
    }

    if (getDescription() != "")
    {
        result += ", " + Tools::trs(getDescription());
    }

    result += ");\n";

    getChildrenCreateOutResultModelListProtectedContent(result, getDef(), resultModelName);

    return result;
}

QString OUTGroupModel::getComputeContent(QString resultName, QString parentName)
{
    QString result = "";

    result += Tools::getIndentation(1) + "CT_StandardItemGroup* " + getName() + "= new CT_StandardItemGroup(" + getDef() + ", " + resultName + ");\n";
    result += Tools::getIndentation(1) + parentName + "->addGroup(" + getName() + ");\n";
    result += Tools::getIndentation(1) + "\n";

    getChildrenComputeContent(result, resultName);

    return result;
}
