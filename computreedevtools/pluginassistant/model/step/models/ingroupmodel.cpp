#include "ingroupmodel.h"
#include "view/step/models/ingroupwidget.h"
#include "model/step/models/initemmodel.h"
#include "model/step/tools.h"

INGroupModel::INGroupModel() : AbstractInModel()
{
    _widget = new INGroupWidget(this);
    setText(getPrefixedAlias());
}

QString INGroupModel::getName()
{
    return "grpIn_" + getAlias();
}

QString INGroupModel::getChoiceMode()
{
    INGroupWidget::ChoiceMode choiceMode = ((INGroupWidget*)_widget)->getChoiceMode();

    if (choiceMode == INGroupWidget::C_OneIfMultiple)
    {
        return "CT_InAbstractGroupModel::CG_ChooseOneIfMultiple";
    }
    return "CT_InAbstractGroupModel::CG_ChooseMultipleIfMultiple";;
}


QString INGroupModel::getFinderMode()
{
    INGroupWidget::FinderMode finderMode = ((INGroupWidget*)_widget)->getFinderMode();

    if (finderMode == INGroupWidget::F_Optional)
    {
        return "CT_InAbstractGroupModel::FG_IsOptional";
    }
    return "CT_InAbstractGroupModel::FG_IsObligatory";
}

void INGroupModel::getIncludes(QSet<QString> &list)
{
    list.insert("#include \"ct_itemdrawable/tools/iterator/ct_groupiterator.h\"\n");
    getChildrenIncludes(list);
}

QString INGroupModel::getCreateInResultModelListProtectedContent(QString parentDEF, QString resultModelName, bool rootGroup)
{
    QString result = "";

    result += Tools::getIndentation(1) + resultModelName;

    if (rootGroup)
    {
        result += "->setRootGroup(";
    } else if (parentDEF == "") {
        result += "->addGroupModel(\"\", ";
    } else {
        result += "->addGroupModel(" + parentDEF +", ";
    }

    result += getDef();

    if (getDisplayableName() != "" || getDescription() != "" || getChoiceMode() != "CT_InAbstractGroupModel::CG_ChooseMultipleIfMultiple")
    {
        result += ", CT_AbstractItemGroup::staticGetType(), " + Tools::trs(getDisplayableName());
    }

    if (getDescription() != "" || getChoiceMode() != "CT_InAbstractGroupModel::CG_ChooseMultipleIfMultiple")
    {
        result += ", " + Tools::trs(getDescription());
    }

    if (getChoiceMode() != "CT_InAbstractGroupModel::CG_ChooseMultipleIfMultiple")
    {
        result += ", " + getChoiceMode();
    }

    result += ");\n";

    getChildrenCreateInResultModelListProtectedContent(result, getDef(), resultModelName);

    return result;
}

QString INGroupModel::getComputeContent(QString parentName, int indent, bool rootGroup)
{
    QString result = "";

    QString iterator = "CT_GroupIterator";
    if (rootGroup) {iterator = "CT_ResultGroupIterator";}

    result += Tools::getIndentation(indent) + iterator + " itIn_" + getAlias() + "(" + parentName + ", this, " + getDef() + ");\n";
    result += Tools::getIndentation(indent) + "while (itIn_" + getAlias() + ".hasNext() && !isStopped())\n";
    result += Tools::getIndentation(indent) + "{\n";
    result += Tools::getIndentation(indent+1) + "const CT_AbstractItemGroup* " + getName() + " = (CT_AbstractItemGroup*) itIn_" + getAlias() + ".next();\n";
    result += Tools::getIndentation(indent+1) + "\n";

    getChildrenComputeContent(result, indent +1);

    result += Tools::getIndentation(indent) + "}\n";
    result += Tools::getIndentation(indent) + "\n";

    return result;
}



