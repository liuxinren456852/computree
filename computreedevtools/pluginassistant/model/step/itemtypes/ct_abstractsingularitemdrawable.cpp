#include "ct_abstractsingularitemdrawable.h"
#include "model/step/tools.h"

CT_AbstractSingularItemDrawable::CT_AbstractSingularItemDrawable() : AbstractItemType()
{
}

QString CT_AbstractSingularItemDrawable::getTypeName()
{
    return "CT_AbstractSingularItemDrawable";
}

QString CT_AbstractSingularItemDrawable::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_AbstractSingularItemDrawable::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    Q_UNUSED(modelName);
    Q_UNUSED(resultName);
    return "";
}
