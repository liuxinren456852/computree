#include "ct_grid2d.h"
#include "model/step/tools.h"

CT_Grid2D::CT_Grid2D() : AbstractTemplatedItemType()
{
}

QString CT_Grid2D::getTypeName()
{
    return "CT_Grid2DXY";
}

QString CT_Grid2D::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Grid2D::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
