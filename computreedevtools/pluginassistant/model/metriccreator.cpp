#include "metriccreator.h"

#include <QTextStream>
#include <QDir>

MetricCreator::MetricCreator(QString directory, QString name) : QObject()
{
    _directory = directory;
    _name = name;
}

QString MetricCreator::createMetricFiles()
{
    QString errors = "";

    QDir dir(_directory);

    if (!dir.exists())
    {
        return tr("Le répertoire choisi n'existe pas', veuillez en choisir un autre svp");
    }

    dir.mkdir("metric");

    if (!create_MetricFile_h()) {errors.append(tr("Impossible de créer le fichier metric (.h)\n"));}
    if (!create_MetricFile_cpp()) {errors.append(tr("Impossible de créer le fichier metric (.cpp)\n"));}

    return errors;
}

bool MetricCreator::create_MetricFile_h()
{
    QFile file(QString("%1/metric/%2.h").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        stream << "#ifndef " << _name.toUpper() << "_H\n";
        stream << "#define " << _name.toUpper() << "_H\n";
        stream << "\n";
        stream << "\n";
        stream << "#include \"ct_metric/abstract/ct_abstractmetric_xyz.h\"\n";
        stream << "\n";
        stream << "class " << _name << " : public CT_AbstractMetric_XYZ\n";
        stream << "{\n";
        stream << "    Q_OBJECT\n";
        stream << "public:\n";
        stream << "\n";
        stream << "    " << _name << "();\n";
        stream << "    " << _name << "(const " << _name << "* other);\n";
        stream << "\n";
        stream << "    QString getName();\n";
        stream << "\n";
        stream << "    void createConfigurationDialog();\n";
        stream << "    void updateParamtersAfterConfiguration();\n";
        stream << "\n";
        stream << "    QString getShortDescription() const;\n";
        stream << "    QString getDetailledDescription() const;\n";
        stream << "\n";
        stream << "    QString getParametersAsString() const;\n";
        stream << "    virtual bool setParametersFromString(QString parameters);\n";
        stream << "\n";
        stream << "    void createAttributes();\n";
        stream << "    void computeMetric();\n";
        stream << "\n";
        stream << "\n";
        stream << "    CT_AbstractConfigurableElement* copy() const;\n";
        stream << "};\n";
        stream << "\n";
        stream << "\n";
        stream << "#endif // " << _name.toUpper() << "_H\n";

        file.close();
        return true;
    }
    return false;
}

bool MetricCreator::create_MetricFile_cpp()
{
    QFile file(QString("%1/metric/%2.cpp").arg(_directory).arg(_name.toLower()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << "#include \"metric/" << _name.toLower() << ".h\"\n";
        stream << "#include \"ct_pointcloudindex/ct_pointcloudindexvector.h\"\n";
        stream << "#include \"ct_iterator/ct_pointiterator.h\"\n";
        stream << "\n";
        stream << _name << "::" << _name << "() : CT_AbstractMetric_XYZ()\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";
        stream << _name << "::" << _name << "(const " << _name << " *other) : CT_AbstractMetric_XYZ(other)\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::getName()\n";
        stream << "{\n";
        stream << "    return QString(\"PB_Name\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::createConfigurationDialog()\n";
        stream << "{\n";
        stream << "    CT_StepConfigurableDialog* configDialog = addConfigurationDialog();\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::updateParamtersAfterConfiguration()\n";
        stream << "{\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::getParametersAsString() const\n";
        stream << "{\n";
        stream << "    QString result = \"\";\n";
        stream << "    return result;\n";
        stream << "}\n";
        stream << "\n";
        stream << "bool " << _name << "::setParametersFromString(QString parameters)\n";
        stream << "{\n";
        stream << "    return true;\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::createAttributes()\n";
        stream << "{\n";
        stream << "    addAttribute(\"ValueName\", CT_AbstractMetric::AT_double, CT_AbstractCategory::DATA_Z);\n";
        stream << "}\n";
        stream << "\n";
        stream << "void " << _name << "::computeMetric()\n";
        stream << "{\n";
        stream << "    double value = -std::numeric_limits<double>::max();\n";
        stream << "    CT_PointIterator itP(_inCloud);\n";
        stream << "    while(itP.hasNext())\n";
        stream << "    {\n";
        stream << "        const CT_Point& point = itP.next().currentPoint();\n";
        stream << "\n";
        stream << "        if (_plotArea->contains(point(0), point(1)) && point(2) > value) {value = point(2);}\n";
        stream << "    }\n";
        stream << "    setAttributeValue(\"ValueName\", value);\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::getShortDescription() const\n";
        stream << "{\n";
        stream << "    return tr(\"short description here\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "QString " << _name << "::getDetailledDescription() const\n";
        stream << "{\n";
        stream << "    return tr(\"\");\n";
        stream << "}\n";
        stream << "\n";
        stream << "CT_AbstractConfigurableElement *" << _name << "::copy() const\n";
        stream << "{\n";
        stream << "    " << _name << "* metric = new " << _name << "(this);\n";
        stream << "    // Copy the parameters values here !\n";
        stream << "    return metric;\n";
        stream << "}\n";


        file.close();
        return true;
    }
    return false;
}

