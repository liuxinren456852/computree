#ifndef CREATEREADERDIALOG_H
#define CREATEREADERDIALOG_H

#include <QDialog>

namespace Ui {
class CreateReaderDialog;
}

class CreateReaderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateReaderDialog(QString code, QWidget *parent = 0);
    ~CreateReaderDialog();

    QString getName();

private:
    Ui::CreateReaderDialog *ui;
};

#endif // CREATEREADERDIALOG_H
