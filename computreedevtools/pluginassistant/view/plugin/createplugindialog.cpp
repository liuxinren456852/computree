#include "view/plugin/createplugindialog.h"
#include "ui_createplugindialog.h"

#include "model/plugincreator.h"

#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

CreatePluginDialog::CreatePluginDialog(QString pluginDirectory, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreatePluginDialog)
{
    ui->setupUi(this);
    ui->directory->setText(pluginDirectory);
    ui->code->setText("");
    ui->name->setText("");
    ui->code->setFocus();

    on_pb_directory_clicked();
}

CreatePluginDialog::~CreatePluginDialog()
{
    delete ui;
}

QString CreatePluginDialog::getDirectory()
{
    return ui->directory->text();
}

QString CreatePluginDialog::getCode()
{
    return ui->code->text();
}

QString CreatePluginDialog::getName()
{
    return ui->name->text();
}

void CreatePluginDialog::on_code_textChanged(const QString &arg1)
{
    ui->code->setText(ui->code->text().toUpper());
}

void CreatePluginDialog::on_pb_directory_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Choix du répertoire du plugin"), ui->directory->text());
    if (!QFileInfo(dir).exists())
    {
        ui->directory->setText("");
        QMessageBox(QMessageBox::Critical, tr("Erreur"),
                    tr("Le répertoire choisi n'existe pas")).exec();
    } else if (!PluginCreator::isValidPluginDirectory(dir))
    {
        ui->directory->setText("");
        QMessageBox(QMessageBox::Critical, tr("Erreur"),
                    tr("Le répertoire choisi ne peut accéder à pluginsharedv2.\n"
                       "Celui-ci devrait se trouver relativement ici : ../../pluginsharedv2")).exec();
    } else {
        ui->directory->setText(dir);
    }
}

void CreatePluginDialog::on_pb_validate_clicked()
{
    if (PluginCreator::isValidPluginDirectory(ui->directory->text()) &&
            PluginCreator::isValidNameAndCode(ui->name->text(), ui->code->text()))
    {
        accept();
    } else {
        QMessageBox(QMessageBox::Critical, tr("Erreur"),
                    tr("Paramètres non valides")).exec();
    }
}
