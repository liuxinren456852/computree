CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}


CONFIG += c++17
include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += concurrent

TARGET = plug_onflsisv2

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    ol_pluginentry.h \
    ol_steppluginmanager.h \
    step/ol_stepcreatepolylines02.h \
    step/ol_stepfilterarcpolylines02.h \
    step/ol_stepthrowparticules05.h

SOURCES += \
   ol_pluginentry.cpp \
   ol_steppluginmanager.cpp \
   step/ol_stepcreatepolylines02.cpp \
   step/ol_stepfilterarcpolylines02.cpp \
    step/ol_stepthrowparticules05.cpp

TRANSLATIONS += languages/pluginonflsis_en.ts \
                languages/pluginonflsis_fr.ts

