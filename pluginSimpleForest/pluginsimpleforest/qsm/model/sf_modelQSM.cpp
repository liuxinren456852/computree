/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_modelQSM.h"

std::shared_ptr<SF_ModelSegment>
SF_ModelQSM::getRootSegment() const
{
  return m_rootSegment;
}

std::shared_ptr<SF_ModelQSM>
SF_ModelQSM::clone()
{
  std::shared_ptr<SF_ModelQSM> qsm(new SF_ModelQSM);
  auto rootClone = m_rootSegment->clone();
  qsm->setID(m_id);
  qsm->setTranslation(m_translation);
  qsm->setRootSegment(rootClone);
  qsm->setSpecies(m_species);
  for (auto segment : qsm->getSegments()) {
    segment->setTree(qsm);
  }
  qsm->sort(SF_ModelSegment::SF_SORTTYPE::GROWTH_LENGTH);
  qsm->setBranchorder();
  qsm->setAllometryDirty(getAllometryDirty());
  return qsm;
}

void
SF_ModelQSM::setRootSegment(const std::shared_ptr<SF_ModelSegment>& rootSegment)
{
  m_rootSegment = rootSegment;
  auto segments = getSegments();
  for (auto seg : segments) {
    seg->setTree(shared_from_this());
  }
}

double
SF_ModelQSM::getVolume() const
{
  const auto volumeQSM = getRootBuildingBrick()->getGrowthVolume();
  return volumeQSM + m_volumeCorrection;
}

void
SF_ModelQSM::setBranchorder()
{
  m_rootSegment->initializeOrder();
  m_rootSegment->computeBranchOrder(0);
  setSegmentId();
  setBuildingBrickId();
  setReverseBranchOrder();
  setReverseSummedBranchOrder();
  setBranchId();
}

void
SF_ModelQSM::setSegmentId()
{
  std::uint32_t index = 0;
  auto segments = getSegments();
  for (const auto& segment : segments) {
    segment->setID(index++);
  }
}

void
SF_ModelQSM::setBuildingBrickId()
{
  std::uint32_t index = 0;
  auto bricks = getBuildingBricks();
  for (const auto& brick : bricks) {
    brick->setID(index++);
  }
}

void
SF_ModelQSM::setReverseBranchOrder()
{
  std::vector<std::shared_ptr<SF_ModelSegment>> leaves = getLeaveSegments();
  for (const auto& leaf : leaves) {
    leaf->computeReverseBranchOrder(1);
  }
}

void
SF_ModelQSM::setReverseSummedBranchOrder()
{
  std::vector<std::shared_ptr<SF_ModelSegment>> leaves = getLeaveSegments();
  for (const auto& leaf : leaves) {
    leaf->computeReverseSummedBranchOrder(1);
  }
}

void
SF_ModelQSM::setBranchId()
{
  auto segments = getSegments();
  std::uint32_t index = 1;
  for (const auto& segment : segments) {
    if (segment->isStartOfBranchOrder(1)) {
      segment->computeBranchID(index++);
    }
  }
}

void
SF_ModelQSM::sort(SF_ModelSegment::SF_SORTTYPE type)
{
  m_rootSegment->sort(type);
  setBranchorder();
}

bool
SF_ModelQSM::getHasCorrectedParameters() const
{
  return m_hasCorrectedParameters;
}

void
SF_ModelQSM::setHasCorrectedParameters(bool hasCorrectedParameters)
{
  m_hasCorrectedParameters = hasCorrectedParameters;
}

double
SF_ModelQSM::getAGrowthLength() const
{
  return m_aGrowthLength;
}

void
SF_ModelQSM::setA(double aGrowthLength)
{
  m_aGrowthLength = aGrowthLength;
}

double
SF_ModelQSM::getBGrowthLength() const
{
  return m_bGrowthLength;
}

void
SF_ModelQSM::setB(double bGrowthLength)
{
  m_bGrowthLength = bGrowthLength;
}

double
SF_ModelQSM::getCGrowthLength() const
{
  return m_cGrowthLength;
}

void
SF_ModelQSM::setC(double cGrowthLength)
{
  m_cGrowthLength = cGrowthLength;
}

double
SF_ModelQSM::getAGrowthVolume() const
{
  return m_aGrowthVolume;
}

void
SF_ModelQSM::setAGrowthVolume(double aGrowthVolume)
{
  m_aGrowthVolume = aGrowthVolume;
}

double
SF_ModelQSM::getBGrowthVolume() const
{
  return m_bGrowthVolume;
}

void
SF_ModelQSM::setBGrowthVolume(double bGrowthVolume)
{
  m_bGrowthVolume = bGrowthVolume;
}

double
SF_ModelQSM::getCGrowthVolume() const
{
  return m_cGrowthVolume;
}

void
SF_ModelQSM::setCGrowthVolume(double cGrowthVolume)
{
  m_cGrowthVolume = cGrowthVolume;
}

int
SF_ModelQSM::getID() const
{
  return m_id;
}

void
SF_ModelQSM::setID(int Id)
{
  m_id = Id;
}

void
SF_ModelQSM::setTranslation(const Eigen::Vector3d& translation)
{
  m_translation = translation;
}

void
SF_ModelQSM::setSpecies(const std::string& species)
{
  m_species = species;
}

SF_ModelQSM::SF_ModelQSM(const int Id) : m_id(Id), m_species("unknownSpecies") {}

std::string
SF_ModelQSM::toString()
{
  constexpr auto sep = ", ";
  std::string str = std::to_string(m_id) + sep + m_species + sep + std::to_string(m_translation.coeff(0)) + sep +
                    std::to_string(m_translation.coeff(1)) + sep + std::to_string(m_translation.coeff(2)) + sep +
                    std::to_string(m_aGrowthVolume) + sep + std::to_string(m_bGrowthVolume) + sep + std::to_string(m_cGrowthVolume) +
                    sep + std::to_string(m_aGrowthLength) + sep + std::to_string(m_bGrowthLength) + sep +
                    std::to_string(m_cGrowthLength) + "\n";
  return str;
}

std::string
SF_ModelQSM::toHeaderString()
{
  return std::string("treeID, treeSpecies, translateX, translateY, translatez, gvA, gvB, gvC, glA, glB, glC\n");
}

void
SF_ModelQSM::translate(const Eigen::Vector3d& translation)
{
  m_translation += translation;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingBricks = getBuildingBricks();
  for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> buildingBrick : buildingBricks) {
    buildingBrick->translate(translation);
  }
}

void
SF_ModelQSM::transform(const Eigen::Affine3f& transform)
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingBricks = getBuildingBricks();
  for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> buildingBrick : buildingBricks) {
    buildingBrick->transform(transform);
  }
}

Eigen::Vector3d
SF_ModelQSM::translateToOrigin()
{
  Eigen::Vector3d translation(0, 0, 0);
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingBricks = getBuildingBricks();
  if (buildingBricks.empty()) {
    return translation;
  }
  for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> buildingBrick : buildingBricks) {
    translation += buildingBrick->getCenter();
  }
  translation /= buildingBricks.size();
  translate(translation);
  return translation;
}

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
SF_ModelQSM::getRootBuildingBrick() const
{
  return m_rootSegment->getBuildingBricks().front();
}

std::shared_ptr<SF_ModelSegment>
SF_ModelQSM::crownStartSegment() const
{
  const auto segments = getSegments();
  const auto largetsBranchStart = std::max_element(
    segments.cbegin(),
    segments.cend(),
    [](const std::shared_ptr<SF_ModelSegment>& segment1, const std::shared_ptr<SF_ModelSegment>& segment2) {
      double growthLength1 = segment1->isStartOfBranchOrder(1) ? segment1->getGrowthLength() : 0.;
      double growthLength2 = segment2->isStartOfBranchOrder(1) ? segment2->getGrowthLength() : 0.;
      return growthLength1 < growthLength2;
    });
  return (*largetsBranchStart)->getParent();
}

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
SF_ModelQSM::crownStartBrick() const
{
  const auto segment = crownStartSegment();
  return segment->getBuildingBricks().back();
}

std::vector<std::shared_ptr<SF_ModelSegment>>
SF_ModelQSM::getSegments() const
{
  std::vector<std::shared_ptr<SF_ModelSegment>> segments;
  if (m_rootSegment != nullptr) {
    segments = getSegments(m_rootSegment);
  }
  return segments;
}

std::vector<std::shared_ptr<SF_ModelSegment>>
SF_ModelQSM::getSegments(const std::shared_ptr<SF_ModelSegment>& segment) const
{
  std::vector<std::shared_ptr<SF_ModelSegment>> segments;
  segments.push_back(segment);
  auto children = segment->getChildren();
  for (const auto& child : children) {
    std::vector<std::shared_ptr<SF_ModelSegment>> childSegments = getSegments(child);
    segments.insert(segments.end(), childSegments.begin(), childSegments.end());
  }
  return segments;
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_ModelQSM::getBuildingBricks() const
{
  std::vector<std::shared_ptr<SF_ModelSegment>> segments = getSegments();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingBricks;
  for (const auto& segment : segments) {
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> segmentBuildingBricks = segment->getBuildingBricks();
    buildingBricks.insert(buildingBricks.end(), segmentBuildingBricks.begin(), segmentBuildingBricks.end());
  }
  return buildingBricks;
}

std::vector<std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>>
SF_ModelQSM::getBranchBuildingBricks() const
{
  const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> allBricks = getBuildingBricks();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> branchStartBricks;
  std::copy_if(allBricks.cbegin(),
               allBricks.cend(),
               std::back_inserter(branchStartBricks),
               [](const std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick) { return brick->startsBranchOrder(); });
  std::vector<std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>> result;
  for (auto brick : branchStartBricks) {
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> branch;
    brick->getBranchBuildingBricks(branch, brick);
    result.push_back(branch);
  }
  return result;
}

std::vector<std::shared_ptr<SF_ModelSegment>>
SF_ModelQSM::getLeaveSegments() const
{
  const std::vector<std::shared_ptr<SF_ModelSegment>> segments = getSegments();
  std::vector<std::shared_ptr<SF_ModelSegment>> leafes;
  std::copy_if(segments.cbegin(), segments.cend(), std::back_inserter(leafes), [](const std::shared_ptr<SF_ModelSegment>& segment) {
    return segment->isLeave();
  });
  return leafes;
}

std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>
SF_ModelQSM::stemBricks() const
{
  const std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> allBricks = getBuildingBricks();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> stemBricks;
  std::copy_if(allBricks.cbegin(),
               allBricks.cend(),
               std::back_inserter(stemBricks),
               [](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) { return brick->getSegment()->getBranchOrder() == 0; });
  return stemBricks;
}

void
SF_ModelQSM::setAllometryDirty(bool flag)
{
  m_allometryDirty = flag;
}

bool
SF_ModelQSM::getAllometryDirty() const
{
  return m_allometryDirty;
}
