/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_mergeQsm.h"

#include "pcl/sf_math.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"

std::shared_ptr<Sf_ModelAbstractBuildingbrick>
SF_MergeQsm::brickToConnectTo(std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2)
{
  const auto& startQsm2 = qsm2->getRootSegment()->getBuildingBricks().front()->getStart();
  const auto& bricks = qsm1->getBuildingBricks();
  auto brickItr = std::min_element(bricks.cbegin(),
                                   bricks.cend(),
                                   [startQsm2](const std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick1,
                                               const std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick2) {
                                     const auto& end1 = brick1->getEnd();
                                     const auto& end2 = brick2->getEnd();
                                     const auto distance1 = SF_Math<float>::distance(startQsm2, end1);
                                     const auto distance2 = SF_Math<float>::distance(startQsm2, end2);
                                     return distance1 < distance2;
                                   });
  return *brickItr;
}

void
SF_MergeQsm::swapIfNeeded(std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2)
{
  const auto qsm1Z = qsm1->getRootSegment()->getBuildingBricks().front()->getStart()[2];
  const auto qsm2Z = qsm2->getRootSegment()->getBuildingBricks().front()->getStart()[2];
  if (qsm2Z < qsm1Z) {
    std::swap(qsm1, qsm2);
  }
}

void
SF_MergeQsm::setQsm(const std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2)
{
  auto segments = qsm2->getSegments();
  for (auto& segment : segments) {
    segment->setTree(qsm1);
  }
}

void
SF_MergeQsm::connect(std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2)
{
  auto brick = brickToConnectTo(qsm1, qsm2);
  brick->splitSegmentAfter();
  setQsm(qsm1, qsm2);

  auto childRoot = qsm2->getRootSegment();
  auto rootBricks = childRoot->getBuildingBricks();
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> connectionCylinder(
    new Sf_ModelCylinderBuildingbrick(brick->getEnd(), rootBricks[0]->getStart(), 0.015));
  connectionCylinder->setFittingType(FittingType::CONNECTQSM);
  rootBricks.insert(rootBricks.begin(), connectionCylinder);
  childRoot->setBuildingBricks(rootBricks);
  brick->getSegment()->addChild(qsm2->getRootSegment());
}

std::shared_ptr<SF_ModelQSM>
SF_MergeQsm::merge(std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2)
{
  if (!qsm1) {
    return qsm2;
  }
  if (!qsm2) {
    return qsm1;
  }
  swapIfNeeded(qsm1, qsm2);
  connect(qsm1, qsm2);
  return qsm1;
}
