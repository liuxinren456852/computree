/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_mergeCylindersInsideSegment.h"

#include "qsm/model/sf_modelCylinderBuildingbrick.h"

void
SF_MergeCylindersInsideSegment::correctSegment(std::shared_ptr<SF_ModelSegment> segment)
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> newBricks;
  auto bricks = segment->getBuildingBricks();
  size_t numberBricks = bricks.size();
  for (size_t i = 0; i < numberBricks - 1;) {
    std::shared_ptr<Sf_ModelAbstractBuildingbrick> first = segment->getBuildingBricks()[i];
    std::shared_ptr<Sf_ModelAbstractBuildingbrick> second = segment->getBuildingBricks()[i + 1];
    auto radius = (first->getRadius() + second->getRadius()) / 2;
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> mergedBrick(
      new Sf_ModelCylinderBuildingbrick(first->getStart(), second->getEnd(), radius));
    mergedBrick->setFittingType(first->getFittingType());
    newBricks.push_back(mergedBrick);
    i += 2;
  }
  if (numberBricks % 2 == 1) {
    newBricks.push_back(bricks.back());
  }
  segment->setBuildingBricks(newBricks);
}

void
SF_MergeCylindersInsideSegment::compute(std::shared_ptr<SF_ModelQSM> qsm)
{
  if (!qsm) {
    return;
  }
  std::vector<std::shared_ptr<SF_ModelSegment>> segments = qsm->getSegments();
  for (auto segment : segments) {
    correctSegment(segment);
  }
}
