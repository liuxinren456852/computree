/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_removefalseconnections.h"

#include "sf_mergeonechildsegments.h"
#include "sf_sortqsm.h"

#include "pcl/sf_math.h"

SF_RemoveFalseConnections::SF_RemoveFalseConnections() {}

void
SF_RemoveFalseConnections::compute(std::shared_ptr<SF_ModelQSM> qsm, float maxAngle)
{
  m_qsm = qsm;
  SF_SortQSM sq;
  sq.compute(m_qsm);
  std::vector<std::shared_ptr<SF_ModelSegment>> leafes = m_qsm->getLeaveSegments();
  std::for_each(leafes.begin(), leafes.end(), [](std::shared_ptr<SF_ModelSegment> leaf) {
    if (leaf->getBuildingBricks().size() <= 1) {
      leaf->remove();
    }
  });
  SF_MergeOneChildSegments moc;
  moc.compute(m_qsm);
  sq.compute(m_qsm);

  std::vector<std::shared_ptr<SF_ModelSegment>> segments = m_qsm->getSegments();
  std::for_each(segments.begin(), segments.end(), [maxAngle](std::shared_ptr<SF_ModelSegment> segment) {
    if (!segment->isRoot()) {
      std::shared_ptr<SF_ModelSegment> parent = segment->getParent();
      std::vector<std::shared_ptr<SF_ModelSegment>> children = parent->getChildren();
      std::for_each(children.begin(), children.end(), [&segment, maxAngle](std::shared_ptr<SF_ModelSegment> child) {
        if (child != segment) {
          float angle = SF_Math<float>::getAngleBetweenDeg(segment->getAxis(), child->getAxis());
          if (angle < maxAngle) {
            if (segment->getBuildingBricks()[0]->getGrowthLength() > child->getBuildingBricks()[0]->getGrowthLength()) {
              child->remove();
            } else {
              segment->remove();
            }
          }
        }
      });
    }
  });
  moc.compute(m_qsm);
  sq.compute(m_qsm);
}
