/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_qsmAllometricCorrection.h"

void
SF_QSMAllometricCorrection::setParams(const SF_ParamAllometricCorrectionNeighboring& params)
{
  m_params = params;
}

SF_ParamAllometricCorrectionNeighboring
SF_QSMAllometricCorrection::params() const
{
  return m_params;
}

void
SF_QSMAllometricCorrection::compute()
{
  if (!m_params._qsm) {
    return;
  }
  const auto translation = m_params._qsm->translateToOrigin();
  auto cylinders = m_params._qsm->getBuildingBricks();
  const auto maxHeightIt = std::min_element(cylinders.cbegin(), cylinders.cend(), [&](const auto& first, const auto& second) {
    return first->getEnd()[2] < second->getEnd()[2];
  });
  constexpr float percentageOfHeight = 0.25f;
  const auto height = (*maxHeightIt)->getEnd()[2] - m_params._qsm->getRootBuildingBrick()->getStart()[2];
  const auto minStemHeight = height * percentageOfHeight;
  const auto minStemZ = m_params._qsm->getRootBuildingBrick()->getStart()[2] + minStemHeight;

  std::for_each(cylinders.begin(), cylinders.end(), [&](std::shared_ptr<Sf_ModelAbstractBuildingbrick>& cylinder) {
    correct(cylinder, minStemZ);
  });
  m_params._qsm->translate(-translation);
}

void
SF_QSMAllometricCorrection::correct(std::shared_ptr<Sf_ModelAbstractBuildingbrick>& cylinder, float minStemZ)
{
  auto isTrunc = cylinder->getSegment()->isStem() && cylinder->getStart()[2] < minStemZ;
  float radiusModel = 0.f;
  if (m_params.m_useGrowthLength) {
    radiusModel = m_params._qsm->getAGrowthLength() * std::pow(cylinder->getGrowthLength(), m_params._qsm->getBGrowthLength()) +
                  m_params._qsm->getCGrowthLength();
  } else if (m_params.m_useVesselVolume) {
    radiusModel = m_params._qsm->getAGrowthVolume() * std::pow(cylinder->getVesselVolume(), m_params._qsm->getBGrowthVolume()) +
                  m_params._qsm->getCGrowthVolume();
  } else {
    radiusModel = m_params._qsm->getAGrowthVolume() * std::pow(cylinder->getGrowthVolume(), m_params._qsm->getBGrowthVolume()) +
                  m_params._qsm->getCGrowthVolume();
  }
  radiusModel = std::max(radiusModel, static_cast<float>(m_params._minRadius));
  auto range = isTrunc ? m_params._range * 2 : m_params._range;
  if (!inRange(cylinder->getRadius(), radiusModel, range)) {
    if (m_params.m_useGrowthLength) {
      cylinder->setRadius(radiusModel, FittingType::ALLOMETRICGROWTHLENGTH);
    } else if (m_params.m_useVesselVolume) {
      cylinder->setRadius(radiusModel, FittingType::ALLOMETRICVESSELVOLUME);
    } else {
      cylinder->setRadius(radiusModel, FittingType::ALLOMETRICGROWTHVOLUME);
    }
  }
}

bool
SF_QSMAllometricCorrection::inRange(float model, float predicted, float range)
{
  const auto deviation = range * predicted;
  const auto min = predicted - deviation;
  const auto max = predicted + deviation;
  return (model > min && model < max);
}
