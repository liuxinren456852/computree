/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_dijkstraLightRecursive.h"

#include "qsm/algorithm/distance/sf_extractFittedPoints.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"

#include "pcl/cloud/segmentation/dijkstra/sf_dijkstra.h"
#include "qsm/algorithm/postprocessing/sf_correctFirstCylinderInSegment.h"
#include "qsm/algorithm/postprocessing/sf_mergeCylindersInsideSegment.h"
#include "qsm/algorithm/sf_QSMCylinder.h"
#include "qsm/build/sf_buildQSM.h"

#include <pcl/common/transforms.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <qsm/algorithm/distance/sf_allocatePtsToSkeleton.h>

// TODO check next 3 includes
#include <pcl/filters/conditional_removal.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <algorithm>

void
SF_DijkstraLightRecursive::setParams(const SF_ParamSpherefollowingRecursive<SF_PointNormal>& params)
{
  m_params = params;
}

void
SF_DijkstraLightRecursive::setCloud(const SF_CloudNormal::Ptr& cloud)
{
  m_cloud = cloud;
}

void
SF_DijkstraLightRecursive::setQsm(const std::shared_ptr<SF_ModelQSM>& qsm)
{
  _qsm = qsm;
  m_bricks = _qsm->getBuildingBricks();
  initializeKdTree();
}

std::shared_ptr<SF_ModelQSM>
SF_DijkstraLightRecursive::getQSM()
{
  return _qsm;
}

float
SF_DijkstraLightRecursive::error()
{
  if (!_qsm || _qsm->getRootSegment()->getBuildingBricks().empty()) {
    return std::numeric_limits<float>::max();
  }
  Sf_CloudToModelDistance<SF_PointNormal> cmd(_qsm, m_cloud, m_params._distanceParams);
  auto dist = cmd.getAverageDistance();
  auto radiusAtRoot = _qsm->getRootSegment()->getRadius();
  if (radiusAtRoot < 0.8 * m_radiusAtRoot) {
    dist = dist * 10;
    if (radiusAtRoot < 0.6 * m_radiusAtRoot) {
      dist = dist * 10;
      if (radiusAtRoot < 0.4 * m_radiusAtRoot) {
        dist = dist * 10;
      }
    }
  }
  return dist;
}

SF_CloudNormal::Ptr
SF_DijkstraLightRecursive::extractUnfittedPoints()
{
  if (!_qsm) {
    return m_cloud;
  }
  SF_CloudToModelDistanceParameters distanceParams;
  distanceParams._method = SF_CLoudToModelDistanceMethod::FIRSTMOMENTUMORDER;
  distanceParams._inlierDistance = m_params.m_unfittedDistance;
  distanceParams._k = 9;
  SF_ExtractFittedPoints<pcl::PointXYZINormal> extract(_qsm, m_cloud, distanceParams);
  extract.compute();
  return extract.cloudUnfitted();
}

std::vector<SF_CloudNormal::Ptr>
SF_DijkstraLightRecursive::clusters(SF_CloudNormal::Ptr cloud, size_t minSize, size_t maxSize, float clusterDistance)
{
  pcl::search::KdTree<SF_PointNormal>::Ptr tree(new pcl::search::KdTree<SF_PointNormal>);
  tree->setInputCloud(cloud);

  std::vector<pcl::PointIndices> clusterIndices;
  pcl::EuclideanClusterExtraction<SF_PointNormal> ec;
  ec.setClusterTolerance(clusterDistance);
  ec.setMinClusterSize(minSize);
  ec.setMaxClusterSize(maxSize);
  ec.setSearchMethod(tree);
  ec.setInputCloud(cloud);
  ec.extract(clusterIndices);
  std::vector<SF_CloudNormal::Ptr> result;
  for (pcl::PointIndices clusterindices : clusterIndices) {
    SF_CloudNormal::Ptr cloudCluster(new SF_CloudNormal);
    for (std::vector<int>::const_iterator pit = clusterindices.indices.begin(); pit != clusterindices.indices.end(); ++pit)
      cloudCluster->points.push_back(cloud->points[*pit]);
    cloudCluster->width = cloudCluster->points.size();
    cloudCluster->height = 1;
    cloudCluster->is_dense = true;
    result.push_back(cloudCluster);
  }
  return result;
}

pcl::ModelCoefficients::Ptr
SF_DijkstraLightRecursive::coefficients(const SF_PointNormal& p)
{
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  coefficients->values.push_back(p.x);
  coefficients->values.push_back(p.y);
  coefficients->values.push_back(p.z);
  coefficients->values.push_back(0.015f);
  return coefficients;
}

SF_PointNormal
SF_DijkstraLightRecursive::closestPoint(const SF_PointNormal& searchPoint, const std::vector<SF_PointNormal>& cloud)
{
  auto minIt = std::min_element(cloud.begin(), cloud.end(), [searchPoint](const SF_PointNormal& p1, const SF_PointNormal& p2) {
    const auto distance1 = SF_Math<float>::distancef(searchPoint.getVector3fMap(), p1.getVector3fMap());
    const auto distance2 = SF_Math<float>::distancef(searchPoint.getVector3fMap(), p2.getVector3fMap());
    return distance1 < distance2;
  });
  return *minIt;
}

std::vector<SF_CloudNormal::Ptr>
SF_DijkstraLightRecursive::sortClusters(std::vector<SF_CloudNormal::Ptr> clusters)
{
  if (_kdtreeQSM) {
    std::vector<std::pair<SF_CloudNormal::Ptr, SF_PointNormal>> clustersPairs;
    for (auto cluster : clusters) {
      SF_PointNormal com;
      Eigen::Vector4d centroid;
      pcl::compute3DCentroid(*cluster, centroid);
      com.x = static_cast<float>(centroid(0));
      com.y = static_cast<float>(centroid(1));
      com.z = static_cast<float>(centroid(2));
      clustersPairs.push_back(std::make_pair(cluster, com));
    }
    std::sort(clustersPairs.begin(),
              clustersPairs.end(),
              [this](std::pair<SF_CloudNormal::Ptr, SF_PointNormal> pair1, std::pair<SF_CloudNormal::Ptr, SF_PointNormal> pair2) {
                SF_CloudNormal::Ptr cloud1(new SF_CloudNormal);
                cloud1->push_back(pair1.second);
                SF_CloudNormal::Ptr cloud2(new SF_CloudNormal);
                cloud2->push_back(pair2.second);
                return minDistance(cloud1) < minDistance(cloud2);
              });
    std::vector<SF_CloudNormal::Ptr> sortedClusters;
    for (auto pair : clustersPairs) {
      sortedClusters.push_back(pair.first);
    }
    return sortedClusters;
  }
  return clusters;
}

void
SF_DijkstraLightRecursive::compute()
{
  computeRadiusAtRoot();
  SF_CloudNormal::Ptr unfitted = extractUnfittedPoints();
  auto size = unfitted->points.size();
  size_t minSize = static_cast<size_t>(m_params.m_minPercentage * size);
  size_t maxSize = static_cast<size_t>(m_params.m_maxPercentage * size);
  std::vector<SF_CloudNormal::Ptr> clusterClouds = clusters(unfitted, minSize, maxSize, m_params.m_clusteringDistance);
  processClusters(clusterClouds);
}

void
SF_DijkstraLightRecursive::setFittingType(std::shared_ptr<SF_ModelQSM> qsm)
{
  auto bricks = qsm->getBuildingBricks();
  FittingType type = _qsm ? FittingType::DIJKSTRALIGHT : FittingType::DIJKSTRA;
  for (auto brick : bricks) {
    if (brick != bricks.front()) {
      brick->setFittingType(type);
    }
  }
}

void
SF_DijkstraLightRecursive::computeRadiusAtRoot()
{
  // TODO share with SphereFollowing init

  auto minZ = std::numeric_limits<float>::max();
  std::for_each(m_cloud->points.begin(), m_cloud->points.end(), [&minZ](const SF_PointNormal& point) {
    if (point.z < minZ)
      minZ = point.z;
  });
  minZ = minZ + 0.1;
  float maxZ = minZ + 1;

  SF_CloudNormal::Ptr lowestSlice(new SF_CloudNormal);
  pcl::ConditionAnd<SF_PointNormal>::Ptr rangeCond(new pcl::ConditionAnd<SF_PointNormal>());
  rangeCond->addComparison(
    pcl::FieldComparison<SF_PointNormal>::ConstPtr(new pcl::FieldComparison<SF_PointNormal>("z", pcl::ComparisonOps::GT, minZ)));
  rangeCond->addComparison(
    pcl::FieldComparison<SF_PointNormal>::ConstPtr(new pcl::FieldComparison<SF_PointNormal>("z", pcl::ComparisonOps::LT, maxZ)));
  pcl::ConditionalRemoval<SF_PointNormal> condrem;
  condrem.setCondition(rangeCond);
  condrem.setInputCloud(m_cloud);
  condrem.setKeepOrganized(false);
  condrem.filter(*lowestSlice);

  pcl::PointIndices::Ptr inliersCylinder(new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr coefficientsCylinder(new pcl::ModelCoefficients);
  pcl::SACSegmentationFromNormals<SF_PointNormal, SF_PointNormal> seg;
  seg.setOptimizeCoefficients(true);
  seg.setModelType(pcl::SACMODEL_CYLINDER);
  seg.setMethodType(pcl::SAC_MLESAC);
  seg.setMaxIterations(m_params._sphereFollowingParams._RANSACIterations);
  seg.setDistanceThreshold(m_params._sphereFollowingParams._inlierDistance);
  seg.setInputCloud(lowestSlice);
  seg.setInputNormals(lowestSlice);
  seg.segment(*inliersCylinder, *coefficientsCylinder);
  if (coefficientsCylinder->values.size() == 7) {
    m_radiusAtRoot = coefficientsCylinder->values[6];
  }
}

void
SF_DijkstraLightRecursive::processClusters(std::vector<SF_CloudNormal::Ptr>& clusters)
{
  while (!clusters.empty()) {
    SF_CloudNormal::Ptr cluster = *clusters.begin();
    if (cluster->points.size() < 3) {
      clusters.erase(clusters.begin());
      continue;
    }
    size_t minIndex = 0;
    size_t maxIndex = 0;
    Eigen::Vector3d closestQSMPoint;
    getMinMax(minIndex, maxIndex, cluster, closestQSMPoint);
    SF_CloudNormal::Ptr seed(new SF_CloudNormal);
    if (_qsm && (cluster->points.at(maxIndex).z < _qsm->getRootSegment()->getBuildingBricks().front()->getStart()[2]) &&
        (cluster->points.at(maxIndex).z < cluster->points.at(minIndex).z)) {
      std::swap(minIndex, maxIndex);
    }
    seed->push_back(cluster->points.at(minIndex));
    // We do not want to loose precomputed normals in Dijkstra
    SF_CloudNormal::Ptr clusterCpy(new SF_CloudNormal);
    *clusterCpy = *cluster;
    SF_Dijkstra dijkstra(clusterCpy, seed, m_params.m_djikstraRange, true);
    auto distances = dijkstra.getDistances();
    std::vector<SF_CloudNormal::Ptr> distanceClustered = distanceClusters(cluster, distances);
    if (distanceClustered.size() <= 1) {
      clusters.erase(clusters.begin());
      continue;
    }
    std::vector<std::vector<SF_CloudNormal::Ptr>> clusterSquared = clusterClusters(distanceClustered);
    if (clusterSquared.size() <= 1) {
      clusters.erase(clusters.begin());
      continue;
    }
    mergeFirstTwoClusters(clusterSquared);
    std::vector<std::vector<SF_PointNormal>> coms = centerOfMass(clusterSquared);
    std::vector<SF_QSMDetectionCylinder> cylinders;
    for (int i = 0; i < static_cast<int>(coms.size()) - 1; i++) {
      std::vector<SF_PointNormal> outer = coms[i + 1];
      std::vector<SF_PointNormal> inner = coms[i];
      for (auto pointOuter : outer) {
        SF_PointNormal pointInner = closestPoint(pointOuter, inner);
        pcl::ModelCoefficients::Ptr coeffStart = coefficients(pointInner);
        pcl::ModelCoefficients::Ptr coeffEnd = coefficients(pointOuter);
        SF_QSMDetectionCylinder cylinder(0, coeffStart, coeffEnd);
        cylinders.push_back(cylinder);
      }
    }
    SF_BuildQSM builder(cylinders, 1000);
    auto childQSM = builder.getTree();
    SF_CorrectFirstCylinderInSegment correct1;
    correct1.compute(childQSM);
    SF_MergeCylindersInsideSegment correct2;
    correct2.compute(childQSM);

    // TODO make parameters accessible here;
    SF_CloudToModelDistanceParameters params;
    params.m_useAngle = true;
    params._method = SF_CLoudToModelDistanceMethod::DISTANCETOAXISSEGMENT;
    params._k = 5;
    params._inlierDistance = m_params.m_djikstraRange;
    SF_AllocatePointsToSkeleton<pcl::PointXYZINormal> allocatePts(childQSM, params, cluster);
    auto brickClusters = allocatePts.brickClusters();
    auto bricks = childQSM->getBuildingBricks();
    int index = 0;
    setFittingType(childQSM);
    // TODO extract whole next for loop
    for (auto brick : bricks) {
      SF_CloudNormal::Ptr cloud = brickClusters[index];
      if (brick->getFittingType() != FittingType::CONNECTQSM &&
          static_cast<int>(cloud->points.size()) > m_params._sphereFollowingParams._minPtsGeometry) {
        {
          std::vector<double> distances;
          for (auto point : cloud->points) {
            auto distance = brick->getDistanceIfOnAxis(point);
            distances.push_back(distance);
          }
          auto radius = SF_Math<double>::getMedian(distances);
          if (radius <= 0.001) {
            radius = 0.001;
          }
          brick->setRadius(radius, FittingType::DIJKSTRALIGHT);
        }
        pcl::SACSegmentationFromNormals<SF_PointNormal, SF_PointNormal> seg;
        // TODO make parameters proper
        if (static_cast<int>(cloud->points.size()) > m_params._sphereFollowingParams._minPtsGeometry) {
          pcl::PointIndices::Ptr inliersCylinder(new pcl::PointIndices);
          pcl::ModelCoefficients::Ptr coefficientsCylinder(new pcl::ModelCoefficients);
          seg.setOptimizeCoefficients(true);
          seg.setModelType(pcl::SACMODEL_CYLINDER);
          seg.setMethodType(pcl::SAC_MLESAC);
          seg.setMaxIterations(m_params._sphereFollowingParams._RANSACIterations);
          seg.setDistanceThreshold(m_params._sphereFollowingParams._inlierDistance);
          seg.setInputCloud(cloud);
          seg.setInputNormals(cloud);
          seg.segment(*inliersCylinder, *coefficientsCylinder);
          if (coefficientsCylinder->values.size() == 7) {
            float angle = SF_Math<float>::getAngleBetweenDeg(
              brick->getAxis(),
              Eigen::Vector3d(coefficientsCylinder->values[3], coefficientsCylinder->values[4], coefficientsCylinder->values[5]));
            if (std::abs(angle) < 25) {
              double radius = brick->getRadius();
              double minRad = radius * 0.5;
              double maxRad = radius * 1.5; // TODO parameterize this
              double fittedRadius = static_cast<double>(coefficientsCylinder->values[6]);
              if (fittedRadius > minRad && fittedRadius < maxRad) {
                brick->setCoefficients(coefficientsCylinder);
              }
            }
          }
        }
      }
      ++index;
    }
    connectQSM(childQSM);
    clusters.erase(clusters.begin());
    clusters = sortClusters(clusters);
  }
}

void
SF_DijkstraLightRecursive::connectQSM(std::shared_ptr<SF_ModelQSM> childQSM)
{
  _qsm = m_mergeQsm.merge(_qsm, childQSM);
  setQsm(_qsm);
}

void
SF_DijkstraLightRecursive::getMinMax(size_t& min, size_t& max, SF_CloudNormal::Ptr cluster, Eigen::Vector3d& closestQSM)
{
  if (!_kdtreeQSM) {
    auto minMax = std::minmax_element(
      cluster->points.begin(), cluster->points.end(), [](const SF_PointNormal& a, const SF_PointNormal& b) { return a.z < b.z; });
    min = minMax.first - cluster->points.begin();
    max = minMax.second - cluster->points.begin();
    return;
  }
  auto minMax = std::minmax_element(
    cluster->points.begin(), cluster->points.end(), [this, &closestQSM](const SF_PointNormal& a, const SF_PointNormal& b) {
      int K = 1;
      std::vector<int> pointIdxNKNSearch(K);
      std::vector<float> pointNKNSquaredDistance(K);
      _kdtreeQSM->nearestKSearch(b, K, pointIdxNKNSearch, pointNKNSquaredDistance);
      float distB = pointNKNSquaredDistance.front();
      _kdtreeQSM->nearestKSearch(a, K, pointIdxNKNSearch, pointNKNSquaredDistance);
      float distA = pointNKNSquaredDistance.front();
      if (distA < distB) {
        closestQSM = m_bricks[pointIdxNKNSearch.front()]->getCenter();
      }

      return distA < distB;
    });
  min = minMax.first - cluster->points.begin();
  max = minMax.second - cluster->points.begin();
}

Eigen::Vector3f
SF_DijkstraLightRecursive::cloudVector(SF_CloudNormal::Ptr& cloud, const size_t minIndex, const size_t maxIndex)
{
  SF_PointNormal pointMin = cloud->points.at(minIndex);
  SF_PointNormal pointMax = cloud->points.at(maxIndex);
  return Eigen::Vector3f(pointMax.x - pointMin.x, pointMax.y - pointMin.y, pointMax.z - pointMin.z);
}

SF_DijkstraLightRecursive::SF_DijkstraLightRecursive() {}

Eigen::Vector3d
SF_DijkstraLightRecursive::translateCloud(SF_CloudNormal::Ptr& cloud, const size_t index)
{
  SF_PointNormal pointMin = cloud->points.at(index);
  Eigen::Vector3f temp = pointMin.getVector3fMap();
  auto translation = temp.cast<double>();
  for (SF_PointNormal& point : cloud->points) {
    point.x = point.x - pointMin.x;
    point.y = point.y - pointMin.y;
    point.z = point.z - pointMin.z;
  }
  return translation;
}

void
SF_DijkstraLightRecursive::initializeKdTree()
{
  _kdtreeQSM.reset(new pcl::KdTreeFLANN<SF_PointNormal>());
  m_centerCloud.reset(new typename pcl::PointCloud<SF_PointNormal>());
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> buildingBricks = _qsm->getBuildingBricks();
  for (size_t i = 0; i < buildingBricks.size(); i++) {
    Eigen::Vector3d pointEigen = buildingBricks[i]->getCenter();
    SF_PointNormal point;
    point.getVector3fMap() = pointEigen.cast<float>();
    point.intensity = static_cast<float>(buildingBricks[i]->getDistanceToRoot());
    m_centerCloud->push_back(std::move(point));
  }
  _kdtreeQSM->setInputCloud(m_centerCloud);
}

std::vector<SF_CloudNormal::Ptr>
SF_DijkstraLightRecursive::distanceClusters(SF_CloudNormal::Ptr cloud, const std::vector<float>& distances)
{
  float maxDistance = *std::max_element(begin(distances), end(distances), [&](float d1, float d2) {
    d1 = (d1 < MAXDISTANCE) ? d1 : 0.f;
    d2 = (d2 < MAXDISTANCE) ? d2 : 0.f;
    return d1 < d2;
  });
  int numberSlices = std::ceil(maxDistance / m_params.m_slice);
  std::vector<SF_CloudNormal::Ptr> clusters;
  if (numberSlices == 0) {
    return clusters;
  }
  for (int i = 0; i < numberSlices; i++) {
    SF_CloudNormal::Ptr cluster(new SF_CloudNormal);
    clusters.push_back(cluster);
  }
  int index = 0;
  for (auto distance : distances) {
    if (distance < MAXDISTANCE) {
      int indexCluster = std::max(0, (int)std::ceil(distance / m_params.m_slice) - 1);
      clusters[indexCluster]->push_back(cloud->points[index]);
    }
    ++index;
  }
  return clusters;
}

std::vector<std::vector<SF_CloudNormal::Ptr>>
SF_DijkstraLightRecursive::clusterClusters(std::vector<SF_CloudNormal::Ptr>& clusterVec)
{
  std::vector<std::vector<SF_CloudNormal::Ptr>> clusterClusters;
  for (auto cluster : clusterVec) {
    std::vector<SF_CloudNormal::Ptr> subClusters = clusters(
      cluster, static_cast<size_t>(1u), std::numeric_limits<size_t>::max(), static_cast<float>(m_params.m_clusterSlize));
    if (!subClusters.empty()) {
      clusterClusters.push_back(subClusters);
    }
  }
  return clusterClusters;
}

std::vector<std::vector<SF_PointNormal>>
SF_DijkstraLightRecursive::centerOfMass(std::vector<std::vector<SF_CloudNormal::Ptr>>& clusterClusters)
{
  std::vector<std::vector<SF_PointNormal>> result;
  for (size_t i = 0; i < clusterClusters.size(); i++) {
    std::vector<SF_CloudNormal::Ptr> clusters = clusterClusters[i];
    std::vector<SF_PointNormal> coms;
    for (auto cloud : clusters) {
      SF_PointNormal com;
      Eigen::Vector4d centroid;
      pcl::compute3DCentroid(*cloud, centroid);
      com.x = centroid(0);
      com.y = centroid(1);
      com.z = centroid(2);
      coms.push_back(com);
    }
    if (!coms.empty()) {
      result.push_back(coms);
    }
  }
  return result;
}

void
SF_DijkstraLightRecursive::mergeFirstTwoClusters(std::vector<std::vector<SF_CloudNormal::Ptr>>& clusterClusters)
{
  auto size = static_cast<size_t>(clusterClusters.size());
  for (size_t i = 0; i < std::min(size, static_cast<size_t>(2u)); i++) {
    std::vector<SF_CloudNormal::Ptr>& clusters = clusterClusters[i];
    for (size_t j = 1; j < clusters.size(); j++) {
      *clusters[0] += *clusters[j];
    }
    clusters.resize(1);
  }
}

float
SF_DijkstraLightRecursive::minDistance(SF_CloudNormal::Ptr cloud) const // TODO just take point
{
  float distance = std::numeric_limits<float>::max();
  for (SF_PointNormal& point1 : cloud->points) {
    std::vector<int> indices(1);
    std::vector<float> distances(1);
    if (_kdtreeQSM->nearestKSearch(point1, 1, indices, distances)) {
      const auto& centerPoint = m_centerCloud->points[indices[0]];
      const auto actualDistance = std::sqrt(distances[0]) + centerPoint.intensity;
      if (actualDistance < distance) {
        distance = actualDistance;
      }
    }
  }
  return distance;
}
