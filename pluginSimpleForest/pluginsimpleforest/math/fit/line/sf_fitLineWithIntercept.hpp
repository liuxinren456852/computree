/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FITLINEWITHINTERCEPT_HPP
#define SF_FITLINEWITHINTERCEPT_HPP

#include "sf_fitLineWithIntercept.h"

#include "sf_fitRansacLineWithIntercept.h"

template<typename T>
void
SF_FitLineWithIntercept<T>::setIntercept(const T& intercept)
{
  m_intercept = intercept;
}

template<typename T>
void
SF_FitLineWithIntercept<T>::setMinPts(const size_t& minPts)
{
  m_minPts = minPts;
}

template<typename T>
void
SF_FitLineWithIntercept<T>::setInlierDistance(const T& inlierDistance)
{
  m_inlierDistance = inlierDistance;
}

template<typename T>
void
SF_FitLineWithIntercept<T>::setIterations(const size_t& iterations)
{
  m_iterations = iterations;
}

template<typename T>
void
SF_FitLineWithIntercept<T>::compute()
{
  SF_FitRansacLineWithIntercept<T> ransacLine;
  ransacLine.setIterations(m_iterations);
  ransacLine.setInlierDistance(m_inlierDistance);
  ransacLine.setIntercept(m_intercept);
  ransacLine.setMinPts(m_minPts);
  ransacLine.setX(SF_FitLine<T>::m_x);
  ransacLine.setY(SF_FitLine<T>::m_y);
  try {
    ransacLine.compute();
  } catch (...) {
    SF_FitLine<T>::m_equation = SF_FitLine<T>::m_errorEquation;
    return;
  }
  auto inliers = ransacLine.inliers();
  auto xVec = inliers.first;
  auto yVec = inliers.second;
  std::vector<T> yCorrected;
  for (auto y : yVec) {
    yCorrected.push_back(y - m_intercept);
  }
  std::vector<T> xy;
  std::vector<T> xx;
  for (size_t i = 0; i < xVec.size(); i++) {
    xy.push_back(xVec[i] * yCorrected[i]);
    xx.push_back(xVec[i] * xVec[i]);
  }
  auto slope = SF_Math<T>::getMean(xy) / SF_Math<T>::getMean(xx);
  SF_FitLine<T>::m_equation = std::make_pair(slope, m_intercept);
}

#endif // SF_FITLINEWITHINTERCEPT_HPP
