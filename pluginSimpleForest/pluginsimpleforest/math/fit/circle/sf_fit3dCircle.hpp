/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FIT3DCIRCLE_HPP
#define SF_FIT3DCIRCLE_HPP

#include "math/fit/circle/sf_fit3dCircle.h"

#include <Eigen/QR>
#include <cassert>

#include <math/fit/circle/sf_fit2dCircle.h>
#include <math/fit/plain/sf_fitPlain.h>

template<typename PointType>
SF_Fit3dCircle<PointType>::SF_Fit3dCircle(typename pcl::PointCloud<PointType>::Ptr cloud_in) : m_cloud(cloud_in)
{}

template<typename PointType>
pcl::ModelCoefficients
SF_Fit3dCircle<PointType>::errorCircle() const
{
  pcl::ModelCoefficients coeff;
  coeff.values.push_back(static_cast<float>(0.f));
  coeff.values.push_back(static_cast<float>(0.f));
  coeff.values.push_back(static_cast<float>(0.f));
  coeff.values.push_back(std::numeric_limits<float>::min());
  return coeff;
}

template<typename PointType>
void
SF_Fit3dCircle<PointType>::compute()
{
  SF_FitPlain<PointType> fitPlane(m_cloud);
  fitPlane.compute();
  auto pointOnPlane = fitPlane.pointOnPlane();
  auto normal = fitPlane.planeNormal();

  Eigen::Vector3f zAxis(0, 0, 1);
  Eigen::Vector3f axis(normal.normal_x, normal.normal_y, normal.normal_z);
  axis = axis.normalized();
  Eigen::Vector3f rotationAxis;
  Eigen::Affine3f transform = Eigen::Affine3f::Identity();
  Eigen::Affine3f backtransform = Eigen::Affine3f::Identity();
  if (zAxis != axis) {
    rotationAxis = zAxis.cross(axis);
    rotationAxis = rotationAxis.normalized();
    float angle = std::acos(zAxis.dot(axis));
    if (std::isnormal(angle)) {
      transform.rotate(Eigen::AngleAxisf(-angle, rotationAxis));
      backtransform.rotate(Eigen::AngleAxisf(angle, rotationAxis));
    }
  }
  typename pcl::PointCloud<PointType>::Ptr transformed(new typename pcl::PointCloud<PointType>);
  pcl::transformPointCloud(*m_cloud, *transformed, transform);
  std::vector<float> x;
  x.reserve(transformed->points.size());
  std::transform(
    transformed->points.cbegin(), transformed->points.cend(), std::back_inserter(x), [](const PointType& p) { return p.x; });
  std::vector<float> y;
  y.reserve(transformed->points.size());
  std::transform(
    transformed->points.cbegin(), transformed->points.cend(), std::back_inserter(y), [](const PointType& p) { return p.y; });

  PointType transformedPointOnPlaneFirst;
  transformedPointOnPlaneFirst.x = pointOnPlane.x;
  transformedPointOnPlaneFirst.y = pointOnPlane.y;
  transformedPointOnPlaneFirst.z = pointOnPlane.z;

  PointType transformedPointOnPlaneSecond;
  transformedPointOnPlaneSecond.x = pointOnPlane.x + normal.normal_x;
  transformedPointOnPlaneSecond.y = pointOnPlane.y + normal.normal_y;
  transformedPointOnPlaneSecond.z = pointOnPlane.z + normal.normal_z;

  typename pcl::PointCloud<PointType>::Ptr transformedPlane(new typename pcl::PointCloud<PointType>);
  typename pcl::PointCloud<PointType>::Ptr plane(new typename pcl::PointCloud<PointType>);
  plane->push_back(transformedPointOnPlaneFirst);
  plane->push_back(transformedPointOnPlaneSecond);
  pcl::transformPointCloud(*plane, *transformedPlane, transform);

  SF_Fit2dCircle<float> circleFit;
  circleFit.setX(x);
  circleFit.setY(y);
  circleFit.compute();
  std::vector<float> params = circleFit.circle();

  PointType transformedPointOnPlane;
  transformedPointOnPlane.x = params[0];
  transformedPointOnPlane.y = params[1];
  transformedPointOnPlane.z = transformedPlane->points[0].z;

  typename pcl::PointCloud<PointType>::Ptr transformedCircleCenter(new typename pcl::PointCloud<PointType>);
  typename pcl::PointCloud<PointType>::Ptr originalCircleCenter(new typename pcl::PointCloud<PointType>);
  transformedCircleCenter->points.push_back(transformedPointOnPlane);
  pcl::transformPointCloud(*transformedCircleCenter, *originalCircleCenter, backtransform);

  m_coefficients.values.clear();
  m_coefficients.values.push_back(originalCircleCenter->points[0].x);
  m_coefficients.values.push_back(originalCircleCenter->points[0].y);
  m_coefficients.values.push_back(originalCircleCenter->points[0].z);
  m_coefficients.values.push_back(params[2]);
}

template<typename T>
pcl::ModelCoefficients
SF_Fit3dCircle<T>::circle()
{
  return m_coefficients;
}

#endif // SF_FIT3DCIRCLE_HPP
