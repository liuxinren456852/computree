/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "SF_GnnFpfh.h"

#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/ml.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>

void
SF_GNNFPFH::createIndices()
{}

void
SF_GNNFPFH::createIndex(pcl::FPFHSignature33, float)
{}

void
SF_GNNFPFH::reset()
{}

SF_GNNFPFH::SF_GNNFPFH(pcl::PointCloud<pcl::FPFHSignature33>::Ptr cloudIn) : SF_AbstractCloud<pcl::FPFHSignature33>(cloudIn) {}

void
SF_GNNFPFH::setParameters(uint32_t numberClusters)
{
  m_numberClusters = numberClusters;
}

void
SF_GNNFPFH::compute()
{
  pcl::PointCloud<pcl::FPFHSignature33>::Ptr cloud = SF_AbstractCloud<pcl::FPFHSignature33>::m_cloudIn;
  std::vector<int>& indices = SF_AbstractCloud<pcl::FPFHSignature33>::_indices;
  indices.clear();
  cv::Mat mat(cloud->points.size(), 33, CV_32FC(1));
  const auto size = cloud->points.size();
  for (std::uint32_t i = 0; i < size; ++i) {
    const pcl::FPFHSignature33& fpfh = cloud->at(i);
    for (std::uint32_t j = 0; j < 33u; ++j) {
      mat.at<float>(i, j) = fpfh.histogram[j];
    }
  }

  {
    using namespace cv;
    using namespace cv::ml;
    Ptr<EM> model = EM::create();
    model->setClustersNumber(static_cast<int>(m_numberClusters));
    Mat logs, labels, probs;
    model->trainEM(mat, logs, labels, probs);

    for (std::uint32_t i = 0; i < size; ++i) {
      const auto row = mat.row(i);
      Mat probs(1, 33, CV_64FC1);
      auto response = model->predict2(row, probs);
      indices.push_back(static_cast<std::uint32_t>(cvRound(response[1])));
    }
  }
}
