/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_DTM_HPP
#define SF_DTM_HPP

#include "sf_dtm.h"

template<typename PointType>
void
SF_DTM<PointType>::setHeightForLayer(std::shared_ptr<PyramidLayer<PointType>> layer)
{
  _DTM = layer->getDTM();
  for (size_t i = 0; i < _DTM->xArraySize(); i++) {
    for (size_t j = 0; j < _DTM->yArraySize(); j++) {
      size_t index{};
      _DTM->index(i, j, index);
      float height = layer->getHeight(index);
      _DTM->setValueAtIndex(index, height);
    }
  }
}

template<typename PointType>
std::shared_ptr<PyramidLayer<PointType>>
SF_DTM<PointType>::buildPyramid()
{
  std::shared_ptr<PyramidLayer<PointType>> rootLayer(new PyramidLayer<PointType>(_groundCloud, _outResult, _outDTM));
  std::shared_ptr<PyramidLayer<PointType>> currentParent = rootLayer;
  float gridSize = currentParent->getGridSize();
  int depth = 1;
  while (gridSize > _minCellSize) {
    depth++;
    std::shared_ptr<PyramidLayer<PointType>> currentChild(new PyramidLayer<PointType>(_groundCloud, depth, _outResult, _outDTM));
    gridSize = currentChild->getGridSize();
    std::shared_ptr<CT_Image2D<float>> childDTM = currentChild->getDTM();
    for (size_t i = 0; i < childDTM->xArraySize(); i++) {
      for (size_t j = 0; j < childDTM->yArraySize(); j++) {
        size_t indexChild{};
        childDTM->index(i, j, indexChild);
        size_t indexParent{ 0u };
        indexParent = getParentIndex(indexChild, currentChild, currentParent);
        updateCoeff(indexChild, indexParent, currentChild, currentParent);
      }
    }
    currentParent = currentChild;
  }
  return currentParent;
}

template<typename PointType>
size_t
SF_DTM<PointType>::getParentIndex(const size_t indexChild,
                                  std::shared_ptr<PyramidLayer<PointType>> currentChild,
                                  std::shared_ptr<PyramidLayer<PointType>> currentParent)
{
  size_t indexParent{ 0u };
  Eigen::Vector2d bot{ 0, 0 }, top{ 0, 0 };
  currentChild->getDTM()->getCellCoordinates(indexChild, bot, top);
  Eigen::Vector2d center = (bot + top) / 2;
  currentParent->getDTM()->indexAtCoords(center[0], center[1], indexParent);
  return indexParent;
}

template<typename PointType>
void
SF_DTM<PointType>::updateCoeff(const size_t indexChild,
                               const size_t indexParent,
                               std::shared_ptr<PyramidLayer<PointType>> currentChild,
                               std::shared_ptr<PyramidLayer<PointType>> currentParent)
{
  pcl::ModelCoefficients parentCoeff = currentParent->getPlaneCoeff(indexParent);
  if (currentChild->canComputePlane(indexChild)) {
    pcl::ModelCoefficients childCoeff = currentChild->computePlane(indexChild);
    Eigen::Vector2f parentHeights = currentChild->getMinMaxHeight(parentCoeff, indexChild);
    Eigen::Vector2f childHeights = currentChild->getMinMaxHeight(childCoeff, indexChild);
    float gridSize = currentChild->getGridSize();
    if (isValid(parentCoeff, childCoeff, childHeights, parentHeights, gridSize)) {
      currentChild->setPlaneCoeff(childCoeff, indexChild);
    } else {
      currentChild->setPlaneCoeff(parentCoeff, indexChild);
    }
  } else {
    currentChild->setPlaneCoeff(parentCoeff, indexChild);
  }
}

template<typename PointType>
bool
SF_DTM<PointType>::isValid(const pcl::ModelCoefficients& parentCoeff,
                           const pcl::ModelCoefficients& childCoeff,
                           const Eigen::Vector2f& childHeights,
                           const Eigen::Vector2f& parentHeights,
                           const float gridSize)
{
  Eigen::Vector3d normal{ childCoeff.values[0], childCoeff.values[1], childCoeff.values[2] };
  Eigen::Vector3d zAxis{ parentCoeff.values[0], parentCoeff.values[1], parentCoeff.values[2] };
  float angle = SF_Math<float>::getAngleBetweenDeg(normal, zAxis);
  float maxDelta = gridSize / 2;
  bool isHorizontal = ((angle < _maxAngle) || (angle > 180 - _maxAngle));
  bool isGoodHeight = ((childHeights[0] > (parentHeights[0] - maxDelta)) && (childHeights[1] < (parentHeights[1] + maxDelta)));
  return (isHorizontal && isGoodHeight);
}

template<typename PointType>
SF_DTM<PointType>::SF_DTM(typename pcl::PointCloud<PointType>::Ptr groundCloud,
                          float maxAngle,
                          float minCellSize,
                          CT_ResultGroup* outResult,
                          CT_AutoRenameModels outDTM)
  : _groundCloud(groundCloud), _maxAngle(maxAngle), _minCellSize(minCellSize), _outResult(outResult), _outDTM(outDTM)
{
  std::shared_ptr<PyramidLayer<PointType>> layer = buildPyramid();
  setHeightForLayer(layer);
}

template<typename PointType>
std::shared_ptr<CT_Image2D<float>>
SF_DTM<PointType>::DTM() const
{
  return _DTM;
}

#endif // SF_DTM_HPP
