/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_abstractStepQSM.h"

SF_AbstractStepQSM::SF_AbstractStepQSM(CT_StepInitializeData& dataInit) : SF_AbstractStep(dataInit) {}

CT_TTreeGroup*
SF_AbstractStepQSM::constructTopology(CT_AbstractResult* result, std::shared_ptr<SF_ModelQSM> qsm)
{
  std::shared_ptr<SF_ModelSegment> sfRoot = qsm->getRootSegment();
  CT_TTreeGroup* tree = new CT_TTreeGroup(_treeGroup.completeName(), result);
  CT_TNodeGroup* root = new CT_TNodeGroup(_stemNodeGroup.completeName(), result);
  tree->setRootNode(root);
  setCylinders(result, root, sfRoot);
  constructRecursively(result, root, sfRoot);
  return tree;
}

void
SF_AbstractStepQSM::constructRecursively(const CT_AbstractResult* result,
                                         CT_TNodeGroup* stemNode,
                                         std::shared_ptr<SF_ModelSegment> segment)
{
  std::vector<std::shared_ptr<SF_ModelSegment>> children = segment->getChildren();
  for (std::shared_ptr<SF_ModelSegment> child : children) {
    CT_TNodeGroup* stemChild = new CT_TNodeGroup(_stemNodeGroup.completeName(), result);
    stemNode->addBranch(stemChild);
    setCylinders(result, stemChild, child);
    constructRecursively(result, stemChild, child);
  }
}

CT_CylinderData*
SF_AbstractStepQSM::constructCylinderData(std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick)
{
  Eigen::Vector3d start = brick->getStart();
  Eigen::Vector3d end = brick->getEnd();
  double radius = brick->getRadius();
  double length = brick->getLength();
  CT_CylinderData* data = new CT_CylinderData((start + end) / 2, Eigen::Vector3d(end - start), radius, length);
  return data;
}

void
SF_AbstractStepQSM::setCylinders(const CT_AbstractResult* result, CT_TNodeGroup* stemNode, std::shared_ptr<SF_ModelSegment> segment)
{
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks = segment->getBuildingBricks();
  for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick : bricks) {
    if (m_onlyGoodFit && !brick->getGoodQuality()) {
      continue;
    }
    CT_TNodeGroup* cylinderGroup = new CT_TNodeGroup(_stemNodeGroup.completeName(), result);
    stemNode->addComponent(cylinderGroup);
    CT_CylinderData* data = constructCylinderData(brick);
    CT_Cylinder* cylinder = new CT_Cylinder(_stemNodeCylinders.completeName(), result, data);
    addAttributes(result, cylinder, brick);
    cylinderGroup->addItemDrawable(cylinder);
  }
}

void
SF_AbstractStepQSM::addQSMToOutResult(CT_OutResultModelGroupToCopyPossibilities* resModelw, QString header, QString group)
{
  QString groupName = header;
  groupName.append(tr("result group"));
  resModelw->addGroupModel(group, _QSMGrp, new CT_StandardItemGroup(), groupName);
  QString ctTreeGroup = header;
  ctTreeGroup.append(tr("CT QSM structure group"));
  resModelw->addGroupModel(_QSMGrp, _treeGroup, new CT_TTreeGroup(), ctTreeGroup);
  resModelw->addGroupModel(_treeGroup, _stemNodeGroup, new CT_TNodeGroup(), tr("QSM"));
  resModelw->addItemModel(_stemNodeGroup, _stemNodeCylinders, new CT_Cylinder(), tr("QSM Cylinder"));
  resModelw->addItemAttributeModel(
    _stemNodeCylinders,
    m_growthVolume,
    new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_NUMBER), NULL, 0),
    tr("log growthVolume"));
  resModelw->addItemAttributeModel(
    _stemNodeCylinders,
    m_growthLength,
    new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_NUMBER), NULL, 0),
    tr("log growthLength"));
  resModelw->addItemAttributeModel(
    _stemNodeCylinders,
    m_branchID,
    new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_NUMBER), NULL, 0),
    tr("Branch ID"));
  resModelw->addItemAttributeModel(
    _stemNodeCylinders,
    m_reversePipeOrder,
    new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_NUMBER), NULL, 0),
    tr("reversePipeOrder"));
  resModelw->addItemAttributeModel(
    _stemNodeCylinders,
    m_fittingType,
    new CT_StdItemAttributeT<int>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_NUMBER), NULL, 0),
    tr("fittingType"));
  resModelw->addItemAttributeModel(
    _stemNodeCylinders,
    m_mean,
    new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_NUMBER), NULL, 0),
    tr("log fitting error"));
}

void
SF_AbstractStepQSM::addInputQSMs()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT2, tr("QSM result"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER2,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("QSM Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER2, DEF_IN_QSM, SF_QSMItem::staticGetType(), tr("QSM Item"));
}

void
SF_AbstractStepQSM::addInputClouds()
{
  CT_InResultModelGroup* resModel = createNewInResultModel(DEF_IN_RESULT, tr("Cloud Result"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Cloud Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("Cloud Item"));
}

void
SF_AbstractStepQSM::createVisualizeCylinders(CT_StepConfigurableDialog* dialog)
{
  dialog->addBool(
    "Check for [<em><b>onlyWellFittedCylinders</b></em>], uncheck if you want to display all cylinders", "", "", m_onlyGoodFit);
}

std::vector<std::pair<std::shared_ptr<SF_ModelQSM>, CT_StandardItemGroup*>>
SF_AbstractStepQSM::getInputQSMs()
{
  std::vector<std::pair<std::shared_ptr<SF_ModelQSM>, CT_StandardItemGroup*>> qsms;
  CT_ResultGroup* outResult = getInputResults().at(0);
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER2);
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();
    const SF_QSMItem* qmsItem = (const SF_QSMItem*)group->firstItemByINModelName(this, DEF_IN_QSM);
    auto qsm = qmsItem->getQsm()->clone();
    qsms.push_back(std::make_pair(qsm, group));
  }
  return qsms;
}

std::vector<const CT_AbstractItemDrawableWithPointCloud*>
SF_AbstractStepQSM::getIputClouds()
{
  std::vector<const CT_AbstractItemDrawableWithPointCloud*> clouds;
  CT_ResultGroup* outResult = getInputResults().at(1);
  CT_ResultGroupIterator outResIt(outResult, this, DEF_IN_GRP_CLUSTER);
  while (!isStopped() && outResIt.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResIt.next();
    const CT_AbstractItemDrawableWithPointCloud* ctCloud = (const CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(
      this, DEF_IN_CLOUD_SEED);
    clouds.push_back(ctCloud);
  }
  return clouds;
}

void
SF_AbstractStepQSM::addAttributes(const CT_AbstractResult* result,
                                  CT_Cylinder* cylinder,
                                  std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick)
{
  cylinder->addItemAttribute(new CT_StdItemAttributeT<float>(
    m_growthVolume.completeName(), CT_AbstractCategory::DATA_NUMBER, result, std::log(brick->getGrowthVolume())));
  cylinder->addItemAttribute(new CT_StdItemAttributeT<float>(
    m_growthLength.completeName(), CT_AbstractCategory::DATA_NUMBER, result, std::log(brick->getGrowthLength())));
  cylinder->addItemAttribute(new CT_StdItemAttributeT<float>(
    m_branchID.completeName(), CT_AbstractCategory::DATA_NUMBER, result, brick->getSegment()->getBranchID()));
  cylinder->addItemAttribute(new CT_StdItemAttributeT<float>(
    m_reversePipeOrder.completeName(), CT_AbstractCategory::DATA_NUMBER, result, brick->getSegment()->getReversePipeBranchOrder()));
  cylinder->addItemAttribute(
    new CT_StdItemAttributeT<float>(m_mean.completeName(), CT_AbstractCategory::DATA_NUMBER, result, std::log(brick->getMean())));
  cylinder->addItemAttribute(
    new CT_StdItemAttributeT<int>(m_fittingType.completeName(), CT_AbstractCategory::DATA_NUMBER, result, brick->getFittingType()));
}

bool
SF_AbstractStepQSM::hasSameSize(const std::vector<std::pair<std::shared_ptr<SF_ModelQSM>, CT_StandardItemGroup*>>& qsms,
                                const std::vector<const CT_AbstractItemDrawableWithPointCloud*>& clouds) const
{
  if (clouds.size() != qsms.size()) {
    QString errorMsg = "Not the same number of clouds and qsms have been imported. Alligning to each qsm a cloud failed. Please "
                       "reconfigure the input of this step.";
    PS_LOG->addMessage(LogInterface::info, LogInterface::step, errorMsg);
    return false;
  }
  return true;
}
