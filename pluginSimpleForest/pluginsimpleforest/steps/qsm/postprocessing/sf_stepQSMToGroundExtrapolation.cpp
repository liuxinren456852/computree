/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepQSMToGroundExtrapolation.h"

#include "converters/CT_To_PCL/sf_converterCTToPCLDTM.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"
#include <ct_itemdrawable/ct_cylinder.h>

SF_StepQSMToGroundExtrapolation::SF_StepQSMToGroundExtrapolation(CT_StepInitializeData& dataInit) : SF_AbstractStepQSM(dataInit) {}

SF_StepQSMToGroundExtrapolation::~SF_StepQSMToGroundExtrapolation() {}

QString
SF_StepQSMToGroundExtrapolation::getStepDescription() const
{
  return tr("QSM To Ground Extrapolation");
}

QString
SF_StepQSMToGroundExtrapolation::getStepDetailledDescription() const
{
  return tr("A list of QSMs and the according DTM is imported. Each stem is extrapolated to the ground level.");
}

QString
SF_StepQSMToGroundExtrapolation::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepQSMToGroundExtrapolation::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepQSMToGroundExtrapolation(dataInit);
}

QStringList
SF_StepQSMToGroundExtrapolation::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepQSMToGroundExtrapolation::createPostConfigurationDialog()
{
  CT_StepConfigurableDialog* configDialog = newStandardPostConfigurationDialog();
  configDialog->addText("<b>QSM To Ground interpolation</b>:");
  configDialog->addEmpty();
  configDialog->addText(QObject::tr("For invention of this step please cite  "
                                    "the following:"),
                        "Hackenberg, J.; Spiecker, H.; Calders, K.; Disney, M.; Raumonen, P.");
  configDialog->addText("",
                        "<em>SimpleTree - An Efficient Open Source Tool to "
                        "Build Tree Models from TLS Clouds.</em>");
  configDialog->addText("", "Forests <b>2015</b>, 6, 4245-4294.");
  configDialog->addEmpty();
  createPostConfigurationDialogCitation(configDialog);
}

void
SF_StepQSMToGroundExtrapolation::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Input Result QSM"));
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("QSM Group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_QSM, SF_QSMItem::staticGetType(), tr("QSM"));

  CT_InResultModelGroup* resModel2 = createNewInResultModel(DEF_IN_RESULT2, tr("Input Result DTM"));
  resModel2->setZeroOrMoreRootGroup();
  resModel2->addGroupModel(
    "", DEF_IN_DTM_GRP, CT_AbstractItemGroup::staticGetType(), tr("DTM Group"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel2->addItemModel(DEF_IN_DTM_GRP, DEF_IN_DTM, CT_Image2D<float>::staticGetType(), tr("DTM"));
}

void
SF_StepQSMToGroundExtrapolation::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    QString name = tr("QSM to ground extrapolation");
    QString sfCylinders = name;
    sfCylinders.append(tr("SF QSM"));
    addQSMToOutResult(resModelw, name, QString::fromUtf8(DEF_IN_GRP_CLUSTER));
    resModelw->addItemModel(_QSMGrp, _outSFQSM, new SF_QSMItem(), sfCylinders);
  }
}

void
SF_StepQSMToGroundExtrapolation::compute()
{
  m_computationsDone = 0;
  const QList<CT_ResultGroup*>& outResultList = getOutResultList();
  CT_ResultGroup* outResult = outResultList.at(0);
  CT_ResultGroup* outResult2 = getInputResults().at(1);
  CT_ResultGroupIterator outResItCloud(outResult, this, DEF_IN_GRP_CLUSTER);
  CT_ResultGroupIterator outResItDTM(outResult2, this, DEF_IN_DTM_GRP);
  Eigen::Vector3d translation;
  std::shared_ptr<SF_ModelDTM> dtm;
  while (!isStopped() && outResItDTM.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItDTM.next();
    CT_Image2D<float>* DTM_CT = (CT_Image2D<float>*)group->firstItemByINModelName(this, DEF_IN_DTM);
    translation = DTM_CT->getCenterCoordinate();
    SF_ConverterCTToPCLDTM converter(translation, DTM_CT);
    dtm = converter.dtmPCL();
  }
  while (!isStopped() && outResItCloud.hasNext()) {
    CT_StandardItemGroup* group = (CT_StandardItemGroup*)outResItCloud.next();
    const SF_QSMItem* QSM_Item = (const SF_QSMItem*)group->firstItemByINModelName(this, DEF_IN_QSM);
    auto qsm = QSM_Item->getQsm()->clone();
    qsm->translate(-translation);
    auto rootBricks = qsm->getRootSegment()->getBuildingBricks();
    std::shared_ptr<Sf_ModelAbstractBuildingbrick> rootBrick = rootBricks.front();
    auto start = rootBrick->getStart();
    auto newStart = start;
    pcl::PointXYZ point;
    point.x = static_cast<float>(start[0]);
    point.y = static_cast<float>(start[1]);
    point.z = static_cast<float>(start[2]);
    auto height = dtm->heightAbove(point);
    newStart[2] = start[2] - height;
    std::shared_ptr<Sf_ModelCylinderBuildingbrick> newRoot(
      new Sf_ModelCylinderBuildingbrick(newStart, start, rootBrick->getRadius() * 1.05));
    newRoot->setFittingType(FittingType::EXTRAPOLATETODTM);
    rootBricks.insert(rootBricks.begin(), newRoot);
    qsm->getRootSegment()->setBuildingBricks(rootBricks);
    qsm->translate(translation);
    SF_ParamQSMedian<SF_PointNormal> param;
    param._qsm = qsm;
    _paramList.push_back(param);
  }
  SF_AbstractStepQSM::addQSM<SF_ParamQSMedian<SF_PointNormal>>(
    outResult, _paramList, QString::fromUtf8(DEF_IN_GRP_CLUSTER), _outSFQSM.completeName());
  _paramList.clear();
}
