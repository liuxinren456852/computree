/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPQSMCORRECTSHOOTS_H
#define SF_STEPQSMCORRECTSHOOTS_H

#include "steps/qsm/sf_abstractStepQSM.h"

class SF_StepQSMCorrectShoots : public SF_AbstractStepQSM
{
  Q_OBJECT

public:
  SF_StepQSMCorrectShoots(CT_StepInitializeData& dataInit);
  ~SF_StepQSMCorrectShoots();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);
  QStringList getStepRISCitations() const;
  QList<SF_ParamAllometricCorrectionNeighboring> _paramList;

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  virtual void createPreConfigurationDialog() {}
  virtual void createPostConfigurationDialog();
  void compute();
  void createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog);

private:
  CT_AutoRenameModels m_outCloudItem;
  CT_AutoRenameModels _outCylinderGroup;
  CT_AutoRenameModels _outCylinders;
  CT_AutoRenameModels _outSFQSM;

  bool m_correctSecondOrder = true;
  double m_radius = 0.005;
};

#endif // SF_STEPQSMCORRECTSHOOTS_H
