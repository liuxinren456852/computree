/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_spherefollowingParametersItem.h"

SF_SphereFollowingParametersItem::SF_SphereFollowingParametersItem() {}

SF_SphereFollowingParametersItem::SF_SphereFollowingParametersItem(const CT_OutAbstractSingularItemModel* model,
                                                                   const CT_AbstractResult* result,
                                                                   SF_ParamSpherefollowingBasic<pcl::PointXYZINormal> params)
  : CT_AbstractItemDrawableWithoutPointCloud(model, result)
{
  _params = params;
}

SF_SphereFollowingParametersItem::SF_SphereFollowingParametersItem(const QString& modelName,
                                                                   const CT_AbstractResult* result,
                                                                   SF_ParamSpherefollowingBasic<pcl::PointXYZINormal> params)
  : CT_AbstractItemDrawableWithoutPointCloud(modelName, result)
{
  _params = params;
}

CT_AbstractItemDrawable*
SF_SphereFollowingParametersItem::copy(const CT_OutAbstractItemModel* model, const CT_AbstractResult* result, CT_ResultCopyModeList)
{
  SF_SphereFollowingParametersItem* ref = new SF_SphereFollowingParametersItem(
    (const CT_OutAbstractSingularItemModel*)model, result, _params);
  ref->setAlternativeDrawManager(getAlternativeDrawManager());
  return ref;
}

CT_AbstractItemDrawable*
SF_SphereFollowingParametersItem::copy(const QString& modelName, const CT_AbstractResult* result, CT_ResultCopyModeList)
{
  SF_SphereFollowingParametersItem* ref = new SF_SphereFollowingParametersItem(modelName, result, _params);
  ref->setAlternativeDrawManager(getAlternativeDrawManager());
  return ref;
}

QString
SF_SphereFollowingParametersItem::getParameterID() const
{
  return _id.completeName();
}

SF_ParamSpherefollowingBasic<pcl::PointXYZINormal>
SF_SphereFollowingParametersItem::getParams() const
{
  return _params;
}
