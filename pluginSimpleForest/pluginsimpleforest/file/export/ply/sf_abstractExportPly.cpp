/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_abstractExportPly.h"

SF_AbstractExportPly::SF_AbstractExportPly() : SF_AbstractExport()
{
  QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
  m_firstColor = SF_ColorFactory::Color::GREEN;
  m_secondColor = SF_ColorFactory::Color::RED;
}

void
SF_AbstractExportPly::writeHeader(QTextStream& outStream, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  outStream << "ply\n"
               "format ascii 1.0\n"
               "comment author: Dr. Jan Hackenberg\n"
               "comment object: QSM computed with SimpleForest\n";
  outStream << "element vertex ";
  outStream << QString::number(getNumberOfPoints(bricks)).append("\n");
  outStream << "property float x\n"
               "property float y\n"
               "property float z\n"
               "property uchar red\n"
               "property uchar green\n"
               "property uchar blue\n";
  outStream << "element face ";
  outStream << QString::number(getNumberOfFaces(bricks)).append("\n");
  outStream << "property list uchar int vertex_index\n"
               "end_header\n";
}

void
SF_AbstractExportPly::getMinMax(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  try {
    m_maxIntensity = 0;
    m_minIntensity = std::numeric_limits<float>::max();
    for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick : bricks) {
      if (m_exportPolicy == SF_ExportPlyPolicy::GROWTH_LENGTH) {
        float logGrowthLength = std::log(brick->getGrowthLength());
        if (logGrowthLength > m_maxIntensity) {
          m_maxIntensity = logGrowthLength;
        }
        if (logGrowthLength < m_minIntensity) {
          m_minIntensity = logGrowthLength;
        }
      } else if (m_exportPolicy == SF_ExportPlyPolicy::GROWTH_VOLUME) {
        float logGrowthVolume = std::log(brick->getGrowthVolume());
        if (logGrowthVolume > m_maxIntensity) {
          m_maxIntensity = logGrowthVolume;
        }
        if (logGrowthVolume < m_minIntensity) {
          m_minIntensity = logGrowthVolume;
        }
      } else {
        m_minIntensity = 0;
        m_maxIntensity = 1;
      }
    }
  } catch (const std::exception& e) {
    std::string errorString = "[SF_ExportPly] : getMinMax() did throw:" + std::string(e.what());
    std::cout << errorString << std::endl;

    if (m_exportPolicy != SF_ExportPlyPolicy::STEM) {
      std::cout << "Setting to SF_ExportPlyPolicy::STEM and recomputing." << std::endl;
      m_exportPolicy = SF_ExportPlyPolicy::STEM;
      getMinMax(bricks);
    } else {
      throw std::runtime_error("Already using SF_ExportPlyPolicy::STEM, still throwing.");
    }
  } catch (...) {
    std::string errorString = "[SF_ExportPly] : getMinMax() did throw and unknown exception.";
    std::cout << errorString << std::endl;
    if (m_exportPolicy != SF_ExportPlyPolicy::STEM) {
      std::cout << "Setting to SF_ExportPlyPolicy::STEM and recomputing." << std::endl;
      m_exportPolicy = SF_ExportPlyPolicy::STEM;
      getMinMax(bricks);
    } else {
      throw std::runtime_error("Already using SF_ExportPlyPolicy::STEM, still throwing.");
    }
  }
}

void
SF_AbstractExportPly::writeCloud(QTextStream& outStream, pcl::PointCloud<pcl::PointXYZI>::Ptr cloud)
{
  for (const pcl::PointXYZI& point : cloud->points) {
    try {
      outStream << QString::number(point.x);
      outStream << " ";
      outStream << QString::number(point.y);
      outStream << " ";
      outStream << QString::number(point.z);
      outStream << " ";
      outStream << SF_ColorFactory::getColorString(m_firstColor, m_secondColor, point.intensity);
      outStream << "\n";
    } catch (...) {
      std::cout << "SF_ExportPly::writeCloud with cloud size: " << cloud->points.size() << " ; " << point << std::endl;
    }
  }
}

pcl::PointCloud<pcl::PointXYZI>::Ptr
SF_AbstractExportPly::brickToCloud(std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick)
{
  try {
    double increment = (2.0 * SF_Math<double>::_PI) / m_resolution;
    double theta = 0;
    const auto center = brick->getCenter();
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
    double length = brick->getLength();
    double radius = brick->getRadius();
    float intensity = brickToIntensity(brick);
    for (int i = 0; i < m_resolution; i++) {
      float x = radius * std::cos(theta);
      float y = radius * std::sin(theta);
      float z = -length / 2;
      pcl::PointXYZI p;
      p.x = x;
      p.y = y;
      p.z = z;
      p.intensity = intensity;
      cloud->push_back(p);
      theta += increment;
    }
    theta = 0;
    for (int i = 0; i < m_resolution; i++) {
      float x = radius * std::cos(theta);
      float y = radius * std::sin(theta);
      float z = length / 2;
      pcl::PointXYZI p;
      p.x = x;
      p.y = y;
      p.z = z;
      p.intensity = intensity;
      cloud->push_back(p);
      theta += increment;
    }

    Eigen::Vector3f zAxis(0, 0, 1);
    Eigen::Vector3d cylinderAxis = brick->getAxis();
    Eigen::Vector3f cylinderAxisF = Eigen::Vector3f(cylinderAxis.coeff(0), cylinderAxis.coeff(1), cylinderAxis.coeff(2));
    cylinderAxisF = cylinderAxisF.normalized();
    Eigen::Vector3f rotationAxis;
    Eigen::Affine3f transform = Eigen::Affine3f::Identity();
    if (zAxis != cylinderAxisF) {
      rotationAxis = zAxis.cross(cylinderAxisF);
      rotationAxis = rotationAxis.normalized();
      float angle = std::acos(zAxis.dot(cylinderAxisF));
      if (std::isnormal(angle)) {
        transform.rotate(Eigen::AngleAxisf(angle, rotationAxis));
      }
    }
    transform.translation() << center.coeff(0), center.coeff(1), center.coeff(2);
    pcl::PointCloud<pcl::PointXYZI>::Ptr transformed(new pcl::PointCloud<pcl::PointXYZI>);
    pcl::transformPointCloud(*cloud, *transformed, transform);
    return transformed;
  } catch (const std::exception& e) {
    std::string errorString = "[SF_AbstractExportPly] : brickToCloud() did throw:" + std::string(e.what());
    std::cout << errorString << std::endl;
  } catch (...) {
    std::string errorString = "[SF_AbstractExportPly] : brickToCloud() did throw and unknown exception.";
    std::cout << errorString << std::endl;
  }
  return nullptr;
}

float
SF_AbstractExportPly::brickToIntensity(std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick)
{
  try {
    float intensity = 0;
    if (m_exportPolicy == SF_ExportPlyPolicy::GROWTH_LENGTH) {
      intensity = std::log(brick->getGrowthLength());
    } else if (m_exportPolicy == SF_ExportPlyPolicy::GROWTH_VOLUME) {
      intensity = std::log(brick->getGrowthVolume());
    } else if (m_exportPolicy == SF_ExportPlyPolicy::STEM) {
      intensity = (brick->getSegment()->getBranchOrder() == 0) ? 1 : 0;
    }
    if ((m_maxIntensity - m_minIntensity) == 0)
      return 1;
    return (intensity - m_minIntensity) / (m_maxIntensity - m_minIntensity);
  } catch (...) {
    std::cout << "SF_ExportPly::brickToIntensity failed." << std::endl;
    return 1;
  }
}

QString
SF_AbstractExportPly::getFullPath(QString path)
{
  if (m_exportPolicy == SF_ExportPlyPolicy::GROWTH_LENGTH) {
    path.append("/ply/growthLength/");
  } else if (m_exportPolicy == SF_ExportPlyPolicy::GROWTH_VOLUME) {
    path.append("/ply/growthVolume/");
  } else if (m_exportPolicy == SF_ExportPlyPolicy::STEM) {
    path.append("/ply/stem/");
  }
  return path;
}
