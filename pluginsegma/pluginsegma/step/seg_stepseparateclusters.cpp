#include "seg_stepseparateclusters.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/ct_scene.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_clusters "clusters"
#define DEFin_raster "raster"

// Constructor : initialization of parameters
SEG_StepSeparateClusters::SEG_StepSeparateClusters(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{

}

// Step description (tooltip of contextual menu)
QString SEG_StepSeparateClusters::getStepDescription() const
{
    return tr("6-  Créer des rasters par couronne");
}

// Step detailled description
QString SEG_StepSeparateClusters::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepSeparateClusters::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepSeparateClusters::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepSeparateClusters(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepSeparateClusters::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Image 2D"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_clusters, CT_Image2D<qint32>::staticGetType(), tr("Clusters"));
    resIn_res->addItemModel(DEFin_grp, DEFin_raster, CT_Image2D<float>::staticGetType(), tr("Raster"));
}

// Creation and affiliation of OUT models
void SEG_StepSeparateClusters::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);
    if (resCpy_res != NULL)
    {
        resCpy_res->addGroupModel(DEFin_grp, _outClusterGroup_ModelName, new CT_StandardItemGroup(), tr("Clusters isolés (grp)"));
        resCpy_res->addItemModel(_outClusterGroup_ModelName, _outCluster_ModelName, new CT_Image2D<float>(), tr("Cluster isolé"));
        resCpy_res->addItemAttributeModel(_outCluster_ModelName, _attClusterNumber_ModelName, new CT_StdItemAttributeT<qint32>(CT_AbstractCategory::DATA_ID), tr("IDcluster"));
        resCpy_res->addItemModel(_outClusterGroup_ModelName, _outMask_ModelName, new CT_Image2D<quint8>(), tr("Masque"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepSeparateClusters::createPostConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void SEG_StepSeparateClusters::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resOut = outResultList.at(0);


    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(resOut, this, DEFin_grp);
    while (itCpy_grp.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        
        CT_Image2D<qint32>* clusters = (CT_Image2D<qint32>*)grp->firstItemByINModelName(this, DEFin_clusters);
        CT_Image2D<float>* raster = (CT_Image2D<float>*)grp->firstItemByINModelName(this, DEFin_raster);

        if (clusters != NULL && raster != NULL)
        {
            QMap<qint32, SEG_StepSeparateClusters::pixelsArea*> areas;

            for (int lin = 0 ; lin < clusters->linDim() ; lin++)
            {
                for (int col = 0 ; col < clusters->colDim() ; col++)
                {
                    qint32 cluster = clusters->value(col, lin);

                    if (!areas.contains(cluster)) {areas.insert(cluster, new SEG_StepSeparateClusters::pixelsArea());}
                    SEG_StepSeparateClusters::pixelsArea *area = areas.value(cluster);

                    if (col < area->_xmin) {area->_xmin = col;}
                    if (col > area->_xmax) {area->_xmax = col;}
                    if (lin < area->_ymin) {area->_ymin = lin;}
                    if (lin > area->_ymax) {area->_ymax = lin;}
                }
            }

            QMapIterator<qint32, SEG_StepSeparateClusters::pixelsArea*> it(areas);
            while (it.hasNext())
            {
                it.next();
                qint32 cluster = it.key();
                if (cluster > 0)
                {
                    SEG_StepSeparateClusters::pixelsArea* area = it.value();

                    // Create a 1-pixel buffer around each crown
                    area->_xmin--;
                    area->_ymin--;
                    area->_xmax++;
                    area->_ymax++;

                    double minx = clusters->minX() + (area->_xmin) * clusters->resolution();
                    double miny = clusters->maxY() - (area->_ymax + 1) * clusters->resolution();
                    size_t dimx = area->_xmax - area->_xmin + 1;
                    size_t dimy = area->_ymax - area->_ymin + 1;


                    CT_StandardItemGroup *clusterGroup = new CT_StandardItemGroup(_outClusterGroup_ModelName.completeName(), resOut);
                    CT_Image2D<float> *clusterImage = new CT_Image2D<float>(_outCluster_ModelName.completeName(), resOut, minx, miny, dimx, dimy, raster->resolution(), 0, raster->NA(), raster->NA());
                    CT_Image2D<quint8> *clusterMask = new CT_Image2D<quint8>(_outMask_ModelName.completeName(), resOut, minx, miny, dimx, dimy, raster->resolution(), 0, 0, 0);

                    clusterImage->addItemAttribute(new CT_StdItemAttributeT<qint32>(_attClusterNumber_ModelName.completeName(),
                                                                                    CT_AbstractCategory::DATA_ID,
                                                                                    resOut,
                                                                                    cluster));


                    for (int lin = area->_ymin ; lin <= area->_ymax ; lin++)
                    {
                        for (int col = area->_xmin ; col <= area->_xmax ; col++)
                        {
                            if (clusters->value(col, lin) == cluster)
                            {
                                int localCol = col - area->_xmin;
                                int localLin = lin - area->_ymin;
                                clusterMask->setValue(localCol, localLin, 1);
                                clusterImage->setValue(localCol, localLin, raster->value(col, lin));
                            }
                        }
                    }
                    clusterImage->computeMinMax();
                    clusterMask->computeMinMax();
                    grp->addGroup(clusterGroup);

                    clusterGroup->addItemDrawable(clusterImage);
                    clusterGroup->addItemDrawable(clusterMask);
                }
            }

            qDeleteAll(areas.values());
        }
    }

}

