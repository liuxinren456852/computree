#include "seg_stepreplacenabyzero.h"

#include "ct_view/ct_stepconfigurabledialog.h"


#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

#include "ct_itemdrawable/ct_image2d.h"
#include "ct_iterator/ct_resultgroupiterator.h"
#include "ct_result/ct_resultgroup.h"

#include "tools/cavityfillmain.h"

#include <QFileInfo>
#include <QDir>

#include <QDebug>

#define DEF_InRes "r"
#define DEF_InGroup "g"
#define DEF_InItem "i"

// Constructor : initialization of parameters
SEG_StepReplaceNAByZero::SEG_StepReplaceNAByZero(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString SEG_StepReplaceNAByZero::getStepDescription() const
{
    return tr("0- Remplacer les valeurs NA par Zéro");
}

// Step detailled description
QString SEG_StepReplaceNAByZero::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepReplaceNAByZero::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepReplaceNAByZero::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepReplaceNAByZero(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepReplaceNAByZero::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *result = createNewInResultModelForCopy(DEF_InRes);
    result->setZeroOrMoreRootGroup();
    result->addGroupModel("", DEF_InGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    result->addItemModel(DEF_InGroup, DEF_InItem, CT_Image2D<float>::staticGetType(), tr("Raster"));
}

// Creation and affiliation of OUT models
void SEG_StepReplaceNAByZero::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_InRes);
    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_InGroup, _outModifiedImageModelName, new CT_Image2D<float>(), tr("Modified Raster"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepReplaceNAByZero::createPostConfigurationDialog()
{
}

void SEG_StepReplaceNAByZero::compute()
{
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator it(outResult, this, DEF_InGroup);

    while(it.hasNext()) {

        CT_StandardItemGroup* group = (CT_StandardItemGroup*) it.next();
        CT_Image2D<float> *inGrid = dynamic_cast<CT_Image2D<float>*>(group->firstItemByINModelName(this, DEF_InItem));

        if(inGrid != NULL)
        {
            CT_Image2D<float> *outGrid = new CT_Image2D<float>(_outModifiedImageModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->colDim(), inGrid->linDim(), inGrid->resolution(), inGrid->level(), inGrid->NA(), inGrid->NA());

            outGrid->getMat() = inGrid->getMat().clone();

            for (size_t xx = 0 ; xx < inGrid->colDim() ; xx++)
            {
                for (size_t yy = 0 ; yy < inGrid->linDim() ; yy++)
                {
                    float val = inGrid->value(xx, yy);
                    if (val == inGrid->NA() || val < 0)
                    {
                        outGrid->setValue(xx, yy, 0);
                    }
                }
            }

            outGrid->computeMinMax();

            group->addItemDrawable(outGrid);
        }
    }
}
