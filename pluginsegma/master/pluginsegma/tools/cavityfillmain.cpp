#include "cavityfillmain.h"

#include <cmath>

#include <QtGlobal>
#include <QDebug>

void CavityFillMain::cavityFillFilter(float* source, int nrows, int ncols, int filter_len, float filter_slope, float laplacian_thres, int median_filter_len, int dilatation, float height_thres, float * target) {
    bool *hmap2;
    float *g;

    hmap2 = find_holes(source, filter_len, filter_slope, laplacian_thres, dilatation, height_thres, nrows, ncols);
    g = copy_holes(source, hmap2, nrows, ncols);
    g = interpolate(g, nrows, ncols);
    median_filter(g, hmap2, median_filter_len, nrows, ncols, target);

    free(hmap2);
    free(g);
}


CavityFillMain::Path CavityFillMain::splitPath(string str){
    Path pth;
    unsigned int lastPoint, lastSlash;

    // Valeurs pas défault
    pth.path.clear();
    pth.mask = STAR;
    pth.ext = "LAS";

    // conversion un string à minuscules
    toLowerCase(str);

    // Identification d'extension
    lastPoint = str.find_last_of(DOT);
    if (lastPoint!=NOT_FOUND) {
        if(lastPoint+1 < str.length())
            pth.ext = str.substr(lastPoint+1);
        str.erase(lastPoint);
    }

    // Identification de la route d'accès
    lastSlash = str.find_last_of(EITHER_SLASH);
    if (lastSlash!=NOT_FOUND) {
        pth.path = str.substr(0,lastSlash+1);
        if (pth.path!=str) {
        str.erase(0,lastSlash+1);
//			str = str.substr(lastSlash+1);
        }
    }

    // le dernier morceau du texte devient le nom du fichier.
    pth.mask = str;

    return pth;
}

/* Conversion de string vers type numérique */
template <class N>
N CavityFillMain::toNum(string str) {
    stringstream ssDataValue;
    N value;

    ssDataValue << str;
    if (!(ssDataValue >> value))
        value = 0;
    return value;
}

float* CavityFillMain::prepare_filtre_elements(int size, float slope) {
    float *fe;

    int line,col,nb_fe;
    float dist,fe_sum,fe_center;
    unsigned long memOffset;

    if(!(fe=(float *)malloc(size*size*sizeof(float))))
        qDebug() << "Plus de memoire pour sauvergarder les données.";

    nb_fe = 0; /* Nombre des elements de filtre */

    fe_sum=0.0;
    for(line=0;line<size;line++){
        printf("\n");
        for(col=0;col<size;col++){
            dist = hypot(line-((int)((size-1)/2.0)),col-((int)((size-1)/2.0)));
            memOffset = CommonTools::mem_offset(line,col,size);
            if(dist<=(float)(size-1)/2.0) {
                *(fe+memOffset) = dist*slope;
                nb_fe++;
            }
            else *(fe+memOffset) = 0.0;
            fe_sum+=*(fe+memOffset);
            printf("%f ",*(fe+memOffset));
        }
    }

    fe_center = (-1)*fe_sum/(float)nb_fe; /* Calcul la valeur de l'element central */

    printf("\n\n");

    fe_sum=0.0;
    for(line=0;line<size;line++){
        printf("\n");
        for(col=0;col<size;col++){
            dist = hypot(line-((int)((size-1)/2.0)),col-((int)((size-1)/2.0)));
            memOffset = CommonTools::mem_offset(line,col,size);
            if(dist<=(float)(size-1)/2.0) *(fe+memOffset) = fe_center+dist*slope;
            else *(fe+memOffset) = 0.0;
            fe_sum+=*(fe+memOffset);
            printf("%f ",*(fe+memOffset));
            }
    }
    printf("\n\nFiltre total = %f\n\n",fe_sum);

    return fe;

}

bool* CavityFillMain::find_holes(const float * scene, int size, float filtre_pente, float laplacian_thres, int dilatation, float height_thres, int snlin, int sncol){
//bool* find_holes(float * scene, float* fe, int size, Params prms, float laplacian_thres, int dilatation, float height_thres, int snlin, int sncol){
    float *hole_score;
    bool *hole_map;
    bool *hole_map2;
    float *fe;

    int i;
    int j;
    int nb_fe;
    int filt_j;
    int filt_i;
    int s_f; /* Grandeur de semi-filtre */
    int nb_fe_temp; /* Compteur pour les regions limitrophes */
    unsigned long memOff;

    fe = prepare_filtre_elements(size, filtre_pente);

    if(!(hole_score=(float *)malloc(snlin*sncol*sizeof(float))))
        qDebug() << "Plus de memoire pour sauvergarder les données.";

    if(!(hole_map=(bool *)malloc(snlin*sncol*sizeof(bool))))
        qDebug() << "Plus de memoire pour sauvergarder les données.";

    if(!(hole_map2=(bool *)malloc(snlin*sncol*sizeof(bool))))
        qDebug() << "Plus de memoire pour sauvergarder les données.";

    for(unsigned int ndx=0;ndx<snlin*sncol;ndx++){
        *(hole_score+ndx)=0.0;
        *(hole_map+ndx)=false;
        *(hole_map2+ndx)=false;
    }

    s_f = (int)((float)(size-1)/2.0);
    nb_fe = size*size;
    printf("\n\n-- Filtrage des trous --");
    for(i=ZERO;i<snlin;i++) {
        if (i%100==ZERO && i!=ZERO) printf("\nligne %d",i);
        for(j=ZERO;j<sncol;j++) {
            memOff = CommonTools::mem_offset(i,j,sncol);
            if(i>=s_f && i<snlin-s_f && j>=s_f && j<sncol-s_f)	{
                for(filt_i=ZERO;filt_i<size;filt_i++) {
                    for(filt_j=ZERO;filt_j<size;filt_j++) {
                        *(hole_score+memOff)+=*(fe+CommonTools::mem_offset(filt_i,filt_j,size))*(*(scene+(i-s_f+filt_i)*sncol+j-s_f+filt_j)/(float)nb_fe);
                    }
                }
            }
            else // Just pour tous les pixels de bordure
            {
                nb_fe_temp=0;
                for(filt_i=ZERO;filt_i<size;filt_i++)
                    for(filt_j=ZERO;filt_j<size;filt_j++)
                        if (i+filt_i-s_f>=ZERO && i+filt_i-s_f<snlin && j+filt_j-s_f>=ZERO && j+filt_j-s_f<sncol) {
                            *(hole_score+memOff)+= *(fe+CommonTools::mem_offset(filt_i,filt_j,size)) * (*(scene+(i-s_f+filt_i)*sncol+j-s_f+filt_j));
                            nb_fe_temp++;
                        }
                *(hole_score+memOff)/=nb_fe_temp;
            }
        }
    }

    /* Trous pour scores et Seuillage. */
    for(i=ZERO;i<snlin;i++) {
        for(j=ZERO;j<sncol;j++) {
            memOff = CommonTools::mem_offset(i,j,sncol);
            if(*(hole_score+memOff)>laplacian_thres || *(scene+memOff)>height_thres) *(hole_map+memOff)=true;
        }
    }

    /* Dilatation de trous */
    bool *dil_buf;
    int dil_op_size, dil_buf_size;

    dil_op_size = dilatation*2 + 1; /* Dilatateur de la grandeur de l'operateur (longueur en pixels) */
    dil_buf_size = dil_op_size*dil_op_size;

    /* Crée le tampon de l'operateur de dilatation */

    if(!(dil_buf=(bool *)malloc(dil_buf_size*sizeof(bool))))
        qDebug() << "Plus de memoire pour l'escene tampon du map de sortie de trous.";

    /* Initialisation du tampon */
    for(int ndx=ZERO;ndx<dil_buf_size;ndx++) *(dil_buf+ndx) = false;

    int bi, bj;
    float buf_dist;

    for(bi=ZERO;bi<dil_op_size;bi++) {
        for(bj=ZERO;bj<dil_op_size;bj++) {
            buf_dist = hypot((abs((int)dilatation-bi)),(dilatation-bj));
            if(buf_dist <= (float)dilatation) *(dil_buf+CommonTools::mem_offset(bi,bj,dil_op_size)) = true;
        }
    }

    /* Applique la dilatation */
    printf("\n\n-- Dilatation des trous --");
    for(i=ZERO;i<snlin;i++) {
        if (i%100==ZERO && i!=ZERO) printf("\nligne %d",i);
        for(j=ZERO;j<sncol;j++) {
            memOff = CommonTools::mem_offset(i,j,sncol);
            if(*(hole_map+memOff) == true) *(hole_map2+memOff) = true;
            if(i>=dilatation && i<snlin-dilatation && j>=dilatation && j<sncol-dilatation)	{
                for(bi=ZERO;bi<dil_op_size;bi++) {
                    for(bj=ZERO;bj<dil_op_size;bj++) {
                        if(*(dil_buf+CommonTools::mem_offset(bi,bj,dil_op_size))==true && *(hole_map+CommonTools::mem_offset(i-dilatation+bi,j-dilatation+bj,sncol))==true) *(hole_map2+memOff) = true;
                    }
                }
            }
        }
    }
    free(hole_score);
    free(hole_map);
    free(dil_buf);
    free(fe);

    return hole_map2;
}

float* CavityFillMain::copy_holes(const float * scene, bool * holes, int snlin, int sncol){
  float * g;

    if(!(g=(float *)malloc(snlin*sncol*sizeof(float))))
        qDebug() << "Plus de memoire pour sauvergarder les données.";

    for(unsigned long ndx=0;ndx<snlin*sncol;ndx++) {
        if(*(holes+ndx) == true)
        {
            *(g+ndx) = HOLE_VALUE;
        } else {
            *(g+ndx) = *(scene+ndx);
        }
    }

    return g;
}

/* Calcul du point interpolé */
float CavityFillMain::get_interpolate_value(float* g, List_Croissance list, unsigned long pxl, unsigned long sncol){
    List_Croissance::point_dist near_pnt;
    vector<List_Croissance::point_dist> near_point;
    int near_points;
    float target_value;

    near_points = list.count_nearests(g, pxl, RANGE);
    if (near_points==EMPTY){
        target_value = HOLE_VALUE; // Modification 2012-02-21
//		target_value = EMPTY; // Modification 2012-01-04
//		return NODATA;
    }
    else{
        target_value = 0;
        // recherche des voisins les plus lourds
        near_point = list.find_heaviests(g, pxl, MIN_WEIGHT);
        for (int ndxNear=0; ndxNear<near_point.size(); ndxNear++){
            near_pnt = near_point[ndxNear];
            target_value += near_point[ndxNear].dist * (*(g+CommonTools::mem_offset(near_pnt.ii,near_pnt.jj,sncol)));
        }

    }
    return target_value;
}

/* Calcul d'interpolation (nouvelle version) David Hernandez: 2011 */
float* CavityFillMain::interpolate(float* g, int snlin, int sncol) {
    float* gi;
    unsigned long pxl;
    float sPxlValue;

    if(!(gi=(float *)malloc(snlin*sncol*sizeof(float))))
        qDebug() << "Plus de memoire pour sauvergarder les données.";

    List_Croissance list_croissance(MAXIMUM_PIXELS, snlin, sncol);
    int i;
    int loopCount = 0;
    bool boMoreEmptyData=false;
    time_t t_start,t_end;
    double dif;

//	time (&t_start);
    printf("\n\n-- Interpolation --");
    do{
        printf("\nPasse: %d",++loopCount);
        boMoreEmptyData=false;
        for (int row=ZERO; row<snlin; row++){
            if (row%100==ZERO && row!=ZERO)
                printf("\nligne = %d",row);
            for (int col=ZERO; col<sncol; col++){
                 pxl = CommonTools::mem_offset(row,col,sncol);

                if (*(g+pxl)==HOLE_VALUE){
                    sPxlValue = get_interpolate_value(g, list_croissance, pxl, sncol);
                    *(gi+pxl)=sPxlValue;
                    if (sPxlValue==HOLE_VALUE) {
                        boMoreEmptyData=true;
                    }
                }
                else
                    *(gi+pxl)=*(g+pxl);
            }
        }

        if (boMoreEmptyData){
            for(pxl=ZERO ; pxl<snlin*sncol ; pxl++) {
                *(g+pxl) = *(gi+pxl);
            }
        }
    } while(boMoreEmptyData && loopCount<MAX_LOOP);

    for(pxl=ZERO ; pxl<snlin*sncol ; pxl++) {
        if (*(gi+pxl)==HOLE_VALUE) {
            *(gi+pxl) = NODATA;
        }
    }

    free(g);
    return gi;
}

float CavityFillMain::get_median(float * mfe, int array_size) {

    int i, j, median_observation;
    float current;

    median_observation = qRound((float)array_size/2.0)-1;

    for(i=1;i<array_size;i++) {
        current = *(mfe+i);
        j = i;
        while ((j > 0) && (*(mfe+j-1) > current)) {
            *(mfe+j) = *(mfe+j-1);
            j =  j - 1;
        }
        *(mfe+j)= current;
    }

    return *(mfe+median_observation);
}

void CavityFillMain::median_filter(float* gi, bool* hole_map2, int msize, int snlin, int sncol, float * out_scene)	{
    float* mfe;
//	float* out_scene;
    int i;
    int j;
    int nb_fe;
    int filt_j;
    int filt_i;
    int s_f; /* Semi filter size */
    unsigned long pxl;
    unsigned long ndx;
    short mfeCount;

    s_f = (int)((float)(msize-1)/2.0);
    nb_fe = msize*msize;

    if(!(mfe=(float *)malloc(msize*msize*sizeof(float))))
        qDebug() << "Plus de memoire pour sauvergarder les données.";

    //if(!(out_scene=(float *)malloc(snlin*sncol*sizeof(float))))
    //	displayErrorMessage("Plus de memoire pour sauvergarder les données.");
    for (ndx=0; ndx<snlin*sncol; ndx++)	*(out_scene+ndx) = NODATA;

    printf("\n\n-- Filtre medianne --");
    for(i =0 ;i < snlin; i++) {
        if (i % 100 == ZERO && i != ZERO)
            printf("\nligne %d", i);
        for(j = 0; j < sncol; j++) {
            pxl = CommonTools::mem_offset(i, j, sncol);
            if (*(gi + pxl) != NODATA){ // code David 2011-10-04

//				if(i >= s_f && i <= snlin - s_f && j>=s_f && j<=sncol-s_f){
                    mfeCount = 0;
                    if(*(hole_map2+pxl) == true){
                        for(filt_i = i - s_f; filt_i <= i + s_f; filt_i++) {
                            for(filt_j = j - s_f; filt_j <= j + s_f; filt_j++) {
                                if (CommonTools::isInside(filt_i, filt_j, snlin, sncol)){
                                    *(mfe+mfeCount) = *(gi + CommonTools::mem_offset(filt_i, filt_j, sncol));
                                    mfeCount++;

//									*(mfe+CommonTools::mem_offset(filt_i,filt_j,msize))=*(gi+CommonTools::mem_offset((i-s_f+filt_i),j-s_f+filt_j,sncol));
                                }
                            }
                        }

                        *(out_scene+pxl) = get_median(mfe, mfeCount);

                    }
                    else *(out_scene+pxl) = *(gi+pxl);
//				}
            }
        }
    }
    free(mfe);
//	return out_scene;
}

// Definition des propiétés de la grille
template <class dataType>
dataType* CavityFillMain::setMem(unsigned long rows, unsigned long cols) {
    dataType* g;
    if(!(g = (dataType*) malloc(rows * cols * sizeof(dataType))))
        qDebug() << "Plus de memoire.\n";

    return g;
}

/* Conversion de string à minuscules */
void CavityFillMain::toLowerCase(string &str){
    for (unsigned short ndx=0; ndx<str.length(); ndx++)
        if (str[ndx]>=65 && str[ndx]<=90)
            str[ndx] += 32;
}

/* Enregistre l'information du fichier En-Tête relié à un fichier FLT */
void CavityFillMain::saveHdrParams(string strFileName, FltHdr hdr) {
    FILE* file;

    if((file=fopen(strFileName.c_str(),"w"))==NULL)
        cout << "Impossible d'ouvrir le fichier d'ecriture" << strFileName;

    fprintf (file, "ncols         %d\n",hdr.ncols);
    fprintf (file, "nrows         %d\n",hdr.nrows);
    fprintf (file, "xllcorner     %f\n",hdr.xllcorner);
    fprintf (file, "yllcorner     %f\n",hdr.yllcorner);
    fprintf (file, "cellsize      %f\n",hdr.cellsize);
    fprintf (file, "NODATA_value  %d\n",hdr.NODATA_value);
    fprintf (file, "byteorder     %s\n",hdr.byteorder);

    fclose(file);
}

