#include "list_croissance.h"

#include "math.h"

List_Croissance::List_Croissance(int max, int nlin, int ncol){
    int axeX=0;
    float dist; // unité = metre
    bool exchange;
    lins=nlin;
    cols=ncol;

    // Tout va dans un vecteur.
    cardinaux[0].ii= 1;cardinaux[0].jj= 1;
    cardinaux[1].ii=-1;cardinaux[1].jj=-1;
    cardinaux[2].ii= 1;cardinaux[2].jj=-1;
    cardinaux[3].ii=-1;cardinaux[3].jj= 1;
    point_dist point;

    while (++axeX<=max){
        for (int axeY=0; axeY<=axeX; axeY++) {
            dist = sqrt(pow((float)axeX,2)+pow((float)axeY,2));
            if (dist <= (float)max)	{					// Rejette les point au délà de la distance maximale
                for (int k=0; k<2; k++){					//  Validation d'inversion d'index
                    if (!((axeX==axeY) && k%2)) {
                        point.ii = (k%2) ? axeY : axeX;
                        point.jj = (k%2) ? axeX : axeY;
                        point.dist=dist;
                        list_croissance.push_back(point);

                    //	Les points tombent sous leurs propres poids... (
                        for (int ndx=1; ndx<list_croissance.size()-1; ndx++){
                            exchange = false;
                            for (int pos=ndx; pos<list_croissance.size()-1; pos++)
                                if (list_croissance[pos].dist < list_croissance[pos-1].dist){
                                    point=list_croissance[pos];
                                    list_croissance[pos]=list_croissance[pos-1];
                                    list_croissance[pos-1]=point;
                                    exchange = true;
                                }
                            if (!exchange) break;
                        }
                    }
                }
            }
        }
    }
}

void List_Croissance::start(void){
    ndxPoint = -1;
    ndxCardinal = 4;
}

List_Croissance::point_dist List_Croissance::next(void){
    ndxCardinal++;
    if (ndxCardinal>=4){
        ndxPoint++;
        ndxCardinal = (list_croissance[ndxPoint].ii!=ZERO && list_croissance[ndxPoint].jj!=ZERO) ? ZERO : 2;
    }
    temp.ii = list_croissance[ndxPoint].ii * cardinaux[ndxCardinal].ii;
    temp.jj = list_croissance[ndxPoint].jj * cardinaux[ndxCardinal].jj;
    temp.dist = list_croissance[ndxPoint].dist;

    return temp;
}

bool List_Croissance::eol(void){
        return (ndxPoint+1>=list_croissance.size() && ndxCardinal+1 >=4);
}

int List_Croissance::count_nearests(float* grid, unsigned long pxl, int rayon){
    unsigned long memOffset;
    int ii, jj, tot_near=0;
    point_dist rel_point;

    int i = pxl/cols;
    int j = pxl%cols;
    this->start();
    while (!this->eol()){
        rel_point = this->next();
        if (rel_point.dist>rayon) break;
        ii = i + rel_point.ii;
        jj = j + rel_point.jj;
        if(CommonTools::isInside(ii,jj,lins,cols)){
            memOffset = CommonTools::mem_offset(ii,jj,cols);
            if (*(grid+memOffset)!=NODATA && *(grid+memOffset)!=HOLE_VALUE) tot_near++;
        }
    }

    return tot_near;
}

vector<List_Croissance::point_dist> List_Croissance::find_heaviests(float* grid, unsigned long pxl, float min){
    unsigned long memOffset;
    int ii, jj;
    float weight, sum_weight;
    point_dist rel_point, abs_point;
    vector<point_dist> list_near;
    int i = pxl/cols;
    int j = pxl%cols;

    // La liste de poids
    list_near.clear();
    this->start();
    while (!this->eol()){
        rel_point = this->next();
        weight = (float)1/(rel_point.dist*rel_point.dist);
        sum_weight=0;
        // Calcul de la sume accumulée
        if (list_near.size()>0) {
            for (int wt=0; wt<list_near.size(); wt++)
                sum_weight+=list_near[wt].dist;
            if (weight/(sum_weight+weight)<min) // condition d'arrêt. Le poids du prochain point est en dessous du poids minimum
                break;
        }

        // le Point absolu dans la matrice
        ii = i + rel_point.ii;
        jj = j + rel_point.jj;
        if(CommonTools::isInside(ii,jj,lins,cols)){
            memOffset = CommonTools::mem_offset(ii,jj,cols);
            // Si le point contient une valeur valide. Il est ajouté a la liste
            if (*(grid+memOffset)!=NODATA && *(grid+memOffset)!=HOLE_VALUE){
                abs_point.ii = ii;
                abs_point.jj = jj;
                abs_point.dist = weight;
                list_near.push_back(abs_point);
            }
        }
    }

    // calcul de la somme des poids
        sum_weight=0;
        for (int wt=0; wt<list_near.size(); wt++)
            sum_weight+=list_near[wt].dist;
    // normalisation des poids de la somme des poids
        for (int wt=0; wt<list_near.size(); wt++)
            list_near[wt].dist=list_near[wt].dist/sum_weight;
    return list_near;
}
