#ifndef LIST_CROISSANCE_H
#define LIST_CROISSANCE_H

#include <string>
#include <vector>

#include "tools/commontools.h"

using namespace std;

class List_Croissance
{

public:

    struct point_dist{
        int ii;
        int jj;
        float dist;
    };

    List_Croissance(int, int, int);
    void start(void);
    point_dist next(void);
    bool eol();
    int count_nearests(float*, unsigned long, int);
    vector<point_dist> find_heaviests(float*, unsigned long, float);

private:
    vector<point_dist> list_croissance;
    point_dist cardinaux[4];
    point_dist temp;
    int ndxCardinal, ndxPoint;
    int lins;
    int cols;


};

#endif // LIST_CROISSANCE_H
