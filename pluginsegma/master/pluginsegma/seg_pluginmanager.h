#ifndef SEG_PLUGINMANAGER_H
#define SEG_PLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class SEG_PluginManager : public CT_AbstractStepPlugin
{
public:
    SEG_PluginManager();
    ~SEG_PluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/segma/wiki");}

    QString getPluginRISCitation() const;
protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();
    bool loadMetrics();


};

#endif // SEG_PLUGINMANAGER_H
