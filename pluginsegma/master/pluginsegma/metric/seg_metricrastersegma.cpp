/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "metric/seg_metricrastersegma.h"
#include "ct_itemdrawable/ct_image2d.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/core/core.hpp"


#include <QDebug>


SEG_MetricRasterSegma::SEG_MetricRasterSegma() : CT_AbstractMetric_Raster()
{
    declareAttributes();
}

SEG_MetricRasterSegma::SEG_MetricRasterSegma(const SEG_MetricRasterSegma &other) : CT_AbstractMetric_Raster(other)
{
    declareAttributes();
    m_configAndResults = other.m_configAndResults;
}

QString SEG_MetricRasterSegma::getShortDescription() const
{
    return tr("Métriques SEGMA");
}

QString SEG_MetricRasterSegma::getDetailledDescription() const
{
    return tr("Les valeurs suivantes sont calculées :<br>"
              "- MinHeight<br>"
              "- MaxHeight<br>"
              "- MaxHeight_X<br>"
              "- MaxHeight_Y<br>"
              "- CrownArea<br>"
              "- CentroidX<br>"
              "- CentroidY<br>"
              "- Eccentricity<br>"
              "- Solidity<br>"
              "- HtoAratio<br>"
              "- CVmax<br>"
              "- CentroidShift<br>"
              "- Vextent<br>"
              "- CrRatio<br>"
              "- Diameter<br>"
              "- Circularity<br>"
              );
}

SEG_MetricRasterSegma::Config SEG_MetricRasterSegma::metricConfiguration() const
{
    return m_configAndResults;
}

void SEG_MetricRasterSegma::setMetricConfiguration(const SEG_MetricRasterSegma::Config &conf)
{
    m_configAndResults = conf;
}

CT_AbstractConfigurableElement *SEG_MetricRasterSegma::copy() const
{
    return new SEG_MetricRasterSegma(*this);
}

void SEG_MetricRasterSegma::computeMetric()
{    
    m_configAndResults.MinHeight.value = std::numeric_limits<double>::max();
    m_configAndResults.MaxHeight.value = -std::numeric_limits<double>::max();
    m_configAndResults.MaxHeight_X.value = 0;
    m_configAndResults.MaxHeight_Y.value = 0;
    m_configAndResults.CrownArea.value = 0;
    m_configAndResults.CentroidX.value = 0;
    m_configAndResults.CentroidY.value = 0;
    m_configAndResults.Eccentricity.value = 0;
    m_configAndResults.Solidity.value = 0;
    m_configAndResults.HtoAratio.value = 0;
    m_configAndResults.CVmax.value = 0;
    m_configAndResults.CentroidShift.value = 0;
    m_configAndResults.Vextent.value = 0;
    m_configAndResults.CrRatio.value = 0;
    m_configAndResults.Diameter.value = 0;
    m_configAndResults.Circularity.value = 0;


    double weightedCentroidX = 0;
    double weightedCentroidY = 0;
    double sdHeights = 0;
    double heightsSum = 0;
    double crownAreaPix = 0;
    double crownAreaPixWithHeights = 0;
    double heightsMean = 0;

    CT_Image2D<quint8> *mask = new CT_Image2D<quint8>(NULL, NULL, _inRaster->minX(), _inRaster->minY(), _inRaster->colDim(), _inRaster->linDim(), _inRaster->resolution(), 0, 0, 0);
    for (size_t xx = 0 ; xx < _inRaster->colDim() ; xx++)
    {
        for (size_t yy = 0 ; yy < _inRaster->linDim() ; yy++)
        {
            size_t index;
            if (_inRaster->index(xx, yy, index))
            {
                if (_inRaster->valueAtIndexAsDouble(index) != _inRaster->NAAsDouble())
                {
                    mask->setValueAtIndex(index, 1);
                }
            }
        }
    }

    // Compute eccentricity, solidity circularty and perimeter
    cv::Mat_<quint8> contours = mask->getMat().clone();
    std::vector<std::vector<cv::Point> > contoursPoints;

    cv::findContours(contours, contoursPoints, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    if (contoursPoints.size() > 0 && (contoursPoints[0].size() >= 5))
    {
        double areaContour = cv::contourArea(contoursPoints[0]);
        double perimeter = 0;

        cv::RotatedRect ellipseRect = cv::fitEllipse(contoursPoints[0]);
        cv::Size2f ellipseAxis = ellipseRect.size;

        double demiGrandAxe = std::max(ellipseAxis.height, ellipseAxis.width) / 2.0;
        double demiPetitAxe = std::min(ellipseAxis.height, ellipseAxis.width) / 2.0;
        m_configAndResults.Eccentricity.value = sqrt(1 - (demiPetitAxe*demiPetitAxe/(demiGrandAxe*demiGrandAxe)));

        perimeter = cv::arcLength(contoursPoints[0],true) * _inRaster->resolution();

        std::vector<cv::Point> convexHullPoints;
        cv::convexHull(contoursPoints[0], convexHullPoints);

        m_configAndResults.Solidity.value = areaContour / cv::contourArea(convexHullPoints);

        m_configAndResults.Circularity.value = 1;
        if (perimeter != 0) {m_configAndResults.Circularity.value = sqrt(areaContour* _inRaster->resolution()* _inRaster->resolution() / M_PI) * 2.0 * M_PI / perimeter;}

    } else {
        m_configAndResults.Eccentricity.value = 1;
        m_configAndResults.Solidity.value = 1;
        m_configAndResults.Circularity.value = 1;
    }


    // Compute attCentroidX, attCentroidY, weightedCentroidX, weightedCentroidY, attCrownArea, sdHeights
    for (size_t xx = 0 ; xx < _inRaster->colDim() ; xx++)
    {
        for (size_t yy = 0 ; yy < _inRaster->linDim() ; yy++)
        {
            size_t index;
            if (_inRaster->index(xx, yy, index))
            {
                Eigen::Vector3d cellCenter;
                _inRaster->getCellCenterCoordinates(index, cellCenter);

                if (mask->valueAtIndex(index) != 0)
                {
                    m_configAndResults.CentroidX.value += cellCenter(0);
                    m_configAndResults.CentroidY.value += cellCenter(1);

                    crownAreaPix += 1;
                }

                float height = _inRaster->valueAtIndexAsDouble(index);
                if (height != _inRaster->NAAsDouble())
                {
                    heightsSum += height;

                    if (height > m_configAndResults.MaxHeight.value)
                    {
                        m_configAndResults.MaxHeight.value = height;
                        m_configAndResults.MaxHeight_X.value = _inRaster->getCellCenterColCoord(xx);
                        m_configAndResults.MaxHeight_Y.value = _inRaster->getCellCenterLinCoord(yy);;
                    }

                    if (height < m_configAndResults.MinHeight.value)
                    {
                        m_configAndResults.MinHeight.value = height;
                    }


                    weightedCentroidX += cellCenter(0) * height;
                    weightedCentroidY += cellCenter(1) * height;

                    sdHeights += height*height;
                    crownAreaPixWithHeights += 1;
                }
            } else {
                qDebug() << "Problème";
            }
        }
    }

    if (crownAreaPix > 0)
    {
        //m_configAndResults.MaxHeight = _inRaster->dataMax();
        m_configAndResults.Vextent.value = m_configAndResults.MaxHeight.value - m_configAndResults.MinHeight.value;
        if (m_configAndResults.MaxHeight.value > 0) {m_configAndResults.CrRatio.value = m_configAndResults.Vextent.value / m_configAndResults.MaxHeight.value;}

        if (crownAreaPixWithHeights > 0) {heightsMean = heightsSum / crownAreaPixWithHeights;}

        m_configAndResults.CentroidX.value /= crownAreaPix;
        m_configAndResults.CentroidY.value /= crownAreaPix;

        if (heightsSum > 0)
        {
            weightedCentroidX /= heightsSum;
            weightedCentroidY /= heightsSum;
        }

        sdHeights = sqrt((1/crownAreaPix)*sdHeights - heightsMean*heightsMean);

        m_configAndResults.CrownArea.value = crownAreaPix * _inRaster->resolution() * _inRaster->resolution();

        m_configAndResults.HtoAratio.value = m_configAndResults.MaxHeight.value / m_configAndResults.CrownArea.value;

        if (m_configAndResults.MaxHeight.value > 0) {m_configAndResults.CVmax.value = sdHeights / m_configAndResults.MaxHeight.value;}

        m_configAndResults.Diameter.value = 2.0*sqrt(m_configAndResults.CrownArea.value / M_PI);

        m_configAndResults.CentroidShift.value = sqrt(pow(m_configAndResults.CentroidX.value - weightedCentroidX, 2) + pow(m_configAndResults.CentroidY.value - weightedCentroidY, 2)) / m_configAndResults.Diameter.value * 10000.0;
    }

    delete mask;


    setAttributeValueVaB(m_configAndResults.MinHeight);
    setAttributeValueVaB(m_configAndResults.MaxHeight);
    setAttributeValueVaB(m_configAndResults.MaxHeight_X);
    setAttributeValueVaB(m_configAndResults.MaxHeight_Y);
    setAttributeValueVaB(m_configAndResults.CrownArea);
    setAttributeValueVaB(m_configAndResults.CentroidX);
    setAttributeValueVaB(m_configAndResults.CentroidY);
    setAttributeValueVaB(m_configAndResults.Eccentricity);
    setAttributeValueVaB(m_configAndResults.Solidity);
    setAttributeValueVaB(m_configAndResults.HtoAratio);
    setAttributeValueVaB(m_configAndResults.CVmax);
    setAttributeValueVaB(m_configAndResults.CentroidShift);
    setAttributeValueVaB(m_configAndResults.Vextent);
    setAttributeValueVaB(m_configAndResults.CrRatio);
    setAttributeValueVaB(m_configAndResults.Diameter);
    setAttributeValueVaB(m_configAndResults.Circularity);
}

void SEG_MetricRasterSegma::declareAttributes()
{
    registerAttributeVaB(m_configAndResults.MinHeight, CT_AbstractCategory::DATA_NUMBER, tr("MinHeight"));
    registerAttributeVaB(m_configAndResults.MaxHeight, CT_AbstractCategory::DATA_NUMBER, tr("MaxHeight"));
    registerAttributeVaB(m_configAndResults.MaxHeight_X, CT_AbstractCategory::DATA_NUMBER, tr("MaxHeight_X"));
    registerAttributeVaB(m_configAndResults.MaxHeight_Y, CT_AbstractCategory::DATA_NUMBER, tr("MaxHeight_Y"));
    registerAttributeVaB(m_configAndResults.CrownArea, CT_AbstractCategory::DATA_NUMBER, tr("CrownArea"));
    registerAttributeVaB(m_configAndResults.CentroidX, CT_AbstractCategory::DATA_NUMBER, tr("CentroidX"));
    registerAttributeVaB(m_configAndResults.CentroidY, CT_AbstractCategory::DATA_NUMBER, tr("CentroidY"));
    registerAttributeVaB(m_configAndResults.Eccentricity, CT_AbstractCategory::DATA_NUMBER, tr("Eccentricity"));
    registerAttributeVaB(m_configAndResults.Solidity, CT_AbstractCategory::DATA_NUMBER, tr("Solidity"));
    registerAttributeVaB(m_configAndResults.HtoAratio, CT_AbstractCategory::DATA_NUMBER, tr("HtoAratio"));
    registerAttributeVaB(m_configAndResults.CVmax, CT_AbstractCategory::DATA_NUMBER, tr("CVmax"));
    registerAttributeVaB(m_configAndResults.CentroidShift, CT_AbstractCategory::DATA_NUMBER, tr("CentroidShift"));
    registerAttributeVaB(m_configAndResults.Vextent, CT_AbstractCategory::DATA_NUMBER, tr("Vextent"));
    registerAttributeVaB(m_configAndResults.CrRatio, CT_AbstractCategory::DATA_NUMBER, tr("CrRatio"));
    registerAttributeVaB(m_configAndResults.Diameter, CT_AbstractCategory::DATA_NUMBER, tr("Diameter"));
    registerAttributeVaB(m_configAndResults.Circularity, CT_AbstractCategory::DATA_NUMBER, tr("Circularity"));
}


