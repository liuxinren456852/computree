#include "seg_stepdetectmaxima.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/types_c.h"

SEG_StepDetectMaxima::SEG_StepDetectMaxima() : SuperClass()
{
    _minHeight = 2.0;
}

QString SEG_StepDetectMaxima::description() const
{
    return tr("3- Détecter les maxima");
}

QString SEG_StepDetectMaxima::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepDetectMaxima::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepDetectMaxima::createNewInstance() const
{
    return new SEG_StepDetectMaxima();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepDetectMaxima::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Raster(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inHeights, tr("Raster"));

    manager.addResult(_inResultDTM, tr("MNTres"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultDTM, _inZeroOrMoreRootGroupDTM);
    manager.addGroup(_inZeroOrMoreRootGroupDTM, _inGroupDTM);
    manager.addItem(_inGroupDTM, _inDTM, tr("MNTitem"));

}

void SEG_StepDetectMaxima::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outMaxima, tr("Maxima"));
}

void SEG_StepDetectMaxima::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Ne pas détécter de maxima en dessous de"), tr("m"), -99999, 99999, 2, _minHeight);
}

void SEG_StepDetectMaxima::compute()
{
    const CT_Image2D<float>* mnt = nullptr;
    for (const CT_Image2D<float>* imageIn : _inDTM.iterateInputs(_inResultDTM))
    {
            mnt = imageIn;
    }

    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* imageIn : grp->singularItems(_inHeights))
        {
            if (isStopped()) {return;}

            Eigen::Vector2d min;
            imageIn->getMinCoordinates(min);
            CT_Image2D<qint32>* maximaImage = new CT_Image2D<qint32>(min(0), min(1), imageIn->xdim(), imageIn->ydim(), imageIn->resolution(), imageIn->level(), -1, 0);
            grp->addSingularItem(_outMaxima, maximaImage);

            setProgress(10);

            cv::Mat_<float> dilatedMat(imageIn->ydim(), imageIn->xdim());
            cv::Mat maximaMat = cv::Mat::zeros(imageIn->ydim(), imageIn->xdim(), CV_32F); // Nécessaire car compare ne prend pas la version template Mat_<Tp> en output !!!

            cv::Mat_<float> &imageMat = (const_cast<CT_Image2D<float>*>(imageIn))->getMat();
            // Détéction des maxima
            cv::dilate(imageMat, dilatedMat, cv::getStructuringElement(cv::MORPH_RECT, cv::Size2d(3,3)));

            cv::compare(imageMat, dilatedMat, maximaMat, cv::CMP_EQ);

            setProgress(30);

            // numérotation des maxima
            cv::Mat labels = cv::Mat::zeros(imageIn->ydim(), imageIn->xdim(), CV_32S); // Nécessaire car compare ne prend pas la version template Mat_<Tp> en output !!!
            cv::connectedComponents(maximaMat, labels);

            cv::Mat_<qint32> labs = labels;
            for (int xx = 0 ; xx < maximaMat.cols ; xx++)
            {
                for (int yy = 0 ; yy < maximaMat.rows ; yy++)
                {
                    float zval = imageMat(yy, xx);

                    if (mnt != nullptr)
                    {
                        float zmnt = mnt->valueAtCoords(imageIn->getCellCenterColCoord(xx), imageIn->getCellCenterLinCoord(yy));
                        if (!qFuzzyCompare(zmnt, mnt->NA()))
                        {
                            zval -= zmnt;
                        }
                    }

                    if (double(zval) < _minHeight)
                    {
                        labs(yy, xx) = 0;
                    }
                }
            }

            setProgress(60);

            maximaImage->getMat() = labels;

            setProgress(80);

            maximaImage->computeMinMax();

            setProgress(99);
        }
    }
}
