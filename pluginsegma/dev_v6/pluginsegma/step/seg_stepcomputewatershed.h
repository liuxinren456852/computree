#ifndef SEG_STEPCOMPUTEWATERSHED_H
#define SEG_STEPCOMPUTEWATERSHED_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"

class SEG_StepComputeWatershed: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    SEG_StepComputeWatershed();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

    void computeWatershed(const CT_Image2D<float> *imageIn, CT_Image2D<qint32> *watershedImage, const CT_Image2D<float> *mnt);

    void mergeLimitsWithClusters(const CT_Image2D<float> *imageIn, CT_Image2D<qint32> *watershedImage);

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    bool    _mergeLimits;
    double _minHeight;


    CT_HandleInResultGroupCopy<>                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inHeights;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                    _inMaxima;
    CT_HandleOutSingularItem<CT_Image2D<qint32> >                   _outWatershed;

    CT_HandleInResultGroup<0,1>                                     _inResultDTM;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroupDTM;
    CT_HandleInStdGroup<>                                           _inGroupDTM;
    CT_HandleInSingularItem<CT_Image2D<float> >                     _inDTM;

    static void computeSumAndCount(const CT_Image2D<float> *imageIn, const CT_Image2D<qint32> *watershedImage, float centerVal, QMap<qint32, float> &sumMap, QMap<qint32, int> &countMap, int x, int y);
    bool hasExactlyOneNeighbourCluster(int &x, int &y, CT_Image2D<qint32> *watershedImage, qint32 &firstVal);
};

#endif // SEG_STEPCOMPUTEWATERSHED_H
