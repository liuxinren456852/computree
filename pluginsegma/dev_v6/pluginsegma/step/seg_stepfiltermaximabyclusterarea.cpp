#include "seg_stepfiltermaximabyclusterarea.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

SEG_StepFilterMaximaByClusterArea::SEG_StepFilterMaximaByClusterArea() : SuperClass()
{
    _maxArea = 4.0;
    _createMaximaPts = true;
}

QString SEG_StepFilterMaximaByClusterArea::description() const
{
    return tr("Filtrer les maxima en fonction de l'aire des clusters");
}

QString SEG_StepFilterMaximaByClusterArea::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepFilterMaximaByClusterArea::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepFilterMaximaByClusterArea::createNewInstance() const
{
    return new SEG_StepFilterMaximaByClusterArea();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepFilterMaximaByClusterArea::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Maxima"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inHeights, tr("Image (hauteurs)"));
    manager.addItem(_inGroup, _inMaxima, tr("Maxima"));
    manager.addItem(_inGroup, _inClusters, tr("Clusters"));
}

void SEG_StepFilterMaximaByClusterArea::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outMaxima, tr("Maxima filtrés"));

    if (_createMaximaPts)
    {
        manager.addGroup(_inGroup, _outGroupMaximaPoint, tr("Maxima filtrés (Pts)"));
        manager.addItem(_outGroupMaximaPoint, _outMaximaPoint, tr("Maximum"));
    }
}

void SEG_StepFilterMaximaByClusterArea::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addBool("", "", tr("Créer des points pour les maxima"), _createMaximaPts);
}

void SEG_StepFilterMaximaByClusterArea::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Aire minimale pour garder un cluster"), "m²", 0, 999999, 2, _maxArea);
}

void SEG_StepFilterMaximaByClusterArea::compute()
{
    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* imageIn : grp->singularItems(_inHeights))
        {
            if (isStopped()) {return;}

            const CT_Image2D<qint32>* maximaIn = grp->singularItem(_inMaxima);
            const CT_Image2D<qint32>* clustersIn = grp->singularItem(_inClusters);

            if (maximaIn != nullptr)
            {
                Eigen::Vector2d min;
                maximaIn->getMinCoordinates(min);
                CT_Image2D<qint32>* filteredMaxima = new CT_Image2D<qint32>(min(0), min(1), maximaIn->xdim(), maximaIn->ydim(), maximaIn->resolution(), maximaIn->level(), maximaIn->NA(), 0);
                grp->addSingularItem(_outMaxima, filteredMaxima);

                filteredMaxima->getMat() = const_cast<CT_Image2D<qint32>*>(maximaIn)->getMat().clone();

                setProgress(20);

                // Get maxima coordinates list
                QMultiMap<qint32, Eigen::Vector3d*> maximaCoords;
                QMultiMap<double, qint32> maximaHeights;

                for (int xx = 0 ; xx < maximaIn->xdim() ; xx++)
                {
                    for (int yy = 0 ; yy < maximaIn->ydim() ; yy++)
                    {
                        qint32 maximaID = maximaIn->value(xx, yy);

                        if (maximaID > 0 && maximaID != maximaIn->NA())
                        {
                            Eigen::Vector3d* coords = new Eigen::Vector3d();
                            if (maximaIn->getCellCenterCoordinates(xx, yy, *coords))
                            {
                                (*coords)(2) = double(imageIn->value(xx, yy));
                                maximaCoords.insert(maximaID, coords);
                                maximaHeights.insert((*coords)(2), maximaID);
                            }
                        }
                    }
                }

                setProgress(25);

                // Compute ordered vector of maxima ids
                QList<qint32> validMaxima;

                QMapIterator<double, qint32> itH(maximaHeights);
                QMap<qint32, int> indices;
                itH.toBack();
                while (itH.hasPrevious())
                {
                    itH.previous();
                    qint32 cl = itH.value();
                    if (!validMaxima.contains(cl))
                    {
                        validMaxima.append(cl);
                        indices.insert(cl, validMaxima.size() - 1);
                    }
                }

                QVector<qint32> orderedMaxima = validMaxima.toVector();
                int mxSize = orderedMaxima.size();
                validMaxima.clear();

                // Create maxima coords vector
                QVector<Eigen::Vector3d> coords(mxSize);
                for (int i = 0 ; i < mxSize ; i++)
                {
                    qint32 id = orderedMaxima.at(i);

                    QList<Eigen::Vector3d*> coordinates = maximaCoords.values(id);
                    coords[i] = *(coordinates.at(0));

                    // Compute position of the current maxima if more than one pixel
                    int size = coordinates.size();
                    if (size > 1)
                    {
                        for (int j = 1 ; j < size ; j++)
                        {
                            Eigen::Vector3d* pos = coordinates.at(j);
                            coords[i](0) += (*pos)(0);
                            coords[i](1) += (*pos)(1);
                            if ((*pos)(2) > coords[i](2)) {coords[i](2) = (*pos)(2);}
                        }

                        coords[i](0) /= size;
                        coords[i](1) /= size;
                    }
                }

                double pixelArea = clustersIn->resolution() * clustersIn->resolution();
                QVector<double> areas(mxSize);
                areas.fill(0);
                for (int xx = 0 ; xx < clustersIn->xdim() ; xx++)
                {
                    for (int yy = 0 ; yy < clustersIn->ydim() ; yy++)
                    {
                        qint32 idCell = clustersIn->value(xx, yy);

                        if (idCell > 0 && idCell != clustersIn->NA())
                        {
                            int i = indices.value(idCell);
                            areas[i] += pixelArea;
                        }
                    }
                }

                setProgress(30);

                // For each maxima, test area
                for (int i = 0 ; i < mxSize ; i++)
                {
                    qint32 id = orderedMaxima.at(i);

                    if (id > 0)
                    {
                        if (areas.at(i) < _maxArea)
                        {
                            orderedMaxima[i] = 0;
                        }
                    }

                    setProgress(float(29.0*i / mxSize + 30.0));
                }

                setProgress(60);

                for (int i = 0 ; i < mxSize ; i++)
                {
                    qint32 cl = orderedMaxima.at(i);
                    if (cl > 0)
                    {
                        validMaxima.append(cl);

                        double x = coords[i](0);
                        double y = coords[i](1);
                        double z = coords[i](2);

                        if (_createMaximaPts)
                        {
                            CT_StandardItemGroup* grpPt = new CT_StandardItemGroup();
                            CT_ReferencePoint* refPoint = new CT_ReferencePoint(x, y, z, 0);
                            grp->addGroup(_outGroupMaximaPoint, grpPt);
                            grpPt->addSingularItem(_outMaximaPoint, refPoint);
                        }
                    }
                }

                setProgress(70);

                QMap<qint32, qint32> newIds;
                qint32 cpt = 1;
                // effectively delete toRemove maximum and numbers them in a continuous way
                for (int xx = 0 ; xx < filteredMaxima->xdim() ; xx++)
                {
                    for (int yy = 0 ; yy < filteredMaxima->ydim() ; yy++)
                    {
                        qint32 maximaID = filteredMaxima->value(xx, yy);

                        if (maximaID > 0)
                        {
                            if (validMaxima.contains(maximaID))
                            {
                                qint32 newId = newIds.value(maximaID, 0);
                                if (newId == 0)
                                {
                                    newId = cpt++;
                                    newIds.insert(maximaID, newId);
                                }
                                filteredMaxima->setValue(xx, yy, newId);
                            } else {
                                filteredMaxima->setValue(xx, yy, 0);
                            }
                        }
                    }
                }
                newIds.clear();
                setProgress(90);

                filteredMaxima->computeMinMax();

                qDeleteAll(maximaCoords.values());
                setProgress(99);
            }
        }
        setProgress(100);
    }
}

