#ifndef SEG_STEPANALYZEANDFITCROWNS_H
#define SEG_STEPANALYZEANDFITCROWNS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "ct_itemdrawable/ct_itemattributelist.h"

class SEG_StepAnalyzeAndFitCrowns: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    SEG_StepAnalyzeAndFitCrowns();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double      _scoreThreshold;
    double      _resolutionOTSU;

    double _attMaxHeight;
    double _attMaxHeight_X;
    double _attMaxHeight_Y;
    double _attMinHeight;
    double _attCrownArea;
    double _attCentroidX;
    double _attCentroidY;
    double _attEccentricity;
    double _attSolidity;
    double _attHtoAratio;
    double _attCVmax;
    double _attCentroidShift;
    double _attVextent;
    double _attCrRatio;
    double _attDiameter;
    double _attCircularity;


    CT_HandleInResultGroupCopy<>                                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                           _inMainGroup;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                                    _in_clusters;
    CT_HandleInStdGroup<>                                                           _inGroup;
    CT_HandleInSingularItem<CT_Image2D<float> >                                     _in_heights;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER> _inAttIDcluster;
    CT_HandleInSingularItem<CT_Image2D<quint8> >                                    _in_mask;

    CT_HandleOutSingularItem<CT_Image2D<qint32> >                     _outClusters;
    CT_HandleOutSingularItem<CT_Image2D<float> >                      _outHeights;
    CT_HandleOutSingularItem<CT_Image2D<quint8> >                     _outMask;
    CT_HandleOutSingularItem<CT_ItemAttributeList>                    _outAttributes;
    CT_HandleOutStdItemAttribute<qint32>                             _outAttIDCluster;
    CT_HandleOutStdItemAttribute<double>                             _outAttMaxHeight;
    CT_HandleOutStdItemAttribute<double>                             _outAttMaxHeight_X;
    CT_HandleOutStdItemAttribute<double>                             _outAttMaxHeight_Y;
    CT_HandleOutStdItemAttribute<double>                             _outAttMinHeight;
    CT_HandleOutStdItemAttribute<double>                             _outAttCrownArea;
    CT_HandleOutStdItemAttribute<double>                             _outAttCentroidX;
    CT_HandleOutStdItemAttribute<double>                             _outAttCentroidY;
    CT_HandleOutStdItemAttribute<double>                             _outAttEccentricity;
    CT_HandleOutStdItemAttribute<double>                             _outAttSolidity;
    CT_HandleOutStdItemAttribute<double>                             _outAttHtoAratio;
    CT_HandleOutStdItemAttribute<double>                             _outAttCVmax;
    CT_HandleOutStdItemAttribute<double>                             _outAttCentroidShift;
    CT_HandleOutStdItemAttribute<double>                             _outAttVextent;
    CT_HandleOutStdItemAttribute<double>                             _outAttCrRatio;
    CT_HandleOutStdItemAttribute<double>                             _outAttDiameter;
    CT_HandleOutStdItemAttribute<double>                             _outAttCircularity;
    CT_HandleOutStdItemAttribute<double>                             _outAttScoreCentroidShift;
    CT_HandleOutStdItemAttribute<double>                             _outAttScoreEccentricity;
    CT_HandleOutStdItemAttribute<double>                             _outAttScoreSolidity;
    CT_HandleOutStdItemAttribute<double>                             _outAttScoreCVmax;
    CT_HandleOutStdItemAttribute<double>                             _outAttScore;
    CT_HandleOutStdItemAttribute<double>                             _outAttOtsuThreshold;


    void computeIndicators(CT_Image2D<float>* heightsOut, CT_Image2D<quint8>* maskOut);
    double standardize(double val, double min, double max, bool invert);
};

#endif // SEG_STEPANALYZEANDFITCROWNS_H
