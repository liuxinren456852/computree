#include "seg_stepanalyzeandfitcrowns.h"
#include "ct_itemdrawable/ct_profile.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/core/core.hpp"

SEG_StepAnalyzeAndFitCrowns::SEG_StepAnalyzeAndFitCrowns() : SuperClass()
{
    _scoreThreshold = 80.0;
    _resolutionOTSU = 0.1;
}

QString SEG_StepAnalyzeAndFitCrowns::description() const
{
    return tr("7- Analyser / Rogner les couronnes");
}

QString SEG_StepAnalyzeAndFitCrowns::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepAnalyzeAndFitCrowns::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepAnalyzeAndFitCrowns::createNewInstance() const
{
    return new SEG_StepAnalyzeAndFitCrowns();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepAnalyzeAndFitCrowns::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Clusters"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inMainGroup);
    manager.addItem(_inMainGroup, _in_clusters, tr("Clusters"));
    manager.addGroup(_inMainGroup, _inGroup, tr("Cluster"));
    manager.addItem(_inGroup, _in_heights, tr("Image (hauteurs)"));
    manager.addItemAttribute(_in_heights, _inAttIDcluster, CT_AbstractCategory::DATA_ID, tr("ID Cluster"));
    manager.addItem(_inGroup, _in_mask, tr("Masque"));
}

void SEG_StepAnalyzeAndFitCrowns::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inMainGroup, _outClusters, tr("Clusters modifiés"));
    manager.addItem(_inGroup, _outHeights, tr("Hauteurs modifiées"));
    manager.addItem(_inGroup, _outMask, tr("Masque Modifié"));
    manager.addItem(_inGroup, _outAttributes, tr("Attributs des clusters"));
    manager.addItemAttribute(_outAttributes, _outAttIDCluster, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), tr("IDCluster"));
    manager.addItemAttribute(_outAttributes, _outAttMaxHeight, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("MaxHeight"));
    manager.addItemAttribute(_outAttributes, _outAttMaxHeight_X, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_X), tr("XMaxHeight"));
    manager.addItemAttribute(_outAttributes, _outAttMaxHeight_Y, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_Y), tr("YMaxHeight"));
    manager.addItemAttribute(_outAttributes, _outAttMinHeight, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("MinHeight"));
    manager.addItemAttribute(_outAttributes, _outAttCrownArea, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("CrownArea"));
    manager.addItemAttribute(_outAttributes, _outAttCentroidX, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_X), tr("XCentroid"));
    manager.addItemAttribute(_outAttributes, _outAttCentroidY, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_Y), tr("YCentroid"));
    manager.addItemAttribute(_outAttributes, _outAttEccentricity, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("Eccentricity"));
    manager.addItemAttribute(_outAttributes, _outAttSolidity, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("Solidity"));
    manager.addItemAttribute(_outAttributes, _outAttHtoAratio, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("HtoAratio"));
    manager.addItemAttribute(_outAttributes, _outAttCVmax, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("CVmax"));
    manager.addItemAttribute(_outAttributes, _outAttCentroidShift, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("CentroidShift"));
    manager.addItemAttribute(_outAttributes, _outAttVextent, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("Vextent"));
    manager.addItemAttribute(_outAttributes, _outAttCrRatio, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("CrRatio"));
    manager.addItemAttribute(_outAttributes, _outAttDiameter, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("Diameter"));
    manager.addItemAttribute(_outAttributes, _outAttCircularity, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("Circularity"));
    manager.addItemAttribute(_outAttributes, _outAttScoreCentroidShift, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("ScoreCentroidShift"));
    manager.addItemAttribute(_outAttributes, _outAttScoreEccentricity, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("ScoreEccentricity"));
    manager.addItemAttribute(_outAttributes, _outAttScoreSolidity, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("ScoreSolidity"));
    manager.addItemAttribute(_outAttributes, _outAttScoreCVmax, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("ScoreCVmax"));
    manager.addItemAttribute(_outAttributes, _outAttScore, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("Score"));
    manager.addItemAttribute(_outAttributes, _outAttOtsuThreshold, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("OtsuThreshold"));
}

void SEG_StepAnalyzeAndFitCrowns::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addDouble(tr("Seuil en-dessous duquel la couronne est amputée"), "%", 0, 100, 0, _scoreThreshold);
    postInputConfigDialog->addDouble(tr("Résolution du profil pour le seuillage d'OTSU"), "m", 0.01, 100, 2, _resolutionOTSU);
}

void SEG_StepAnalyzeAndFitCrowns::compute()
{
    for (CT_StandardItemGroup* mainGrp : _inMainGroup.iterateOutputs(_inResult))
    {

        for (const CT_Image2D<qint32>* clustersIn : mainGrp->singularItems(_in_clusters))
        {
            if (isStopped()) {return;}

            CT_Image2D<qint32>* clustersOut = new CT_Image2D<qint32>(clustersIn->minX(), clustersIn->minY(), clustersIn->xdim(), clustersIn->ydim(), clustersIn->resolution(), clustersIn->level(), -1, -1);
            mainGrp->addSingularItem(_outClusters, clustersOut);

            int cpt = 0;
            int maxCpt = clustersIn->dataMax();

            for (const CT_StandardItemGroup* grpConst : mainGrp->groups(_inGroup))
            {

                if (isStopped()) {return;}

                CT_StandardItemGroup* grp = const_cast<CT_StandardItemGroup*>(grpConst);

                const CT_Image2D<float>* heightsIn = grpConst->singularItem(_in_heights);
                const CT_Image2D<quint8>* maskIn = grpConst->singularItem(_in_mask);

                if (heightsIn != nullptr && maskIn != nullptr)
                {
                    qint32 cluster = 0;

                    // TODOV6 - Plantage à l'exécution de la ligne suivante
                    const CT_AbstractItemAttribute* att = heightsIn->itemAttribute(_inAttIDcluster);
                    if (att != nullptr) {cluster = att->toInt(heightsIn, nullptr);}

                    CT_Image2D<quint8>* maskOut = new CT_Image2D<quint8>(maskIn->minX(), maskIn->minY(), maskIn->xdim(), maskIn->ydim(), maskIn->resolution(), maskIn->level(), maskIn->NA(), 0);
                    grp->addSingularItem(_outMask, maskOut);
                    maskOut->getMat() = const_cast<CT_Image2D<quint8>*>(maskIn)->getMat().clone();

                    CT_Image2D<float>* heightsOut = new CT_Image2D<float>(heightsIn->minX(), heightsIn->minY(), heightsIn->xdim(), heightsIn->ydim(), heightsIn->resolution(), heightsIn->level(), heightsIn->NA(), 0);
                    grp->addSingularItem(_outHeights, heightsOut);
                    heightsOut->getMat() = const_cast<CT_Image2D<float>*>(heightsIn)->getMat().clone();

                    heightsOut->computeMinMax();
                    maskOut->computeMinMax();

                    // Calcul des indicateurs
                    computeIndicators(heightsOut, maskOut);

                    // Compute scores
                    double attScoreCentroidShift = standardize(_attCentroidShift, 2.0, 15.1, true);
                    double attScoreEccentricity = standardize(_attEccentricity, 0.3, 0.8, true);
                    double attScoreSolidity = standardize(_attSolidity, 0.3, 0.8, false);
                    double attScoreCVmax = standardize(_attCVmax, 0.8, 0.12, true);

                    double OTSUvalue = -1;
                    double attScore = (attScoreCentroidShift + attScoreEccentricity + attScoreSolidity + attScoreCVmax) / 4.0;

                    if (attScore < _scoreThreshold)
                    {
                        // Création du profil de distribution de hauteur
                        CT_Profile<float>* profile = CT_Profile<float>::createProfileFromSegment(0, 0, _attMinHeight, 0, 0, _attMaxHeight, _resolutionOTSU, -1, 0);

                        for (int xx = 0 ; xx < heightsOut->xdim() ; xx++)
                        {
                            for (int yy = 0 ; yy < heightsOut->ydim() ; yy++)
                            {
                                size_t index;
                                if (heightsOut->index(xx, yy, index))
                                {
                                    float height = heightsOut->value(xx, yy);
                                    if (!qFuzzyCompare(height, heightsOut->NA()))
                                    {
                                        profile->addValueForXYZ(0, 0, double(height), 1);
                                    }
                                } else {
                                    qDebug() << "Problème";
                                }
                            }
                        }

                        // Calcul du seuil d'OTSU
                        OTSUvalue = profile->getOtsuThreshold(nullptr, nullptr) + _attMinHeight;

                        // Filtrage du masque
                        for (int xx = 0 ; xx < heightsOut->xdim() ; xx++)
                        {
                            for (int yy = 0 ; yy < heightsOut->ydim() ; yy++)
                            {
                                size_t index;
                                if (heightsOut->index(xx, yy, index))
                                {
                                    float height = heightsOut->value(xx, yy);
                                    if (!qFuzzyCompare(height, heightsOut->NA()) && double(height) < OTSUvalue)
                                    {
                                        maskOut->setValue(xx, yy, 0);
                                    }
                                } else {
                                    qDebug() << "Problème";
                                }
                            }
                        }

                        // Remplissage des trous dans le masque
                        std::vector<std::vector<cv::Point> > contours2;
                        cv::findContours(maskOut->getMat(), contours2, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
                        cv::Scalar color(1);
                        cv::drawContours(maskOut->getMat(), contours2, -1, color, -1);

                        // Ne conserve d'un segment (le plus gros)
                        cv::Mat labels;
                        cv::Mat stats;
                        cv::Mat centroids;
                        int Nsegs = cv::connectedComponentsWithStats(maskOut->getMat(), labels, stats, centroids, 4);
                        if (Nsegs > 2)
                        {
                            int maxArea = 0;
                            int keptLabel = 1;
                            for (int i = 1 ; i < Nsegs ; i++)
                            {
                                int area = stats.at<int>(i, cv::CC_STAT_AREA);
                                if (area > maxArea)
                                {
                                    maxArea = area;
                                    keptLabel = i;
                                }
                            }

                            for (int xx = 0 ; xx < maskOut->xdim() ; xx++)
                            {
                                for (int yy = 0 ; yy < maskOut->ydim() ; yy++)
                                {
                                    size_t index;
                                    if (maskOut->index(xx, yy, index))
                                    {
                                        if (labels.at<int>(yy, xx) != keptLabel)
                                        {
                                            maskOut->setValue(xx, yy, maskOut->NA());
                                        }
                                    } else {
                                        qDebug() << "Problème";
                                    }
                                }
                            }
                        }

                        // Création du raster final de hauteur
                        for (int xx = 0 ; xx < heightsOut->xdim() ; xx++)
                        {
                            for (int yy = 0 ; yy < heightsOut->ydim() ; yy++)
                            {
                                size_t index;
                                if (heightsOut->index(xx, yy, index))
                                {
                                    if (maskOut->value(xx, yy) == 0 || maskOut->value(xx, yy) == maskOut->NA())
                                    {
                                        heightsOut->setValue(xx, yy, heightsOut->NA());
                                    }
                                } else {
                                    qDebug() << "Problème";
                                }
                            }
                        }

                        heightsOut->computeMinMax();
                        maskOut->computeMinMax();

                        // Mise à jour des indicateurs
                        computeIndicators(heightsOut, maskOut);
                    }

                    // Mise à jour du raster final des clusters
                    for (int xx = 0 ; xx < maskOut->xdim() ; xx++)
                    {
                        for (int yy = 0 ; yy < maskOut->ydim() ; yy++)
                        {
                            size_t index;
                            if (maskOut->index(xx, yy, index))
                            {
                                if (maskOut->value(xx, yy) >0)
                                {
                                    double x = maskOut->getCellCenterColCoord(xx);
                                    double y = maskOut->getCellCenterLinCoord(yy);
                                    clustersOut->setValueAtCoords(x, y, cluster);
                                }
                            } else {
                                qDebug() << "Problème";
                            }
                        }
                    }

                    // Ajout des valeurs d'attributs dans le résultat de sortie
                    CT_ItemAttributeList *attributes = new CT_ItemAttributeList();
                    grp->addSingularItem(_outAttributes, attributes);

                    attributes->addItemAttribute(_outAttIDCluster,           new CT_StdItemAttributeT<qint32>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID)   , cluster));
                    attributes->addItemAttribute(_outAttMaxHeight,           new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attMaxHeight));
                    attributes->addItemAttribute(_outAttMaxHeight_X,         new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_X), _attMaxHeight_X));
                    attributes->addItemAttribute(_outAttMaxHeight_Y,         new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_Y), _attMaxHeight_Y));
                    attributes->addItemAttribute(_outAttMinHeight,           new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attMinHeight));
                    attributes->addItemAttribute(_outAttCrownArea,           new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attCrownArea));
                    attributes->addItemAttribute(_outAttCentroidX,           new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_X),     _attCentroidX));
                    attributes->addItemAttribute(_outAttCentroidY,           new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_Y),     _attCentroidY));
                    attributes->addItemAttribute(_outAttEccentricity,        new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attEccentricity));
                    attributes->addItemAttribute(_outAttSolidity,            new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attSolidity));
                    attributes->addItemAttribute(_outAttHtoAratio,           new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attHtoAratio));
                    attributes->addItemAttribute(_outAttCVmax,               new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attCVmax));
                    attributes->addItemAttribute(_outAttCentroidShift,       new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attCentroidShift));
                    attributes->addItemAttribute(_outAttVextent,             new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attVextent));
                    attributes->addItemAttribute(_outAttCrRatio,             new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attCrRatio));
                    attributes->addItemAttribute(_outAttDiameter,            new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attDiameter));
                    attributes->addItemAttribute(_outAttCircularity,         new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), _attCircularity));

                    attributes->addItemAttribute(_outAttScoreCentroidShift,  new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), attScoreCentroidShift));
                    attributes->addItemAttribute(_outAttScoreEccentricity,   new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), attScoreEccentricity));
                    attributes->addItemAttribute(_outAttScoreSolidity,       new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), attScoreSolidity));
                    attributes->addItemAttribute(_outAttScoreCVmax,          new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), attScoreCVmax));
                    attributes->addItemAttribute(_outAttScore,               new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), attScore));
                    attributes->addItemAttribute(_outAttOtsuThreshold,       new CT_StdItemAttributeT<double>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), OTSUvalue));


                }
                setProgress(float((cpt++ / maxCpt) * 90.0));
            }
            clustersOut->computeMinMax();

            setProgress(100);
        }
    }
}

void SEG_StepAnalyzeAndFitCrowns::computeIndicators(CT_Image2D<float>* heightsOut, CT_Image2D<quint8>* maskOut)
{
    _attMinHeight = double(heightsOut->dataMin());
    _attMaxHeight = double(heightsOut->dataMin());
    _attMaxHeight_X = 0;
    _attMaxHeight_Y = 0;
    _attCrownArea = 0;
    _attCentroidX = 0;
    _attCentroidY = 0;
    _attEccentricity = 0;
    _attSolidity = 0;
    _attHtoAratio = 0;
    _attCVmax = 0;
    _attCentroidShift = 0;
    _attVextent = 0;
    _attCrRatio = 0;
    _attDiameter = 0;
    _attCircularity = 0;

    double weightedCentroidX = 0;
    double weightedCentroidY = 0;
    double sdHeights = 0;
    double heightsSum = 0;
    double crownAreaPix = 0;
    double crownAreaPixWithHeights = 0;
    double heightsMean = 0;

    // Compute eccentricity, solidity circularty and perimeter
    cv::Mat_<quint8> contours = maskOut->getMat().clone();
    std::vector<std::vector<cv::Point> > contoursPoints;

    cv::findContours(contours, contoursPoints, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    if (contoursPoints.size() > 0 && (contoursPoints[0].size() >= 5))
    {
        double areaContour = cv::contourArea(contoursPoints[0]);
        double perimeter = 0;

        cv::RotatedRect ellipseRect = cv::fitEllipse(contoursPoints[0]);
        cv::Size2f ellipseAxis = ellipseRect.size;

        double demiGrandAxe = double(std::max(ellipseAxis.height, ellipseAxis.width) / 2.0f);
        double demiPetitAxe = double(std::min(ellipseAxis.height, ellipseAxis.width) / 2.0f);
        _attEccentricity = sqrt(1.0 - (demiPetitAxe*demiPetitAxe/(demiGrandAxe*demiGrandAxe)));

        //cv::Moments moments = cv::moments(contoursPoints[0]);
        //int cx = int(moments.m10/moments.m00); // centroid image
        //int cy = int(moments.m01/moments.m00); // centroid image

        perimeter = cv::arcLength(contoursPoints[0],true) * heightsOut->resolution();

        std::vector<cv::Point> convexHullPoints;
        cv::convexHull(contoursPoints[0], convexHullPoints);

        _attSolidity = areaContour / cv::contourArea(convexHullPoints);

        _attCircularity = 1;
        if (!qFuzzyCompare(perimeter, 0.0)) {_attCircularity = sqrt(areaContour* heightsOut->resolution()* heightsOut->resolution() / M_PI) * 2.0 * M_PI / perimeter;}

    } else {
        _attEccentricity = 1;
        _attSolidity = 1;
        _attCircularity = 1;
    }

    // Compute attCentroidX, attCentroidY, weightedCentroidX, weightedCentroidY, attCrownArea, sdHeights
    for (int xx = 0 ; xx < heightsOut->xdim() ; xx++)
    {
        for (int yy = 0 ; yy < heightsOut->ydim() ; yy++)
        {
            size_t index;
            if (heightsOut->index(xx, yy, index))
            {
                Eigen::Vector3d cellCenter;
                heightsOut->getCellCenterCoordinates(index, cellCenter);

                if (maskOut->valueAtIndex(index) != maskOut->NA())
                {
                    _attCentroidX += cellCenter(0);
                    _attCentroidY += cellCenter(1);

                    crownAreaPix += 1;
                }

                float height = heightsOut->value(xx, yy);
                if (!qFuzzyCompare(height, heightsOut->NA()))
                {
                    heightsSum += double(height);

                    if (double(height) >= _attMaxHeight)
                    {
                        _attMaxHeight = double(height);
                        _attMaxHeight_X = heightsOut->getCellCenterColCoord(xx);
                        _attMaxHeight_Y = heightsOut->getCellCenterLinCoord(yy);;
                    }

                    weightedCentroidX += cellCenter(0) * double(height);
                    weightedCentroidY += cellCenter(1) * double(height);

                    sdHeights += pow(double(height), 2);
                    crownAreaPixWithHeights += 1;
                }
            } else {
                qDebug() << "Problème";
            }
        }
    }

    if (crownAreaPix > 0)
    {
        //_attMaxHeight = heightsOut->dataMax();
        _attVextent = _attMaxHeight - _attMinHeight;
        if (_attMaxHeight > 0) {_attCrRatio = _attVextent / _attMaxHeight;}

        if (crownAreaPixWithHeights > 0) {heightsMean = heightsSum / crownAreaPixWithHeights;}

        _attCentroidX /= crownAreaPix;
        _attCentroidY /= crownAreaPix;

        if (heightsSum > 0)
        {
            weightedCentroidX /= heightsSum;
            weightedCentroidY /= heightsSum;
        }

        sdHeights = sqrt((1/crownAreaPix)*sdHeights - heightsMean*heightsMean);

        _attCrownArea = crownAreaPix * heightsOut->resolution() * heightsOut->resolution();

        _attHtoAratio = _attMaxHeight / _attCrownArea;

        if (_attMaxHeight > 0) {_attCVmax = sdHeights / _attMaxHeight;}

        _attDiameter = 2.0*sqrt(_attCrownArea / M_PI);

        _attCentroidShift = sqrt(pow(_attCentroidX - weightedCentroidX, 2) + pow(_attCentroidY - weightedCentroidY, 2)) / _attDiameter * 10000.0;
    }
}

double SEG_StepAnalyzeAndFitCrowns::standardize(double val, double min, double max, bool invert)
{
    double result = val;
    if (result > max) {result = max;}
    if (result < min) {result = min;}

    result = 100.0 * (result - min) / (max - min);
    if (invert) {result  = 100.0 - result;}
    return result;
}
