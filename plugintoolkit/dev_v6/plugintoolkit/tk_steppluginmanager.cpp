#include "tk_steppluginmanager.h"
#include "ct_actions/ct_actionsseparator.h"

#include "step/tk_stepcentercloud.h"
#include "step/tk_stepcolorbyaxis.h"
#include "step/tk_stepcolorrandom.h"
#include "step/tk_stepestimatenormalsrandom.h"
#include "step/tk_stepextractbboxpart.h"
#include "step/tk_stepextractbox.h"
#include "step/tk_stepextractcylinder.h"
#include "step/tk_stepextractsphere.h"
#include "step/tk_stepextractverticalcloudpart.h"
#include "step/tk_stepextractzslice.h"
#include "step/tk_stepextractzslicefromattributes.h"
#include "step/tk_stepfacecolorrandom.h"
#include "step/tk_stepfacenormalestimator.h"
#include "step/tk_stepfilterpointsbyattribute.h"
#include "step/tk_stepmergeclouds.h"
#include "step/tk_steprotatecloud.h"
#include "step/tk_stepscalecloud.h"
#include "step/tk_translatecloud.h"

#include "step/tk_stepextractplot.h"
#include "step/tk_stepextractplotbasedondtm.h"
//#include "step/tk_stepextractpointsinverticalcylinders.h"
#include "step/tk_stepreducepointsdensity.h"
#include "step/tk_stepslicepointcloud.h"
#include "step/tk_steptransformpointcloud.h"

#ifdef USE_PCL
//#include "step/tk_stepnormalestimator.h"
#endif

TK_StepPluginManager::TK_StepPluginManager() : CT_AbstractStepPlugin()
{
}

TK_StepPluginManager::~TK_StepPluginManager()
{
}

QString TK_StepPluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin ToolKit for Computree\n"
           "AU  - Ravaglia, Joris\n"
           "AU  - Krebs, Michael\n"
           "AU  - Piboule, Alexandre\n"
           "PB  - Office National des Forêts, RDI Department\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-toolkit/wiki\n"
           "ER  - \n";
}


bool TK_StepPluginManager::loadGenericsStep()
{    
    addNewBetaStep<TK_StepExtractZSliceFromAttributes>(CT_StepsMenu::LP_Extract);
    addNewOtherStep<TK_StepFaceColorRandom>(QObject::tr("Générer (test)"));
    addNewMeshesStep<TK_StepFaceNormalEstimator>(CT_StepsMenu::LP_Create);
    addNewOtherStep<TK_StepEstimateNormalsRandom>(QObject::tr("Générer (test)"));
    addNewPointsStep<TK_StepCenterCloud>(CT_StepsMenu::LP_Transform);
    addNewPointsStep<TK_StepColorByAxis>(CT_StepsMenu::LP_Colorize);
    addNewOtherStep<TK_StepColorRandom>(QObject::tr("Générer (test)"));
    addNewPointsStep<TK_StepExtractBBoxPart>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractBox>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractCylinder>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractSphere>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractVerticalCloudPart>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepExtractZSlice>(CT_StepsMenu::LP_Extract);
    addNewPointsStep<TK_StepFilterPointsByAttribute>(CT_StepsMenu::LP_Filter);
    addNewPointsStep<TK_StepMergeClouds>(CT_StepsMenu::LP_Create);
    addNewPointsStep<TK_StepRotateCloud>(CT_StepsMenu::LP_Transform);
    addNewPointsStep<TK_StepScaleCloud>(CT_StepsMenu::LP_Transform);
    addNewPointsStep<TK_TranslateCloud>(CT_StepsMenu::LP_Transform);

        addNewPointsStep<TK_StepExtractPlot>(CT_StepsMenu::LP_Extract);
        addNewPointsStep<TK_StepExtractPlotBasedOnDTM>(CT_StepsMenu::LP_Extract);
//        addNewPointsStep<TK_StepExtractPointsInVerticalCylinders>(CT_StepsMenu::LP_Extract);
        addNewPointsStep<TK_StepReducePointsDensity>(CT_StepsMenu::LP_Filter);
        addNewPointsStep<TK_StepSlicePointCloud>(CT_StepsMenu::LP_Extract);
        addNewPointsStep<TK_StepTransformPointCloud>(CT_StepsMenu::LP_Transform);

#ifdef USE_PCL
//    addNewPointsStep<TK_StepNormalEstimator>(QObject::tr("Normales"));
#endif

    return true;
}

bool TK_StepPluginManager::loadOpenFileStep()
{
    return true;
}

bool TK_StepPluginManager::loadCanBeAddedFirstStep()
{
    return true;
}

bool TK_StepPluginManager::loadActions()
{
    return true;
}

bool TK_StepPluginManager::loadExporters()
{
    return true;
}

bool TK_StepPluginManager::loadReaders()
{
    return true;
}

