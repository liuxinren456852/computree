#include "tk_stepmergeclouds.h"

#include "ct_log/ct_logmanager.h"

TK_StepMergeClouds::TK_StepMergeClouds() : SuperClass()
{
}

QString TK_StepMergeClouds::description() const
{
    return tr("Fusionner des nuages de points");
}

QString TK_StepMergeClouds::detailledDescription() const
{
    return tr("Cette étape permet de fusionner plusieurs nuages de points");
}

CT_VirtualAbstractStep* TK_StepMergeClouds::createNewInstance() const
{
    // cree une copie de cette etape
    return new TK_StepMergeClouds();
}

//////////////////// PROTECTED //////////////////

void TK_StepMergeClouds::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scènes à fusionner"));
}

// Création et affiliation des modèles OUT
void TK_StepMergeClouds::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResult(_outResult);
    manager.setRootGroup(_outResult, _outRootGroup);
    manager.addItem(_outRootGroup, _outScene, tr("Scène fusionnée"));
}

void TK_StepMergeClouds::compute()
{
    size_t n_points = 0;
    size_t i = 0;

    for (const CT_AbstractItemDrawableWithPointCloud* inScene : _inScene.iterateInputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
        n_points += pointCloudIndex->size();

    }

    CT_PointCloudIndexVector *resPointCloudIndex = new CT_PointCloudIndexVector();
    resPointCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);

    for (const CT_AbstractItemDrawableWithPointCloud* inScene : _inScene.iterateInputs(_inResult))
    {

        if (isStopped()) {delete resPointCloudIndex; return;}

        const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();

        // Extraction des points de la placette
        CT_PointIterator itP(pointCloudIndex);
        while(itP.hasNext())
        {
            if (isStopped()) {delete resPointCloudIndex; return;}

            size_t index = itP.next().currentGlobalIndex();

            resPointCloudIndex->addIndex(index);

            // progres de 0 à 100
            if (i % 10000 == 0) {setProgress(float(90.0*i/n_points));}
            ++i;

        }
    }

    if (resPointCloudIndex->size() > 0)
    {
        resPointCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);

        for (CT_ResultGroup* outRes : _outResult.iterateOutputs())
        {
            // creation du groupe
            CT_StandardItemGroup *outGroup = new CT_StandardItemGroup();
            outRes->addRootGroup(_outRootGroup, outGroup);

            // creation et ajout de la scene
            CT_Scene *outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(resPointCloudIndex));
            outScene->updateBoundingBox();
            outGroup->addSingularItem(_outScene, outScene);

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène fusionnée comporte %1 points.")).arg(resPointCloudIndex->size()));
        }
    } else {
        delete resPointCloudIndex;
        PS_LOG->addMessage(LogInterface::warning, LogInterface::step, tr("Aucun point à fusionner"));
    }


}
