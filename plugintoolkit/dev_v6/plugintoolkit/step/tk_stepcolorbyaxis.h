#ifndef TK_STEPCOLORBYAXIS_H
#define TK_STEPCOLORBYAXIS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"

class TK_StepColorByAxis : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepColorByAxis();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    int _axis;

    CT_HandleInResultGroupCopy<>                                            _inResult;
    CT_HandleInStdZeroOrMoreGroup                                           _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                   _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>          _inScene;
    CT_HandleOutSingularItem<CT_PointsAttributesScalarTemplated<double> >   _outPointAttributes;

};

#endif // TK_STEPCOLORBYAXIS_H
