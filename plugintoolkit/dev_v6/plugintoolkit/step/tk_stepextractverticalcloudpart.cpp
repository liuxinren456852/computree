#include "tk_stepextractverticalcloudpart.h"

#define UPPER_MODE 0
#define LOWER_MODE 1
#define ABSOLUTE_TYPE 0
#define RELATIVE_TYPE 1

TK_StepExtractVerticalCloudPart::TK_StepExtractVerticalCloudPart() : SuperClass()
{
    _limitType = UPPER_MODE;
    _limitMethod = RELATIVE_TYPE;

    _absThickness = 5.0;
    _relThickness = 0.25;
}


QString TK_StepExtractVerticalCloudPart::description() const
{
    return tr("Extraire les points dans une tranche haute/basse");
}


CT_VirtualAbstractStep* TK_StepExtractVerticalCloudPart::createNewInstance() const
{
    return new TK_StepExtractVerticalCloudPart();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepExtractVerticalCloudPart::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène source"));
}


void TK_StepExtractVerticalCloudPart::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}


void TK_StepExtractVerticalCloudPart::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    CT_ButtonGroup &bgSliceType = postInputConfigDialog->addButtonGroup(_limitType);
    postInputConfigDialog->addExcludeValue(tr("Tranche haute"), "", "", bgSliceType, UPPER_MODE);
    postInputConfigDialog->addExcludeValue(tr("Tranche basse"), "", "", bgSliceType, LOWER_MODE);

    postInputConfigDialog->addEmpty();

    CT_ButtonGroup &bgMethod = postInputConfigDialog->addButtonGroup(_limitMethod);
    postInputConfigDialog->addExcludeValue(tr("Limite absolue"), "", "", bgMethod, ABSOLUTE_TYPE);
    postInputConfigDialog->addDouble      (tr("    Seuil en valeur absolue"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _absThickness);

    postInputConfigDialog->addEmpty();

    postInputConfigDialog->addExcludeValue(tr("Limite relative"), "", "", bgMethod, RELATIVE_TYPE);
    postInputConfigDialog->addDouble      (tr("    Seuil en valeur relative"), "%", 0, 100, 2, _relThickness, 100);

}

void TK_StepExtractVerticalCloudPart::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();
            extractedCloud->setSortType(CT_AbstractCloudIndex::NotSorted);

            // Cloud bounding box
            Eigen::Vector3d min;
            Eigen::Vector3d max;
            inScene->boundingBox(min, max);

            double thickness = _absThickness;
            if (_limitMethod == RELATIVE_TYPE)
            {
                thickness = (max(2) - min(2))*_relThickness;
            }

            double zmin = -std::numeric_limits<double>::max();
            double zmax = std::numeric_limits<double>::max();

            if (_limitType == UPPER_MODE)
            {
                zmin = max(2) - thickness;
            } else {
                zmax = min(2) + thickness;
            }

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext())
            {
                if (isStopped()) {delete extractedCloud; return;}

                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if (point(2) <= zmax && point(2) >= zmin)
                {
                    extractedCloud->addIndex(index);
                }

                setProgress(float(100.0*i++ /nbPoints));
                }

            if (extractedCloud->size() > 0)
            {
                extractedCloud->setSortType(CT_AbstractCloudIndex::SortedInAscendingOrder);
                CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                group->addSingularItem(_outScene, outScene);
            }
        }
    }
}
