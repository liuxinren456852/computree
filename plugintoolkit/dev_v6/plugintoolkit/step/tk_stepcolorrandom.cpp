#include "tk_stepcolorrandom.h"

#include <time.h>

TK_StepColorRandom::TK_StepColorRandom() : SuperClass()
{
}


QString TK_StepColorRandom::description() const
{
    return tr("Coloriser points aléatoirement");
}


CT_VirtualAbstractStep* TK_StepColorRandom::createNewInstance() const
{
    return new TK_StepColorRandom();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepColorRandom::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène coloriser"));

}


void TK_StepColorRandom::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outAttributeColor, tr("Couleurs aléatoires"));
}

void TK_StepColorRandom::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            size_t nbPoints = inScene->pointCloudIndex()->size();

            // On declare un nuage de couleur que l'on va remplir aleatoirement
            CT_ColorCloudStdVector *colorCloud = new CT_ColorCloudStdVector(nbPoints);

            srand(uint(time(nullptr)));
            for ( size_t i = 0 ; i < nbPoints ; i++ )
            {
                if (isStopped()) {delete colorCloud; return;}

                CT_Color &currentColor = colorCloud->colorAt(i);

                currentColor.r() = uchar(rand() % 256);
                currentColor.g() = uchar(rand() % 256);
                currentColor.b() = uchar(rand() % 256);

                setProgress(float(100.0*i /nbPoints));
            }

            // On cree les attributs que l'on met dans un groupe
            CT_PointsAttributesColor* colorAttribute = new CT_PointsAttributesColor(inScene->pointCloudIndexRegistered(), colorCloud);
            group->addSingularItem(_outAttributeColor, colorAttribute);
        }
    }
}
