#include "tk_stepextractbboxpart.h"

TK_StepExtractBBoxPart::TK_StepExtractBBoxPart() : SuperClass()
{
    _botX = 1.0;
    _botY = 1.0;
    _botZ = 1.0;
    _topX = 1.0;
    _topY = 1.0;
    _topZ = 1.0;
}


QString TK_StepExtractBBoxPart::description() const
{
    return tr("Extraire les points d'une partie de la boite englobante");
}


CT_VirtualAbstractStep* TK_StepExtractBBoxPart::createNewInstance() const
{
    return new TK_StepExtractBBoxPart();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepExtractBBoxPart::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène source"));
}


void TK_StepExtractBBoxPart::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}


void TK_StepExtractBBoxPart::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Réduire depuis en X- de"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botX);
    postInputConfigDialog->addDouble(tr("Réduire depuis en Y- de"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botY);
    postInputConfigDialog->addDouble(tr("Réduire depuis en Z- de"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botZ);
    postInputConfigDialog->addDouble(tr("Réduire depuis en X+ de"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topX);
    postInputConfigDialog->addDouble(tr("Réduire depuis en Y+ de"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topY);
    postInputConfigDialog->addDouble(tr("Réduire depuis en Z+ de"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topZ);
}

void TK_StepExtractBBoxPart::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();

            // Cloud bounding box
            Eigen::Vector3d min;
            Eigen::Vector3d max;
            inScene->boundingBox(min, max);

            max(0) -=  _topX;
            max(1) -=  _topY;
            max(2) -=  _topZ;

            min(0) += _botX;
            min(1) += _botY;
            min(2) += _botZ;

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext())
            {
                if (isStopped()) {delete extractedCloud; return;}

                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if ( point(0) <= max(0) &&
                     point(1) <= max(1) &&
                     point(2) <= max(2) &&
                     point(0) >= min(0) &&
                     point(1) >= min(1) &&
                     point(2) >= min(2) )
                {
                    extractedCloud->addIndex(index);
                }

                setProgress(float(100.0*i++ /nbPoints));
                }

            if (extractedCloud->size() > 0)
            {
                CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                group->addSingularItem(_outScene, outScene);
            }
        }
    }
}
