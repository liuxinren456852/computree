#ifndef TK_STEPREDUCEPOINTSDENSITY_H
#define TK_STEPREDUCEPOINTSDENSITY_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d.h"

class TK_StepReducePointsDensity : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepReducePointsDensity();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double _resolution;                            /*!< sample grid resolution*/
    double _halfResolution;                            /*!< sample grid resolution*/
    double _minx;
    double _miny;
    double _minz;
    size_t _dimx;
    size_t _dimy;
    size_t _dimz;

    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;

    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;

    size_t gridIndex(const double &x, const double &y, const double &z, size_t &colx, size_t &liny, size_t &levz) const;
    void cellCoordinates(const size_t &colx, const size_t &liny, const size_t &levz, double &x, double &y, double &z) const;

};

#endif // TK_STEPREDUCEPOINTSDENSITY_H
