#include "tk_stepcolorrandom.h"

#include <time.h>

// Utilise le depot
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"

#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_colorcloud/ct_colorcloudstdvector.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "inputResult"
#define DEF_groupIn_inputScene "inputGroup"
#define DEF_itemIn_scene "inputScene"

// Alias for indexing out models
#define DEF_itemOut_color "colorItem"
#define DEF_groupOut_color "colorGroup"
#define DEF_resultOut_color "colorResult"

// Constructor : initialization of parameters
TK_StepColorRandom::TK_StepColorRandom(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString TK_StepColorRandom::getStepDescription() const
{
    return tr("Coloriser points aléatoirement");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepColorRandom::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepColorRandom(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepColorRandom::createInResultModelListProtected()
{
    CT_InResultModelGroup *resultInModel = createNewInResultModel(DEF_resultIn_inputResult, tr("Scene"), "", false);

    resultInModel->setRootGroup(DEF_groupIn_inputScene,
                                tr("Group"),
                                "",
                                CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);

    resultInModel->addItemModel(DEF_groupIn_inputScene,
                                DEF_itemIn_scene,
                                CT_Scene::staticGetType(),
                                tr("Scene"));
}

// Creation and affiliation of OUT models
void TK_StepColorRandom::createOutResultModelListProtected()
{
    // ****************************************************
    // On sort un resultats correspondant a l'attribut de hauteur que l'on va pouvoir colorier :
    //  - un groupe d'item (groupe de hauteur)
    //      - un item (attribut hauteurs)
    // ****************************************************

    CT_OutResultModelGroup *resultOutModel = createNewOutResultModel(DEF_resultOut_color, tr("Colored point cloud"));

    resultOutModel->setRootGroup(DEF_groupOut_color,
                                 new CT_StandardItemGroup(),
                                 tr("Group"));

    resultOutModel->addItemModel(DEF_groupOut_color,
                                 DEF_itemOut_color,
                                 new CT_PointsAttributesColor(),
                                 tr("Random Color"));
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepColorRandom::createPostConfigurationDialog()
{
    // No dialog box
}

void TK_StepColorRandom::compute()
{
    CT_ResultGroup* resultIn_inputResult = getInputResultsForModel(DEF_resultIn_inputResult).first();

    CT_ResultGroup* resultOut_color = getOutResultList().at(0);

    CT_ResultGroupIterator it(resultIn_inputResult, this, DEF_groupIn_inputScene);

    if(!isStopped()
          && it.hasNext())
    {
        const CT_AbstractItemGroup *group = it.next();
        const CT_Scene* itemIn_scene = (const CT_Scene*)group->firstItemByINModelName(this, DEF_itemIn_scene);

        if(itemIn_scene != NULL)
        {
            size_t nbPoints = itemIn_scene->getPointCloudIndex()->size();

            // On declare un nuage de couleur que l'on va remplir aleatoirement
            CT_ColorCloudStdVector *colorCloud = new CT_ColorCloudStdVector(nbPoints);

            srand( time(NULL) );
            for ( size_t i = 0 ; i < nbPoints && !isStopped() ; i++ )
            {
                CT_Color &currentColor = colorCloud->colorAt(i);

                currentColor.r() = rand() % 256;
                currentColor.g() = rand() % 256;
                currentColor.b() = rand() % 256;

                setProgress( 100.0*i /nbPoints );
            }

            // On cree les attributs que l'on met dans un groupe
            CT_StandardItemGroup*     groupOut_color = new CT_StandardItemGroup(DEF_groupOut_color, resultOut_color);
            CT_PointsAttributesColor* colorAttribute = new CT_PointsAttributesColor(DEF_itemOut_color, resultOut_color, itemIn_scene->getPointCloudIndexRegistered(), colorCloud);

            groupOut_color->addItemDrawable( colorAttribute );
            resultOut_color->addGroup(groupOut_color);
        }
    }
}
