#include "tk_stepcolorbyaxis.h"

// Utilise le depot
#include "ct_global/ct_context.h"

// Utilise les attributs optionnels
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_iterator/ct_pointiterator.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "inputResult"
#define DEF_groupIn_inputScene "inputGroup"
#define DEF_itemIn_scene "inputScene"

// Alias for indexing out models
#define DEF_itemOut_attribute "attributeItem"
#define DEF_groupOut_attribute "attributeGroup"
#define DEF_resultOut_attribute "attributeResult"

#define OX 0
#define OY 1
#define OZ 2

// Constructor : initialization of parameters
TK_StepColorByAxis::TK_StepColorByAxis(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _axis = OZ;
}

// Step description (tooltip of contextual menu)
QString TK_StepColorByAxis::getStepDescription() const
{
    return tr("Coloriser points selon X/Y/Z");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepColorByAxis::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepColorByAxis(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepColorByAxis::createInResultModelListProtected()
{
    CT_InResultModelGroup *resultModel = createNewInResultModel(DEF_resultIn_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_inputScene, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_groupIn_inputScene, DEF_itemIn_scene, CT_Scene::staticGetType(), tr("Scene(s)"));
}

// Creation and affiliation of OUT models
void TK_StepColorByAxis::createOutResultModelListProtected()
{
    // ****************************************************
    // On sort un resultats correspondant a l'attribut de hauteur que l'on va pouvoir colorier :
    //  - un groupe d'item (groupe de hauteur)
    //      - un item (attribut hauteurs)
    // ****************************************************

    QString chosenAxis;
    switch ( _axis )
    {
    case OX :
    {
        chosenAxis = "Ox";
        break;
    }

    case OY :
    {
        chosenAxis = "Oy";
        break;
    }

    case OZ :
    {
        chosenAxis = "Oz";
        break;
    }

    default :
    {
        break;
    }
    }
    QString itemName = tr("Colored with axis ") + chosenAxis;

    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_attribute, tr("Colored point cloud"));

    resultModel->setRootGroup(DEF_groupOut_attribute, new CT_StandardItemGroup(), tr("Group"));
    resultModel->addItemModel(DEF_groupOut_attribute, DEF_itemOut_attribute,
                              new CT_PointsAttributesScalarTemplated<double>(),
                              itemName,
                              tr("Grey level"));
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepColorByAxis::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText("Colour with axis","","");

    CT_ButtonGroup &axisChoice = configDialog->addButtonGroup( _axis );
    configDialog->addExcludeValue("", "", tr("Axe X"), axisChoice, OX);
    configDialog->addExcludeValue("", "", tr("Axe Y"), axisChoice, OY);
    configDialog->addExcludeValue("", "", tr("Axe Z"), axisChoice, OZ);
}

void TK_StepColorByAxis::compute()
{
    CT_ResultGroup* resultIn_inputResult = getInputResults().first();
    CT_ResultGroup* resultOut_attribute = getOutResultList().first();


    CT_ResultItemIterator it(resultIn_inputResult, this, DEF_itemIn_scene);
    if (it.hasNext())
    {
        // On recupere le nuage de points de la scene en entree et son nombre de points
        const CT_Scene* itemIn_scene = (const CT_Scene*)it.next();
        const CT_AbstractPointCloudIndex *cloudIndex = itemIn_scene->getPointCloudIndex();
        CT_PointIterator itP(cloudIndex);

        // On declare un tableau d'attributs double que l'on va remplir avec les coordonnées correspondant a l'axe demandé
        CT_StandardCloudStdVectorT<double> *attribute = new CT_StandardCloudStdVectorT<double>();

        size_t i = 0;
        // On applique la translation a tous les points du nuage
        while (itP.hasNext()&& !isStopped())
        {
            attribute->addT( itP.next().currentPoint()[_axis] );

            setProgress( 100.0*i++ /cloudIndex->size() );
            waitForAckIfInDebugMode();
        }

        // On recupere le min et max de l'attribut en fonction de l'axe choisi
        double minAttribute, maxAttribute;
        switch ( _axis )
        {
        case OX :
        {
            minAttribute = itemIn_scene->minX();
            maxAttribute = itemIn_scene->maxX();
            break;
        }

        case OY :
        {
            minAttribute = itemIn_scene->minY();
            maxAttribute = itemIn_scene->maxY();
            break;
        }

        case OZ :
        {
            minAttribute = itemIn_scene->minZ();
            maxAttribute = itemIn_scene->maxZ();
            break;
        }

        default :
        {
            break;
        }
        }

        // On cree les attributs que l'on met dans un groupe
        CT_StandardItemGroup*                       groupOut_attribute = new CT_StandardItemGroup(DEF_groupOut_attribute, resultOut_attribute);
        CT_PointsAttributesScalarTemplated<double>*  itemOut_attribute  = new CT_PointsAttributesScalarTemplated<double>(DEF_itemOut_attribute,
                                                                                                                       resultOut_attribute,
                                                                                                                       itemIn_scene->getPointCloudIndexRegistered(),
                                                                                                                       attribute,
                                                                                                                       minAttribute,
                                                                                                                       maxAttribute);

        groupOut_attribute->addItemDrawable( itemOut_attribute );
        resultOut_attribute->addGroup(groupOut_attribute);
    }

}
