<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>QObject</name>
    <message>
        <location filename="../tk_steppluginmanager.cpp" line="86"/>
        <source>Générer (test)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tk_steppluginmanager.cpp" line="90"/>
        <source>Normales</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepCenterCloud</name>
    <message>
        <source>Recentrage d&apos;un nuage de points</source>
        <translation type="vanished">Recentrage d&apos;un nuage de points</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="49"/>
        <source>Recentrer nuage de points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="63"/>
        <location filename="../step/tk_stepcentercloud.cpp" line="67"/>
        <source>Scene(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="66"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="73"/>
        <source>Centered Point Cloud</source>
        <translation>Nuage de points centré</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="76"/>
        <source>Centered Scene</source>
        <translation>Scène centrée</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="77"/>
        <source>Reverse transformation matrix</source>
        <translation>Matrice de transformation inverse</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="88"/>
        <source>Center of bounding box</source>
        <translation>Centre le la boite englobante</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="89"/>
        <source>Center of bounding box with minimum height</source>
        <translation>Centre de la boite englobante, avec Z = Zmin</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="90"/>
        <source>Centroid</source>
        <translation>Barycentre des points</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="91"/>
        <source>Centroid with minimum height</source>
        <translation>Barycentre des points, avec Z = Zmin</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="92"/>
        <source>Keep as it is</source>
        <translation>Pas de transformation</translation>
    </message>
</context>
<context>
    <name>TK_StepColorByAxis</name>
    <message>
        <source>Colorisation d&apos;un nuage de points // axe</source>
        <translation type="vanished">Colorisation d&apos;un nuage de points // axe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="43"/>
        <source>Coloriser points selon X/Y/Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="57"/>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="61"/>
        <source>Scene(s)</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="60"/>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="103"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="99"/>
        <source>Colored with axis </source>
        <translation>Colorisation en fonction de l&apos;axe </translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="101"/>
        <source>Colored point cloud</source>
        <translation>Nuage de points colorisé</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="107"/>
        <source>Grey level</source>
        <translation>Niveau de gris</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="118"/>
        <source>Axe X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="119"/>
        <source>Axe Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="120"/>
        <source>Axe Z</source>
        <translation></translation>
    </message>
    <message>
        <source>Result containing group(s) of single floatting attribute</source>
        <translation type="vanished">Résultat contenant des groupes d&apos;attributs de type float</translation>
    </message>
    <message>
        <source>Group of single floatting attribute</source>
        <translation type="vanished">Groupe d&apos;attributs de type float</translation>
    </message>
</context>
<context>
    <name>TK_StepColorRandom</name>
    <message>
        <source>Colorisation d&apos;un nuage de points au hasard</source>
        <translation type="vanished">Colorisation d&apos;un nuage de points au hasard</translation>
    </message>
    <message>
        <source>modeleResultatInput</source>
        <translation type="vanished">Scène</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="vanished">Résultat</translation>
    </message>
    <message>
        <source>modeleGroupSceneInput</source>
        <translation type="vanished">Scène (Grp)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="39"/>
        <source>Coloriser points aléatoirement</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="56"/>
        <location filename="../step/tk_stepcolorrandom.cpp" line="79"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>modeleSceneInput</source>
        <translation type="vanished">Scène</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="53"/>
        <location filename="../step/tk_stepcolorrandom.cpp" line="63"/>
        <source>Scene</source>
        <translation>Scène</translation>
    </message>
    <message>
        <source>result from TK_StepCoulorRandom</source>
        <translation type="vanished">Résulat de TK_StepCoulorRandom</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="75"/>
        <source>Colored point cloud</source>
        <translation>Nuage de points colorisé</translation>
    </message>
    <message>
        <source>Result containing group(s) of color attribute</source>
        <translation type="vanished">Résultat contenant des groups avec des attributs couleur</translation>
    </message>
    <message>
        <source>Group of single color attribute</source>
        <translation type="vanished">Groupe contenant un attribut couleur</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="84"/>
        <source>Random Color</source>
        <translation>Couleur aléatoire</translation>
    </message>
    <message>
        <source>Optional color attribute</source>
        <translation type="vanished">Attribut couleur optionnel</translation>
    </message>
</context>
<context>
    <name>TK_StepEstimateNormalsRandom</name>
    <message>
        <source>Création de normales aléatoires</source>
        <translation type="vanished">Création de normales aléatoires</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="40"/>
        <source>Créer normales aléatoires</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="54"/>
        <source>Scene(s)</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="57"/>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="72"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="58"/>
        <source>Item(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="58"/>
        <source>Item(s) contenant un nuage de point</source>
        <translation></translation>
    </message>
    <message>
        <source>result from TK_StepEstimateNormalRandom</source>
        <translation type="vanished">Résultat de TK_StepEstimateNormalRandom</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="70"/>
        <source>Generated normals</source>
        <translation>Nomales générée</translation>
    </message>
    <message>
        <source>Result containing group(s) of normal attribute</source>
        <translation type="vanished">Résultat contenant des groupes avec des attributs normales</translation>
    </message>
    <message>
        <source>Group of single normal attribute</source>
        <translation type="vanished">Groupe contenant un attribut normales</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="73"/>
        <source>Random Normals</source>
        <translation>Normales aléatoires</translation>
    </message>
    <message>
        <source>Optional normal attribute</source>
        <translation type="vanished">Attribut normales optionnel</translation>
    </message>
</context>
<context>
    <name>TK_StepExtractBBoxPart</name>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="41"/>
        <source>Extraire les points d&apos;une partie de la boite englobante</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="55"/>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="59"/>
        <source>Scene(s)</source>
        <translation>Scènes(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="58"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="68"/>
        <source>Extracted Scene</source>
        <translation>Scène extraite</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="76"/>
        <source>Réduire depuis en X- de</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="77"/>
        <source>Réduire depuis en Y- de</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="78"/>
        <source>Réduire depuis en Z- de</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="79"/>
        <source>Réduire depuis en X+ de</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="80"/>
        <source>Réduire depuis en Y+ de</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="81"/>
        <source>Réduire depuis en Z+ de</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepExtractBox</name>
    <message>
        <source>Extraction d&apos;un nuage de points // boite englobante</source>
        <translation type="vanished">Extraction d&apos;un nuage de points // boite englobante</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="41"/>
        <source>Extraire les points dans une boite englobante</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="55"/>
        <location filename="../step/tk_stepextractbox.cpp" line="59"/>
        <source>Scene(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="58"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="69"/>
        <source>Extracted Scene (box)</source>
        <translation>Scène extraite (box)</translation>
    </message>
    <message>
        <source>Extracted Sub Cloud</source>
        <translation type="vanished">Sous nuage de points extrait</translation>
    </message>
    <message>
        <source>Extracted Scene</source>
        <translation type="vanished">Scène extraite</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="78"/>
        <source>Coin en bas à gauche</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="82"/>
        <source>Coin en haut à droite</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepExtractCylinder</name>
    <message>
        <source>Extraction d&apos;un nuage de points // cylindre</source>
        <translation type="vanished">Extraction d&apos;un nuage de points // cylindre</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="41"/>
        <source>Extraire les points dans un cylindre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="55"/>
        <location filename="../step/tk_stepextractcylinder.cpp" line="59"/>
        <source>Scene(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="58"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Extracted Sub Cloud</source>
        <translation type="vanished">Sous nuage de points extrait</translation>
    </message>
    <message>
        <source>Extracted Scene</source>
        <translation type="vanished">Scène extaite</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="69"/>
        <source>Extracted Scene (cylinder)</source>
        <translation>Scènes extraite (cylindre)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="78"/>
        <source>Rayon du cylindre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="79"/>
        <source>Centre du cylindre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="82"/>
        <source>Hauteur (Z)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="83"/>
        <source>Z Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="84"/>
        <source>Z Maximum</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepExtractSphere</name>
    <message>
        <source>Extraction d&apos;un nuage de points // sphère</source>
        <translation type="vanished">Extraction d&apos;un nuage de points // sphère</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="40"/>
        <source>Extraire les points dans une sphère</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="54"/>
        <location filename="../step/tk_stepextractsphere.cpp" line="58"/>
        <source>Scene(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="57"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="68"/>
        <source>Extracted Scene (sphere)</source>
        <translation>Scène extraite (sphère)</translation>
    </message>
    <message>
        <source>Extracted Sub Cloud</source>
        <translation type="vanished">Sous nuage de points extrait</translation>
    </message>
    <message>
        <source>Extracted Scene</source>
        <translation type="vanished">Scène extraite</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="77"/>
        <source>Rayon de la sphère</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="78"/>
        <source>Centre de la sphère</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepExtractVerticalCloudPart</name>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="45"/>
        <source>Extraire les points dans une tranche haute/basse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="59"/>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="63"/>
        <source>Scene(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="62"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="72"/>
        <source>Extracted Scene</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="82"/>
        <source>Tranche haute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="83"/>
        <source>Tranche basse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="88"/>
        <source>Limite absolue</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="89"/>
        <source>    Seuil en valeur absolue</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="93"/>
        <source>Limite relative</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="94"/>
        <source>    Seuil en valeur relative</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepExtractZSlice</name>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="44"/>
        <source>Extraire les points dans une tranche horizontale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="60"/>
        <source>Z Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="61"/>
        <source>Z Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="67"/>
        <location filename="../step/tk_stepextractzslice.cpp" line="71"/>
        <source>Scene(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="70"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="80"/>
        <source>Extracted Scene</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepExtractZSliceFromAttributes</name>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="47"/>
        <source>Extraire les points dans une tranche horizontale (attributs)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="64"/>
        <source>Fixer Z Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="65"/>
        <source>Fixer Z Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="67"/>
        <source>N.B. : Les limites non fixées seront déterminées à partir d&apos;un attribut d&apos;item.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="78"/>
        <source>Z Minimum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="80"/>
        <source>N.B. : Z Minimum déterminé à partir d&apos;un attribut d&apos;item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="85"/>
        <source>Z Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="87"/>
        <source>N.B. : Z Maximum déterminé à partir d&apos;un attribut d&apos;item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="95"/>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="99"/>
        <source>Scene(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="98"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="103"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="106"/>
        <source>Zmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="110"/>
        <source>Zmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="121"/>
        <source>Extracted Scene</source>
        <translation>Scène extraite</translation>
    </message>
</context>
<context>
    <name>TK_StepFaceColorRandom</name>
    <message>
        <source>Colorisation de faces au hasard</source>
        <translation type="vanished">Colorisation de faces au hasard</translation>
    </message>
    <message>
        <source>Colorisaser les faces aléatoirement</source>
        <translation type="vanished">Colorizer les faces aléatoirement</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="38"/>
        <source>Coloriser les faces aléatoirement</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="52"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="56"/>
        <source>Mesh model</source>
        <translation>Modèle de Mesh</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="55"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>MeshModel</source>
        <translation type="vanished">Modèle de mesh</translation>
    </message>
    <message>
        <source>Colored mesh</source>
        <translation type="vanished">Mesh colorisé</translation>
    </message>
    <message>
        <source>Result containing group(s) of color attribute</source>
        <translation type="vanished">Résultat contenant des groups avec des attributs couleur</translation>
    </message>
    <message>
        <source>Group of single color attribute</source>
        <translation type="vanished">Groupe contenant un attribut couleur</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="74"/>
        <source>Random Color</source>
        <translation>Couleur aléatoire</translation>
    </message>
</context>
<context>
    <name>TK_StepFaceNormalEstimator</name>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="34"/>
        <source>Calcul des normales des faces</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="48"/>
        <source>Scene(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="51"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="52"/>
        <source>Item(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="52"/>
        <source>Item(s) contenant des faces</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="67"/>
        <source>Random Normals</source>
        <translation>Normales aléatoires</translation>
    </message>
</context>
<context>
    <name>TK_StepFilterPointsByAttribute</name>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="40"/>
        <source>Filter les points sur un attribut</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="54"/>
        <source>Scene(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="57"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="58"/>
        <source>Scene</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="59"/>
        <source>Attribute</source>
        <translation>Attribut</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="69"/>
        <source>Filtered Scene</source>
        <translation>Scène filtrée</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="78"/>
        <source>Valeur minimum (incluse)</source>
        <translation>Minimum value (included)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="79"/>
        <source>Valeur maximum (incluse)</source>
        <translation>Maximum value (included)</translation>
    </message>
</context>
<context>
    <name>TK_StepMergeClouds</name>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="61"/>
        <source>Fusionner des nuages de points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="66"/>
        <source>Cette étape permet de fusionner plusieurs nuages de points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="79"/>
        <source>Scène(s) à fusionner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="83"/>
        <source>Scène à fusionner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="89"/>
        <location filename="../step/tk_stepmergeclouds.cpp" line="92"/>
        <source>Scène fusionnée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="156"/>
        <source>La scène fusionnée comporte %1 points.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="159"/>
        <source>Aucun point à fusionner</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepNormalEstimator</name>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="35"/>
        <source>Estimation de normales dans un nuage de point</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="41"/>
        <source>No detailled description for this step</source>
        <translation>Pas de description détaillée pour cette étape</translation>
    </message>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="74"/>
        <source>Point cloud</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="86"/>
        <source>curvature</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="165"/>
        <source>Problème le nombre de normales estimées ne correspond pas au nombre de points du nuage d&apos;entrée</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepRotateCloud</name>
    <message>
        <source>Rotation d&apos;un nuage de points</source>
        <translation type="vanished">Rotation d&apos;un nuage de points</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="54"/>
        <source>Rotation des points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="68"/>
        <location filename="../step/tk_steprotatecloud.cpp" line="72"/>
        <source>Scene(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="71"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Rotated Sub Cloud</source>
        <translation type="vanished">Sous nuage de points tourné</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="82"/>
        <source>Rotated Scene</source>
        <translation>Scène tournée</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="91"/>
        <source>Angle de rotation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="92"/>
        <source>Axe de rotation (direction)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="96"/>
        <source>Point sur l&apos;axe</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepScaleCloud</name>
    <message>
        <source>Mise à l&apos;échelle d&apos;un nuage de points</source>
        <translation type="vanished">Mise à l&apos;échelle d&apos;un nuage de points</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="42"/>
        <source>Homothétie des points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="56"/>
        <location filename="../step/tk_stepscalecloud.cpp" line="60"/>
        <source>Scene(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="59"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Scaled Sub Cloud</source>
        <translation type="vanished">Nuge de points mis à l&apos;échelle</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="70"/>
        <source>Scaled Scene</source>
        <translation>Scène mise à l&apos;échelle</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="79"/>
        <source>Facteur multiplicatif X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="80"/>
        <source>Facteur multiplicatif Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="81"/>
        <source>Facteur multiplicatif Z</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepSplitScene</name>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="28"/>
        <source>Extraire des tranches horizontales</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="33"/>
        <source>To Do</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="46"/>
        <source>Scène(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="50"/>
        <source>Scène</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="59"/>
        <source>Groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="60"/>
        <source>Scène extraite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="68"/>
        <source>Epaisseur d&apos;une tranche :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="69"/>
        <source>Espacement des tranches :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="86"/>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepThresholdIntensity</name>
    <message>
        <source>Seuillage d&apos;un nuage de points // intensité</source>
        <translation type="vanished">Seuillage d&apos;un nuage de points // intensité</translation>
    </message>
    <message>
        <source>Scene(s)</source>
        <translation type="vanished">Scène(s)</translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="vanished">Groupe</translation>
    </message>
    <message>
        <source>Scene</source>
        <translation type="vanished">Scène</translation>
    </message>
    <message>
        <source>Intensities</source>
        <translation type="vanished">Intensités</translation>
    </message>
    <message>
        <source>Filtered Scene (by intensity)</source>
        <translation type="vanished">Scène filtrée (par intensité)</translation>
    </message>
    <message>
        <source>Scene&apos;s Intensity</source>
        <translation type="vanished">Intensité de la scène</translation>
    </message>
    <message>
        <source>Intensity minimum</source>
        <translation type="vanished">Intensité minimum</translation>
    </message>
    <message>
        <source>Intensity maximum</source>
        <translation type="vanished">Intensité maximum</translation>
    </message>
    <message>
        <source>Scene&apos;s Intnesity</source>
        <translation type="vanished">Intensité de la scène</translation>
    </message>
    <message>
        <source>Filtered Sub Cloud</source>
        <translation type="vanished">Sous nuage de points filtré</translation>
    </message>
    <message>
        <source>Filtered Scene</source>
        <translation type="vanished">Scène filtrée</translation>
    </message>
</context>
<context>
    <name>TK_TranslateCloud</name>
    <message>
        <source>Translation d&apos;un nuage de points</source>
        <translation type="vanished">Translation d&apos;un nuage de points</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="42"/>
        <source>Translation des points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="56"/>
        <location filename="../step/tk_translatecloud.cpp" line="60"/>
        <source>Scene(s)</source>
        <translation>Scène(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="59"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Translated Sub Cloud</source>
        <translation type="vanished">Sous nuage de points translaté</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="70"/>
        <source>Translated Scene</source>
        <translation>Scène translatée</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="79"/>
        <source>Translation selon X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="80"/>
        <source>Translation selon Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="81"/>
        <source>Translation selon Z</source>
        <translation></translation>
    </message>
</context>
</TS>
