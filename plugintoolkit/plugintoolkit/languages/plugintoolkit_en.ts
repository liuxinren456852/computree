<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="../tk_steppluginmanager.cpp" line="86"/>
        <source>Générer (test)</source>
        <translation>Generate (test)</translation>
    </message>
    <message>
        <location filename="../tk_steppluginmanager.cpp" line="90"/>
        <source>Normales</source>
        <translation>Normals</translation>
    </message>
</context>
<context>
    <name>TK_StepCenterCloud</name>
    <message>
        <source>Recentrage d&apos;un nuage de points</source>
        <translation type="vanished">Point cloud centering</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="49"/>
        <source>Recentrer nuage de points</source>
        <translation>Center point clouds</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="63"/>
        <location filename="../step/tk_stepcentercloud.cpp" line="67"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="66"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="73"/>
        <source>Centered Point Cloud</source>
        <translation>Centered Point Cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="76"/>
        <source>Centered Scene</source>
        <translation>Centered Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="77"/>
        <source>Reverse transformation matrix</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="88"/>
        <source>Center of bounding box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="89"/>
        <source>Center of bounding box with minimum height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="90"/>
        <source>Centroid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="91"/>
        <source>Centroid with minimum height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepcentercloud.cpp" line="92"/>
        <source>Keep as it is</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepColorByAxis</name>
    <message>
        <source>Colorisation d&apos;un nuage de points // axe</source>
        <translation type="vanished">Colorization of point cloud // axis</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="43"/>
        <source>Coloriser points selon X/Y/Z</source>
        <translation>Colorize points from X/Y/Z</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="57"/>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="61"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="60"/>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="103"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="99"/>
        <source>Colored with axis </source>
        <translation>Colored with axis </translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="101"/>
        <source>Colored point cloud</source>
        <translation>Colored point cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="107"/>
        <source>Grey level</source>
        <translation>Grey level</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="118"/>
        <source>Axe X</source>
        <translation>X axis</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="119"/>
        <source>Axe Y</source>
        <translation>Y axis</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorbyaxis.cpp" line="120"/>
        <source>Axe Z</source>
        <translation>Z axis</translation>
    </message>
    <message>
        <source>Result containing group(s) of single floatting attribute</source>
        <translation type="vanished">Result containing group(s) of single floatting attribute</translation>
    </message>
    <message>
        <source>Group of single floatting attribute</source>
        <translation type="vanished">Group of single floatting attribute</translation>
    </message>
</context>
<context>
    <name>TK_StepColorRandom</name>
    <message>
        <source>Colorisation d&apos;un nuage de points au hasard</source>
        <translation type="vanished">Point could random colorization</translation>
    </message>
    <message>
        <source>modeleResultatInput</source>
        <translation type="vanished">Scene</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="vanished">Result</translation>
    </message>
    <message>
        <source>modeleGroupSceneInput</source>
        <translation type="vanished">Scene (Grp)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="39"/>
        <source>Coloriser points aléatoirement</source>
        <translation>Colorize points at random</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="56"/>
        <location filename="../step/tk_stepcolorrandom.cpp" line="79"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>modeleSceneInput</source>
        <translation type="vanished">Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="53"/>
        <location filename="../step/tk_stepcolorrandom.cpp" line="63"/>
        <source>Scene</source>
        <translation>Scene</translation>
    </message>
    <message>
        <source>result from TK_StepCoulorRandom</source>
        <translation type="vanished">Result from TK_StepCoulorRandom</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="75"/>
        <source>Colored point cloud</source>
        <translation>Colored point cloud</translation>
    </message>
    <message>
        <source>Result containing group(s) of color attribute</source>
        <translation type="vanished">Result containing group(s) of color attribute</translation>
    </message>
    <message>
        <source>Group of single color attribute</source>
        <translation type="vanished">Group of single color attribute</translation>
    </message>
    <message>
        <location filename="../step/tk_stepcolorrandom.cpp" line="84"/>
        <source>Random Color</source>
        <translation>Random Color</translation>
    </message>
    <message>
        <source>Optional color attribute</source>
        <translation type="vanished">Optional color attribute</translation>
    </message>
</context>
<context>
    <name>TK_StepEstimateNormalsRandom</name>
    <message>
        <source>Création de normales aléatoires</source>
        <translation type="vanished">Creation of random normals</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="40"/>
        <source>Créer normales aléatoires</source>
        <translation>Create random normals</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="54"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="57"/>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="72"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="58"/>
        <source>Item(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="58"/>
        <source>Item(s) contenant un nuage de point</source>
        <translation>Items(s) containing point cloud</translation>
    </message>
    <message>
        <source>result from TK_StepEstimateNormalRandom</source>
        <translation type="vanished">result from TK_StepEstimateNormalRandom</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="70"/>
        <source>Generated normals</source>
        <translation>Generated normals</translation>
    </message>
    <message>
        <source>Result containing group(s) of normal attribute</source>
        <translation type="vanished">Result containing group(s) of normal attribute</translation>
    </message>
    <message>
        <source>Group of single normal attribute</source>
        <translation type="vanished">Group of single normal attribute</translation>
    </message>
    <message>
        <location filename="../step/tk_stepestimatenormalsrandom.cpp" line="73"/>
        <source>Random Normals</source>
        <translation>Random Normals</translation>
    </message>
    <message>
        <source>Optional normal attribute</source>
        <translation type="vanished">Optional normal attribute</translation>
    </message>
</context>
<context>
    <name>TK_StepExtractBBoxPart</name>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="41"/>
        <source>Extraire les points d&apos;une partie de la boite englobante</source>
        <translation>Extract points in a part of the bounding box</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="55"/>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="59"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="58"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="68"/>
        <source>Extracted Scene</source>
        <translation>Extracted Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="76"/>
        <source>Réduire depuis en X- de</source>
        <translation>Reduce from X- by</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="77"/>
        <source>Réduire depuis en Y- de</source>
        <translation>Reduce from Y- by</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="78"/>
        <source>Réduire depuis en Z- de</source>
        <translation>Reduce from Z- by</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="79"/>
        <source>Réduire depuis en X+ de</source>
        <translation>Reduce from X+ by</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="80"/>
        <source>Réduire depuis en Y+ de</source>
        <translation>Reduce from Y+ by</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbboxpart.cpp" line="81"/>
        <source>Réduire depuis en Z+ de</source>
        <translation>Reduce from Z+ by</translation>
    </message>
</context>
<context>
    <name>TK_StepExtractBox</name>
    <message>
        <source>Extraction d&apos;un nuage de points // boite englobante</source>
        <translation type="vanished">Extraction of a point cloud // bounding box</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="41"/>
        <source>Extraire les points dans une boite englobante</source>
        <translation>Extract points in a specified bounding box</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="55"/>
        <location filename="../step/tk_stepextractbox.cpp" line="59"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="58"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="69"/>
        <source>Extracted Scene (box)</source>
        <translation></translation>
    </message>
    <message>
        <source>Extracted Sub Cloud</source>
        <translation type="vanished">Extracted Sub Cloud</translation>
    </message>
    <message>
        <source>Extracted Scene</source>
        <translation type="vanished">Extracted Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="78"/>
        <source>Coin en bas à gauche</source>
        <translation>Lower left corner</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractbox.cpp" line="82"/>
        <source>Coin en haut à droite</source>
        <translation>Upper right corner</translation>
    </message>
</context>
<context>
    <name>TK_StepExtractCylinder</name>
    <message>
        <source>Extraction d&apos;un nuage de points // cylindre</source>
        <translation type="vanished">Extraction of a point cloud // cylinder</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="41"/>
        <source>Extraire les points dans un cylindre</source>
        <translation>Extract points in a cylinder</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="55"/>
        <location filename="../step/tk_stepextractcylinder.cpp" line="59"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="58"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>Extracted Sub Cloud</source>
        <translation type="vanished">Extracted Sub Cloud</translation>
    </message>
    <message>
        <source>Extracted Scene</source>
        <translation type="vanished">Extracted Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="69"/>
        <source>Extracted Scene (cylinder)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="78"/>
        <source>Rayon du cylindre</source>
        <translation>Cylinder radius</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="79"/>
        <source>Centre du cylindre</source>
        <translation>Cylinder center</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="82"/>
        <source>Hauteur (Z)</source>
        <translation>Height (Z)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="83"/>
        <source>Z Minimum</source>
        <translation>Minimum Z</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractcylinder.cpp" line="84"/>
        <source>Z Maximum</source>
        <translation>Maximum Z</translation>
    </message>
</context>
<context>
    <name>TK_StepExtractSphere</name>
    <message>
        <source>Extraction d&apos;un nuage de points // sphère</source>
        <translation type="vanished">Extraction of a point cloud // sphere</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="40"/>
        <source>Extraire les points dans une sphère</source>
        <translation>Extract points in a sphere</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="54"/>
        <location filename="../step/tk_stepextractsphere.cpp" line="58"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="57"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="68"/>
        <source>Extracted Scene (sphere)</source>
        <translation></translation>
    </message>
    <message>
        <source>Extracted Sub Cloud</source>
        <translation type="vanished">Extracted Sub Cloud</translation>
    </message>
    <message>
        <source>Extracted Scene</source>
        <translation type="vanished">Extracted Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="77"/>
        <source>Rayon de la sphère</source>
        <translation>Sphere radius</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractsphere.cpp" line="78"/>
        <source>Centre de la sphère</source>
        <translation>Sphere center</translation>
    </message>
</context>
<context>
    <name>TK_StepExtractVerticalCloudPart</name>
    <message>
        <source>Extraire les points hauts/bas d&apos;une scène</source>
        <translation type="vanished">Extract upper/lower points in scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="45"/>
        <source>Extraire les points dans une tranche haute/basse</source>
        <translation>Extract points in upper / lower slice</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="59"/>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="63"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="62"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="72"/>
        <source>Extracted Scene</source>
        <translation>Extracted Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="82"/>
        <source>Tranche haute</source>
        <translation>Upper slice</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="83"/>
        <source>Tranche basse</source>
        <translation>Lower slice</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="88"/>
        <source>Limite absolue</source>
        <translation>Absolute limite</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="89"/>
        <source>    Seuil en valeur absolue</source>
        <translation>Absolute threshold value</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="93"/>
        <source>Limite relative</source>
        <translation>Relative limit</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractverticalcloudpart.cpp" line="94"/>
        <source>    Seuil en valeur relative</source>
        <translation>Relative threshold value</translation>
    </message>
    <message>
        <source>Limite basse de l&apos;extraction</source>
        <translation type="vanished">Lower limit for extraction</translation>
    </message>
    <message>
        <source>- Pas de limite basse</source>
        <translation type="vanished">- No lower limit</translation>
    </message>
    <message>
        <source>- Limite basse absolue</source>
        <translation type="vanished">- Absolute lower limit</translation>
    </message>
    <message>
        <source>          Seuil en valeur absolue</source>
        <translation type="vanished">Absolute threshold value</translation>
    </message>
    <message>
        <source>- Limite basse relative</source>
        <translation type="vanished">- Relative lower value</translation>
    </message>
    <message>
        <source>          Seuil en valeur relative</source>
        <translation type="vanished">Relative threshold value</translation>
    </message>
    <message>
        <source>Limite haute de l&apos;extraction</source>
        <translation type="vanished">Upper limit for extraction</translation>
    </message>
    <message>
        <source>- Pas de limite haute</source>
        <translation type="vanished">- No upper limit</translation>
    </message>
    <message>
        <source>- Limite haute absolue</source>
        <translation type="vanished">- Absolute upper limit</translation>
    </message>
    <message>
        <source>- Limite haute relative</source>
        <translation type="vanished">- Relative upper limit</translation>
    </message>
</context>
<context>
    <name>TK_StepExtractZSlice</name>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="44"/>
        <source>Extraire les points dans une tranche horizontale</source>
        <translation>Extract points in a horizontal slice</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="60"/>
        <source>Z Minimum</source>
        <translation>Minimum Z</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="61"/>
        <source>Z Maximum</source>
        <translation>Maximum Z</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="67"/>
        <location filename="../step/tk_stepextractzslice.cpp" line="71"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="70"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslice.cpp" line="80"/>
        <source>Extracted Scene</source>
        <translation>Extracted Scene</translation>
    </message>
</context>
<context>
    <name>TK_StepExtractZSliceFromAttributes</name>
    <message>
        <source>Extraire les points dans une tranche horizontale</source>
        <translation type="vanished">Extract points in a horizontal slice</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="47"/>
        <source>Extraire les points dans une tranche horizontale (attributs)</source>
        <translation>Extract points in a horizontal slice (attributs)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="64"/>
        <source>Fixer Z Minimum</source>
        <translation>Fix minimum Z value</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="65"/>
        <source>Fixer Z Maximum</source>
        <translation>Fix maximum Z value</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="67"/>
        <source>N.B. : Les limites non fixées seront déterminées à partir d&apos;un attribut d&apos;item.</source>
        <translation>N.B.: Not fixed limits will be taken from item attributes</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="78"/>
        <source>Z Minimum</source>
        <translation>Minimum Z value</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="80"/>
        <source>N.B. : Z Minimum déterminé à partir d&apos;un attribut d&apos;item</source>
        <translation>N.B.: Minimum Z value determined from an item attribute</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="85"/>
        <source>Z Maximum</source>
        <translation>Maximum Z</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="87"/>
        <source>N.B. : Z Maximum déterminé à partir d&apos;un attribut d&apos;item</source>
        <translation>N.B.: Maximum Z value determined from an item attribute</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="95"/>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="99"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="98"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="103"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="106"/>
        <source>Zmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="110"/>
        <source>Zmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepextractzslicefromattributes.cpp" line="121"/>
        <source>Extracted Scene</source>
        <translation>Extracted Scene</translation>
    </message>
</context>
<context>
    <name>TK_StepFaceColorRandom</name>
    <message>
        <source>Colorisation de faces au hasard</source>
        <translation type="vanished">Faces colorization at random</translation>
    </message>
    <message>
        <source>Colorisaser les faces aléatoirement</source>
        <translation type="vanished">Colorize faces randomly</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="38"/>
        <source>Coloriser les faces aléatoirement</source>
        <translation>Colorize faces randomly</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="52"/>
        <source>Result</source>
        <translation>Result</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="56"/>
        <source>Mesh model</source>
        <translation>Mesh model</translation>
    </message>
    <message>
        <source>modeleGroupMeshInput</source>
        <translation type="obsolete">modeleGroupMeshInput</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="55"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>MeshModel</source>
        <translation type="vanished">Mesh model</translation>
    </message>
    <message>
        <source>Colored mesh</source>
        <translation type="vanished">Colored mesh</translation>
    </message>
    <message>
        <source>Result containing group(s) of color attribute</source>
        <translation type="vanished">Result containing group(s) of color attribute</translation>
    </message>
    <message>
        <source>Group of single color attribute</source>
        <translation type="vanished">Group of single color attribute</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacecolorrandom.cpp" line="74"/>
        <source>Random Color</source>
        <translation>Random Color</translation>
    </message>
</context>
<context>
    <name>TK_StepFaceNormalEstimator</name>
    <message>
        <source>Calcul les normales des faces</source>
        <translation type="vanished">Computes faces normals</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="34"/>
        <source>Calcul des normales des faces</source>
        <translation>Computes faces normals</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="48"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="51"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="52"/>
        <source>Item(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="52"/>
        <source>Item(s) contenant des faces</source>
        <translation>Items(s) containing faces</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfacenormalestimator.cpp" line="67"/>
        <source>Random Normals</source>
        <translation>Random Normals</translation>
    </message>
</context>
<context>
    <name>TK_StepFilterPointsByAttribute</name>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="40"/>
        <source>Filter les points sur un attribut</source>
        <translation>Filter points on an attribute</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="54"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="57"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="58"/>
        <source>Scene</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="59"/>
        <source>Attribute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="69"/>
        <source>Filtered Scene</source>
        <translation>Filtered Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="78"/>
        <source>Valeur minimum (incluse)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepfilterpointsbyattribute.cpp" line="79"/>
        <source>Valeur maximum (incluse)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TK_StepMergeClouds</name>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="61"/>
        <source>Fusionner des nuages de points</source>
        <translation>Merge point clouds</translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="66"/>
        <source>Cette étape permet de fusionner plusieurs nuages de points</source>
        <translation>This step merges some point clouds</translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="79"/>
        <source>Scène(s) à fusionner</source>
        <translation>Scene(s) to merge</translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="83"/>
        <source>Scène à fusionner</source>
        <translation>Scene to merge</translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="89"/>
        <location filename="../step/tk_stepmergeclouds.cpp" line="92"/>
        <source>Scène fusionnée</source>
        <translation>Merged scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="156"/>
        <source>La scène fusionnée comporte %1 points.</source>
        <translation>The merged scene contains %1 points.</translation>
    </message>
    <message>
        <location filename="../step/tk_stepmergeclouds.cpp" line="159"/>
        <source>Aucun point à fusionner</source>
        <translation>No points to merge</translation>
    </message>
</context>
<context>
    <name>TK_StepNormalEstimator</name>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="35"/>
        <source>Estimation de normales dans un nuage de point</source>
        <translation>Estimation of normals in a point cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="41"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="74"/>
        <source>Point cloud</source>
        <translation>Nuage de points</translation>
    </message>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="86"/>
        <source>curvature</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepnormalestimator.cpp" line="165"/>
        <source>Problème le nombre de normales estimées ne correspond pas au nombre de points du nuage d&apos;entrée</source>
        <translation>Error: the number of estimated normals don&apos;t correspond to number of point in input cloud</translation>
    </message>
</context>
<context>
    <name>TK_StepRotateCloud</name>
    <message>
        <source>Rotation d&apos;un nuage de points</source>
        <translation type="vanished">Rotation of a point cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="54"/>
        <source>Rotation des points</source>
        <translation>Points rotation</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="68"/>
        <location filename="../step/tk_steprotatecloud.cpp" line="72"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="71"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>Rotated Sub Cloud</source>
        <translation type="vanished">Rotated Sub Cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="82"/>
        <source>Rotated Scene</source>
        <translation>Rotated Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="91"/>
        <source>Angle de rotation</source>
        <translation>Rotation angle</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="92"/>
        <source>Axe de rotation (direction)</source>
        <translation>Rotation axis (direction)</translation>
    </message>
    <message>
        <location filename="../step/tk_steprotatecloud.cpp" line="96"/>
        <source>Point sur l&apos;axe</source>
        <translation>Point on the axis</translation>
    </message>
</context>
<context>
    <name>TK_StepScaleCloud</name>
    <message>
        <source>Mise à l&apos;échelle d&apos;un nuage de points</source>
        <translation type="vanished">Scaling of a point cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="42"/>
        <source>Homothétie des points</source>
        <translation>Points scaling</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="56"/>
        <location filename="../step/tk_stepscalecloud.cpp" line="60"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="59"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>Scaled Sub Cloud</source>
        <translation type="vanished">Scaled Sub Cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="70"/>
        <source>Scaled Scene</source>
        <translation>Scaled Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="79"/>
        <source>Facteur multiplicatif X</source>
        <translation>X scaling factor</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="80"/>
        <source>Facteur multiplicatif Y</source>
        <translation>Y scaling factor</translation>
    </message>
    <message>
        <location filename="../step/tk_stepscalecloud.cpp" line="81"/>
        <source>Facteur multiplicatif Z</source>
        <translation>Z scaling factor</translation>
    </message>
    <message>
        <source>Créer de nouveaux points ?</source>
        <translation type="vanished">Create new points ?</translation>
    </message>
</context>
<context>
    <name>TK_StepSplitScene</name>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="28"/>
        <source>Extraire des tranches horizontales</source>
        <translation>Extract horizontal slices</translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="33"/>
        <source>To Do</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="46"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="50"/>
        <source>Scène</source>
        <translation>Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="59"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="60"/>
        <source>Scène extraite</source>
        <translation>Extracted scene</translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="68"/>
        <source>Epaisseur d&apos;une tranche :</source>
        <translation>Slice thickness:</translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="69"/>
        <source>Espacement des tranches :</source>
        <translation>Slices spacing:</translation>
    </message>
    <message>
        <location filename="../step/tk_stepsplitscene.cpp" line="86"/>
        <source>La scène d&apos;entrée comporte %1 points.</source>
        <translation>The input scene contains %1 points.</translation>
    </message>
</context>
<context>
    <name>TK_StepThresholdIntensity</name>
    <message>
        <source>Seuillage d&apos;un nuage de points // intensité</source>
        <translation type="vanished">Thresholding of a point cloud // intensity</translation>
    </message>
    <message>
        <source>Filter les points sur l&apos;intensité</source>
        <translation type="vanished">Filter points by intensity</translation>
    </message>
    <message>
        <source>Scene(s)</source>
        <translation type="vanished">Scene(s)</translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="vanished">Group</translation>
    </message>
    <message>
        <source>Scene</source>
        <translation type="vanished">Scene</translation>
    </message>
    <message>
        <source>Scene&apos;s Intensity</source>
        <translation type="vanished">Scene&apos;s Intensity</translation>
    </message>
    <message>
        <source>Intensity minimum</source>
        <translation type="vanished">Minimum intensity</translation>
    </message>
    <message>
        <source>Intensity maximum</source>
        <translation type="vanished">Maximum intensity</translation>
    </message>
    <message>
        <source>Scene&apos;s Intnesity</source>
        <translation type="vanished">Scene&apos;s Intensity</translation>
    </message>
    <message>
        <source>Filtered Sub Cloud</source>
        <translation type="vanished">Filtered Sub Cloud</translation>
    </message>
    <message>
        <source>Filtered Scene</source>
        <translation type="vanished">Filtered Scene</translation>
    </message>
</context>
<context>
    <name>TK_TranslateCloud</name>
    <message>
        <source>Translation d&apos;un nuage de points</source>
        <translation type="vanished">Translation of a point cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="42"/>
        <source>Translation des points</source>
        <translation>Points translation</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="56"/>
        <location filename="../step/tk_translatecloud.cpp" line="60"/>
        <source>Scene(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="59"/>
        <source>Group</source>
        <translation>Group</translation>
    </message>
    <message>
        <source>Translated Sub Cloud</source>
        <translation type="vanished">Translated Sub Cloud</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="70"/>
        <source>Translated Scene</source>
        <translation>Translated Scene</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="79"/>
        <source>Translation selon X</source>
        <translation>X offset</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="80"/>
        <source>Translation selon Y</source>
        <translation>Y offset</translation>
    </message>
    <message>
        <location filename="../step/tk_translatecloud.cpp" line="81"/>
        <source>Translation selon Z</source>
        <translation>Z offset</translation>
    </message>
</context>
</TS>
