#include "tk_stepextractbboxpart.h"

// Utilise le depot
#include "ct_global/ct_context.h"

#include <assert.h>
#include <limits>

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_iterator/ct_pointiterator.h"

// Alias for indexing in models
#define DEF_inputResult "inputResult"
#define DEF_inGrp "inputGroup"
#define DEF_inScene "inputScene"

// Constructor : initialization of parameters
TK_StepExtractBBoxPart::TK_StepExtractBBoxPart(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _botX = 1.0;
    _botY = 1.0;
    _botZ = 1.0;
    _topX = 1.0;
    _topY = 1.0;
    _topZ = 1.0;
}

// Step description (tooltip of contextual menu)
QString TK_StepExtractBBoxPart::getStepDescription() const
{
    return tr("Extraire les points d'une partie de la boite englobante");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepExtractBBoxPart::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepExtractBBoxPart(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepExtractBBoxPart::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_inputResult, tr("Scene(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_inGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultModel->addItemModel(DEF_inGrp, DEF_inScene, CT_Scene::staticGetType(), tr("Scene(s)"));
}

// Creation and affiliation of OUT models
void TK_StepExtractBBoxPart::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_inputResult);

    if(res != NULL)
        res->addItemModel(DEF_inGrp, _outScene_ModelName, new CT_Scene(), tr("Extracted Scene"));
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepExtractBBoxPart::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Réduire depuis en X- de"), "m", -1e+10, 1e+10, 4, _botX);
    configDialog->addDouble(tr("Réduire depuis en Y- de"), "m", -1e+10, 1e+10, 4, _botY);
    configDialog->addDouble(tr("Réduire depuis en Z- de"), "m", -1e+10, 1e+10, 4, _botZ);
    configDialog->addDouble(tr("Réduire depuis en X+ de"), "m", -1e+10, 1e+10, 4, _topX);
    configDialog->addDouble(tr("Réduire depuis en Y+ de"), "m", -1e+10, 1e+10, 4, _topY);
    configDialog->addDouble(tr("Réduire depuis en Z+ de"), "m", -1e+10, 1e+10, 4, _topZ);
}

void TK_StepExtractBBoxPart::compute()
{
    CT_ResultGroup* res = getOutResultList().first();

    CT_ResultGroupIterator it(res, this, DEF_inGrp);
    while (it.hasNext() && !isStopped())
    {        
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) it.next();
        const CT_Scene* scene = (const CT_Scene*)grp->firstItemByINModelName(this, DEF_inScene);

        if (scene != NULL)
        {
            const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();

            // Cloud bounding box
            Eigen::Vector3d min;
            Eigen::Vector3d max;
            scene->getBoundingBox(min, max);


            max(0) -=  _topX;
            max(1) -=  _topY;
            max(2) -=  _topZ;

            min(0) += _botX;
            min(1) += _botY;
            min(2) += _botZ;

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext() && !isStopped())
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if ( point(0) <= max(0) &&
                     point(1) <= max(1) &&
                     point(2) <= max(2) &&
                     point(0) >= min(0) &&
                     point(1) >= min(1) &&
                     point(2) >= min(2) )
                {
                    extractedCloud->addIndex(index);
                }

                setProgress( 100.0*i++ /nbPoints );
                }

            if (extractedCloud->size() > 0)
            {
                CT_Scene* outScene = new CT_Scene(_outScene_ModelName.completeName(), res, PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                grp->addItemDrawable(outScene);
            }
        }
    }
}
