#ifndef TK_STEPROTATECLOUD_H
#define TK_STEPROTATECLOUD_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

class TK_StepRotateCloud : public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    TK_StepRotateCloud(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private :

    // Step parameters
    double    _alpha;    /*!< Angle de la rotation */
    double    _axisX;    /*!< Direction de l'axe de rotation */
    double    _axisY;    /*!< Direction de l'axe de rotation */
    double    _axisZ;    /*!< Direction de l'axe de rotation */
    double    _x;    /*!< Coordonnees d'un point sur l'axe de rotation */
    double    _y;    /*!< Coordonnees d'un point sur l'axe de rotation */
    double    _z;    /*!< Coordonnees d'un point sur l'axe de rotation */


    CT_AutoRenameModels     _outSceneModelName;
};

#endif // TK_STEPROTATECLOUD_H
