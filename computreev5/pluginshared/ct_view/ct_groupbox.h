#ifndef CT_GROUPBOX_H
#define CT_GROUPBOX_H

#include "ct_view/ct_genericconfigurablewidget.h"
#include "ct_view/ct_widgetwithvaluereferenceinterface.h"

class QGroupBox;

class CT_GroupBox : public CT_GenericConfigurableWidget, public CT_WidgetWithValueReferenceInterface
{
    Q_OBJECT

public:
    CT_GroupBox(const QString& title, QWidget *parent = NULL);

    QString type() const { return metaObject()->className(); }

    /**
     * @brief Redefined because in a group box you cannot add a new group box !
     */
    CT_GroupBox* addNewGroupBox(const QString& title) { Q_UNUSED(title) return NULL; }

    /**
     * @brief Create the QGroupBox and return it
     */
    QWidget* createWidget(QWidget &parent);

    /**
     * @brief Redefined to call it to all widgets in this group box
     */
    void updateValue();

    /**
     * @brief Redefined to check all widgets in this group box
     */
    bool isValueAndWidgetValueDifferent() const;

    void saveSettings(SettingsWriterInterface& writer) const;
    bool restoreSettings(SettingsReaderInterface& reader);

    /**
     * @brief Get all values of this widget to save it in a script
     */
    QList<SettingsNodeGroup*> getAllValues() const;

    /**
     * @brief Set all values that was readed in a script to fill elements of the widget and update class members
     * @return false if it was an error, true otherwise
     */
    bool setAllValues(const QList<SettingsNodeGroup*> &list);

private:
    QGroupBox* m_gp;

public slots:
    /**
     * @brief Call it to expand or collapse the group box
     */
    void collapseOrExpandGroupBox(bool expand);
};

#endif // CT_GROUPBOX_H
