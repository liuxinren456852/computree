#include "step/gen_stepgeneratetorus.h"

#include <ctime>

// Using the point cloud deposit
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_point.h"
#include "ct_coordinates/ct_defaultcoordinatesystem.h"

// Alias for indexing out models
#define DEF_resultOut_resultScene "resultScene"
#define DEF_groupOut_groupScene "groupScene"
#define DEF_itemOut_scene "scene"

#include <assert.h>
#include <limits>
#include "ct_math/ct_mathpoint.h"

#define RAD_TO_DEG 57.2957795131
#define DEG_TO_RAD 0.01745329251

// Constructor : initialization of parameters
GEN_StepGenerateTorus::GEN_StepGenerateTorus(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _centerX = 0;
    _centerY = 0;
    _centerZ = 0;
    _radiusCircle = 1;
    _distanceToCenter = 3;
    _alphaMin = 0;
    _alphaMax = 360;
    _betaMin = 0;
    _betaMax = 360;
    _resAlpha = 1;
    _resBeta = 1;
    _noiseAlpha = 0;
    _noiseBeta = 0;
    _noiseRadiusCircle = 0;
    _noiseDistanceToCenter = 0;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateTorus::getStepDescription() const
{
    return tr("Créer un Torus de points");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateTorus::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateTorus(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateTorus::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateTorus::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScene, tr("Generated Point Cloud"));

    resultModel->setRootGroup(DEF_groupOut_groupScene, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScene, DEF_itemOut_scene, new CT_Scene(), tr("Generated Torus"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateTorus::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText(tr("Thore center"), "", "");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _centerX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _centerY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _centerZ, 0);
    configDialog->addText(tr("Thore radius"), "", "");
    configDialog->addDouble(tr("Circle radius"), "", 1e-10, 1e+10, 4, _radiusCircle, 0);
    configDialog->addDouble(tr("Distance to center"), "", 1e-10, 1e+10, 4, _distanceToCenter, 0);
    configDialog->addText(tr("Limits"), "", "");
    configDialog->addDouble(tr("Minimum alpha"), "", 0, 359.9999, 4, _alphaMin, 0);
    configDialog->addDouble(tr("Maximum alpha"), "", 0, 360, 4, _alphaMax, 0);
    configDialog->addDouble(tr("Minimum beta"), "", 0, 359.9999, 4, _betaMin, 0);
    configDialog->addDouble(tr("Maximum beta"), "", 0, 360, 4, _betaMax, 0);
    configDialog->addText(tr("Resolution"), "", "");
    configDialog->addDouble(tr("Alpha"), "", 0.0001, 1e+10, 4, _resAlpha, 0);
    configDialog->addDouble(tr("Beta"), "", 0.0001, 1e+10, 4, _resBeta, 0);
    configDialog->addText(tr("Add noise"), "", "");
    configDialog->addDouble(tr("Alpha"), "", 0, 1e+10, 4, _noiseAlpha, 0);
    configDialog->addDouble(tr("Beta"), "", 0, 1e+10, 4, _noiseBeta, 0);
    configDialog->addDouble(tr("Circle radius"), "", 0, 1e+10, 4, _noiseRadiusCircle, 0);
    configDialog->addDouble(tr("Distance to center"), "", 0, 1e+10, 4, _noiseDistanceToCenter, 0);
}

void GEN_StepGenerateTorus::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    /******************************************************************************
     *      User's Compute
     ******************************************************************************/
    assert( _alphaMin < _alphaMax );
    assert( _betaMin < _betaMax );

    // Conversion des angles en radians
    _alphaMin *= DEG_TO_RAD;
    _alphaMax *= DEG_TO_RAD;
    _betaMin *= DEG_TO_RAD;
    _betaMax *= DEG_TO_RAD;
    _resAlpha *= DEG_TO_RAD;
    _resBeta *= DEG_TO_RAD;
    _noiseAlpha *= DEG_TO_RAD;
    _noiseBeta *= DEG_TO_RAD;

    // On initialise l'aleatoire pour le bruit par la suite
    srand( time(0) );

    int nbPts = 0;
    int nbPtsTheta = ceil( (_alphaMax - _alphaMin) / _resAlpha );
    int nbPtsPhi = ceil( (_betaMax - _betaMin) / _resBeta );
    int nbPtsTotal = nbPtsPhi * nbPtsTheta;

    CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    // Construction du tore
    float valAlpha, valBeta, valRadius, valDistance;
    for ( float i = _alphaMin ; (i < _alphaMax) && !isStopped() ; i += _resAlpha )
    {
        for ( float j = _betaMin ; (j <= _betaMax) && !isStopped() ; j += _resBeta )
        {
            // On ajoute un point en tenant compte de la variabilité en epsilone
            valAlpha = i - _noiseAlpha + ( ((double)rand()/RAND_MAX) * 2 * _noiseAlpha );
            valBeta = j - _noiseBeta + ( ((double)rand()/RAND_MAX) * 2 * _noiseBeta );
            valRadius = _radiusCircle - _noiseRadiusCircle + ( ((double)rand()/RAND_MAX) * 2 * _noiseRadiusCircle );
            valDistance = _distanceToCenter - _noiseDistanceToCenter + ( ((double)rand()/RAND_MAX) * 2 * _noiseDistanceToCenter );

            undepositPointCloud->addPoint( Eigen::Vector3d( (valDistance +  valRadius * cos(valAlpha)) * cos(valBeta), (valDistance +  valRadius * cos(valAlpha)) * sin(valBeta), valRadius * sin(valAlpha) ));


            nbPts++;

            // Barre de progression (multiplie par 100/6 parce qu'on a huit face et qu'on est a la premiere
            setProgress(  100.0*nbPts / (float)nbPtsTotal  );

            // On regarde si on est en debug mode
            waitForAckIfInDebugMode();
        }
    }

    // On enregistre le nuage de points cree dans le depot
    CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

    if(!isStopped()) {
        // ------------------------------
        // Create OUT groups and items

        // ----------------------------------------------------------------------------
        // Works on the result corresponding to DEF_resultOut_resultScene
        CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupScene, resultOut_resultScene);

        // UNCOMMENT Following lines and complete parameters of the item's contructor
        CT_Scene* itemOut_scene = new CT_Scene(DEF_itemOut_scene, resultOut_resultScene, depositPointCloud);
        itemOut_scene->updateBoundingBox();;

        groupOut_groupScene->addItemDrawable(itemOut_scene);
        resultOut_resultScene->addGroup(groupOut_groupScene);
    }

    // Conversion des angles en degres
    _alphaMin *= RAD_TO_DEG;
    _alphaMax *= RAD_TO_DEG;
    _betaMin *= RAD_TO_DEG;
    _betaMax *= RAD_TO_DEG;
    _resAlpha *= RAD_TO_DEG;
    _resBeta *= RAD_TO_DEG;
    _noiseAlpha *= RAD_TO_DEG;
    _noiseBeta *= RAD_TO_DEG;
}
