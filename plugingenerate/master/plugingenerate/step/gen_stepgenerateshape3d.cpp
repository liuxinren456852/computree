#include "gen_stepgenerateshape3d.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

#include "ct_result/ct_resultgroup.h"

#include <ctime>

#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/ct_ellipse.h"
#include "ct_itemdrawable/ct_line.h"
#include "ct_itemdrawable/ct_sphere.h"

// Alias for indexing out models
#define DEF_resultOut_resultShape "GeneratedShape"
#define DEF_groupOut_groupShape "SHGroup"

#define DEF_groupOut_groupCircle "CircleGroup"
#define DEF_groupOut_groupCylinder "CylinderGroup"
#define DEF_groupOut_groupEllipse "EllipseGroup"
#define DEF_groupOut_groupLine "LineGroup"
#define DEF_groupOut_groupSphere "SphereGroup"

#define DEF_ItemOut_ItemCircle "CircleItem"
#define DEF_ItemOut_ItemCylinder "CylinderItem"
#define DEF_ItemOut_ItemEllipse "EllipseItem"
#define DEF_ItemOut_ItemLine "LineItem"
#define DEF_ItemOut_ItemSphere "SphereItem"

// Constructor : initialization of parameters
GEN_StepGenerateShape3D::GEN_StepGenerateShape3D(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _nbCircle = 5;
    _nbCylinder = 5;
    _nbEllipse = 5;
    _nbLine = 5;
    _nbSphere = 5;

    _minx = -10;
    _miny = -10;
    _minz = -10;

    _maxx = 10;
    _maxy = 10;
    _maxz = 10;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateShape3D::getStepDescription() const
{
    return tr("Créer des Formes Géométriques 3D");
}

// Step detailled description
QString GEN_StepGenerateShape3D::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString GEN_StepGenerateShape3D::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateShape3D::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateShape3D(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateShape3D::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateShape3D::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultShape, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupShape, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addGroupModel(DEF_groupOut_groupShape, DEF_groupOut_groupCircle);
    resultModel->addGroupModel(DEF_groupOut_groupShape, DEF_groupOut_groupCylinder);
    resultModel->addGroupModel(DEF_groupOut_groupShape, DEF_groupOut_groupEllipse);
    resultModel->addGroupModel(DEF_groupOut_groupShape, DEF_groupOut_groupLine);
    resultModel->addGroupModel(DEF_groupOut_groupShape, DEF_groupOut_groupSphere);

    resultModel->addItemModel(DEF_groupOut_groupCircle, DEF_ItemOut_ItemCircle, new CT_Circle(), tr("Generated Circle 3D"));
    resultModel->addItemModel(DEF_groupOut_groupCylinder, DEF_ItemOut_ItemCylinder, new CT_Cylinder(), tr("Generated Cylinder"));
    resultModel->addItemModel(DEF_groupOut_groupEllipse, DEF_ItemOut_ItemEllipse, new CT_Ellipse(), tr("Generated Ellipse 3D"));
    resultModel->addItemModel(DEF_groupOut_groupLine, DEF_ItemOut_ItemLine, new CT_Line(), tr("Generated Line 3D"));
    resultModel->addItemModel(DEF_groupOut_groupSphere, DEF_ItemOut_ItemSphere, new CT_Sphere(), tr("Generated Sphere"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateShape3D::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addInt(tr("Nombre de cercles 3D"), "", 0, 9999, _nbCircle);
    configDialog->addInt(tr("Nombre de cylindres"), "", 0, 9999, _nbCylinder);
    configDialog->addInt(tr("Nombre d'ellipses 3D"), "", 0, 9999, _nbEllipse);
    configDialog->addInt(tr("Nombre de lignes 3D"), "", 0, 9999, _nbLine);
    configDialog->addInt(tr("Nombre de sphères"), "", 0, 9999, _nbSphere);
    configDialog->addDouble(tr("Xmin"), "", -1e+10, 1e+10, 2, _minx);
    configDialog->addDouble(tr("Xmax"), "", -1e+10, 1e+10, 2, _maxx);
    configDialog->addDouble(tr("Ymin"), "", -1e+10, 1e+10, 2, _miny);
    configDialog->addDouble(tr("Ymax"), "", -1e+10, 1e+10, 2, _maxy);
    configDialog->addDouble(tr("Zmin"), "", -1e+10, 1e+10, 2, _minz);
    configDialog->addDouble(tr("Zmax"), "", -1e+10, 1e+10, 2, _maxz);
}

void GEN_StepGenerateShape3D::compute()
{
    CT_ResultGroup* resultOut = getOutResultList().first();

    CT_StandardItemGroup* groupBase = new CT_StandardItemGroup(DEF_groupOut_groupShape, resultOut);
    resultOut->addGroup(groupBase);

    double deltaX = fabs(_maxx - _minx);
    double deltaY = fabs(_maxy - _miny);
    double deltaZ = fabs(_maxz - _minz);

    srand( time(0) );

    for (int i = 0 ; (i < _nbCircle) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupCircle, resultOut);

        double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double z = _minz + ((double)rand()/RAND_MAX) * deltaZ;
        double r = ((double)rand()/RAND_MAX);

        double dx = rand();
        double dy = rand();
        double dz = rand();

        CT_CircleData *data = new CT_CircleData(Eigen::Vector3d(x, y, z), Eigen::Vector3d(dx, dy, dz), r);
        CT_Circle *item = new CT_Circle(DEF_ItemOut_ItemCircle, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _nbCylinder) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupCylinder, resultOut);

        double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double z = _minz + ((double)rand()/RAND_MAX) * deltaZ;
        double r = ((double)rand()/RAND_MAX);
        double h = ((double)rand()/RAND_MAX);

        CT_CylinderData *data = new CT_CylinderData(Eigen::Vector3d(x, y, z), Eigen::Vector3d(0, 0, 1), r, h);
        CT_Cylinder *item = new CT_Cylinder(DEF_ItemOut_ItemCylinder, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _nbEllipse) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupEllipse, resultOut);

        double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double z = _minz + ((double)rand()/RAND_MAX) * deltaZ;
        double r1 = ((double)rand()/RAND_MAX);
        double r2 = ((double)rand()/RAND_MAX);

        CT_LineData axisA(Eigen::Vector3d(x-r1, y, z), Eigen::Vector3d(x+r1, y, z));
        CT_LineData axisB(Eigen::Vector3d(x, y-r2, z), Eigen::Vector3d(x, y+r2, z));

        CT_EllipseData *data = new CT_EllipseData(Eigen::Vector3d(x, y, z), axisA, axisB);
        CT_Ellipse *item = new CT_Ellipse(DEF_ItemOut_ItemEllipse, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _nbLine) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupLine, resultOut);

        double x1 = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y1 = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double z1 = _minz + ((double)rand()/RAND_MAX) * deltaY;
        double x2 = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y2 = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double z2 = _minz + ((double)rand()/RAND_MAX) * deltaY;

        CT_LineData *data = new CT_LineData(Eigen::Vector3d(x1, y1, z1), Eigen::Vector3d(x2, y2, z2));
        CT_Line *item = new CT_Line(DEF_ItemOut_ItemLine, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _nbSphere) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupSphere, resultOut);

        double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double z = _minz + ((double)rand()/RAND_MAX) * deltaZ;
        double r = ((double)rand()/RAND_MAX);

        CT_SphereData *data = new CT_SphereData(Eigen::Vector3d(x, y, z), r);
        CT_Sphere *item = new CT_Sphere(DEF_ItemOut_ItemSphere, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }
}
