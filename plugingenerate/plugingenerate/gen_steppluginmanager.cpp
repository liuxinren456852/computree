#include "gen_steppluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"

// Item generation
#include "step/gen_stepgenerateraster2d.h"
#include "step/gen_stepgenerateraster3d.h"
#include "step/gen_stepgenerateraster4d.h"
#include "step/gen_stepgenerateray.h"
#include "step/gen_stepgeneratescanner.h"
#include "step/gen_stepgenerateshape2d.h"
#include "step/gen_stepgenerateshape3d.h"

// Point cloud generation
#include "step/gen_stepgenerateplanecloud.h"
#include "step/gen_stepgeneratecubecloud.h"
#include "step/gen_stepgeneratecylindercloud.h"
#include "step/gen_stepgeneratecone.h"
#include "step/gen_stepgeneratespherecloud.h"
#include "step/gen_stepgeneratetorus.h"
#include "step/gen_stepgeneratequadraticsurface.h"
#include "step/gen_stepgeneraterandomcloud.h"

// Mesh Generation
#include "step/gen_stepgeneratecylindermesh.h"

GEN_StepPluginManager::GEN_StepPluginManager() : CT_AbstractStepPlugin()
{
}

GEN_StepPluginManager::~GEN_StepPluginManager()
{
}

QString GEN_StepPluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin Generate for Computree\n"
           "AU  - Ravaglia, Joris\n"
           "AU  - Krebs, Michael\n"
           "PB  - Office National des Forêts, RDI Department\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-generate/wiki\n"
           "ER  - \n";
}



bool GEN_StepPluginManager::loadGenericsStep()
{
    return true;
}

bool GEN_StepPluginManager::loadOpenFileStep()
{
    return true;
}

bool GEN_StepPluginManager::loadCanBeAddedFirstStep()
{

    addNewOtherStep<GEN_StepGeneratePlaneCloud>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateCubeCloud>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateCylinder>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateCone>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateSphereCloud>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateTorus>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateQuadraticSurface>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateRandomCloud>(QObject::tr("Générer (test)"));

    addNewOtherStep<GEN_StepGenerateCylinderMesh>(QObject::tr("Générer (test)"));

    addNewOtherStep<GEN_StepGenerateRaster2DFloat>(QObject::tr("Générer (test)"));

#ifdef USE_OPENCV
    addNewOtherStep<GEN_StepGenerateRaster4DFloat>(QObject::tr("Générer (test)"));
#endif

    addNewOtherStep<GEN_StepGenerateRaster3DFloat>(QObject::tr("Générer (test)"));

    addNewOtherStep<GEN_StepGenerateRay>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateScanner>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateShape2D>(QObject::tr("Générer (test)"));
    addNewOtherStep<GEN_StepGenerateShape3D>(QObject::tr("Générer (test)"));

    return true;
}

bool GEN_StepPluginManager::loadActions()
{
    return true;
}

bool GEN_StepPluginManager::loadExporters()
{
    return true;
}

bool GEN_StepPluginManager::loadReaders()
{
    return true;
}

