#include "gen_stepgeneraterandomcloud.h"

#include <ctime>

// Using the point cloud deposit
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

#include "ct_point.h"
#include "ct_coordinates/ct_defaultcoordinatesystem.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"

// Alias for indexing out models
#define DEF_resultOut_resultScene "resultScene"
#define DEF_groupOut_groupScene "groupScene"
#define DEF_itemOut_scene "scene"

#include <limits>

// Constructor : initialization of parameters
GEN_StepGenerateRandomCloud::GEN_StepGenerateRandomCloud(CT_StepInitializeData &dataInit) : CT_AbstractStepCanBeAddedFirst(dataInit)
{
    _nPts = 1000;
    _botX = -10;
    _botY = -10;
    _botZ = -10;
    _topX = 10;
    _topY = 10;
    _topZ = 10;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateRandomCloud::getStepDescription() const
{
    return tr("Créer des points Aléatoires");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateRandomCloud::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new GEN_StepGenerateRandomCloud(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void GEN_StepGenerateRandomCloud::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void GEN_StepGenerateRandomCloud::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScene, tr("Generated Point Cloud"));

    resultModel->setRootGroup(DEF_groupOut_groupScene, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScene, DEF_itemOut_scene, new CT_Scene(), tr("Generated Random Cloud"));
}

// Semi-automatic creation of step parameters DialogBox
void GEN_StepGenerateRandomCloud::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addInt(tr("Number fo points"), "", 0, 1e+07, _nPts);
    configDialog->addText(tr("Bottom left extremum"),"","");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _botX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _botY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _botZ, 0);
    configDialog->addText(tr("Top right extremum"),"","");
    configDialog->addDouble("X", "", -1e+10, 1e+10, 4, _topX, 0);
    configDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _topY, 0);
    configDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _topZ, 0);
}

void GEN_StepGenerateRandomCloud::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    // On initialise l'aleatoire pour le bruit par la suite
    srand( time(0) );

    CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    float sizeX = _topX - _botX;
    float sizeY = _topY - _botY;
    float sizeZ = _topZ - _botZ;

    for ( int i = 0 ; (i < _nPts) && !isStopped() ; i++ )
    {
        undepositPointCloud->addPoint( Eigen::Vector3d( _botX + ( ((double)rand()/RAND_MAX) * sizeX ), _botY + ( ((double)rand()/RAND_MAX) * sizeY ), _botZ + ( ((double)rand()/RAND_MAX) * sizeZ ) ));

        // Barre de progression (multiplie par 100/6 parce qu'on a huit face et qu'on est a la premiere
        setProgress(100.0*i / (float)_nPts );

        // On regarde si on est en debug mode
        waitForAckIfInDebugMode();
    }

    // On enregistre le nuage de points cree dans le depot
    CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

    if(!isStopped()) {
        CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupScene, resultOut_resultScene);

        CT_Scene* itemOut_scene = new CT_Scene(DEF_itemOut_scene, resultOut_resultScene, depositPointCloud);
        itemOut_scene->updateBoundingBox();

        groupOut_groupScene->addItemDrawable(itemOut_scene);
        resultOut_resultScene->addGroup(groupOut_groupScene);
    }

}
