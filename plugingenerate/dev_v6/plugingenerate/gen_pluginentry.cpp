#include "gen_pluginentry.h"
#include "gen_steppluginmanager.h"

GEN_PluginEntry::GEN_PluginEntry()
{
    _stepPluginManager = new GEN_StepPluginManager();
}

GEN_PluginEntry::~GEN_PluginEntry()
{
    delete _stepPluginManager;
}

QString GEN_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* GEN_PluginEntry::getPlugin() const
{
    return _stepPluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_generate, GEN_PluginEntry)
#endif
