#include "step/gen_stepgeneratetorus.h"

#include <ctime>

// Using the point cloud deposit

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_point.h"
#include "ct_coordinates/ct_defaultcoordinatesystem.h"

// Alias for indexing out models

#include <assert.h>

#include "ct_math/ct_mathpoint.h"

#define RAD_TO_DEG 57.2957795131
#define DEG_TO_RAD 0.01745329251

GEN_StepGenerateTorus::GEN_StepGenerateTorus() : CT_AbstractStepCanBeAddedFirst()
{
    _centerX = 0;
    _centerY = 0;
    _centerZ = 0;
    _radiusCircle = 1;
    _distanceToCenter = 3;
    _alphaMin = 0;
    _alphaMax = 360;
    _betaMin = 0;
    _betaMax = 360;
    _resAlpha = 1;
    _resBeta = 1;
    _noiseAlpha = 0;
    _noiseBeta = 0;
    _noiseRadiusCircle = 0;
    _noiseDistanceToCenter = 0;
}

QString GEN_StepGenerateTorus::description() const
{
    return tr("Créer un Torus de points");
}

CT_VirtualAbstractStep* GEN_StepGenerateTorus::createNewInstance()
{
    return new GEN_StepGenerateTorus();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateTorus::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateTorus::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScene, tr("Generated Point Cloud"));

    resultModel->setRootGroup(DEF_groupOut_groupScene, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScene, DEF_itemOut_scene, new CT_Scene(), tr("Generated Torus"));
}

void GEN_StepGenerateTorus::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addText(tr("Thore center"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _centerX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _centerY, 0);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _centerZ, 0);
    postInputConfigDialog->addText(tr("Thore radius"), "", "");
    postInputConfigDialog->addDouble(tr("Circle radius"), "", 1e-10, std::numeric_limits<double>::max(), 4, _radiusCircle, 0);
    postInputConfigDialog->addDouble(tr("Distance to center"), "", 1e-10, std::numeric_limits<double>::max(), 4, _distanceToCenter, 0);
    postInputConfigDialog->addText(tr("Limits"), "", "");
    postInputConfigDialog->addDouble(tr("Minimum alpha"), "", 0, 359.9999, 4, _alphaMin, 0);
    postInputConfigDialog->addDouble(tr("Maximum alpha"), "", 0, 360, 4, _alphaMax, 0);
    postInputConfigDialog->addDouble(tr("Minimum beta"), "", 0, 359.9999, 4, _betaMin, 0);
    postInputConfigDialog->addDouble(tr("Maximum beta"), "", 0, 360, 4, _betaMax, 0);
    postInputConfigDialog->addText(tr("Resolution"), "", "");
    postInputConfigDialog->addDouble(tr("Alpha"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resAlpha, 0);
    postInputConfigDialog->addDouble(tr("Beta"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resBeta, 0);
    postInputConfigDialog->addText(tr("Add noise"), "", "");
    postInputConfigDialog->addDouble(tr("Alpha"), "", 0, std::numeric_limits<double>::max(), 4, _noiseAlpha, 0);
    postInputConfigDialog->addDouble(tr("Beta"), "", 0, std::numeric_limits<double>::max(), 4, _noiseBeta, 0);
    postInputConfigDialog->addDouble(tr("Circle radius"), "", 0, std::numeric_limits<double>::max(), 4, _noiseRadiusCircle, 0);
    postInputConfigDialog->addDouble(tr("Distance to center"), "", 0, std::numeric_limits<double>::max(), 4, _noiseDistanceToCenter, 0);
}

void GEN_StepGenerateTorus::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    /******************************************************************************
     *      User's Compute
     ******************************************************************************/
    assert( _alphaMin < _alphaMax );
    assert( _betaMin < _betaMax );

    // Conversion des angles en radians
    _alphaMin *= DEG_TO_RAD;
    _alphaMax *= DEG_TO_RAD;
    _betaMin *= DEG_TO_RAD;
    _betaMax *= DEG_TO_RAD;
    _resAlpha *= DEG_TO_RAD;
    _resBeta *= DEG_TO_RAD;
    _noiseAlpha *= DEG_TO_RAD;
    _noiseBeta *= DEG_TO_RAD;

    // On initialise l'aleatoire pour le bruit par la suite
    srand( time(0) );

    int nbPts = 0;
    int nbPtsTheta = ceil( (_alphaMax - _alphaMin) / _resAlpha );
    int nbPtsPhi = ceil( (_betaMax - _betaMin) / _resBeta );
    int nbPtsTotal = nbPtsPhi * nbPtsTheta;

    CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    // Construction du tore
    float valAlpha, valBeta, valRadius, valDistance;
    for ( float i = _alphaMin ; (i < _alphaMax) && !isStopped() ; i += _resAlpha )
    {
        for ( float j = _betaMin ; (j <= _betaMax) && !isStopped() ; j += _resBeta )
        {
            // On ajoute un point en tenant compte de la variabilité en epsilone
            valAlpha = i - _noiseAlpha + ( ((double)rand()/RAND_MAX) * 2 * _noiseAlpha );
            valBeta = j - _noiseBeta + ( ((double)rand()/RAND_MAX) * 2 * _noiseBeta );
            valRadius = _radiusCircle - _noiseRadiusCircle + ( ((double)rand()/RAND_MAX) * 2 * _noiseRadiusCircle );
            valDistance = _distanceToCenter - _noiseDistanceToCenter + ( ((double)rand()/RAND_MAX) * 2 * _noiseDistanceToCenter );

            undepositPointCloud->addPoint( Eigen::Vector3d( (valDistance +  valRadius * cos(valAlpha)) * cos(valBeta), (valDistance +  valRadius * cos(valAlpha)) * sin(valBeta), valRadius * sin(valAlpha) ));

            nbPts++;

            // Barre de progression (multiplie par 100/6 parce qu'on a huit face et qu'on est a la premiere
            setProgress(  100.0*nbPts / (float)nbPtsTotal  );

            // On regarde si on est en debug mode
            waitForAckIfInDebugMode();
        }
    }

    // On enregistre le nuage de points cree dans le depot
    CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

    if(!isStopped()) {
        // ------------------------------
        // Create OUT groups and items

        // ----------------------------------------------------------------------------
        // Works on the result corresponding to DEF_resultOut_resultScene
        CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupScene, resultOut_resultScene);

        // UNCOMMENT Following lines and complete parameters of the item's contructor
        CT_Scene* itemOut_scene = new CT_Scene(DEF_itemOut_scene, resultOut_resultScene, depositPointCloud);
        itemOut_scene->updateBoundingBox();;

        groupOut_groupScene->addItemDrawable(itemOut_scene);
        resultOut_resultScene->addGroup(groupOut_groupScene);
    }

    // Conversion des angles en degres
    _alphaMin *= RAD_TO_DEG;
    _alphaMax *= RAD_TO_DEG;
    _betaMin *= RAD_TO_DEG;
    _betaMax *= RAD_TO_DEG;
    _resAlpha *= RAD_TO_DEG;
    _resBeta *= RAD_TO_DEG;
    _noiseAlpha *= RAD_TO_DEG;
    _noiseBeta *= RAD_TO_DEG;
}
