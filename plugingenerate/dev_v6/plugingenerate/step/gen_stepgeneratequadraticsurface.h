#ifndef GEN_STEPGENERATEQUADRATICSURFACE_H
#define GEN_STEPGENERATEQUADRATICSURFACE_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateQuadraticSurface : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateQuadraticSurface();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private :
    double _a;
    double _b;
    double _c;
    double _d;
    double _e;
    double _f;

    double _xMin;
    double _xMax;
    double _yMin;
    double _yMax;

    double _resX;
    double _resY;
};

#endif // GEN_STEPGENERATEQUADRATICSURFACE_H
