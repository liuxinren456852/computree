#include "step/gen_stepgeneratescanner.h"

#include "ct_itemdrawable/ct_scanner.h"

// Alias for indexing out models

GEN_StepGenerateScanner::GEN_StepGenerateScanner() : CT_AbstractStepCanBeAddedFirst()
{
    _posX = 0;
    _posY = 0;
    _posZ = 0;
    _initTheta = 0;
    _initPhi = 0;
    _hFov = 360;
    _vFov = 120;
    _hRes = 0.036;
    _vRes = 0.036;
    _clockWise = true;
}

QString GEN_StepGenerateScanner::description() const
{
    return tr("Créer une Position de Scan");
}

CT_VirtualAbstractStep* GEN_StepGenerateScanner::createNewInstance()
{
    return new GEN_StepGenerateScanner();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateScanner::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateScanner::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScanner, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupScanner, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScanner, DEF_itemOut_itemScanner, new CT_Scanner(), tr("Generated Scanner"));
}

void GEN_StepGenerateScanner::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addText(tr("Position"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _posX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _posY, 0);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _posZ, 0);
    postInputConfigDialog->addText(tr("Initial angles"), "", "");
    postInputConfigDialog->addDouble(tr("Theta"), tr("degres"), 0, 360, 4, _initTheta, 0);
    postInputConfigDialog->addDouble(tr("Phi"), tr("degres"), 0, 180, 4, _initPhi, 0);
    postInputConfigDialog->addText(tr("Field of view"), "", "");
    postInputConfigDialog->addDouble(tr("Theta"), tr("degres"), -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _hFov, 0);
    postInputConfigDialog->addDouble(tr("Phi"), tr("degres"), 0.0001, 180, 4, _vFov, 0);
    postInputConfigDialog->addText(tr("Resolution"), "", "");
    postInputConfigDialog->addDouble(tr("Theta"), tr("degres"), 0.0001, std::numeric_limits<double>::max(), 4, _hRes, 0);
    postInputConfigDialog->addDouble(tr("Phi"), tr("degres"), 0.0001, std::numeric_limits<double>::max(), 4, _vRes, 0);
    postInputConfigDialog->addEmpty();
    postInputConfigDialog->addDouble(tr("Clockwise"), "", 0, 100, 2, _clockWise, 0);
}

void GEN_StepGenerateScanner::compute()
{
    CT_ResultGroup* resultOut_resultScanner = getOutResultList().first();

    CT_StandardItemGroup* groupOut_groupScanner = new CT_StandardItemGroup(DEF_groupOut_groupScanner, resultOut_resultScanner);

    CT_Scanner* itemOut_itemScanner = new CT_Scanner(DEF_itemOut_itemScanner, resultOut_resultScanner, 0, Eigen::Vector3d(_posX, _posY, _posZ), Eigen::Vector3d(0,0,1), _hFov, _vFov, _hRes, _vRes, _initTheta, _initPhi, _clockWise );

    groupOut_groupScanner->addItemDrawable(itemOut_itemScanner);
    resultOut_resultScanner->addGroup(groupOut_groupScanner);

}
