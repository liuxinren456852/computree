#include "step/gen_stepgenerateshape2d.h"

#include "ct_itemdrawable/ct_box2d.h"
#include "ct_itemdrawable/ct_circle2d.h"
#include "ct_itemdrawable/ct_point2d.h"
#include "ct_itemdrawable/ct_line2d.h"
#include "ct_itemdrawable/ct_polygon2d.h"
#include "ct_itemdrawable/ct_polyline2d.h"

#include <ctime>

// Alias for indexing out models

GEN_StepGenerateShape2D::GEN_StepGenerateShape2D() : CT_AbstractStepCanBeAddedFirst()
{
    _boxNb = 5;
    _circleNb = 5;
    _pointNb = 5;
    _lineNb = 5;
    _polygonNb = 5;
    _polylineNb = 5;
    _minx = -10;
    _miny = -10;
    _maxx = 10;
    _maxy = 10;
}

QString GEN_StepGenerateShape2D::description() const
{
    return tr("Créer des Formes Géométriques 2D");
}

CT_VirtualAbstractStep* GEN_StepGenerateShape2D::createNewInstance()
{
    return new GEN_StepGenerateShape2D();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateShape2D::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateShape2D::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultShape2D, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupShape2D, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addGroupModel(DEF_groupOut_groupShape2D, DEF_groupOut_groupBox2D);
    resultModel->addGroupModel(DEF_groupOut_groupShape2D, DEF_groupOut_groupCircle2D);
    resultModel->addGroupModel(DEF_groupOut_groupShape2D, DEF_groupOut_groupPoint2D);
    resultModel->addGroupModel(DEF_groupOut_groupShape2D, DEF_groupOut_groupLine2D);
    resultModel->addGroupModel(DEF_groupOut_groupShape2D, DEF_groupOut_groupPolygon2D);
    resultModel->addGroupModel(DEF_groupOut_groupShape2D, DEF_groupOut_groupPolyline2D);

    resultModel->addItemModel(DEF_groupOut_groupBox2D, DEF_ItemOut_ItemBox2D, new CT_Box2D(), tr("Generated Box 2D"));
    resultModel->addItemModel(DEF_groupOut_groupCircle2D, DEF_ItemOut_ItemCircle2D, new CT_Circle2D(), tr("Generated Circle 2D"));
    resultModel->addItemModel(DEF_groupOut_groupPoint2D, DEF_ItemOut_ItemPoint2D, new CT_Point2D(), tr("Generated Point 2D"));
    resultModel->addItemModel(DEF_groupOut_groupLine2D, DEF_ItemOut_ItemLine2D, new CT_Line2D(), tr("Generated Line 2D"));
    resultModel->addItemModel(DEF_groupOut_groupPolygon2D, DEF_ItemOut_ItemPolygon2D, new CT_Polygon2D(), tr("Generated Polygon 2D"));
    resultModel->addItemModel(DEF_groupOut_groupPolyline2D, DEF_ItemOut_ItemPolyline2D, new CT_Polyline2D(), tr("Generated Polyline 2D"));

}

void GEN_StepGenerateShape2D::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addInt(tr("Nombre de rectangles 2D"), "", 0, 9999, _boxNb);
    postInputConfigDialog->addInt(tr("Nombre de cercles 2D"), "", 0, 9999, _circleNb);
    postInputConfigDialog->addInt(tr("Nombre de points 2D"), "", 0, 9999, _pointNb);
    postInputConfigDialog->addInt(tr("Nombre de lignes 2D"), "", 0, 9999, _lineNb);
    postInputConfigDialog->addInt(tr("Nombre de polygones 2D"), "", 0, 9999, _polygonNb);
    postInputConfigDialog->addInt(tr("Nombre de polylignes 2D"), "", 0, 9999, _polylineNb);
    postInputConfigDialog->addDouble(tr("Xmin"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _minx);
    postInputConfigDialog->addDouble(tr("Xmax"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _maxx);
    postInputConfigDialog->addDouble(tr("Ymin"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _miny);
    postInputConfigDialog->addDouble(tr("Ymax"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 2, _maxy);
}

void GEN_StepGenerateShape2D::compute()
{
    CT_ResultGroup* resultOut = getOutResultList().first();

    CT_StandardItemGroup* groupBase = new CT_StandardItemGroup(DEF_groupOut_groupShape2D, resultOut);
    resultOut->addGroup(groupBase);

    double deltaX = fabs(_maxx - _minx);
    double deltaY = fabs(_maxy - _miny);

    srand( time(0) );

    for (int i = 0 ; (i < _boxNb) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupBox2D, resultOut);

        double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double h = ((double)rand()/RAND_MAX) * 10;
        double w = ((double)rand()/RAND_MAX) * 10;

        CT_Box2DData *data = new CT_Box2DData(Eigen::Vector2d(x, y), h, w);
        CT_Box2D *item = new CT_Box2D(DEF_ItemOut_ItemBox2D, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _circleNb) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupCircle2D, resultOut);

        double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double r = ((double)rand()/RAND_MAX);

        CT_Circle2DData *data = new CT_Circle2DData(Eigen::Vector2d(x, y), r);
        CT_Circle2D *item = new CT_Circle2D(DEF_ItemOut_ItemCircle2D, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _pointNb) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupPoint2D, resultOut);

        double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y = _miny + ((double)rand()/RAND_MAX) * deltaY;

        CT_Point2DData *data = new CT_Point2DData(Eigen::Vector2d(x, y));
        CT_Point2D *item = new CT_Point2D(DEF_ItemOut_ItemPoint2D, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _lineNb) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupLine2D, resultOut);

        double x1 = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y1 = _miny + ((double)rand()/RAND_MAX) * deltaY;
        double x2 = _minx + ((double)rand()/RAND_MAX) * deltaX;
        double y2 = _miny + ((double)rand()/RAND_MAX) * deltaY;

        CT_Line2DData *data = new CT_Line2DData(Eigen::Vector2d(x1, y1), Eigen::Vector2d(x2, y2));
        CT_Line2D *item = new CT_Line2D(DEF_ItemOut_ItemLine2D, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _polygonNb) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupPolygon2D, resultOut);

        int nv = rand() % 15 +1;

        QVector<Eigen::Vector2d *> vertices(nv);

        for (int j = 0 ; j < nv ; j++)
        {
            double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
            double y = _miny + ((double)rand()/RAND_MAX) * deltaY;

            vertices[j] = new Eigen::Vector2d(x, y);
        }

        CT_Polygon2DData *data = new CT_Polygon2DData(vertices, false);
        CT_Polygon2D *item = new CT_Polygon2D(DEF_ItemOut_ItemPolygon2D, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

    for (int i = 0 ; (i < _polylineNb) && !isStopped() ; i++)
    {
        CT_StandardItemGroup *group = new CT_StandardItemGroup(DEF_groupOut_groupPolyline2D, resultOut);

        int nv = rand() % 15 +1;

        QVector<Eigen::Vector2d *> vertices(nv);

        for (int j = 0 ; j < nv ; j++)
        {
            double x = _minx + ((double)rand()/RAND_MAX) * deltaX;
            double y = _miny + ((double)rand()/RAND_MAX) * deltaY;

            vertices[j] = new Eigen::Vector2d(x, y);
        }

        CT_Polyline2DData *data = new CT_Polyline2DData(vertices, false);
        CT_Polyline2D *item = new CT_Polyline2D(DEF_ItemOut_ItemPolyline2D, resultOut, data);

        groupBase->addGroup(group);
        group->addItemDrawable(item);
    }

}

