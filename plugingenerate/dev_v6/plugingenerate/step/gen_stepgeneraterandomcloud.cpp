#include "gen_stepgeneraterandomcloud.h"

#include <ctime>

#include "ct_global/ct_repositorymanager.h"
#include "ct_cloudindex/registered/abstract/ct_abstractnotmodifiablecloudindexregisteredt.h"
#include "ct_point.h"

#include <limits>

// Constructor : initialization of parameters
GEN_StepGenerateRandomCloud::GEN_StepGenerateRandomCloud() : SuperClass()
{
    _nPts = 1000;
    _botX = -10;
    _botY = -10;
    _botZ = -10;
    _topX = 10;
    _topY = 10;
    _topZ = 10;
}

// Step description (tooltip of contextual menu)
QString GEN_StepGenerateRandomCloud::description() const
{
    return tr("Créer des points Aléatoires");
}

// Step copy method
CT_VirtualAbstractStep* GEN_StepGenerateRandomCloud::createNewInstance() const
{
    return new GEN_StepGenerateRandomCloud();
}

void GEN_StepGenerateRandomCloud::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addInt(tr("Number fo points"), "", 0, 1e+07, _nPts);
    postInputConfigDialog->addText(tr("Bottom left extremum"),"","");
    postInputConfigDialog->addDouble("X", "", -1e+10, 1e+10, 4, _botX, 0);
    postInputConfigDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _botY, 0);
    postInputConfigDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _botZ, 0);
    postInputConfigDialog->addText(tr("Top right extremum"),"","");
    postInputConfigDialog->addDouble("X", "", -1e+10, 1e+10, 4, _topX, 0);
    postInputConfigDialog->addDouble("Y", "", -1e+10, 1e+10, 4, _topY, 0);
    postInputConfigDialog->addDouble("Z", "", -1e+10, 1e+10, 4, _topZ, 0);
}

void GEN_StepGenerateRandomCloud::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.setNotNeedInputResult();
}

void GEN_StepGenerateRandomCloud::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResult(m_hOutResult, tr("Generated Point Cloud"));
    manager.setRootGroup(m_hOutResult, m_hOutRootGroup);
    manager.addItem(m_hOutRootGroup, m_hOutScene, tr("Generated Random Cloud"));
}

void GEN_StepGenerateRandomCloud::compute()
{
    // On initialise l'aleatoire pour le bruit par la suite
    srand( time(0) );

    for(CT_ResultGroup* result : m_hOutResult.iterateOutputs()) {

        CT_StandardItemGroup* rootGroup = m_hOutRootGroup.createInstance();
        result->addRootGroup(m_hOutRootGroup, rootGroup);

        CT_AbstractUndefinedSizePointCloud* undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

        const double sizeX = _topX - _botX;
        const double sizeY = _topY - _botY;
        const double sizeZ = _topZ - _botZ;

        for(int i=0; (i<_nPts) && !isStopped(); ++i)
        {
            undepositPointCloud->addPoint( Eigen::Vector3d( _botX + ( ((double)rand()/RAND_MAX) * sizeX ),
                                                            _botY + ( ((double)rand()/RAND_MAX) * sizeY ),
                                                            _botZ + ( ((double)rand()/RAND_MAX) * sizeZ ) ));

            // Barre de progression (multiplie par 100/6 parce qu'on a huit face et qu'on est a la premiere
            setProgress(100.0*i / float(_nPts) );

            // On regarde si on est en debug mode
            waitForAckIfInDebugMode();
        }

        // On enregistre le nuage de points cree dans le depot
        CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

        if(isStopped())
            return;

        CT_Scene* itemOut_scene = new CT_Scene(depositPointCloud);
        itemOut_scene->updateBoundingBox();

        rootGroup->addSingularItem(m_hOutScene, itemOut_scene);
    }
}
