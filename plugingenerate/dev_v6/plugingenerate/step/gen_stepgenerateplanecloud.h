#ifndef GEN_STEPGENERATEPLANECLOUD_H
#define GEN_STEPGENERATEPLANECLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGeneratePlaneCloud : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGeneratePlaneCloud();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _height;    /*!< Hauteur du plan */
    double    _botX;    /*!< Coordonnees du point inferieur gauche du plan */
    double    _botY;    /*!< Coordonnees du point inferieur gauche du plan */
    double    _topX;    /*!< Coordonnees du point superieur droit du plan */
    double    _topY;    /*!< Coordonnees du point superieur droit du plan */
    double    _resX;    /*!< Espace entre deux points sur l'axe Ox */
    double    _resY;    /*!< Espace entre deux point selon Oy */
    double    _noiseX;
    double    _noiseY;
    double    _noiseZ;
    double    _axisX;
    double    _axisY;
    double    _axisZ;
};

#endif // GEN_STEPGENERATEPLANECLOUD_H
