#ifndef GEN_STEPGENERATESHAPE2D_H
#define GEN_STEPGENERATESHAPE2D_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateShape2D : public CT_AbstractStepCanBeAddedFirst
{

    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateShape2D();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    int     _boxNb;
    int     _circleNb;
    int     _pointNb;
    int     _lineNb;
    int     _polygonNb;
    int     _polylineNb;

    double  _minx;
    double  _maxx;
    double  _miny;
    double  _maxy;

};

#endif // GEN_STEPGENERATESHAPE2D_H
