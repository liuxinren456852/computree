#ifndef INFIELD_H
#define INFIELD_H

#include <QObject>
#include "ct_step/abstract/ct_abstractstep.h"

class InField : public QObject
{
    Q_OBJECT

public:

    InField ()
	{

	}

    QString _name;
    QString _type;
    QString _desc;
    bool    _multi;
    bool    _opt;

    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::BOOLEAN>        _inAttLogical;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER_INT>     _inAttInteger;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER>         _inAttNumeric;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::STRING>         _inAttCharacter;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>            _inAttFactor;

};

#endif // INFIELD_H
