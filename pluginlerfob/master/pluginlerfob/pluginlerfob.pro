CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
}

include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += concurrent

TARGET = plug_lerfob

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    lb_pluginentry.h \
    lb_pluginmanager.h \
    step/lb_stepcharacterizelog.h \
    step/lb_stepcharacterisecurvature.h \
    reader/lb_reader_bil3d.h

SOURCES += \
    lb_pluginentry.cpp \
    lb_pluginmanager.cpp \
    step/lb_stepcharacterizelog.cpp \
    step/lb_stepcharacterisecurvature.cpp \
    reader/lb_reader_bil3d.cpp

TRANSLATIONS += languages/pluginlerfob_en.ts \
                languages/pluginlerfob_fr.ts

