#ifndef LB_STEPCHARACTERIZELOG_H
#define LB_STEPCHARACTERIZELOG_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_circle.h"

#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Geometry"

/*!
 * \class LB_StepCharacterizeLog
 * \ingroup Steps_LB
 * \brief <b>Provide Log Characteristics: volume, maximal deflection,....</b>
 *
 * Provide log characteristics 
 *
 *
 */

class LB_StepCharacterizeLog: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    LB_StepCharacterizeLog(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

    /*! \brief Volume of the frustum of cone
      *
      * Compute the volume of a frustum of a right circular cone
      */
    float volumeFrustum( float buttRadius, float topRadius, float height);
    float volumeCylinder( float radius,  float height);
    double distance( Eigen::Quaterniond p1 , Eigen::Quaterniond p2);
    Eigen::Vector3d pointToLine(const Eigen::Vector3d  lineOrigin, const Eigen::Vector3d  lineDirection,const  Eigen::Vector3d Point3D);
    double triangleArea(const Eigen::Vector3d vertexA, const Eigen::Vector3d  vertexB, const Eigen::Vector3d vertexC);


private:

    // no Step parameters

    static bool orderByIncreasingZ(CT_Circle* c1, CT_Circle* c2)
    {
        return c1->getCenterZ() > c2->getCenterZ();
    }


};

#endif // LB_STEPCHARACTERIZELOG_H
