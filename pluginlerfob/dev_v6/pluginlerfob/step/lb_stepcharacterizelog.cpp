/****************************************************************************

 Copyright (C) 2014 the Institut National de Recherches Agronomique (INRA), France

 All rights reserved.

 Contact : constant@inra.fr

 Developers : ThiÃÂ©ry Constant (INRA/LERFOB)

 This file is part of PluginLERFOB library 1.0

 PluginLERFOB is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginLERFOB is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginLERFOB.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
 **************************************************************************************/
#include "lb_stepcharacterizelog.h"
#include "ct_global/ct_context.h"

#include "ct_itemdrawable/ct_pointcluster.h"
#include "ct_itemdrawable/ct_line.h"
#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_itemdrawable/ct_attributeslist.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_math/ct_mathpoint.h"

// Alias for indexing models in models
#define DEFin_resSections "resSections"
#define DEFin_Section "Section"
//#define DEFin_Clusters "Clusters"
#define DEFin_Cluster "Cluster"
#define DEFin_Circle "Circle"

// Alias for indexing models out models
#define DEFout_resLogsChar "resLogsChar"
#define DEFout_logChar "logChar"

// Old Alias for indexing models out models

#define DEF_groupOut_curv "curv"
#define DEF_groupOut_debsec "deb"
#define DEF_itemOut_cir "cir"
#define DEF_itemOut_lin "g2v"
#define DEF_itemOut_lincline "linc"
#define DEF_itemOut_wincline "winc"
#define DEF_itemOut_tincline "tinc"
#define DEF_itemOut_cyl "cyl"
#define DEF_itemOut_tan "tan"
#define DEF_itemOut_nor "nor"
#define DEF_itemOut_bin "bin"
#define DEF_itemOut_osc "osc"
#define DEF_itemOut_iin "iin"
#define DEF_itemOut_ijn "jin"
#define DEF_itemOut_ikn "kin"

#define DEF_itemOut_len1 "len1"
#define DEF_itemOut_len2 "len2"
#define DEF_itemOut_dia1 "dia1"
#define DEF_itemOut_dia2 "dia2"
#define DEF_itemOut_dib1 "dib1"
#define DEF_itemOut_dib2 "dib2"
#define DEF_itemOut_circleDef "cdf"
#define DEF_itemOut_circleTop "cto"
#define DEF_itemOut_circleBut "cbu"

#define DEFout_SectionAttributes "cylat"
#define DEFout_LinearAttributes "linat"
#define DEF_Out_VolCyl "vol1"
#define DEF_Out_VolCone "vol2"
#define DEF_Out_FlexLine "flex1"
#define DEF_Out_FlexArea "flex2"
#define DEF_Out_TaperMean "taav"
#define DEF_Out_TaperMin "tami"
#define DEF_Out_TaperMax "tama"






// Constructor : initialization of parameters
LB_StepCharacterizeLog::LB_StepCharacterizeLog(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    //No Dialog Box
}

// Step description (tooltip of contextual menu)
QString LB_StepCharacterizeLog::getStepDescription() const
{
    return tr("Caractérisation du billon");
}

// Step detailled description
QString LB_StepCharacterizeLog::getStepDetailledDescription() const
{
    return tr("Cette étape permet de caractériser le(s) billon(s) présent(s) dans la scène <br>"
              "<br />"
              "En entrée, les données utilisées proviennent de nuages 3D décrivant des billons décomposés en  clusters horizontaux contigus sur lesquels des cercles ont été ajustés."
              "<br />"
              "En sortie, certaines caractéristiques sont rendues visibles à travers des objets les figurant, mais d'autres sont seulement numériques et sont accessibles via l'objet attribut et/ou sont affichées dans la fenêtre Log."
              "<br />Ces caractéristiques concernent:<br /> "
              "<ul>"
              "<li> l'<b>inclinaison</b> globale est établie à partir des centres des cercles d'extrémité dits de fin bout et gros bout. Trois visualisations lui sont associés : </li>"
              "<ul>"
              "<li> Un segment incliné </li>"
              "<li> Un segment horizontal, correspondant à la projection horizontale du précédent indiquant la direction et l'amplitude de l'inclinaison au niveau de la base du billon </li>"
              "<li> Un cercle horizontal à la base du billon dont le rayon correspond à la longueur du segment précédent </li>"
              "</ul>"
              "<li> la <b>flexuosité</b> définie à partir de la polyligne joignant les centres des cercles et exprimée par deux valeurs <\li>"
              "<ul>"
              "<li> la longueur de la polyligne à rapporter à la longueur du segment représentant l'inclinaison globale' </li>"
              "<li> l'aire de la surface gauche définie par la polyligne et le segment représentant l'inclinaison globale, calculée par triangulation' </li>"
              "</ul>"
              "<li> le <b>volume de bois rond </b> calculé de deux manières différentes, à partir de la somme des volumes de: </li>"
              "<ul>"
              "<li>  troncs de cône définis à partir de deux cercles consécutifs' </li>"
              "<li>  cylindres définis à partir de chaque cercle et de la distance le séparant du voisin' </li>"
              "</ul>"
              "<li> le <b>défilement </b> calculé à partir de la distance séparant deux cercles consécutifs, et de leurs rayons respectifs. Les valeurs moyenne, minimale et maximale sont restituées dana la liste d'attributs  <\li>"
              "<li> le <b>volume de bois scié  </b> calculé en tenant compte de la position de la section la plus décalée par rapport à l''inclinaison globale. Ce volume est calculé à partir la longueur du billon et de l'intersection du cercle associé à cette section avec les deux cercles correspondant aux extrémités du billon. Cette intersection peut aller de la section de fin bout à une valeur nulle, le cas général étant la réunion de quatre 1/4 d'ellipse. Chaque quart d'ellipse est définie pas les longueurs de ses demi-axes a et b, desquels il est possible de définir le rectangle d'aire maximal s'inscrivant à l'intérieur et qui a pour côtés a et b divisés par la racine carrée de 2. <\li>"
              "</ul>"

              );
}

// Step copy method
CT_VirtualAbstractStep* LB_StepCharacterizeLog::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LB_StepCharacterizeLog(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LB_StepCharacterizeLog::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_resSections = createNewInResultModel(DEFin_resSections, tr("resSections"), tr("résultat comportant des sections"));
    resIn_resSections->setRootGroup(DEFin_Section, CT_AbstractItemGroup::staticGetType(), tr("Section(Billon)"));
    resIn_resSections->addGroupModel(DEFin_Section, DEFin_Cluster, CT_AbstractItemGroup::staticGetType(), tr("Cluster(groupe)"), tr("Groupe contenant les clusters contenues dans la section"));
    //    resIn_resSections->addItemModel(DEFin_Cluster, DEFin_Cluster, CT_PointCluster::staticGetType(), tr("Cluster"), tr("Cluster horizontal"));
    resIn_resSections->addItemModel(DEFin_Cluster, DEFin_Circle, CT_Circle::staticGetType(), tr("Circle"), tr("Cercle ajusté sur un cluster horizontal"));

}

// Creation and affiliation of OUT models
void LB_StepCharacterizeLog::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_resLogsChar = createNewOutResultModel(DEFout_resLogsChar, tr("Caractéristiques des Billons"), tr("Caractéristiques Sections )\n"));
    res_resLogsChar->setRootGroup(DEFout_logChar, new CT_StandardItemGroup(), tr("Caractéristiques Billon"), tr("groupe contenant les characteristiques d'un billon"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_cir, new CT_Circle(), tr("Amplitude de l'inclinaison"), tr("Cercle représentant l'amplitude de l'inclinaison, centré sur la base du billon."));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_lin, new CT_Line(), tr("Projection horizontale inclinaison"), tr("Segment horizontal dont l'origine est le centre du bas du billon. Il indique l'amplitude de l'inclinaison totale par sa longueur, et sa direction"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_lincline, new CT_Line(), tr("Inclinaison gloable"), tr("segment inclinaison totale "));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_cyl, new CT_Cylinder(), tr("cylindre équivalent"), tr("cylindre de même volume que le billon et incliné de la même facon "));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_dia1, new CT_Line(), tr("Demi-diamètre a1"), tr("Demi-diamètre a1 d1/4 ellipse"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_dia2, new CT_Line(), tr("Demi-diamètre a2"), tr("Demi-diamètre a2 d1/4 ellipse"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_dib1, new CT_Line(), tr("Demi-diamètre b1"), tr("Demi-diamètre b1 d1/4 ellipse"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_dib2, new CT_Line(), tr("Demi-diamètre b2"), tr("Demi-diamètre b2 d1/4 ellipse"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_len1, new CT_Line(), tr("Longueur1"), tr("Longueur du tronçon inférieur"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_len2, new CT_Line(), tr("Longueur2"), tr("Longueur du tronçon supérieur"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_circleTop, new CT_Circle(), tr("Cercle fin bout"), tr("Cercle fin bout"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_circleDef, new CT_Circle(), tr("Cercle flèche max"), tr("Cercle où la flèche par rapport à  l'inclinaison globale est maximale"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEF_itemOut_circleBut, new CT_Circle(), tr("Cercle gros bout"), tr("Cercle gros bout"));
    res_resLogsChar->addItemModel(DEFout_logChar, DEFout_SectionAttributes, new CT_AttributesList(), tr("Attributs du Billon"));

    res_resLogsChar->addItemAttributeModel(DEFout_SectionAttributes, DEF_Out_VolCyl,
                                           new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), NULL, 0),
                                           tr("Volume1"));
    res_resLogsChar->addItemAttributeModel(DEFout_SectionAttributes, DEF_Out_VolCone,
                                           new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), NULL, 0),
                                           tr("Volume2"));
    res_resLogsChar->addItemAttributeModel(DEFout_SectionAttributes, DEF_Out_FlexLine,
                                           new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), NULL, 0),
                                           tr("Longueur Courbe"));
    res_resLogsChar->addItemAttributeModel(DEFout_SectionAttributes, DEF_Out_FlexArea,
                                           new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_AREA), NULL, 0),
                                           tr("Aire flexuosité"));
    res_resLogsChar->addItemAttributeModel(DEFout_SectionAttributes, DEF_Out_TaperMean,
                                           new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_AREA), NULL, 0),
                                           tr("Défilement Moyen"));
    res_resLogsChar->addItemAttributeModel(DEFout_SectionAttributes, DEF_Out_TaperMax,
                                           new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_AREA), NULL, 0),
                                           tr("Défilement Max"));
    res_resLogsChar->addItemAttributeModel(DEFout_SectionAttributes, DEF_Out_TaperMin,
                                           new CT_StdItemAttributeT<float>(NULL, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_AREA), NULL, 0),
                                           tr("Défilement Min"));

}

// Semi-automatic creation of step parameters DialogBox
void LB_StepCharacterizeLog::createPostConfigurationDialog()
{
    //No parameters for this step
}

void LB_StepCharacterizeLog::compute()
{
    Eigen::Vector3d centre;
    Eigen::Vector3d end;
    Eigen::Vector3d verticale (0.0,0.0,1.0);
    Eigen::Quaterniond zero4D (0.,0.,0.,0.);
    Eigen::Vector3d volumeCentre;
    Eigen::Vector3d volumeAxis(0.,0.,1.);
    Eigen::Vector3d vjDeflectionMax, vkDeflectionMax, positionWidthMax;
    Eigen::Vector3d ptIncline, previousPtIncline;// points correspondant au point de la polyligne sur la ligne d'inclinaison

    double zsol = 10000.;
    float disthor = 0.;

    float volumeHeight = 0;
    float volumeRadius = 0;
    float volumeLength = 0;
    float volume=0;
    float volume2=0;
    float deltaZ=0;
    float deltaR=0;
    float deltaTaper=0;
    float stumpRadius =0;
    float topRadius=0;
    float taperMean, taperMin, taperMax;
    float flexuosity(0), areaPolTriangle(0), areaIncTriangle(0);
    float deflection(0.), deflectionMax(0);
    float rayonMin(1000);
    float halfWidth(1000);
    int indDeflectionMax(0);


    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_resSections = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_resLogsChar = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Section(resIn_resSections, this, DEFin_Section);
    qint64 cptGroupIn_secs =0;

    while (itIn_Section.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_Section = (CT_AbstractItemGroup*) itIn_Section.next();

        qDebug() <<"CptSection" << cptGroupIn_secs+1;
        std::vector<Eigen::Quaterniond> Polyline(1);
        std::vector <double> Radii(1);
        qint64 indice= 0;
        double curvedAbscissa=0.;
        qint64 cptGroupIn_sec =0;
        Eigen::Vector3d viIncline, vjIncline, vkIncline;
        deltaZ=0;
        deltaR=0;
        zsol =10000.;
        cptGroupIn_secs++;
        volume =0;
        volume2=0;
        stumpRadius =0;
        topRadius =0;
        volumeHeight =0;
        taperMean =0.; taperMin=100; taperMax=0.;

        // Create sorted (by Z) circle list
        QList<CT_Circle*> circleList;
        CT_GroupIterator itIn_Cluster(grpIn_Section, this, DEFin_Cluster);
        while (itIn_Cluster.hasNext() && !isStopped())
        {
            const CT_AbstractItemGroup* grpIn_Cluster = (CT_AbstractItemGroup*) itIn_Cluster.next();
            CT_Circle* circle = (CT_Circle*)grpIn_Cluster->firstItemByINModelName(this, DEFin_Circle);

            if (circle != NULL)
            {
                circleList.append(circle);
            }
        }
        std::sort(circleList.begin(), circleList.end(), LB_StepCharacterizeLog::orderByIncreasingZ);

        if (circleList.size() > 1)
        {
            CT_Circle* lowerCircle = NULL;
            CT_Circle* upperCircle = NULL;

            for (int ic = 0 ; ic < circleList.size() - 1 ; ic++)
            {
                lowerCircle = circleList.at(ic);
                upperCircle = circleList.at(ic + 1);

                Polyline[indice].x() = upperCircle->getCenterX();
                Polyline[indice].y() = upperCircle->getCenterY();
                Polyline[indice].z() = upperCircle->getCenterZ();

                if(indice == 0)
                {
                    curvedAbscissa = 0.;
                }
                else
                {
                    curvedAbscissa += distance(Polyline[indice-1],Polyline[indice]);
                }

                Polyline[indice].w() = curvedAbscissa;
                Radii[indice] = upperCircle->getRadius();
                indice++;
                Polyline.push_back(zero4D);
                Radii.push_back(0);

                deltaZ = (float)(upperCircle->getCenterZ() - lowerCircle->getCenterZ());
                deltaR = (float)(upperCircle->getRadius() - lowerCircle->getRadius());
                deltaTaper = -deltaR/deltaZ;
                qDebug() << cptGroupIn_sec << "Def" << deltaTaper << "dR " << deltaR << "dZ" << deltaZ;
                taperMean += deltaTaper;

                if(deltaTaper > taperMax) taperMax =deltaTaper;
                if(deltaTaper < taperMin) taperMin =deltaTaper;

                // Calcul du correctif associé aux demi-épaisseurs de Cluster en début de section.
                if(cptGroupIn_sec ==2)
                {
                    stumpRadius = -deltaR/2 + lowerCircle->getRadius();
                    volume += volumeFrustum(stumpRadius, lowerCircle->getRadius(), deltaZ/2);
                    volumeHeight += deltaZ/2;
                }

                volume += volumeFrustum(lowerCircle->getRadius(),upperCircle->getRadius(),fabs(deltaZ));
                volume2 += volumeCylinder(upperCircle->getRadius(), deltaZ );
                volumeHeight += (float) fabs(lowerCircle->getCenterZ()-upperCircle->getCenterZ());//Incremental calculation of the height of the cylinder for verification purposes
            }

            // Calcul du correctif associé aux demi-épaisseurs de Cluster en fin de section
            topRadius = deltaR/2 + upperCircle->getRadius();
            volume += volumeFrustum(lowerCircle->getRadius(), topRadius, deltaZ/2.);
            //Ajout du coorectif associé au dernier cercle deltaz supposé identique au précédent
            volume2 += volumeCylinder(upperCircle->getRadius(), deltaZ);

            Polyline.pop_back();
            Radii.pop_back();
            int lend = Polyline.size();
            taperMean/=(float) (cptGroupIn_sec-1);


            //End of the iterations of the circles in the section
            //---------------------------------------------------

            CT_StandardItemGroup* grp_logChar= new CT_StandardItemGroup(DEFout_logChar, res_resLogsChar);
            CT_AttributesList* SectionAttributes = new CT_AttributesList(DEFout_SectionAttributes,res_resLogsChar );

            CT_Circle* bottomCircle = circleList.first();
            CT_Circle* topCircle = circleList.last();


            //Definition of the circle featuring the magnitude of incline
            centre = bottomCircle->getCenter();
            disthor =  sqrt(pow(topCircle->getCenterX()-bottomCircle->getCenterX(),2)+pow(topCircle->getCenterY()-bottomCircle->getCenterY(),2));

            CT_CircleData* circleincdata = new CT_CircleData(centre,verticale,disthor);
            CT_Circle* itemOut_cir = new CT_Circle(DEF_itemOut_cir, res_resLogsChar,circleincdata);
            //    grp_logChar->addItemDrawable(item_leanMag);
            grp_logChar->addItemDrawable(itemOut_cir);



            //            qDebug() << "Compute : Fin de dÃÂ©finition du cercle inclinaison";
            //            grp_logChar->addItemDrawable(itemOut_cir);
            //            qDebug() << "Compute :Ajout Ã  chasec du cercle inclinaison";

            // Definition of the line at the bottom of the tree showing the direction of leaning

            end = topCircle->getCenter();
            end.z() = bottomCircle->getCenterZ();

            CT_LineData *linchordata = new CT_LineData(centre, end);
            CT_Line* itemOut_lin = new CT_Line(DEF_itemOut_lin, res_resLogsChar, linchordata);
            grp_logChar->addItemDrawable(itemOut_lin);

            //definition of the line defining the global inclination
            viIncline.x() = topCircle->getCenterX() - bottomCircle->getCenterX();
            viIncline.y() = topCircle->getCenterY() - bottomCircle->getCenterY();
            viIncline.z() = topCircle->getCenterZ() - bottomCircle->getCenterZ();

            CT_LineData* lincline = new CT_LineData(bottomCircle->getCenter(), topCircle->getCenter());
            CT_Line* itemOut_lincline = new CT_Line(DEF_itemOut_lincline, res_resLogsChar, lincline);
            grp_logChar->addItemDrawable(itemOut_lincline);
            qDebug() << "Vecteur Inclinaison" << viIncline.x() << ";" << viIncline.y() << ";" << viIncline.z();
            viIncline.normalize();

            // qDebug() << "Compute : Fin calcul inclinaison";


            //Definition of  the equivalent cylinder to the volume of the trunk, with the same global inclination and the same length
            volumeCentre.x() = (topCircle->getCenterX()+bottomCircle->getCenterX())/2.;
            volumeCentre.y() = (topCircle->getCenterY()+ bottomCircle->getCenterY())/2.;
            volumeCentre.z() = (topCircle->getCenterZ()+bottomCircle->getCenterZ())/2.;
            volumeLength = (float) sqrt (pow((topCircle->getCenterX()-bottomCircle->getCenterX()),2)+ pow((topCircle->getCenterY()-bottomCircle->getCenterY()),2)+ pow((topCircle->getCenterZ()-bottomCircle->getCenterZ()),2));
            volumeAxis.x() = (topCircle->getCenterX()-bottomCircle->getCenterX())/volumeLength;
            volumeAxis.y() = (topCircle->getCenterY()-bottomCircle->getCenterY())/volumeLength;
            volumeAxis.z() = (topCircle->getCenterZ()-bottomCircle->getCenterZ())/volumeLength;
            volumeRadius = (float) sqrt(volume/volumeLength/M_PI);

            CT_CylinderData *cylinderincdata =new CT_CylinderData(volumeCentre,volumeAxis,volumeRadius,volumeLength);
            CT_Cylinder* itemOut_cyl = new CT_Cylinder(DEF_itemOut_cyl, res_resLogsChar,cylinderincdata);
            //               CT_AttributesList* SectionAttributes = new CT_AttributesList(DEFout_SectionAttributes,res_resLogsChar );

            grp_logChar->addItemDrawable(itemOut_cyl);



            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Section %1 Volume /Cone %2 m3").arg(cptGroupIn_secs,0,10).arg(volume,0,'g',6));
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Section %1 Volume /Cylindre %2 m3").arg(cptGroupIn_secs,0,10).arg(volume2,0,'g',6));
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Section %1 Défilement [m/m] Moyen %2 Min %3 Max %4 ").arg(cptGroupIn_secs,0,10).arg(taperMean,0,'g',6).arg(taperMin,0,'g',6).arg(taperMax,0,'g',6));


            SectionAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEF_Out_VolCyl,
                                                                                CT_AbstractCategory::DATA_VALUE,
                                                                                res_resLogsChar , volume2));
            SectionAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEF_Out_VolCone,
                                                                                CT_AbstractCategory::DATA_VALUE,
                                                                                res_resLogsChar , volume));
            SectionAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEF_Out_TaperMean,
                                                                                CT_AbstractCategory::DATA_VALUE,
                                                                                res_resLogsChar , taperMean));
            SectionAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEF_Out_TaperMin,
                                                                                CT_AbstractCategory::DATA_VALUE,
                                                                                res_resLogsChar , taperMin));
            SectionAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEF_Out_TaperMax,
                                                                                CT_AbstractCategory::DATA_VALUE,
                                                                                res_resLogsChar , taperMax));


            deflection=0.;
            deflectionMax=0.;
            rayonMin = 1000;
            halfWidth = 1000.;
            indDeflectionMax=0 ;
            flexuosity = 0.; areaPolTriangle=0.; areaIncTriangle =0;

            for (qint64 i= 0; i < (qint64)(Polyline.size()); i++)
            {

                // 1st Step to calculate the sawing volume (
                // Calcul du vecteur le plus court liant le point de la polyligne àla droite représentant l'inclinaison globale
                //viIncline est le vecteur directeur de l'inclinaison globale


                if(i >= 1 && i < ((int)Polyline.size() -1))
                {

                    //vjIncline est le vecteur joignat l'inclinaison globale à  la polyligne centrale
                    vjIncline = pointToLine(centre,viIncline,Polyline[i].vec());

                    // 3D coordinates of the point belonging to the segment global inclination
                    ptIncline.x() = Polyline[i].x() + vjIncline.x();
                    ptIncline.y() = Polyline[i].y() + vjIncline.y();
                    ptIncline.z() = Polyline[i].z() + vjIncline.z();

                    if(i== 1)
                    {
                        flexuosity = triangleArea(Polyline[i].vec(),ptIncline , Polyline[i-1].vec() );

                    }

                    else if(i== (qint64)(Polyline.size()-2))
                    {
                        areaPolTriangle = triangleArea(ptIncline,previousPtIncline, Polyline[i-1].vec());
                        areaIncTriangle = triangleArea(Polyline[i].vec(),ptIncline, Polyline[i-1].vec());
                        flexuosity += (areaPolTriangle +  areaIncTriangle + triangleArea(Polyline[i].vec(), ptIncline, Polyline[i+1].vec()));
                    }
                    else
                    {
                        areaPolTriangle = triangleArea(ptIncline,previousPtIncline, Polyline[i-1].vec());
                        areaIncTriangle = triangleArea(Polyline[i].vec(),ptIncline, Polyline[i-1].vec());
                        flexuosity += (areaPolTriangle +  areaIncTriangle);
                    }

                    deflection = vjIncline.norm();
                    vjIncline.normalize();
                    //vkIncline est le vecteur complétant le trièdre
                    vkIncline = viIncline.cross(vjIncline);


                    if (Radii[i] < rayonMin) rayonMin = Radii[i];
                    if (deflection > deflectionMax)
                    {
                        deflectionMax = deflection;
                        indDeflectionMax = i;
                        vjDeflectionMax = vjIncline;
                        vkDeflectionMax = vkIncline;
                    }

                    previousPtIncline = ptIncline;

                }

            }

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Section %1 Critere lineaire de flexuosite %2 m").arg(cptGroupIn_secs,0,10).arg(Polyline[lend-1].w(),0,'g',4));
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Section %1 Critere surfacique de flexuosite %2 m2").arg(cptGroupIn_secs,0,10).arg(flexuosity,0,'g',4));
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Section %1 Fleche maximale %2 m").arg(cptGroupIn_secs,0,10).arg(deflectionMax,0,'g',4));

            SectionAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEF_Out_FlexLine,
                                                                                CT_AbstractCategory::DATA_VALUE,
                                                                                res_resLogsChar , Polyline[lend-1].w()));
            SectionAttributes->addItemAttribute(new CT_StdItemAttributeT<float>(DEF_Out_FlexArea,
                                                                                CT_AbstractCategory::DATA_VALUE,
                                                                                res_resLogsChar , flexuosity));
            Eigen::Vector3d vk (0. , 0., 1.);
            double position (0.);
            Eigen::Vector3d Polyonvincline(Polyline[indDeflectionMax].vec().x()-Polyline[0].vec().x(),Polyline[indDeflectionMax].vec().y()-Polyline[0].vec().y(),Polyline[indDeflectionMax].vec().z()-Polyline[0].vec().z());
            double length1 (Polyonvincline.dot(viIncline));
            Eigen::Vector3d centreDebit( length1*viIncline.x() + Polyline[0].x(),length1*viIncline.y() + Polyline[0].y(),length1*viIncline.z() + Polyline[0].z() );
            double length2 = distance(Polyline[indDeflectionMax], Polyline[Polyline.size() -1]);

            // Calcul position du point d'intersection sur vjDeflectionMax
            double halfPeri = (deflectionMax + Radii[indDeflectionMax] + rayonMin)/2.0;// half Perimeter of a triangle
            double auxil = halfPeri*(halfPeri-deflectionMax)*(halfPeri-Radii[indDeflectionMax])*(halfPeri-rayonMin);
            qDebug()<< "Taille Polyligne" << Polyline.size();
            if (auxil >=0.0) {
                halfWidth = 2/deflectionMax*sqrt(auxil); //hauteur du triangle quelconque = Demi dimension max pour largeur
                position = sqrt(Radii[indDeflectionMax]*Radii[indDeflectionMax] - halfWidth*halfWidth);
                positionWidthMax.x() = Polyline[indDeflectionMax].vec().x() + vjDeflectionMax.x()*position;
                positionWidthMax.y() = Polyline[indDeflectionMax].vec().y() + vjDeflectionMax.y()*position;
                positionWidthMax.z() = Polyline[indDeflectionMax].vec().z() + vjDeflectionMax.z()*position;
            }
            else{
                positionWidthMax.x() = Polyline[Polyline.size()-1].vec().x();
                positionWidthMax.y() = Polyline[Polyline.size()-1].vec().y();
                positionWidthMax.z() = Polyline[Polyline.size()-1].vec().z();
                halfWidth =0.0;
            }

            Eigen::Vector3d* vLength1 = new Eigen::Vector3d(positionWidthMax.x()- viIncline.x()*length1,positionWidthMax.y()- viIncline.y()*length1, positionWidthMax.z()-viIncline.z()*length1);
            Eigen::Vector3d* vLength2 = new Eigen::Vector3d(positionWidthMax.x()+ viIncline.x()*length2,positionWidthMax.y()+ viIncline.y()*length2, positionWidthMax.z()+viIncline.z()*length2);
            Eigen::Vector3d* vDiaa1 = new Eigen::Vector3d(positionWidthMax.x() - vkDeflectionMax.x()*halfWidth,positionWidthMax.y()- vkDeflectionMax.y()* halfWidth, positionWidthMax.z()-vkDeflectionMax.z()*halfWidth);
            Eigen::Vector3d* vDiaa2 = new Eigen::Vector3d(positionWidthMax.x()+ vkDeflectionMax.x()*halfWidth,positionWidthMax.y()+ vkDeflectionMax.y()*halfWidth, positionWidthMax.z()+vkDeflectionMax.z()*halfWidth);
            Eigen::Vector3d* vDiab1 = new Eigen::Vector3d(centreDebit.x()+ vjDeflectionMax.x()*(Radii[indDeflectionMax] - deflectionMax) ,centreDebit.y()+ vjDeflectionMax.y()*(Radii[indDeflectionMax] - deflectionMax) , centreDebit.z()+vjDeflectionMax.z()*(Radii[indDeflectionMax] - deflectionMax) );
            Eigen::Vector3d* vDiab2 = new Eigen::Vector3d(centreDebit.x()- vjDeflectionMax.x()*rayonMin ,centreDebit.y()- vjDeflectionMax.y()*rayonMin, centreDebit.z()-vjDeflectionMax.z()*rayonMin);

            CT_LineData *length1Data = new CT_LineData(positionWidthMax, *vLength1);
            CT_LineData *length2Data = new CT_LineData(positionWidthMax, *vLength2);
            CT_LineData *Diaa1Data = new CT_LineData(positionWidthMax, *vDiaa1);
            CT_LineData *Diaa2Data = new CT_LineData(positionWidthMax, *vDiaa2);
            CT_LineData *Diab1Data = new CT_LineData(positionWidthMax, *vDiab1);
            CT_LineData *Diab2Data = new CT_LineData(positionWidthMax, *vDiab2);
            CT_CircleData *circleTopEndData = new CT_CircleData(Polyline[Polyline.size() -1].vec(), vk, Radii[Polyline.size()-1]);
            CT_CircleData *circleMaxDeflectionData = new CT_CircleData(Polyline[indDeflectionMax].vec(), vk, Radii[indDeflectionMax]);
            CT_CircleData *circleButtEndData = new CT_CircleData(Polyline[0].vec(), vk, Radii[0]);

            // Computation of the half-Diameters of the 4 fourth of ellipse defining the sawable portion of the section

            double a1 = CT_MathPoint::distance3D(positionWidthMax, *vDiab1);
            double a2 = CT_MathPoint::distance3D(positionWidthMax, *vDiab2);
            double b1 = CT_MathPoint::distance3D(positionWidthMax, *vDiaa1);
            double b2 = CT_MathPoint::distance3D(positionWidthMax, *vDiaa2);

            double length = length1+length2;
            double sawnVolume = length/2*(a1*b1+a1*b2+a2*b1+a2*b2);
            if ((a1*b1*a2*b2)< 1.0e-08){
                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Section %1 Attention Problème Volume Sciage").arg(cptGroupIn_secs,0,10));
            }
            else {
                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr(" Section %1 Volume Sciage %2 [m3] ( l: %3 a1: %4  a2: %5 b1: %6 b2: %7 m)").arg(cptGroupIn_secs,0,10).arg(sawnVolume,0,'g',4).arg(length,0,'g',4).arg(a1,0,'g',4).arg(a2,0,'g',4).arg(b1,0,'g',4).arg(b2,0,'g',4));
            }
            CT_Line *itemOut_diaa1 = new CT_Line(DEF_itemOut_dia1,res_resLogsChar,Diaa1Data);
            CT_Line *itemOut_diaa2 = new CT_Line(DEF_itemOut_dia2,res_resLogsChar,Diaa2Data);
            CT_Line *itemOut_diab1 = new CT_Line(DEF_itemOut_dib1,res_resLogsChar,Diab1Data);
            CT_Line *itemOut_diab2 = new CT_Line(DEF_itemOut_dib2,res_resLogsChar,Diab2Data);
            CT_Line *itemOut_length1 = new CT_Line(DEF_itemOut_len1,res_resLogsChar,length1Data);
            CT_Line *itemOut_length2 = new CT_Line(DEF_itemOut_len2,res_resLogsChar,length2Data);
            CT_Circle *itemOut_circleTop = new CT_Circle(DEF_itemOut_circleTop,res_resLogsChar,circleTopEndData);
            CT_Circle *itemOut_circleDef = new CT_Circle(DEF_itemOut_circleDef,res_resLogsChar,circleMaxDeflectionData);
            CT_Circle *itemOut_circleBut = new CT_Circle(DEF_itemOut_circleBut,res_resLogsChar,circleButtEndData);

            grp_logChar->addItemDrawable(itemOut_diaa1);
            grp_logChar->addItemDrawable(itemOut_diaa2);
            grp_logChar->addItemDrawable(itemOut_diab1);
            grp_logChar->addItemDrawable(itemOut_diab2);
            grp_logChar->addItemDrawable(itemOut_length1);
            grp_logChar->addItemDrawable(itemOut_length2);
            grp_logChar->addItemDrawable(itemOut_circleTop);
            grp_logChar->addItemDrawable(itemOut_circleDef);
            grp_logChar->addItemDrawable(itemOut_circleBut);
            grp_logChar->addItemDrawable(SectionAttributes);
            res_resLogsChar->addGroup(grp_logChar);
        }

    } //End of the section iteration
}


/*
  Compute the volume of a frustum of a right circular cone
 */
float LB_StepCharacterizeLog::volumeFrustum( float buttRadius, float topRadius, float height)
{
    float volume =0;
    volume = M_PI*height/3*(buttRadius*buttRadius + buttRadius*topRadius + topRadius*topRadius);
    return(volume);
}

/*
 Compute the volume of a cylinder
*/
float LB_StepCharacterizeLog::volumeCylinder( float radius,  float height)
{
    float volume =0;
    volume = M_PI*height*(radius*radius);
    return(volume);
}


/*
Compute the distance between two 4D points(x,y,z,s)
*/
double LB_StepCharacterizeLog::distance(  Eigen::Quaterniond  p1 ,  Eigen::Quaterniond  p2)
{
    double dist=0.;
    dist = sqrt(pow(p1.x() - p2.x(), 2)+ pow(p1.y() -p2.y(),2)+pow(p1.z() -p2.z(),2));
    return dist;
}



/*
Compute the 3D vector orthogonal to a line defined by a 3D point as origin and a 3D unit vector as direction and joining a 3D point
*/
Eigen::Vector3d LB_StepCharacterizeLog::pointToLine(const Eigen::Vector3d lineOrigin, const Eigen::Vector3d  lineDirection, const Eigen::Vector3d Point3D)
{

    Eigen::Vector3d  vaux(lineOrigin.x()-Point3D.x(),lineOrigin.y()-Point3D.y(),lineOrigin.z()-Point3D.z());

    Eigen::Vector3d resultat = vaux - lineDirection*vaux.dot(lineDirection);

    return(resultat);

}


/*
Compute the area of a triangle by HÃÂ©ron's formula from the  coordinates of the 3 vertices
*/
double LB_StepCharacterizeLog::triangleArea(const Eigen::Vector3d vertexA, const Eigen::Vector3d  vertexB, const Eigen::Vector3d vertexC)
{
    double area(0), halfPerimeter (0);
    double edgeA(0), edgeB(0), edgeC(0);

    edgeA = CT_MathPoint::distance3D(vertexB, vertexC);
    edgeB = CT_MathPoint::distance3D(vertexA, vertexC);
    edgeC = CT_MathPoint::distance3D(vertexA, vertexB);

    //qDebug() << "A" << edgeA << "B" << edgeB << "C" << edgeC ;
    halfPerimeter = (edgeA+edgeB+edgeC)/2.0;
    area = sqrt(halfPerimeter*(halfPerimeter - edgeA)*(halfPerimeter - edgeB)*(halfPerimeter - edgeC));
    return(area);
}
