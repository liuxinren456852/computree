CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += concurrent

TARGET = plug_artsfree

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    arfr_pluginentry.h \
    arfr_steppluginmanager.h \
    step/arfr_stephorizontalclustering04.h \
    step/arfr_stephorizontalclustering05.h
SOURCES += \
    arfr_pluginentry.cpp \
    arfr_steppluginmanager.cpp \
    step/arfr_stephorizontalclustering04.cpp \
    step/arfr_stephorizontalclustering05.cpp

TRANSLATIONS += languages/pluginartsfree_en.ts \
                languages/pluginartsfree_fr.ts

