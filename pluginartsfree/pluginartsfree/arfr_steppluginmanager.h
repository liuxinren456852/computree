/****************************************************************************
 Copyright (C) 2010-2012 the Association de Recherche Technologie et Sciences (ARTS), Ecole Nationale Supérieure d'Arts et Métiers (ENSAM), Cluny, France.
                         All rights reserved.

 Contact : Michael.KREBS@ENSAM.EU

 Developers : Michaël KREBS (ARTS/ENSAM)

 This file is part of PluginARTSFREE library.

 PluginARTSFREE is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginARTSFREE is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginARTSFREE.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ARFR_STEPPLUGINMANAGER_H
#define ARFR_STEPPLUGINMANAGER_H

#include "ct_abstractstepplugin.h"

class ARFR_StepPluginManager : public CT_AbstractStepPlugin
{
public:
    ARFR_StepPluginManager();
    ~ARFR_StepPluginManager();

    QString getPluginURL() const {return QString("http://rdinnovation.onf.fr/projects/plugin-arts-free/wiki");}

    QString getPluginRISCitation() const;
protected:

    bool loadGenericsStep();
    bool loadOpenFileStep();
    bool loadCanBeAddedFirstStep();
    bool loadActions();
    bool loadExporters();
    bool loadReaders();
};

#endif // ARFR_STEPPLUGINMANAGER_H
