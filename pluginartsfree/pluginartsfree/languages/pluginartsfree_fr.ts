<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ARFR_StepHorizontalClustering04</name>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="70"/>
        <source>Clustering par tranches horizontales (v4)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="75"/>
        <source>Cette étape vise à constituer de petits groupes de points aggrégés. L&apos;idée est d&apos;obtenir, dans le cas de troncs d&apos;arbres des arcs de cercle peu épais.&lt;br&gt;Pour ce faire, l&apos;étape fonctionne en deux étapes :&lt;ul&gt;&lt;li&gt; La scène est découpée en tranches horizontales (Layers) de l&apos; &lt;b&gt;épaisseur&lt;/b&gt; choisie&lt;/li&gt;&lt;li&gt; Dans chacune des tranches, les points sont aggrégés en clusters en fonction de leur espacement en (x,y)&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;La &lt;b&gt;distance maximale séparant deux points d&apos;un même groupe&lt;/b&gt; est spécifiée en paramètre.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="95"/>
        <source>Scène(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="97"/>
        <source>Groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="98"/>
        <source>Scène à clusteriser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="105"/>
        <source>Distance maximum pour intégrer un point à un groupe :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="106"/>
        <source>Epaisseur des tranches horizontales :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="111"/>
        <source>Scène clusterisée</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="113"/>
        <source>Niveau Z (Grp)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="114"/>
        <source>Cluster (Grp))</source>
        <translation>Cluster (Grp)</translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="115"/>
        <source>Points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="153"/>
        <source>La scène à clusteriser comporte %1 points.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering04.cpp" line="235"/>
        <source>L&apos;étape a généré %1 couches horizontales.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ARFR_StepHorizontalClustering05</name>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="68"/>
        <source>Clustering par tranches horizontales (v5)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="73"/>
        <source>Cette étape vise à constituer de petits groupes de points aggrégés. L&apos;idée est d&apos;obtenir, dans le cas de troncs d&apos;arbres des arcs de cercle peu épais.&lt;br&gt;Pour ce faire, l&apos;étape fonctionne en deux étapes :&lt;ul&gt;&lt;li&gt; La scène est découpée en tranches horizontales (Layers) de l&apos; &lt;b&gt;épaisseur&lt;/b&gt; choisie&lt;/li&gt;&lt;li&gt; Dans chacune des tranches, les points sont aggrégés en clusters en fonction de leur espacement en (x,y)&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;La &lt;b&gt;distance maximale séparant deux points d&apos;un même groupe&lt;/b&gt; est spécifiée en paramètre.&lt;br&gt;Cette version 05 de l&apos;étape permet de traiter séparément chaque scène d&apos;entrée.&lt;br&gt;De plus dans cette version, le résultat d&apos;entrée est en copie.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="95"/>
        <source>Scène(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="97"/>
        <source>Groupe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="98"/>
        <source>Scène à clusteriser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="105"/>
        <source>Distance maximum pour intégrer un point à un groupe :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="106"/>
        <source>Epaisseur des tranches horizontales :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="115"/>
        <source>Niveau Z (Grp)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="116"/>
        <source>Cluster (Grp)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="117"/>
        <source>Points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="158"/>
        <source>La scène à clusteriser comporte %1 points.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/arfr_stephorizontalclustering05.cpp" line="240"/>
        <source>L&apos;étape a généré %1 couches horizontales.</source>
        <translation></translation>
    </message>
</context>
</TS>
