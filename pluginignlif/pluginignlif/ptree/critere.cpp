#include "critere.h"

#include <QDebug>

using namespace std;

double getScore(vector<Eigen::Vector3d> &branche, vector<Eigen::Vector3d> &bound, int k, Param &p)
{   qDebug()<<"In get score ";
    if(branche.size()<4){return 0.0;}
    double score, pT, pCH, pEL, pCG;
    qDebug()<<" calcul ";
    qDebug()<<"bound.size()"<<bound.size()<< "k= " << k << "branche.size()"<<branche.size();
    pT  = getScoreT(branche, bound, k, p);
    qDebug()<<" retour du score pT = "<<pT;
    pCH = getScoreCH(branche, bound);
    qDebug()<<" retour du score pCH = "<<pCH;
    pEL = getScoreEL(branche);
    qDebug()<<" retour du score pEL = "<<pEL;
    pCG = getScoreCG(branche, bound);
    qDebug()<<" retour du score  pCG = "<< pCG;

    score = (pCH+pEL+pCG+pT)/4;
    qDebug()<<" retour du score = "<<score;
    return score;
}

double getScoreT(vector<Eigen::Vector3d> &branche,vector<Eigen::Vector3d> &bound, int k, Param &p)
{
    double pT;
    qDebug()<<"In getScoreT";
    // Heigh computation
    qDebug()<<"Heigh computation";
    double height = branche[0][2];
    int length = branche.size();
    qDebug()<<"Heigh = "<< height ;
    // convexHull area computation
    double area;
    area  = ConvexHull::computeArea(bound);
    qDebug()<<"area = "<< area ;
    // Score computation
    double coef;
    int nbPts;


// Number of points
    double d = p.getdensity();
    coef    = d*log(height);
    if(coef<0){coef=0;}
    nbPts = (coef*k);


    pT = (length - nbPts/3)/(nbPts*2/3);

    if(pT<0){pT=0;}
    if(pT>1){pT=1;}


    return pT;
}
double getScoreCH(vector<Eigen::Vector3d> &branche, vector<Eigen::Vector3d> &bound)
{
    double pCH;
    const double Pi = 3.141592653589793238462;

    //CH area
    double dmax, aireCH, aireCercle;
    dmax    =  0 ;
    bound   =  ConvexHull::buildConvexHull(branche);
    aireCH  =  ConvexHull::computeArea(bound);

    //Circle area
    unsigned int pts;
    int queue;
    queue = floor(branche.size()*0.05);
    std::vector<double> d;
    for(pts=1;pts<branche.size();pts++){

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // ERREUR PROBABLE : deux fois X dans la distance
        // d.push_back(Distance(Eigen::Vector2d(branche[pts](0),branche[pts](0) ), Eigen::Vector2d(branche[0](0),branche[0](0))));

        d.push_back(sqrt(pow(branche[pts](0) - branche[0](0), 2) + pow(branche[pts](1) - branche[0](1), 2)));
        sort(d.begin() ,d.end());
    }
    if(branche.size()==1){
        d.push_back(1000000);
    }
    dmax       =  d.at(d.size()-1- queue);
    aireCercle =  Pi * dmax*dmax;

    // Score
    pCH = aireCH/aireCercle;
    if(pCH>1){pCH=1;}

    return pCH;
}
double getScoreEL(vector<Eigen::Vector3d> &branche)
{
    double  pEL;

    // Averages computation
    double mx,my;
    mx = 0.0;
    my = 0.0;
    unsigned int i;
    for (i=0;i<branche.size();i++){
        mx += branche[i][0];
        my += branche[i][1];
    }
    mx = mx/branche.size();
    my = my/branche.size();

    //  coef of covariance matrix

    double c11=0.0, c12=0.0, c21=0.0, c22=0.0;
    for (i=0;i<branche.size();i++){
        c11 += (branche[i][0]-mx)*(branche[i][0]-mx);
        c12 += (branche[i][0]-mx)*(branche[i][1]-my);
        c22 += (branche[i][1]-my)*(branche[i][1]-my);
    }
    c11 = c11/(branche.size()-1);
    c12 = c12/(branche.size()-1);
    c22 = c22/(branche.size()-1);
    c21 = c12;

    //Score

    pEL= min(c11,c22)/max(c11,c22);

    return pEL;
}
double getScoreCG(vector<Eigen::Vector3d> &branche, vector<Eigen::Vector3d> &bound)
{
    double pCG;

    double xS,yS;
    xS = branche[0][0];
    yS = branche[0][1];


    //// Vertex relative to the gravity center


    // gravity center computation

    double x_bar,y_bar;
    x_bar=0.0;
    y_bar=0.0;
    unsigned int i;
    for (i=1;i<branche.size();i++){

        x_bar += branche[i][0]/branche.size();
        y_bar += branche[i][1]/branche.size();
    }

    // Distance to the vertex

    double distSCG  = sqrt(pow(x_bar - xS,2) + pow(y_bar - yS,2));

    // average distance to convexHull
    double dmoy = 0.0;
    for(i=0;i<bound.size();i++){
        dmoy += sqrt(pow(bound[i](0) - xS,2) + pow(bound[i](1) - yS,2));
    }
    dmoy = dmoy / bound.size();

    //score
    double index;
    index     = distSCG/dmoy;
    if (index>0.5){
        index = 0.5;
    }
    pCG       = 1.0-2*index;

    return pCG;
}
