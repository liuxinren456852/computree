/****************************************************************************
* MeshLab                                                           o o     *
* A versatile mesh processing toolbox                             o     o   *
*                                                                _   O  _   *
* Copyright(C) 2005                                                \/)\/    *
* Visual Computing Lab                                            /\/|      *
* ISTI - Italian National Research Council                           |      *
*                                                                    \      *
* All rights reserved.                                                      *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *   
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
* for more details.                                                         *
*                                                                           *
****************************************************************************/
/****************************************************************************
New version of the PTrees algorithm

Ahmed HAMROUNI 09-2014
****************************************************************************/

#include <QtGui>

#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <limits>

#include "filter_segDyn.h"
#include "tools.h"
#include "segmentation.h"
#include "param.h"
#include "point.h"
#include "sommet.h"
#include "arbre.h"
#include "foret.h"

#include <algorithm>
#include <vector>
#include <list>
#include <cfloat>
#include <fstream>

#include <QDebug>


using namespace std;



PTREESPlugin::PTREESPlugin()
{
    typeList << FP_PTREES;

    foreach(FilterIDType tt , types())
        actionList << new QAction(filterName(tt), this);
}


// ST() must return the  very short string describing each filtering action
// (this string is used also to define the menu entry)
QString PTREESPlugin::filterName(FilterIDType filterId) const
{
    switch(filterId) {
    case FP_PTREES :  return QString("PTREES");
    default : assert(0);
    }
    return QString();
}



// Info() must return the longer string describing each filtering action
// (this string is used in the About plugin dialog)
QString PTREESPlugin::filterInfo(FilterIDType filterId) const
{
    switch(filterId) {
    case FP_PTREES :  return QString("Identify individual trees in a Lidar 3D Point cloud aquired over a forested area.");
    default : assert(0);
    }
    return QString("Unknown Filter");
}



// The FilterClass describes in which generic class of filters it fits.
// This choice affect the submenu in which each filter will be placed
// More than a single class can be choosen.
PTREESPlugin::FilterClass PTREESPlugin::getClass(QAction *a)
{
    switch(ID(a))
    {
    case FP_PTREES :  return MeshFilterInterface::PointSet;
    default : assert(0);
    }
    return MeshFilterInterface::Generic;
}


// This function define the needed parameters for each filter. Return true if the filter has some parameters
// it is called every time, so you can set the default value of parameters according to the mesh
// For each parameter you need to define,
// - the name of the parameter,
// - the string shown in the dialog
// - the default value
// - a possibly long string describing the meaning of that parameter (shown as a popup help in the dialog)
void PTREESPlugin::initParameterSet(QAction *action,MeshModel &m, RichParameterSet & parlst)
{
    switch(ID(action))	 {
    case FP_PTREES:
        // 		  parlst.addParam   (new RichBool ("UpdateNormals",
        //											true,
        //											"Recompute normals",
        //											"Toggle the recomputation of the normals after the random displacement.\n\n"
        //											"If disabled the face normals will remains unchanged resulting in a visually pleasant effect."));
        //			parlst.addParam(new RichAbsPerc("Displacement",
        //												m.cm.bbox.Diag()/100.0,0,m.cm.bbox.Diag(),
        //												"Max displacement",
        //												"The vertex are displaced of a vector whose norm is bounded by this value"));
        parlst.addParam(new RichInt("PtGrdX",100,"Number Cells on X","Resolution of the grid according to X axis")) ;

        parlst.addParam(new RichInt("PtGrdY",100,"Number Cells on Y","Resolution of the grid according to Y axis")) ;

        parlst.addParam(new RichInt("PtGrdZ",100,"Number Cells on Z","Resolution of the grid according to Z axis")) ;

        parlst.addParam(new RichInt("knnMin", 10,"Minimum Number of Neighbors", "Number of Points used to analyse the point local environment")) ;

        parlst.addParam(new RichInt("knnMax",30, "Maximum Number of Neighbors","Number of Points used to analyse the point local environment")) ;

        parlst.addParam(new RichInt("Scale",5,"Scale","variation of the number of neighbors between two scales")) ;

        parlst.addParam(new RichFloat("Density",2.0,"Density","Density")) ;

        parlst.addParam(new RichFloat("MinDist",1.0,"Minimum Distance between apices","Minimum Distance between 2 neighboring apices")) ;

        parlst.addParam(new RichFloat("MinH",3.0,"Minimum height","Minimum height to be processed")) ;

        parlst.addParam(new RichInt("GRD",1,"Ground treatment","Ground treatment")) ;

        parlst.addParam(new RichFloat("CoefCompa",1,"CoefComparison","Coefficient used for comparison beetween two scales")) ;


        break;

    default : assert(0);
    }
}



// The Real Core Function doing the actual mesh processing.
// Move Vertex of a random quantity
bool PTREESPlugin::applyFilter(QAction */*filter*/, MeshDocument &md, RichParameterSet & par)
{
    clock_t start_ = clock();
    // Redirecting log to a file
    ofstream file;
    file.open ("log.txt");
    qDebug().rdbuf(file.rdbuf());
    qDebug()<<" file open SegDyn 09/2014";


    //// Initialization

    // Defining the parameters of the algorithm
    qDebug()<<"Defining the parameters of the algorithm";
    Param param;
    param.setbound(Eigen::Vector3i( par.getInt("PtGrdX"), par.getInt("PtGrdY"), par.getInt("PtGrdZ")));
    param.setminDist(par.getFloat("MinDist"));
    param.setminH(par.getFloat("MinH"));
    param.setdensity(par.getFloat("Density"));
    param.setneighborV(Eigen::Vector2i(par.getInt("knnMin"), par.getInt("knnMax")));
    param.setscale(par.getInt("Scale"));
    param.setGrd(par.getInt("GRD"));

    // Assign in m the mesh contained in md
    qDebug()<<"Assign in m the mesh contained in md";
    MeshModel* m=md.mm();
    MeshModel* mSort = md.addNewMesh("","mesh sort");
    CMeshO::PerVertexAttributeHandle< int > label = vcg::tri::Allocator<CMeshO>::AddPerVertexAttribute< int >  (mSort->cm,std::string("label")) ;
    CMeshO::PerVertexAttributeHandle< int > isSommet = vcg::tri::Allocator<CMeshO>::AddPerVertexAttribute< int >  (mSort->cm,std::string("isSommet")) ;
    CMeshO::PerVertexAttributeHandle< int > isSommetv2 = vcg::tri::Allocator<CMeshO>::AddPerVertexAttribute< int >  (mSort->cm,std::string("isSommetv2")) ;

    // vector : type generique c++ pour stocker des vecteurs
    std::vector<Eigen::Vector4d> listVert;
    Eigen::Vector4d pointXYZH;
    unsigned int i;
    for(i = 0; i< m->cm.vert.size(); i++)
    {
        pointXYZH  = Eigen::Vector4d((m->cm.vert[i].P()[0]),(m->cm.vert[i].P()[1]),(m->cm.vert[i].P()[2]),(m->cm.vert[i].Q()));
        listVert.push_back(pointXYZH);
    }

    //classe toute la liste en utilisant l'objet myobject
    sort(listVert.begin(), listVert.end(), compare4fby3);

    //  On replace le tout dans un mesh trie.
    unsigned int j;
    for(j=0;j<listVert.size();j++)
    {
        // allocation d'un vertex
        vcg::tri::Allocator<CMeshO>::AddVertices(mSort->cm, 1);
        // édition des coordonnées 3D du vertex
        mSort->cm.vert[mSort->cm.vn-1].P()         = Eigen::Vector3d(listVert.at(j)[0], listVert.at(j)[1], listVert.at(j)[2]);
        mSort->cm.vert[mSort->cm.vn-1].Q()         = listVert.at(j)[3];
        label[mSort->cm.vert[mSort->cm.vn-1]]      = -1 ;
        isSommet[mSort->cm.vert[mSort->cm.vn-1]]   = -1 ;
        isSommetv2[mSort->cm.vert[mSort->cm.vn-1]] = -1 ;
    }


    //// Traitement
    qDebug()<<"Traitement";
    ///cb(0,"Processing...");
    Eigen::Vector2i neighborV = param.getneighborV(); // a recup
    int scale              = param.getscale(); // a recup
    int k                  = neighborV[1];

    // Segmentations
    vector <Foret> resultats;

    Segmentation Segm;
    vector<Point> Nuage;

    for (unsigned int i=0;i<listVert.size();i++)
    {
        Point Ptmp(Eigen::Vector3d(listVert[i][0],listVert[i][1],listVert[i][2]),listVert[i][3],i);
        Nuage.push_back(Ptmp);
    }
    Segm._Nuage=Nuage;

    //Normal
    ///cb(0,"Segmentation");
    Foret forest;

    while(k >= neighborV[0])
    {
        Segm.nb_lab=0;

        clock_t start = clock();
        qDebug()<<"__________________ new scale :  "<< k <<"__________________";
        forest = Segm.segmPPL(m,mSort, label,forest,k, param);
        qDebug()<<"__________________Fin segmentation scale : "<<k<< "_______________________";

        clock_t end  = clock();
        double delay = double((double)(end - start) / CLOCKS_PER_SEC);
        qDebug()<<"delay segmentation : "<< delay/60;

        qDebug()<< " Mise a jour des labels";

                for(unsigned int e=0;e<Segm._Nuage.size();e++)
                {
                    label[mSort->cm.vert[e]]=-1;
                }
                int nbarbres = forest.getNbTrees();
                for(int e=0;e<nbarbres;e++)
                {
                    Arbre Treetmp = forest.extractTree(e);
                    int indicesommet = Treetmp.getIndiceSommet();
                    if(isSommetv2[indicesommet]<0)
                    {
                        isSommetv2[indicesommet]= k;
                    }
                }
                resultats.push_back(forest);
                forest.clearForest();
        k       = k - scale;
    }
//      Affichage du contenu du vecteur de resultats
        qDebug()<<"Fin des segmentation";
        qDebug()<<"Selection des Apex";
        ///cb(0,"Selection des Apex...");
//     Selection des sommets
        Foret newforest = Segm.getApex(resultats,param);

        vector<Sommet> Sommets = newforest.getListeSommets();
        int e =0;

        for(vector<Sommet>::iterator it=Sommets.begin();it!=Sommets.end();++it)
        {
            Sommet sommettmp = *it;
            int labelsommet = sommettmp.getlab();
            qDebug()<<"labelsommet : "<<labelsommet;
            int indicesommet = sommettmp.getIndice();
            label[mSort->cm.vert[indicesommet]]= labelsommet;
            isSommet[mSort->cm.vert[indicesommet]]= e;
            e++;
        }
        // Segmentation finale
        ///cb(0,"Segmentation finale...");
        clock_t start = clock();
        Segm.segmFIN(m, mSort, Sommets, label,isSommet,isSommetv2, param);
        clock_t end  = clock();
        double delay = double((double)(end - start) / CLOCKS_PER_SEC);
        qDebug()<<"delay segmentation : "<< delay/60;



        clock_t end_  = clock();
        double delay_ = double((double)(end_ - start_) / CLOCKS_PER_SEC);
        qDebug()<<"delay Ptrees : "<< delay_/60;
    return true;
}

QString PTREESPlugin::filterScriptFunctionName( FilterIDType filterID )
{
    switch(filterID) {
    case FP_PTREES :  return QString("IndividualTreeCrownExtractionFromLidarPoints");
    default : assert(0);
    }
    return QString();
}



Q_EXPORT_PLUGIN(PTREESPlugin)
