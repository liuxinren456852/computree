#include "point.h"

Point::Point():_label(-1)/*,_numCh(-1)*/
{
}

Point::Point(int indice):_label(-1)/*,_numCh(-1)*/,_indice(indice)
{
}
Point::Point(Eigen::Vector3d coord,double height,int indice):_label(-1)/*,_numCh(-1)*/,_indice(indice),_3dcoord(coord),_height(height)
{
}
const Eigen::Vector3d&  Point::getcoord() const
{
    return _3dcoord;

}

void  Point::setcoord(Eigen::Vector3d coord)
{
     _3dcoord= coord;
}

int  Point::getlab()
{
    return _label;
}

void Point::setlab(int label)
{
    _label=label;
}

//int  Point::getCh()
//{
//    return _numCh;
//}

//void  Point::setCh(int num)
//{
//    _numCh=num;
//}

int Point::getIndice()
{
    return _indice;
}

void Point::setIndice(int ind)
{
    _indice=ind;
}
double Point::getHeight()
{
    return _height;
}

void Point::setHeight(double h)
{
    _height=h;
}
