#ifndef LIF_STEPPTREESEGMENTATION_H
#define LIF_STEPPTREESEGMENTATION_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"


class LIF_StepPTreeSegmentation: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    LIF_StepPTreeSegmentation(CT_StepInitializeData &dataInit);

    ~LIF_StepPTreeSegmentation();

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     * 
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    QString _scales;
    double _distDominated;
    double _resSolidityGrid;
    double _resGrid;

    CT_AutoRenameModels     _outGroupModelName;
    CT_AutoRenameModels     _outSceneModelName;
    CT_AutoRenameModels     _outCHModelName;

    CT_AutoRenameModels     _outScene_score_size_ModelName;
    CT_AutoRenameModels     _outScene_score_regularity_ModelName;
    CT_AutoRenameModels     _outScene_score_solidity_ModelName;
    CT_AutoRenameModels     _outScene_score_summitOffset_ModelName;
    CT_AutoRenameModels     _outScene_score_ModelName;
    CT_AutoRenameModels     _outScene_k_ModelName;



    static bool sortInReverseOrder(int a, int b)
    {
        return a > b;
    }


};

#endif // LIF_STEPPTREESEGMENTATION_H
