#ifndef PARAM_H
#define PARAM_H

#include "Eigen/Core"
#include <QList>

class Param
{

public :

    Param(const QList<int> &scale, double distDominated, double resSolidityGrid, double resolution);

    QList<int>             _scales;
    double          _distDominated;
    double          _resSolidityGrid;
    double          _resolution;

};


#endif // PARAM_H
