/****************************************************************************
 Copyright (C) 2016 the Institut National de l'Information Geographique et Forestière (IGN), France
                         All rights reserved.

 Contact : cedric.vega@ign.fr

 Developers : Cedric Véga (IGN)
              Jules Morel (IFP / IGN)
              Alexandre Piboule (ONF - transfert in Computree)

 This file is part of PluginIGNLIF library.

 PluginIGNLIF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIGNLIF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginIGNLIF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef LIF_STEPDTM_H
#define LIF_STEPDTM_H

#include "ct_step/abstract/ct_abstractstep.h"

class LIF_StepDTM : public CT_AbstractStep
{
    // IMPORTANT in order to obtain step name
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    LIF_StepDTM();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    double  _resolution;
    double  _thresholdLowPoints;
    double  _threshold;
    double  _thresholdFine;

};
#endif // LIF_STEPDTM_H
