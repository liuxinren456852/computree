#ifndef FOREST_H
#define FOREST_H

#include "tree.h"

#include <QVector>

class Forest
{
public:
    Forest(int pointNb);

    QList<Tree*>    _trees;
    QVector<int>    _labels;
};

#endif // FOREST_H
