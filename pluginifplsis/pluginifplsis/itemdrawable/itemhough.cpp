#include "itemhough.h"

const itemHoughDrawManager itemHough::dem_hough_DRAW_MANAGER;

itemHough::itemHough() : CT_AbstractItemDrawableWithoutPointCloud()
{
    //poly = NULL;
    setBaseDrawManager(&dem_hough_DRAW_MANAGER);
}

itemHough::itemHough(const CT_OutAbstractModel *model,
                                       const CT_AbstractResult *result,
                               hough *pH) : CT_AbstractItemDrawableWithoutPointCloud(model, result), h(*pH)
{
    //poly = *polygon;
    setBaseDrawManager(&dem_hough_DRAW_MANAGER);
}

itemHough::itemHough(const QString &modelName,
                                       const CT_AbstractResult *result,
                               hough *pH) : CT_AbstractItemDrawableWithoutPointCloud(modelName, result), h(*pH)
{
    //poly = *polygon;
    setBaseDrawManager(&dem_hough_DRAW_MANAGER);
}

itemHough::~itemHough()
{
    //delete poly;
}

QString itemHough::getType() const
{
    return staticGetType();
}

QString itemHough::staticGetType()
{
    return CT_AbstractItemDrawableWithoutPointCloud::staticGetType() + "/dem_houghsurface";
}

const hough& itemHough::getHough() const
{
    return h;
}

CT_AbstractItemDrawable* itemHough::copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList)
{
    itemHough *t2d = new itemHough(model, result, NULL);
    t2d->setId(id());

    //if(poly != NULL)
        //t2d->poly = poly->copy();

    t2d->setAlternativeDrawManager(getAlternativeDrawManager());

    return t2d;
}
