#include "itemhoughdrawmanager.h"
#include "itemhough.h"

const QString itemHoughDrawManager::INDEX_CONFIG_HOUGH_VISIBLE = itemHoughDrawManager::staticInitConfigHoughVisible();
const QString itemHoughDrawManager::INDEX_CONFIG_SURFACE_VISIBLE = itemHoughDrawManager::staticInitConfigSurfaceVisible();
const QString itemHoughDrawManager::INDEX_CONFIG_SPHERE_VISIBLE = itemHoughDrawManager::staticInitConfigSphereVisible();
const QString itemHoughDrawManager::INDEX_CONFIG_CYLINDERS_VISIBLE = itemHoughDrawManager::staticInitConfigCylindersVisible();
const QString itemHoughDrawManager::INDEX_CONFIG_CYLINDERS_FITTING_VISIBLE = itemHoughDrawManager::staticInitConfigCylindersFittingVisible();
const QString itemHoughDrawManager::INDEX_CONFIG_SURFACES_VISIBLE = itemHoughDrawManager::staticInitConfigSurfacesVisible();
const QString itemHoughDrawManager::INDEX_CONFIG_REF_VISIBLE = itemHoughDrawManager::staticInitConfigRefVisible();

itemHoughDrawManager::itemHoughDrawManager(QString drawConfigurationName) : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? "Hough" : drawConfigurationName)
{

}

itemHoughDrawManager::~itemHoughDrawManager()
{
}

void itemHoughDrawManager::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    const itemHough &item = dynamic_cast<const itemHough&>(itemDrawable);

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_HOUGH_VISIBLE).toBool())
        drawHough(view, painter, item);
    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SURFACE_VISIBLE).toBool())
        drawTriangles(view, painter, item);
    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SPHERE_VISIBLE).toBool())
        drawSphere(view, painter, item);
    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_CYLINDERS_VISIBLE).toBool())
        drawCylinders(view, painter, item);
    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_CYLINDERS_FITTING_VISIBLE).toBool())
        drawCylindersFitting(view, painter, item);
    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SURFACES_VISIBLE).toBool())
        drawSpheresSurface(view, painter, item);
    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_REF_VISIBLE).toBool())
        drawRefs(view, painter, item);
}

CT_ItemDrawableConfiguration itemHoughDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));
    item.addNewConfiguration(itemHoughDrawManager::staticInitConfigHoughVisible(), "Normals", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemHoughDrawManager::staticInitConfigSurfaceVisible(), "Surface", CT_ItemDrawableConfiguration::Bool, true);
    item.addNewConfiguration(itemHoughDrawManager::staticInitConfigCylindersVisible(), "Cylinders Hough", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemHoughDrawManager::staticInitConfigCylindersFittingVisible(), "Cylinders Fitting", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemHoughDrawManager::staticInitConfigSphereVisible(), "Sphere", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemHoughDrawManager::staticInitConfigSurfacesVisible(), "Surfaces", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(itemHoughDrawManager::staticInitConfigRefVisible(), "Local refs", CT_ItemDrawableConfiguration::Bool, false);

    return item;
}

// PROTECTED //

QString itemHoughDrawManager::staticInitConfigHoughVisible()
{
    return "DEM_HO";
}

QString itemHoughDrawManager::staticInitConfigSurfaceVisible()
{
    return "DEM_TR";
}

QString itemHoughDrawManager::staticInitConfigSphereVisible()
{
    return "DEM_SP";
}

QString itemHoughDrawManager::staticInitConfigSurfacesVisible()
{
    return "DEM_SU";
}

QString itemHoughDrawManager::staticInitConfigRefVisible()
{
    return "DEM_LR";
}
QString itemHoughDrawManager::staticInitConfigCylindersVisible()
{
    return "DEM_CY";
}

QString itemHoughDrawManager::staticInitConfigCylindersFittingVisible()
{
    return "DEM_CYF";
}


void itemHoughDrawManager::drawHough(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const
{
    painter.setColor(71, 79, 71);

    hough h = item.getHough();

    /*std::vector<houghResult> res = h.getResults();

    for(int i=0;i<res.size();i++)
    {
        pcl::PointXYZ min,max;
        min = res.at(i).getBox().getMin();
        max = res.at(i).getBox().getMax();

        painter.drawLine(min.x,min.y,min.z,max.x,min.y,min.z);
        painter.drawLine(min.x,min.y,min.z,min.x,max.y,min.z);
        painter.drawLine(max.x,max.y,min.z,max.x,min.y,min.z);
        painter.drawLine(max.x,max.y,min.z,min.x,max.y,min.z);

        painter.drawLine(min.x,min.y,max.z,max.x,min.y,max.z);
        painter.drawLine(min.x,min.y,max.z,min.x,max.y,max.z);
        painter.drawLine(max.x,max.y,max.z,max.x,min.y,max.z);
        painter.drawLine(max.x,max.y,max.z,min.x,max.y,max.z);

        painter.drawLine(min.x,min.y,min.z,min.x,min.y,max.z);
        painter.drawLine(min.x,max.y,min.z,min.x,max.y,max.z);
        painter.drawLine(max.x,min.y,min.z,max.x,min.y,max.z);
        painter.drawLine(max.x,max.y,min.z,max.x,max.y,max.z);
    }*/

   /* pcl::PointCloud<pcl::PointXYZ> pts = h.getPtsCloud();

    for(std::size_t i=0; i<pts.size();i=i+1)
    {
        pcl::PointXYZ pt =  pts.at(i);
        float normN = sqrt(pt.normal_x*pt.normal_x+pt.normal_y*pt.normal_y+pt.normal_z*pt.normal_z);
        painter.drawLine(pt.x,pt.y,pt.z,pt.x+0.05*pt.normal_x/normN,pt.y+0.05*pt.normal_y/normN,pt.z+0.05*pt.normal_z/normN);
    }*/

}

void itemHoughDrawManager::drawTriangles(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const
{
    painter.setColor(0, 80, 150);

    const isoSurface iso = item.getHough().getLevelSet();

    for(std::size_t i=0; i<iso.getTriangles().size();i++)
    {
        pcl::PointXYZ x0 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[0]);
        pcl::PointXYZ x1 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[1]);
        pcl::PointXYZ x2 = iso.getMapPoints().at(iso.getTriangles().at(i).pointIndex[2]);

        painter.drawLine(x0.x,x0.y,x0.z,x1.x,x1.y,x1.z);
        painter.drawLine(x1.x,x1.y,x1.z,x2.x,x2.y,x2.z);
        painter.drawLine(x2.x,x2.y,x2.z,x0.x,x0.y,x0.z);
    }
}

void itemHoughDrawManager::drawSphere(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const
{
    painter.setColor(0, 75, 75);

    hough h = item.getHough();

    std::vector<houghSphere> sp = h.getListSphere();

    for(int i=0;i<sp.size();i++)
    {
        pcl::PointXYZ center =  sp.at(i).getCenter();
        float radius = sp.at(i).getRadius();
        painter.drawPartOfSphere(center.x,center.y,center.z,radius,0.,2*M_PI,0.,2*M_PI);
    }
}

void itemHoughDrawManager::drawCylinders(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const
{
    painter.setColor(0, 75, 75);

    /*const hough h = item.getHough();

    std::vector<cylinder> lc = h.getCylinders();

    for(int i=0;i<lc.size();i++)
    {
        cylinder c =  lc.at(i);
        painter.drawCylinder(c.c_x,c.c_y,c.c_z - 0.5*c.c_h ,c.c_r,c.c_h);
    }*/
}

void itemHoughDrawManager::drawCylindersFitting(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const
{
    painter.setColor(0, 120, 150);

   /* const hough h = item.getHough();

    std::vector<houghSphere> sp = h.getListSphere();
    std::vector<cylinder> lc = h.getCylinders();

    for(int i=0;i<sp.size();i++)
    {
        cylinder c =  lc.at(i);
        localCellCylinder cyl =  sp.at(i).getLocalCell();
        //painter.drawCylinder(cyl.getCentroid()(0),cyl.getCentroid()(1),cyl.getCentroid()(2) - 0.5*c.c_h ,cyl.getRadiusCylinderFitting(),c.c_h);
        /*QVector3D center;
        center.setX(cyl.getCentroid()(0));
        center.setY(cyl.getCentroid()(1));
        center.setZ(cyl.getCentroid()(2));

        QVector3D direction;
        direction.setX(sp.at(i).getLocalCell().getW()(0));
        direction.setY(sp.at(i).getLocalCell().getW()(1));
        direction.setZ(sp.at(i).getLocalCell().getW()(2));

        painter.drawCylinder3D(center,direction,cyl.getRadiusCylinderFitting(),c.c_h);
    }*/
}

void itemHoughDrawManager::drawRefs(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const
{
    hough h = item.getHough();
    std::vector<houghSphere> sp = h.getListSphere();

    for(std::size_t i=0; i<sp.size();i++)
    {
        pcl::PointXYZ center =  sp.at(i).getCenter();
        painter.setColor(255, 0, 0);
        painter.drawLine(center.x,center.y,center.z,center.x+0.1*sp.at(i).getLocalCell().getU()[0],center.y+0.1*sp.at(i).getLocalCell().getU()[1],center.z+0.1*sp.at(i).getLocalCell().getU()[2]);
        painter.setColor(0, 255, 0);
        painter.drawLine(center.x,center.y,center.z,center.x+0.1*sp.at(i).getLocalCell().getV()[0],center.y+0.1*sp.at(i).getLocalCell().getV()[1],center.z+0.1*sp.at(i).getLocalCell().getV()[2]);
        painter.setColor(0, 0, 255);
        painter.drawLine(center.x,center.y,center.z,center.x+0.1*sp.at(i).getLocalCell().getW()[0],center.y+0.1*sp.at(i).getLocalCell().getW()[1],center.z+0.1*sp.at(i).getLocalCell().getW()[2]);
    }

    painter.setColor(61, 69, 61);

    for(int i=0;i<sp.size();i++)
    {
        pcl::PointXYZ min,max;
        min = sp.at(i).getBBmin();
        max = sp.at(i).getBBmax();

        painter.drawLine(min.x,min.y,min.z,max.x,min.y,min.z);
        painter.drawLine(min.x,min.y,min.z,min.x,max.y,min.z);
        painter.drawLine(max.x,max.y,min.z,max.x,min.y,min.z);
        painter.drawLine(max.x,max.y,min.z,min.x,max.y,min.z);

        painter.drawLine(min.x,min.y,max.z,max.x,min.y,max.z);
        painter.drawLine(min.x,min.y,max.z,min.x,max.y,max.z);
        painter.drawLine(max.x,max.y,max.z,max.x,min.y,max.z);
        painter.drawLine(max.x,max.y,max.z,min.x,max.y,max.z);

        painter.drawLine(min.x,min.y,min.z,min.x,min.y,max.z);
        painter.drawLine(min.x,max.y,min.z,min.x,max.y,max.z);
        painter.drawLine(max.x,min.y,min.z,max.x,min.y,max.z);
        painter.drawLine(max.x,max.y,min.z,max.x,max.y,max.z);
    }
}

void itemHoughDrawManager::drawSpheresSurface(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const
{
    painter.setColor(0, 90, 55);

    hough h = item.getHough();

    std::vector<houghSphere> sp = h.getListSphere();

    for(std::size_t i=0;i<sp.size();i++)
    {
        const isoSurface iso = sp.at(i).getLevelSet();

        for(std::size_t j=0; j<iso.getTriangles().size();j++)
        {
            pcl::PointXYZ x0 = iso.getMapPoints().at(iso.getTriangles().at(j).pointIndex[0]);
            pcl::PointXYZ x1 = iso.getMapPoints().at(iso.getTriangles().at(j).pointIndex[1]);
            pcl::PointXYZ x2 = iso.getMapPoints().at(iso.getTriangles().at(j).pointIndex[2]);

            painter.drawLine(x0.x,x0.y,x0.z,x1.x,x1.y,x1.z);
            painter.drawLine(x1.x,x1.y,x1.z,x2.x,x2.y,x2.z);
            painter.drawLine(x2.x,x2.y,x2.z,x0.x,x0.y,x0.z);
        }
    }
}
