/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "rectangle.h"

#include <iostream>

rectangle::rectangle(float pXMin, float pXMax, float pYMin, float pYMax)
{
    xMin = pXMin;
    xMax = pXMax;
    yMin = pYMin;
    yMax = pYMax;

    deltaX = xMax - xMin;
    deltaY = yMax - yMin;
}

void rectangle::keepPointIn(pcl::PointCloud<pcl::PointXYZ> pPts,pcl::PointCloud<pcl::PointXYZ>   &pPtsOut)
{
    for(std::size_t i=0;i<pPts.size();i++)
    {
        float xc = pPts.at(i).x;
        float yc = pPts.at(i).y;
        float zc = pPts.at(i).z;

        if(xc>=xMin && xc<xMax && yc>=yMin && yc<yMax)
        {
            pcl::PointXYZ p;
            p.x=xc;
            p.y=yc;
            p.z=zc;
            pPtsOut.push_back(p);
        }
    }
}
