/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "itemfrbftree.h"

const itemFrbfTreeDrawManager itemFrbfTree::dem_rbfTree_DRAW_MANAGER;

itemFrbfTree::itemFrbfTree() : CT_AbstractItemDrawableWithoutPointCloud()
{
    //poly = NULL;
    setBaseDrawManager(&dem_rbfTree_DRAW_MANAGER);
}

itemFrbfTree::itemFrbfTree(const CT_OutAbstractSingularItemModel *model,
                                       const CT_AbstractResult *result,
                               const continuousQsmTree *t) : CT_AbstractItemDrawableWithoutPointCloud(model, result), tree(*t)
{
    //poly = *polygon;
    setBaseDrawManager(&dem_rbfTree_DRAW_MANAGER);
}

itemFrbfTree::itemFrbfTree(const QString &modelName,
                                       const CT_AbstractResult *result,
                               const continuousQsmTree *t) : CT_AbstractItemDrawableWithoutPointCloud(modelName, result), tree(*t)
{
    //poly = *polygon;
    setBaseDrawManager(&dem_rbfTree_DRAW_MANAGER);
}

itemFrbfTree::~itemFrbfTree()
{
    //delete poly;
}

QString itemFrbfTree::getType() const
{
    return staticGetType();
}

QString itemFrbfTree::staticGetType()
{
    return CT_AbstractItemDrawableWithoutPointCloud::staticGetType() + "/dem_treesurface";
}

const continuousQsmTree& itemFrbfTree::getTree() const
{
    return tree;
}

CT_AbstractItemDrawable* itemFrbfTree::copy(const CT_OutAbstractItemModel *model, const CT_AbstractResult *result, CT_ResultCopyModeList copyModeList)
{
    itemFrbfTree *t2d = new itemFrbfTree((const CT_OutAbstractSingularItemModel *)model, result, NULL);
    t2d->setId(id());

    //if(poly != NULL)
        //t2d->poly = poly->copy();

    t2d->setAlternativeDrawManager(getAlternativeDrawManager());

    return t2d;
}
