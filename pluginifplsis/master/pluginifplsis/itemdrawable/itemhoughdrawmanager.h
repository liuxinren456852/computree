#ifndef DEM_HOUGHDRAWMANAGER_H
#define DEM_HOUGHDRAWMANAGER_H

#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractitemdrawablewithoutpointclouddrawmanager.h"

class itemHough;

class itemHoughDrawManager : public CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager
{
public:
    itemHoughDrawManager(QString drawConfigurationName = "");
    virtual ~itemHoughDrawManager();

    virtual void draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const;

protected:

    const static QString INDEX_CONFIG_HOUGH_VISIBLE;
    const static QString INDEX_CONFIG_SURFACE_VISIBLE;
    const static QString INDEX_CONFIG_SPHERE_VISIBLE;
    const static QString INDEX_CONFIG_SURFACES_VISIBLE;
    const static QString INDEX_CONFIG_CYLINDERS_VISIBLE;
    const static QString INDEX_CONFIG_CYLINDERS_FITTING_VISIBLE;
    const static QString INDEX_CONFIG_REF_VISIBLE;

    static QString staticInitConfigHoughVisible();
    static QString staticInitConfigSurfaceVisible();
    static QString staticInitConfigSphereVisible();
    static QString staticInitConfigSurfacesVisible();
    static QString staticInitConfigCylindersVisible();
    static QString staticInitConfigCylindersFittingVisible();
    static QString staticInitConfigRefVisible();

    virtual CT_ItemDrawableConfiguration createDrawConfiguration(QString drawConfigurationName) const;

    void drawHough(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &h) const;
    void drawTriangles(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &h) const;
    void drawSphere(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &h) const;
    void drawSpheresSurface(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const;
    void drawCylinders(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const;
    void drawCylindersFitting(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const;
    void drawRefs(GraphicsViewInterface &view, PainterInterface &painter, const itemHough &item) const;
};

#endif // DEM_HOUGHDRAWMANAGER_H
