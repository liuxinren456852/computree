/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef LOCALCELLPATCH_H
#define LOCALCELLPATCH_H

#include "localCell.h"

class localCellPatch : public localCell
{
public:

    localCellPatch(){}
    localCellPatch(const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius=0., bool useWeight=false);
    localCellPatch(pcl::PointXYZ center, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius=0., bool useWeight=false);
    localCellPatch(pcl::PointXYZ center, pcl::PointXYZ normal, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius=0., bool useWeight=false);

    //compute coeffs of quadric in the global basis
    virtual  void getGlobalQuadric();

    //get coef of quadric and error by using least square reg
    virtual void computeLeastSquareReg(float radius, bool useWeight);
};

#endif // LOCALCELLPATCH_H
