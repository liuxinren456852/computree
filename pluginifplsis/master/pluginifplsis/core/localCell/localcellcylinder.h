/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef LOCALCELLCYLINDER_H
#define LOCALCELLCYLINDER_H

#include "localCell.h"

class localCellCylinder : public localCell
{
public:
    localCellCylinder(){}
    localCellCylinder(const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight);
    localCellCylinder(pcl::PointXYZ center, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight);
    localCellCylinder(pcl::PointXYZ center, pcl::PointXYZ normal, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight);
    localCellCylinder(pcl::PointXYZ center, pcl::PointXYZ normal, float radius);

    double getRadiusCylinderFitting(){return radiusCylinderFitting;}

    //compute coeffs of quadric in the global basis
    virtual  void getGlobalQuadric();
    virtual  void getGlobalQuadricAngle();

    //get coef of quadric and error by using least square reg
    virtual void computeLeastSquareReg(float radius, bool useWeight);

    virtual void computeLeastSquareRegAngle();

private:
    double radiusCylinderFitting;
};

#endif // LOCALCELLCYLINDER_H
