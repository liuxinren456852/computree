#include "normalsestimator.h"

//DFS visitor, got help from http://stackoverflow.com/questions/14126/how-to-create-a-c-boost-undirected-graph-and-traverse-it-in-depth-first-search
class MSTVisitor : public boost::default_dfs_visitor{
public:
    MSTVisitor(std::vector<std::string> vNames, pcl::PointCloud<pcl::PointNormal>* pCloud):vertNames(vNames),cloud(pCloud){}
    template < typename Edge, typename Graph >
    void tree_edge(Edge e, const Graph& g) const {
        VertexIndexMap vMap = get(boost::vertex_index,g);

        pcl::PointNormal pi = cloud->at(source(e,g));
        pcl::PointNormal pj = cloud->at(target(e,g));
        float prodScal = pi.normal_x*pj.normal_x+pi.normal_y*pj.normal_y+pi.normal_z*pj.normal_z;
        if(prodScal<0.)
        {
            cloud->at(target(e,g)).normal[0] = - cloud->at(target(e,g)).normal_x;
            cloud->at(target(e,g)).normal[1] = - cloud->at(target(e,g)).normal_y;
            cloud->at(target(e,g)).normal[2] = - cloud->at(target(e,g)).normal_z;
        }
    }
private:
    std::vector<std::string> vertNames;
    mutable pcl::PointCloud<pcl::PointNormal>* cloud;
};

normalsEstimator::normalsEstimator(pcl::PointCloud<pcl::PointXYZ> pPointCloud):pointCloud(pPointCloud)
{}

pcl::PointCloud<pcl::PointNormal> normalsEstimator::getNormals(int nbNeighborsNormals)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_smoothed (pointCloud.makeShared());

    //std::cout<<"    Normals : estimation"<<std::endl;
    pcl::NormalEstimation<pcl::PointXYZ,pcl::Normal>  ne;
    pcl::search::KdTree<pcl::PointXYZ>::Ptr treeNE (new pcl::search::KdTree<pcl::PointXYZ>);
    ne.setInputCloud (cloud_smoothed);
    ne.setSearchMethod(treeNE);
    ne.setKSearch (nbNeighborsNormals);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>());
    ne.compute (*cloud_normals);

    //std::cout<<"    Normals : smoothing ... "<<std::endl;
    pcl::PointCloud<pcl::PointNormal> cloud_smoothed_normals;
    pcl::concatenateFields (*cloud_smoothed, *cloud_normals, cloud_smoothed_normals);
    pcl::search::KdTree<pcl::PointNormal> treePtsNormals;
    treePtsNormals.setInputCloud(cloud_smoothed_normals.makeShared());

    // build a K-nearest neighbors graph
    pcl::KdTreeFLANN<pcl::PointNormal> kdtree;
    kdtree.setInputCloud (cloud_smoothed_normals.makeShared());

    GraphPoisson cloud_graph;

    for (size_t point_i = 0; point_i < cloud_smoothed_normals.size (); ++point_i)
    {
        std::vector<int> k_indices (nbNeighborsNormals);
        std::vector<float> k_distances (nbNeighborsNormals);
        kdtree.nearestKSearch (static_cast<int> (point_i), nbNeighborsNormals, k_indices, k_distances);

        for (int k_i = 0; k_i < static_cast<int> (k_indices.size ()); ++k_i){
            pcl::PointNormal pi = cloud_smoothed_normals.at(point_i);
            pcl::PointNormal pj = cloud_smoothed_normals.at(k_indices[k_i]);
            float w = 1 - fabs(pi.normal_x*pj.normal_x+pi.normal_y*pj.normal_y+pi.normal_z*pj.normal_z);
            add_edge (point_i, k_indices[k_i], Weight (w), cloud_graph);
        }
    }

    const size_t E = num_edges (cloud_graph),V = num_vertices (cloud_graph);
    //std::cout<<"    Normals : The graph has "<<V<<" vertices and "<<E<<" edges"<<std::endl;

    std::vector <EdgePoisson> spanning_tree;
    kruskal_minimum_spanning_tree(cloud_graph, std::back_inserter(spanning_tree));

    //std::cout<<"    Normals : size of most spanning tree : "<<spanning_tree.size()<<std::endl;

    GraphPoisson MST;

    //create a list of original names for the MST graph.
    std::vector<std::string> mstNames(num_vertices(cloud_graph));
    //Index map for verts in MST, all graphs use an indepenent index system.
    VertexIndexMap mstIndexMap = get(boost::vertex_index, MST);

    for(size_t i = 0; i < spanning_tree.size(); ++i){

        EdgePoisson e = spanning_tree.at(i);
        Vertex v1 = source(e,cloud_graph);
        Vertex v2 = target(e,cloud_graph);

        //insert the edge to the MST graph
        // Both graphs will share the vertices in verts list.
        std::pair<EdgePoisson,bool> myPair = boost::add_edge(v1,v2,MST);

        //get the vertex index in the MST and set the name to that of original graph
        // mstNames will be used by the visitor
        mstNames.at(mstIndexMap[v1]) = v1;
        mstNames.at(mstIndexMap[v2]) = v2;
    }

    MSTVisitor vis(mstNames,&cloud_smoothed_normals);
    //std::cout << "    Normals : Depth first search on most spanning tree" << std::endl;
    boost::depth_first_search(MST, boost::visitor(vis).root_vertex(0));

    return cloud_smoothed_normals;
}

