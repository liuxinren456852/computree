/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "ifp_stepComputeMnt.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_triangulation2d.h"
#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_iterator/ct_pointiterator.h"
#include "itemdrawable/itemfrbfsurface.h"

#include "ct_accessor/ct_pointaccessor.h"

#include "core/surfacefrbf.h"
#include "plot/polygonalizer.h"

#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include <time.h>
#include <ctime>

// Alias for indexing in models
#define DEF_resultIn_resIn "resIn"
#define DEF_groupIn "grpIn"
#define DEF_itemIn_inputPt "inputPt"

// Constructor : initialization of parameters
IFP_stepComputeMnt::IFP_stepComputeMnt(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _error = 0.01;
    _nbx = 100;

    _nbrPointsMin = 10;
    _minSizeLeaf = 0.2;

    _histoRange = 0.10;
    _histoWindowSizeSmooth = 5;

    _thresholdErrorBetweenNeighbors = 10;
}

// Step description (tooltip of contextual menu)
QString IFP_stepComputeMnt::getStepDescription() const
{
    return tr("Compute the digital terrain model (IFP-LSIS)");
}

QString IFP_stepComputeMnt::getStepDetailledDescription() const
{
    return tr("This step extracts the Digital Terrain Model (DTM) from a point cloud based on local minima coming from the step <em>IFP_stepGetMinPtsPerSurface</em>. The result is provided as a triangulated irregular network (TIN)"
              "<br>The method is made of three main steps:"
              "<ul>"
              "<li>A quadtree construction allowing a dynamic multi-scale local approximation of the ground surface</li>"
              "<li>A refinement step dedicated to identify and correct approximation errors in the quadtree cells</li>"
              "<li>The computation of the implicit function and its polygonization</li>"
              "</ul>"
              "<br>The quadtree subdivision of space is used to provide the supporting neighborhood for CSRBF and parametric patches."
              "The quadtree is iteratively built according to the quality of ”local reconstruction” with respect to data. "
              "Indeed the quadtree structure is driven by the quality of the parametric patches thus providing an optimal scale structure for digital terrain modeling using CSRBF.");
}

QStringList IFP_stepComputeMnt::getStepRISCitations() const
{
    return QStringList() << tr("TY  - JOUR\n"
                               "TI  - Digital terrain model reconstruction from terrestrial LiDAR data using compactly supported radial basis\n"
                               "T2  - IEEE Computer Graphics and Applications\n"
                               "A1  - Morel, Jules\n"
                               "A2  - Bac, Alexandra\n"
                               "A3  - Vega, Cedric\n"
                               "PY  - 2017\n");
}

// Step copy methodmin
CT_VirtualAbstractStep* IFP_stepComputeMnt::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new IFP_stepComputeMnt(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void IFP_stepComputeMnt::createInResultModelListProtected()
{  
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_resultIn_resIn, tr("Scène"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn);
    resultModel->addItemModel(DEF_groupIn, DEF_itemIn_inputPt, CT_Scene::staticGetType(), tr("Scène"));
}

// Creation and affiliation of OUT models
void IFP_stepComputeMnt::createOutResultModelListProtected()
{   
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_resultIn_resIn);

    if (resultModel != NULL)
    {
//        resultModel->addItemModel(DEF_groupIn, _outdemSurfModelName, new itemFrbfsurface(), tr("FRBF surface"));
        resultModel->addItemModel(DEF_groupIn, _outmntPtsModelName, new CT_Triangulation2D(), tr("DTM TIN"));
        resultModel->addItemModel(DEF_groupIn, _outsceneModelName, new CT_Scene(), tr("Scene FRBF"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void IFP_stepComputeMnt::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText("[Quadtree building]","","");
    configDialog->addDouble("        Error Threshold    ", "", 1e-05, 1000, 5, _error);
    configDialog->addInt("        Minimum number of points per leaf    ", "", 1, 10000, _nbrPointsMin);
    configDialog->addDouble("        Minimum size of leaf    ", "m", 1e-03, 1000, 3, _minSizeLeaf);
    configDialog->addText("[Histogram filtering]","","");
    configDialog->addDouble("        Range of histogram    ", "m", 1e-03, 1000, 3, _histoRange);
    configDialog->addInt("        Size of window for histogram smoothing    ", "", 1, 10000, _histoWindowSizeSmooth);
    configDialog->addText("[Neighbors filtering]","","");
    configDialog->addDouble("        Threshold on error between neighbors    ", "", 1e-03, 1e9, 3, _thresholdErrorBetweenNeighbors);
    configDialog->addText("[Polygonizing]","","");
    configDialog->addInt("        Scalar field discretization    ", "", 1, 10000, _nbx);
}

void IFP_stepComputeMnt::compute()
{
    // récupération du résultats OUT
    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator itrGr(outResult, this, DEF_groupIn);

    while (itrGr.hasNext())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) itrGr.next();
        const CT_Scene *itemIn = (CT_Scene*) group->firstItemByINModelName(this, DEF_itemIn_inputPt);

        if (itemIn != NULL)
        {
            CT_PointAccessor accessor;

            const CT_AbstractPointCloudIndex*   indexCloud = itemIn->getPointCloudIndex();

            //conversion in pcl points
            pcl::PointCloud<pcl::PointXYZ>  ptsCloud;
            for(size_t i=0;i<indexCloud->size();i++){
                pcl::PointXYZ pt;
                const CT_Point& point_i   = accessor.constPointAt(indexCloud->constIndexAt(i));
                pt.x = point_i[0];
                pt.y = point_i[1];
                pt.z = point_i[2];
                ptsCloud.push_back(pt);
            }

            setProgress(10);

            clock_t t = clock();
            PS_LOG->addMessage(LogInterface::info, LogInterface::action, tr("[ Computation of the models ]"));
            surfaceFrbf sFrbf(ptsCloud,_error, _nbrPointsMin, _minSizeLeaf, _histoRange, _histoWindowSizeSmooth, _thresholdErrorBetweenNeighbors);
            setProgress(50);

            t = clock() - t;
            std::ostringstream sCompute;
            sCompute<<((float)t)/CLOCKS_PER_SEC<<" s";
            PS_LOG->addMessage(LogInterface::info, LogInterface::action, QString(sCompute.str().c_str()));
            t = clock();

            PS_LOG->addMessage(LogInterface::info, LogInterface::action, tr("[ Rendering ]"));
            sFrbf.polygonizeLevelSet(_nbx,_nbx,_nbx,0.);
            setProgress(80);

            PS_LOG->addMessage(LogInterface::info, LogInterface::action, tr("[ MNT Points Creation ]"));
            CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

            const isoSurface iso = sFrbf.getLevelSet();
            for(std::size_t i=0; i<iso.getMapPoints().size();i++)
            {
                pcl::PointXYZ x0 = iso.getMapPoints().at(i);
                undepositPointCloud->addPoint(createCtPoint(x0.x, x0.y,x0.z));
            }

            // On enregistre le nuage de points cree dans le depot
            CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

//            itemFrbfsurface* itemOut_demSurf = new itemFrbfsurface(_outdemSurfModelName.completeName(), outResult, &sFrbf);
//            group->addItemDrawable(itemOut_demSurf);

            CT_Scene* itemOut_scene = new CT_Scene(_outsceneModelName.completeName(), outResult, depositPointCloud);
            itemOut_scene->updateBoundingBox();
            const CT_AbstractPointCloudIndex *pointCloudIndex = itemOut_scene->getPointCloudIndex();
            group->addItemDrawable(itemOut_scene);

            CT_DelaunayTriangulation *delaunay = new CT_DelaunayTriangulation();
            delaunay->init(itemOut_scene->minX() - 1.0, itemOut_scene->minY() - 1.0, itemOut_scene->maxX() + 1.0, itemOut_scene->maxY() + 1.0);

            CT_PointIterator itP(pointCloudIndex);
            while(itP.hasNext() && !isStopped())
            {
                const CT_Point &point =itP.next().currentPoint();

                Eigen::Vector3d* pt = new Eigen::Vector3d(point);
                delaunay->addVertex(pt, true);
            }
            setProgress(90);

            delaunay->doInsertion();
            delaunay->updateCornersZValues();

            CT_Triangulation2D* triangulation = new CT_Triangulation2D(_outmntPtsModelName.completeName(), outResult, delaunay);
            group->addItemDrawable(triangulation);

        }
    }


}
