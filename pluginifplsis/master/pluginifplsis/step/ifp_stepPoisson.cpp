/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "ifp_stepPoisson.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"

#include "ct_result/ct_resultgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_triangulation2d.h"
#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_itemdrawable/ct_pointsattributesnormal.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_iterator/ct_groupiterator.h"
#include "itemdrawable/itemfrbftree.h"

#include "ct_accessor/ct_pointaccessor.h"

#include "core/surfacefrbf.h"
#include "plot/polygonalizer.h"

#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include "ct_itemdrawable/ct_ttreegroup.h"
#include "ct_itemdrawable/ct_tnodegroup.h"
#include "ct_itemdrawable/ct_itemattributelist.h"
#include "ct_itemdrawable/ct_opfmeshmodel.h"
#include "ct_itemdrawable/model/outModel/ct_outopfnodegroupmodel.h"
#include "ct_itemdrawable/tools/iterator/ct_itemiterator.h"

#include "ct_mesh/ct_face.h"
#include "ct_mesh/ct_edge.h"
#include "ct_mesh/tools/ct_meshallocatort.h"

#include "qsm/continuousQsmTree.h"
#include "itemdrawable/itemsimpletree.h"
#include "utils/treetopology.h"

// Alias for indexing in models
#define DEF_resultIn_resIn "resIn"
#define DEF_groupIn_grpIn "grpInQsm"
#define DEF_resultIn_resInQSM "resInQsm"
#define DEF_groupIn_grpInQSM "grpIn"
#define DEF_itemIn_inputPt "inputPt"
#define DEF_itemIn_inputNormals "inputNormals"
#define DEF_itemIn_inputQSM "inputQSM"

#define DEFin_tree "tree"
#define DEFin_grpSegments "grpSegments"
#define DEFin_segment "segment"
#define DEFin_mesh "mesh"

// Alias for indexing out models
#define DEF_resultOut_resOut "resOut"
#define DEF_groupOut_grpOut "grpOut"
#define DEF_itemOut_Qsm "qsm"
#define DEF_itemOut_QsmMesh "qsmMesh"
#define DEF_itemOut_QsmSurf "qsmSurf"

#include <iostream>

// Constructor : initialization of parameters
IFP_stepPoisson::IFP_stepPoisson(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    searchHalfEdges=false;

    _nbNeighborsNormals=10;
    _depth=10;
    _samplePerNode=1.;
    _solverDivide=10;
    _isoDivide=8;
    _confidence=true;
    _pointWeight=0.0;
}

// Step description (tooltip of contextual menu)
QString IFP_stepPoisson::getStepDescription() const
{
    return tr("Reconstruction of Poisson surface from an oriented point cloud");
}

QString IFP_stepPoisson::getStepDetailledDescription() const
{
    return tr("This method casts the surface reconstruction from oriented points as a spatial Poisson problem."
              "This Poisson formulation considers all the points at once, without resorting to heuristic spatial partitioning or blending, and is therefore highly resilient to data noise."
              "This step relies on the implementation of the PCL library Poisson surface reconstruction, which is based on Kazhdan et al. (2013)");
}

QStringList IFP_stepPoisson::getStepRISCitations() const
{
    return QStringList() << tr("TY  - JOUR\n"
                               "TI  - Screened poisson surface reconstruction\n"
                               "T2  - ACM Transactions on Graphics (TOG)\n"
                               "A1  - Kazhdan, Michael\n"
                               "A2  - Hoppe, Hugues\n"
                               "PY  - 2013\n");
}

// Step copy methodmin
CT_VirtualAbstractStep* IFP_stepPoisson::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new IFP_stepPoisson(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void IFP_stepPoisson::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_resultIn_resIn, tr("Scène"), "", true);
    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_grpIn);
    resultModel->addItemModel(DEF_groupIn_grpIn, DEF_itemIn_inputPt, CT_Scene::staticGetType(), tr("Tree Point cloud"));
    resultModel->addItemModel(DEF_groupIn_grpIn, DEF_itemIn_inputNormals, CT_PointsAttributesNormal::staticGetType(), tr("Normals"));
}

// Creation and affiliation of OUT models
void IFP_stepPoisson::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_resultIn_resIn);

    if (resultModel != NULL)
    {
        resultModel->addItemModel(DEF_groupIn_grpIn, _out_QsmSurfModelName, new CT_MeshModel(), tr("Continuous surface"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void IFP_stepPoisson::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addInt("Reconstruction depth","",1,16,_depth);
    configDialog->addDouble("Interpolation weight", "", 0.0, 100., 3, _pointWeight);
    configDialog->addDouble("Samples per node", "", 1.0, 100., 3, _samplePerNode);
    configDialog->addInt("Solver depth","",1,16,_solverDivide);
    configDialog->addInt("Polygonizer depth","",1,16,_isoDivide);
    configDialog->addBool("Normal confidence","","",_confidence);
    configDialog->addBool(tr("Research of half edges ?"), "", "", searchHalfEdges);
}

void IFP_stepPoisson::compute()
{

    CT_ResultGroup *outResult = getOutResultList().first();

    CT_ResultGroupIterator itr(outResult, this, DEF_groupIn_grpIn);

    while (itr.hasNext())
    {
        CT_StandardItemGroup* group = (CT_StandardItemGroup*) itr.next();

        const CT_Scene *itemIn = (CT_Scene*) group->firstItemByINModelName(this, DEF_itemIn_inputPt);
        const CT_PointsAttributesNormal *itemNormals = (CT_PointsAttributesNormal*) group->firstItemByINModelName(this, DEF_itemIn_inputNormals);


        const CT_AbstractPointCloudIndex* normalPointIndex = itemNormals->getPointCloudIndex();
        CT_AbstractNormalCloud* normalCloud = itemNormals->getNormalCloud();

        CT_PointIterator itP(normalPointIndex);

        pcl::PointCloud<pcl::PointNormal> pclNormals;
        while(itP.hasNext())
        {
            const CT_Point &point =itP.next().currentPoint();
            size_t localIndex = itP.cIndex();
            CT_Normal& normal = normalCloud->normalAt(localIndex);

            pcl::PointNormal pt;
            pt.x = point[0];
            pt.y = point[1];
            pt.z = point[2];
            pt.normal_x = normal[0];
            pt.normal_y = normal[1];
            pt.normal_z = normal[2];

            pclNormals.push_back(pt);
        }

        pcl::Poisson<pcl::PointNormal>  poisson;
        poisson.setDepth (_depth);
        poisson.setSolverDivide(_solverDivide);
        poisson.setSamplesPerNode(_samplePerNode);
        poisson.setIsoDivide(_isoDivide);
        poisson.setConfidence(_confidence);
        poisson.setPointWeight(_pointWeight);
        poisson.setInputCloud (pclNormals.makeShared());
        pcl::PolygonMesh meshPCL;
        setProgress(75);
        poisson.reconstruct (meshPCL);

        std::cout<<"Poisson Reconstruction done"<<std::endl;

        pcl::PCLPointCloud2 ptsPoisson = meshPCL.cloud;
        pcl::PointCloud<pcl::PointXYZ> cloud;
        pcl::fromPCLPointCloud2(ptsPoisson,cloud);
        std::vector<pcl::Vertices> listPoly = meshPCL.polygons;

        std::cout << "      pcl mesh pts: "<< cloud.size()<< std::endl;
        std::cout << "      pcl mesh tri: "<< listPoly.size()<< std::endl;

        if(cloud.size()>0){
            Eigen::Vector3d bboxMin;
            bboxMin[0] = std::numeric_limits<double>::max();
            bboxMin[1] = bboxMin[0];
            bboxMin[2] = bboxMin[0];

            Eigen::Vector3d bboxMax;
            bboxMax[0] = -bboxMin[0];
            bboxMax[1] = -bboxMin[0];
            bboxMax[2] = -bboxMin[0];
            CT_Mesh* mesh = buildMesh(cloud,listPoly,bboxMin,bboxMax);

            CT_MeshModel* itemOut_surfMesh = new CT_MeshModel(_out_QsmSurfModelName.completeName(), outResult,mesh);
            itemOut_surfMesh->setBoundingBox(bboxMin[0], bboxMin[1], bboxMin[2], bboxMax[0], bboxMax[1], bboxMax[2]);
            group->addItemDrawable(itemOut_surfMesh);
        }else{
            PS_LOG->addMessage(LogInterface::error, LogInterface::action, QString("The surface could not be retrieved as the normal field provided is not consistent. Simplify the point cloud and/or filter the outliers before computing the normals"));
        }
    }
}

bool IFP_stepPoisson::loadVertex(pcl::PointXYZ& x0, CT_Point &point)
{
    point(0) = x0.x;
    point(1) = x0.y;
    point(2) = x0.z;
    return true;
}

CT_Mesh* IFP_stepPoisson::buildMesh(pcl::PointCloud<pcl::PointXYZ>& pts,std::vector<pcl::Vertices>& tri,Eigen::Vector3d& bboxMin,Eigen::Vector3d& bboxMax)
{
    CT_Mesh* mesh = new CT_Mesh();

    int nv = pts.size();
    int nf = tri.size();

    CT_MutablePointIterator beginV = CT_MeshAllocator::AddVertices(mesh, nv);
    CT_MutableFaceIterator beginF = CT_MeshAllocator::AddFaces(mesh, nf);
    CT_MutableEdgeIterator beginH = CT_MeshAllocator::AddHEdges(mesh, nf*3);

    size_t beginVertexIndex = beginV.next().cIndex();
    beginV.toFront();

    size_t nfLoaded = 0;
    size_t nvLoaded = 0;
    CT_Point point;

    for(int i=0;i<pts.size();i++)
    {
        pcl::PointXYZ p = pts.at(i);
        if(loadVertex(p, point)) {
            beginV.next().replaceCurrentPoint(point);
            updateMinMax(point, bboxMin, bboxMax);
            ++nvLoaded;
        }
    }
    for(int i=0;i<tri.size();i++)
    {
        pcl::Vertices t = tri.at(i);
        if(loadFace(t, beginVertexIndex, mesh, beginF, beginH)) {
            ++nfLoaded;
        }
    }
    return mesh;
}

bool IFP_stepPoisson::loadFace(pcl::Vertices tri, const size_t &beginVertexIndex, CT_Mesh *mesh, CT_MutableFaceIterator &itFaces, CT_MutableEdgeIterator &itEdges)
{
    size_t p0,p1,p2;

    p0 = beginVertexIndex + tri.vertices[0];
    p1 = beginVertexIndex + tri.vertices[1];
    p2 = beginVertexIndex + tri.vertices[2];

    CT_Face &face = itFaces.next().cT();
    size_t faceIndex = itFaces.cIndex();

    CT_Edge *twinE1 = NULL;
    CT_Edge *twinE2 = NULL;
    CT_Edge *twinE3 = NULL;

    if(searchHalfEdges){
        twinE1 = findHEdgeTwin(mesh, p0, p1);
        twinE2 = findHEdgeTwin(mesh, p1, p2);
        twinE3 = findHEdgeTwin(mesh, p2, p0);
    }

    size_t e1Index = itEdges.next().cIndex();
    size_t e2Index;
    size_t e3Index;

    face.setEdge(e1Index);

    CT_Edge &e1 = itEdges.cT();
    e1.setPoint0(p0);
    e1.setPoint1(p1);
    e1.setFace(faceIndex);

    CT_Edge &e2 = itEdges.next().cT();
    e2.setPoint0(p1);
    e2.setPoint1(p2);
    e2.setFace(faceIndex);
    e2Index = itEdges.cIndex();

    CT_Edge &e3 = itEdges.next().cT();
    e3.setPoint0(p2);
    e3.setPoint1(p0);
    e3.setFace(faceIndex);
    e3Index = itEdges.cIndex();

    e1.setNext(e2Index);
    e1.setPrevious(e3Index);
    e2.setNext(e3Index);
    e2.setPrevious(e1Index);
    e3.setNext(e1Index);
    e3.setPrevious(e2Index);

    if(twinE1 != NULL)
    {
        e1.setTwin(twinE1);
        twinE1->setTwin(e1Index);
    }

    if(twinE2 != NULL)
    {
        e2.setTwin(twinE2);
        twinE2->setTwin(e2Index);
    }

    if(twinE3 != NULL)
    {
        e3.setTwin(twinE3);
        twinE3->setTwin(e3Index);
    }

    return true;
}

void IFP_stepPoisson::updateMinMax(const CT_Point &point, Eigen::Vector3d &bboxMin, Eigen::Vector3d &bboxMax)
{
    bboxMin[0] = qMin(point[CT_Point::X], bboxMin[0]);
    bboxMin[1] = qMin(point[CT_Point::Y], bboxMin[1]);
    bboxMin[2] = qMin(point[CT_Point::Z], bboxMin[2]);

    bboxMax[0] = qMax(point[CT_Point::X], bboxMax[0]);
    bboxMax[1] = qMax(point[CT_Point::Y], bboxMax[1]);
    bboxMax[2] = qMax(point[CT_Point::Z], bboxMax[2]);
}

CT_Edge* IFP_stepPoisson::findHEdgeTwin(const CT_Mesh *mesh, const size_t &p0, const size_t &p1)
{
    if(mesh->abstractHedge() == NULL)
        return 0;

    CT_EdgeIterator it(mesh->abstractHedge());

    while(it.hasNext())
    {
        CT_Edge &he = it.next().cT();

        if(((he.iPointAt(0) == p1)
            && (he.iPointAt(1) == p0)))
            return &he;
    }

    return 0;
}
