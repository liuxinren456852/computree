TEMPLATE      = subdirs
SUBDIRS       = computreev5/base.pro \
                computreev5/library/ctlibpcl/ctlibpcl.pro \
                pluginartsfree/pluginartsfree/pluginartsfree.pro \
                pluginonf/pluginonf/pluginonf.pro \
                pluginonflsis/pluginonflsis/pluginonflsis.pro \
                pluginlerfob/pluginlerfob/pluginlerfob.pro \
                pluginignlif/pluginignlif/pluginignlif.pro \
                pluginlvox/pluginlvox/plugin_lvox.pro \
                pluginsegma/pluginsegma/pluginsegma.pro \
                plugintoolkit/plugintoolkit/plugintoolkit.pro \
                pluginifplsis/pluginifplsis/pluginifplsis.pro \
                pluginrscript/pluginrscript/pluginrscript.pro \
                pluginSimpleForest/pluginsimpleforest/pluginsimpleforest.pro \
               # pluginsimpletree/pluginsimpletree/pluginsimpletree.pro \
                pluginudeslsis/pluginhoughspace/pluginhoughspace.pro \
                plugingenerate/plugingenerate/plugingenerate.pro \
                computreev5\ComputreeBatch\ComputreeBatch.pro
CONFIG       += console ordered

